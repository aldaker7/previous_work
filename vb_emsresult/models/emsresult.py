from openerp import models, api, fields

class ems_asset_job(models.Model):
    _name = 'vb.ems_asset_job'
    _description = 'EMS asset job'
    _rec_name = 'date'

    # field
    date = fields.Date(string='Date')
    today_asset_count = fields.Integer(string='Today asset count',default=0)
    added_assets_count = fields.Integer(string='Added assets count',default=0)
    removed_assets_count = fields.Integer(string='Removed assets count',default=0)

    @api.multi
    def get_ems_list(self):
        if self.date:
            try:
                tree_id = self.env['ir.model.data'].get_object_reference('vb_ems_asset_job_dtls','view_ems_batch_result_tree')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'EMS assets list',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode':'tree',
                        'res_model': 'vb.ems_asset_job_dtls',
                        'view_id': False,
                        'domain':[('ems_job_id','=',self.id)],
                        'target': 'current',
                }    

class ems_asset_job_dtls(models.Model):
    _name = 'vb.ems_asset_job_dtls'
    _description = 'ems asset_job_dtls'

    # field
    ems_job_id = fields.Integer(string='EMS id', default=0) 
    type = fields.Char(string='Type')
    asset_code = fields.Char(string='Asset code')
    asset_name = fields.Char(string='Asset name')
    market_symbol = fields.Char(string='Market symbol')
    isin = fields.Char(string='isin')
    ric = fields.Char(string='ric')
    file_name = fields.Char(string='file name') 

    
