# -*- coding: utf-8 -*-

{
    'name': 'VB EMS Result',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Bank reconciliation Module
======================================================

This module contains the views and functions for ems results.

    """,
    'data': [        
        'views/views_ems.xml',
        'views/actions_ems.xml',
        'views/menus_ems.xml',
            ],

    'installable': True,
    'auto_install': False,
    'application': True,
}