# -*- coding: utf-8 -*-

{
    'name': 'VB Archive',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Archive Module
======================================================

This module contains the functions for archiving of data.

    """,
    'data': [],

    'installable': True,
    'auto_install': False,
    'application': True,
}
