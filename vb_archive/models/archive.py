from openerp import models, fields

# Model for CUSTOMER_ARCH table

class customer_arch(models.Model):
    _name = 'vb.customer_arch'
    _description = 'Customer Archive'
    
    # fields
       
    name = fields.Char(string='Customer name',index=True)
    code = fields.Char( string='Customer code',index=True)
    title_id = fields.Integer(string='Title')
    title = fields.Char(string='Title')
    formal_title = fields.Char(string='Formal title',help='Enter if not in title list')
    email1 = fields.Char(string='Primary email',index=True)
    email2 = fields.Char(string='Alternate email')
    mobile_phone1 = fields.Char(string='Primary mobile no')
    mobile_phone2 = fields.Char(string='Alternate mobile no')
    home_phone = fields.Char(string='Home phone')
    work_phone = fields.Char(string='Work phone')
    work_phone_ext = fields.Char(string='Extension')
    main_phone = fields.Selection([
        ('Mobile1', 'Primary mobile'),
        ('Mobile2', 'Alternate mobile'),
        ('Home', 'Home'),
        ('Work', 'Work'),
        ], string='Primary phone', default='Mobile1')
    date_of_birth = fields.Date(string='Date of birth')
    country_of_birth_id = fields.Integer(string='Country of birth')
    gender = fields.Selection([
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('N/A', 'Not applicable'),
        ], string='Gender', default='Male')
    marital_status = fields.Selection([
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Divorced', 'Divorced'),
        ('Widowed', 'Widowed'),
        ], string='Marital status', default='Single')
    employment_status = fields.Selection([
        ('SelfEmployed', 'Self Employed'),
        ('Employed', 'Employed'),
        ('UnEmployed', 'Unemployed'),
        ('Student', 'Student'),
        ('Retired', 'Retired'),
        ], string='Employment status', default='Employed')
    customer_class_id = fields.Integer(string='Customer classification')
    customer_type = fields.Selection([
        ('Individual', 'Individual'),
        ('BizOrg', 'Business organization'),
        ('Trader', 'Trader'),
        ], string='Customer type', default='Individual')
    bumi_status = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No')
        ], string='Bumiputera', default='No')
    bizregno = fields.Char(string='Business registration number')
    trader_profile_id = fields.Integer(string='Trader profile')
    trader_code = fields.Char(string='Trader code')
    nationality_id = fields.Integer(string='Country of nationality')
    residency_id = fields.Integer(string='Country of residency')
    national_id_no = fields.Char(string='Primary ID number',index=True)
    national_id_type_id = fields.Integer(string='Primary ID type')
    national_id_expiry = fields.Date(string='Primary ID expiry')
    other_id_no = fields.Char(string='Other ID number')
    other_id_type_id = fields.Integer(string='Other ID type')
    other_id_expiry = fields.Date(string='Other ID expiry date')
    race_id = fields.Integer(string='Race')
    picture = fields.Binary(string='Picture')
    picture_small = fields.Binary(string='Picture')
    comments = fields.Text(string='Comments')    
    date_signed_up = fields.Datetime(string='Date/time signed up')
    date_activated = fields.Datetime(string='Date/time activated')
    activated_by = fields.Integer(string='Activated by')
    date_suspended = fields.Datetime(string='Date/time suspended')
    suspended_by = fields.Integer(string='Suspended by')
    reason_suspended_id = fields.Integer(string='Reason for suspension')
    date_uplifted = fields.Datetime(string='Date/time suspension uplifted')
    uplifted_by = fields.Integer(string='Uplifted by')
    date_dormant = fields.Datetime(string='Date/time dormant')
    last_activity_date = fields.Datetime(string='Last activity date')
    trader_id = fields.Integer(string='Trader')    
    state = fields.Selection([
        ('Basic', 'Basic signup only'),
        ('New', 'New'),
        ('Rejected', 'Rejected'),
        ('Active', 'Activated'),
        ('Suspended', 'Suspended'),
        ('Dormant', 'Dormant'),
        ], string='Status')
    stage = fields.Selection([
        ('Signup', 'Signup'),
        ('Rejected', 'Rejected'),
        ('Activated', 'Activated'),
        ('Suspended', 'Suspended'),
        ('Uplifted', 'Uplifted'),
        ('Dormant', 'Dormant'),
        ], string='Stage')
    login_name = fields.Char(string='Login name',index=True)
    email1_verified = fields.Boolean(string='Primary email verified',default=False)
    email2_verified = fields.Boolean(string='Alternate email verified',default=False)
    email1_date_verified = fields.Datetime(string='Date primary email verified')
    email2_date_verified = fields.Datetime(string='Date alternate email verified')
    mobile1_verified = fields.Boolean(string='Mobile number 1 verified',default=False)
    mobile2_verified = fields.Boolean(string='Mobile number 2 verified',default=False)
    mobile1_date_verified = fields.Datetime(string='Date mobile number 1 verified')
    mobile2_date_verified = fields.Datetime(string='Date mobile number 2 verified')
    active = fields.Boolean(string='Active',default=False)    
    signup_ip = fields.Text(string='Signup IP address')
    signup_date = fields.Datetime(string='When first signup')
    id_expiry_check = fields.Boolean(string='ID expiry check',default=False)
    questionaire_score = fields.Integer(string='Questionaire scoring')
    info_source_id = fields.Integer(string='Info source')
    info_source_specs = fields.Char(string='Info source specs')
    education_inst = fields.Char(string='Educational institution')
    p_id = fields.Integer('Original ID')

# Model for CUSTOMER BANK table

class customer_bank_arch(models.Model):
    _name = 'vb.customer_bank_arch'
    _description = 'Customer bank archive'

    # field
    bank_id = fields.Integer(string='Bank')
    customer_id = fields.Integer(string='Customer',index=True)
    bank_acct_name = fields.Char(string='Bank account name')
    bank_acct_no = fields.Char(string='Bank account number')
    bank_branch = fields.Char(string='Bank branch')
    primary_acct = fields.Boolean(string='Primary bank account',default=False)
    active = fields.Boolean(string='Active',default=True)    
    p_id = fields.Integer('Original ID')

# Model for CUSTOMER JOB table

class customer_job_arch(models.Model):
    _name = 'vb.customer_job_arch'
    _description = 'Customer job archive'

    # fields
    customer_id = fields.Integer(string='Customer',index=True)
    employer = fields.Char(string='Employer name',)
    employer_biztype = fields.Char(string='Employer business type')
    employer_bizregno = fields.Char(string='Employer registration number')
    occupation_id = fields.Integer(string='Occupation')
    designation = fields.Char(string='Designation')
    date_start = fields.Date(string='Date started')
    date_end = fields.Date(string='Date until')
    last_salary = fields.Float(string='Last drawn salary')
    last_salary_asat = fields.Date(string='Last drawn salary as at')
    currency_id = fields.Integer(comodel_name='vb.currency', string='Currency of salary')
    note = fields.Text(string='Note')
    address1 = fields.Char(string='Address 1')
    address2 = fields.Char(string='Address 2')
    address3 = fields.Char(string='Address 3')
    city = fields.Char(string='City')
    postcode = fields.Char(string='Postal code')
    state_id = fields.Integer(string='State')
    country_id = fields.Integer(string='Country')
    phone = fields.Char(string='Telephone')
    comments = fields.Text(string='Comments')    
    active = fields.Boolean(string='Active',default=True)    
    p_id = fields.Integer('Original ID')

# Model for CUSTOMER NEXT OF KIN table

class customer_nok_arch(models.Model):
    _name = 'vb.customer_nok_arch'
    _description = 'Customer next of kin'

    # fields
    customer_id = fields.Integer(string='Customer',index=True)
    name = fields.Char(string='Next of kin name')
    title_id = fields.Integer(string='Title')
    title = fields.Char(string='Title')
    formal_title = fields.Char(string='Formal title')
    relationship_type = fields.Selection([
        ('Spouse', 'Spouse'),
        ('Son', 'Son'),
        ('Daughter', 'Daughter'),
        ('Brother', 'Brother'),
        ('Sister', 'Sister'),
        ('Father', 'Father'),
        ('Mother', 'Mother'),
        ('Relative', 'Relative'),
        ], string='Relationship type')
    nationality_id = fields.Integer(string='Country of nationality')
    national_id_type_id = fields.Integer(string='Primary ID type')
    national_id_no = fields.Char(string='Primary ID number')
    national_id_expiry = fields.Date(string='Primary ID expiry')
    other_id_no = fields.Char(string='Other ID number')
    other_id_type_id = fields.Integer(string='Other ID type')
    other_id_expiry = fields.Date(string='Other ID expiry date')
    race_id = fields.Integer(string='Race')
    date_of_birth = fields.Date(string='Date of birth')
    country_of_birth_id = fields.Integer(string='Country of birth')
    address1 = fields.Char(string='Address 1')
    address2 = fields.Char(string='Address 2')
    address3 = fields.Char(string='Address 3')
    city = fields.Char(string='City')
    postcode = fields.Char(string='Postal code')
    state_id = fields.Integer(string='State')
    country_id = fields.Integer(string='Country')
    phone = fields.Char(string='Telephone')
    employer = fields.Char(string='Employer')
    employer_biztype = fields.Char(string='Employer business type')
    employer_bizregno = fields.Char(string='Employer registration number')
    occupation_id = fields.Integer(string='Occupation')
    gender = fields.Selection([
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other'),
        ], string='Gender', default='Male')
    comments = fields.Text(string='Comments')    
    active = fields.Boolean(string='Active',default=True)    
    p_id = fields.Integer('Original ID')

# Model for CUSTOMER ADDRESS table

class customer_address_arch(models.Model):
    _name = 'vb.customer_address_arch'
    _description = 'Customer address'

    # fields
    customer_id = fields.Integer(string='Customer',index=True)
    address_type = fields.Selection([
        ('Permanent', 'Permanent'),
        ('Residential', 'Residential'),
        ('Work', 'Work'),
        ('Mailing', 'Mailing'),
        ('Other', 'Other'),
        ], string='Address type')                                        
    address1 = fields.Char(string='Address 1',)
    address2 = fields.Char(string='Address 2')
    address3 = fields.Char(string='Address 3')
    city = fields.Char(string='City',)
    postcode = fields.Char(string='Postal code',)
    state_id = fields.Integer(string='State')
    country_id = fields.Integer(string='Country')
    display_address = fields.Char(string='Display address')
    default_rec = fields.Boolean(string='Default address',default=False)
    geolocation = fields.Char(string='Geo location')
    geopicture = fields.Binary(string='Picture of geolocation')
    list_seq = fields.Integer(string='List order')
    active = fields.Boolean(string='Active',default=True)    
    comments = fields.Text(string='Comments')    
    p_id = fields.Integer('Original ID')
    
# Model for CUSTOMER ACCOUNT table

class customer_acct_arch(models.Model):
    _name = 'vb.customer_acct_arch'
    _description = 'Customer account archive'
    # Fields
    name = fields.Char(string='Account name')
    acct_no = fields.Char(string='Account number',index=True)
    other_acct_no = fields.Char(string='Other account number',index=True)
    acct_class_id = fields.Integer(string='Product')
    customer_id = fields.Integer(string='Customer',index=True)
    branch_id = fields.Integer(string='Branch')
    currency_id = fields.Integer(string='Currency')
    market_id = fields.Integer(string='Market')
    email = fields.Char(string='Account email')
    date_opened = fields.Datetime(string='Date/time opened')
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Integer(string='Rejected by')
    reason_rejected_id = fields.Integer(string='Reason for rejection')
    date_activated = fields.Datetime(string='Date/time activated')
    activated_by = fields.Integer(string='Activated by')
    date_suspended = fields.Datetime(string='Date/time suspended')
    reason_suspended_id = fields.Integer(string='Reason for suspension')
    suspended_by = fields.Integer(string='Suspended by')
    closing_request_date = fields.Datetime(string='Date/time close requested')
    date_closed = fields.Datetime(string='Date/time closed')
    closed_by = fields.Integer(string='Closed by')
    reason_closed_id = fields.Integer(string='Reason for closing')
    date_uplifted = fields.Datetime(string='Date/time suspension uplifted')
    uplifted_by = fields.Integer(string='Uplifted by')
    reason_uplifted_id = fields.Integer(string='Reason for uplift')
    comments = fields.Text(string='Comments')
    date_inactive = fields.Datetime(string='Date/time inactive')
    date_dormant = fields.Datetime(string='Date/time dormant')
    last_activity_date = fields.Datetime(string='Last activity date')
    margin_ratio = fields.Float(string='Margin ratio',digits=(7,2),default=0)
    margin_call_flag = fields.Boolean(string='Margin call flag',default=False)
    last_margin_eval = fields.Date(string='Date of last margin evaluation')
    last_margin_call = fields.Date(string='Date of last margin call')
    margin_call_count = fields.Float(string='Margin call count',digits=(3,0),default=0)
    finance_period = fields.Float(string='Finance period',digits=(3,0),default=0)
    offer_date = fields.Date(string='Letter of offer date')
    facility_date = fields.Date(string='Facility agreement date')
    facility_requested = fields.Float(string='Facility requested',digits=(15,2),default=0)
    facility_expiry_date = fields.Date(string='Facility expiry date')
    facility_guarantee = fields.Text(string='Facility guarantee')
    cds_no = fields.Char(string='CDS no')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('Basic', 'Basic signup only'),
        ('New', 'New'),
        ('IncompInfo', 'Conditional reject'),
        ('Rejected', 'Rejected'),
        ('Checked', 'Risk checked'),
        ('CdsFailed', 'CDS failed'),
        ('Active', 'Activated'),
        ('Suspended', 'Suspended'),
        ('PendingClose', 'Pending close'),
        ('Closed', 'Closed'),
        ('CloseFailed', 'Close failed'),
        ('Inactive', 'In-Active'),
        ('Dormant', 'Dormant'),
        ], string='Status',default='New')
    stage = fields.Selection([
        ('Signup', 'Signup'),
        ('Rejected', 'Rejected'),
        ('Activated', 'Activated'),
        ('Closed', 'Closed'),
        ('Inactive', 'Inactive/dormant'),
        ], string='Stage',default='Signup')
    signup_ip = fields.Text(string='Signup IP address')
    signup_date = fields.Datetime(string='Date/time first signup')
    is_riskcheck_exist = fields.Boolean(string='Is check',default=False)
    trade_allowed = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Trade allowed', default='No')
    risk_check_status = fields.Selection([
        ('Passed', 'Passed'),
        ('Failed', 'Failed'),
        ('None','None'),
        ], string='Risk check status')
    risk_check_date = fields.Datetime(string='Date/time checked')
    financier_id = fields.Integer(string='Financing bank')
    financing_limit = fields.Float(string='Financing limit',digits=(15,2),default=0)
    cds_open_date = fields.Date(string='CDS open date')
    notified_on_active = fields.Boolean(string='Notified on active',default=False)
    notified_date = fields.Datetime(string='Date/time notified')
    questionaire_score = fields.Integer(string='Questionaire scoring')
    card_tran_name = fields.Char('Card holder name')
    reason_incomplete = fields.Integer(string='Reason for Incomplete')
    resubmit = fields.Boolean(string='Re-submit signup flag',default=False)
    resubmit_date = fields.Datetime(string='Re-submit date/time')
    p_id = fields.Integer('Original ID')

# Model for ACCOUNT RISK CHECKLIST table

class customer_acct_riskcheck_arch(models.Model):
    _name = 'vb.customer_acct_riskcheck_arch'
    _description = 'Customer account risk check archive'
    
    # fields
    acct_id = fields.Integer(string='Customer account',index=True)
    name = fields.Char(string='Risk check name')
    checked_by = fields.Integer(string='Checked by')
    state = fields.Selection([
        ('New', 'New'),
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('ByPass', 'Bypass'),
       ], string='Status', default='New')
    checked_date = fields.Datetime(string='Date/time checked')
    checked_ok = fields.Boolean(string='Checked OK',default=False)
    checked_bypass = fields.Boolean(string='Bypassed',default=False)
    checked_failed = fields.Boolean(string='Failed',default=False)
    result_obtained = fields.Integer('Result obtained',default=0)
    result_max = fields.Integer('Maximum result required',default=0)
    result_min = fields.Integer('Minimum result required',default=0)
    bypassed_by = fields.Integer(string='Bypassed by')
    bypassed_date = fields.Datetime(string='Date/time pypassed')    
    code = fields.Char(string='Risk check code')
    when_first_checked = fields.Datetime(string='Date/time first checked')
    when_last_checked = fields.Datetime(string='Date/time last checked')
    approver1 = fields.Char(string='Approver #1')
    approver2 = fields.Char(string='Approver #2')
    p_id = fields.Integer('Original ID')
    
# Model for ACCOUNT QUESTIONAIRE RESPONSE table

class customer_acct_questionaire_arch(models.Model):
    _name = 'vb.customer_acct_questionaire_arch'
    _description = 'Response to questionaire archive'

    # fields
    acct_id = fields.Integer(string='Customer account',index=True)
    name = fields.Char(string='Description')
    set = fields.Char(string='Set')
    section = fields.Char(string='Section')
    question_no = fields.Integer(string='Question no')
    question_text = fields.Char(string='Question')
    response = fields.Char(string='Response to question')
    required = fields.Selection([
        ('Yes', 'Yes'),
        ('Conditional', 'Conditional'),
        ], string='Required response')
    desired_response = fields.Char(string='Desired response')
    scoring = fields.Integer(string='Scoring')
    state = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('None', 'No response'),
       ], string='Status')
    p_id = fields.Integer('Original ID')

class card_tran_arch(models.Model):
    _name = 'vb.card_tran_arch'
    _description = 'Credit card transaction'
    _rec_name = 'tran_refno'
    _order = 'acct_id,tran_date'    
    
    # fields
    acct_id = fields.Integer(string='Customer account',index=True)
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Datetime(string='Transaction date')
    tran_type = fields.Char(string='Transaction type')
    tran_amt = fields.Float(string='Transaction amount',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amount',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction local GST',digits=(15,2),default=0)
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    issuing_bank = fields.Char(string='Issuing bank')
    issuing_country = fields.Char(string='Issuing country')
    issuing_country_id = fields.Integer(string='Card issuing country')
    card_number = fields.Char(string='Credit card number')
    card_expiry = fields.Char(string='Card expiry in MM/YY')
    name_on_card = fields.Char(string='Name on card')
    approval_code = fields.Char(string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    currency_id = fields.Integer(string='Currency')
    response_code = fields.Char(string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_gateway = fields.Char(string='Payment gateway')
    order_refno = fields.Char(string='Order reference')
    state = fields.Selection([
        ('New', 'New'),
        ('Pending', 'Pending'),
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('Posted', 'Posted'),
        ('Cancelled', 'Cancelled'),
        ('Refunded', 'Refunded'),
        ], string='Status')
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    payment_code = fields.Char(string='Payment code')
    payment_mode = fields.Char(string='Payment mode')
    bank_refno = fields.Char(string='Bank reference')
    other_refno = fields.Char(string='Other reference')
    matched_bank = fields.Char(string='Matched with bank')
    matched_date = fields.Datetime(string='Matched date')
    matched_amt = fields.Float(string='Matched amount',digits=(15,2),default=0)
    bank_matching_amt = fields.Float(string='Bank matching amount',digits=(15,2),default=0)
    comments = fields.Text(string='Comments')
    print_status = fields.Char(string='Print status')
    print_date = fields.Datetime(string='Date/time printed')
    match_status = fields.Char(string='Matched status')
    cardholder_name = fields.Char(string='Cardholder name')
    p_id = fields.Integer('Original ID')
    
class document_arch(models.Model):
    _name = 'vb.document_arch'
    _description = 'Central document store - archive'
    _order = 'name'
    
    # fields
    name = fields.Char(string='Document name',index=True)
    doc_class_id = fields.Integer(string='Document class')
    doc_type_id = fields.Integer(string='Document type')
    doc_version = fields.Char(string='Doc version')
    file_loc = fields.Char(string='File location')
    file_name = fields.Char(string='File name attachment')
    notes = fields.Text(string='Notes and comments')
    date_valid = fields.Date(string='Valid until')
    # The next 3 fields are optional and only if document is associated to the customer or customer account level
    customer_id = fields.Integer(string='Customer',index=True)
    acct_id = fields.Integer(string='Customer account',index=True)
    submit_date = fields.Datetime(string='Date/time submitted')
    submit_by = fields.Integer(string='Submitted by')
    apprej_date = fields.Datetime(string='Date/time approved/rejected')
    apprej_by = fields.Integer(string='Approved/rejected by')
    archived_date = fields.Datetime(string='Date/time archived')
    archived_by = fields.Integer(string='Archived by')
    document = fields.Binary('Document', attachment=True)
    permission = fields.Selection([
        ('Public', 'Public'),
        ('MemberAll', 'All members'),
        ('MemberInd', 'Individual member'),
        ('Internal', 'Internal user only'),
        ], string='Permission', default='Internal')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Checked'),
        ('Rejected', 'Rejected'),
        ('Archived', 'Archived'),
        ], string='Status', default='New')
    doc_date = fields.Date(string='Document date')
    p_id = fields.Integer('Original ID')

