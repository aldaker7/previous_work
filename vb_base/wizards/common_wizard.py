from openerp import models, fields, api, _
from datetime import datetime
from openerp.exceptions import Warning
from json import dumps
import json
import requests


class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'
    
    message= fields.Char('message')

    #This method will be used to suspend a service option when state in new
    @api.multi
    def suspend_service_option(self):
        serv_recs = self.env['vb.service_option'].search([('id','in', self._context.get('active_ids'))])
        if serv_recs:
            serv_recs.write({'state':'Suspended','suspended_by':self._uid,'comments':self.note, 'suspended_date':datetime.now()})
            
     #This method will be used to suspend a service option when state in new
    @api.multi
    def activate_service_option(self):
        serv_recs = self.env['vb.service_option'].search([('id','in', self._context.get('active_ids'))])
        if serv_recs:
            serv_recs.write({'state':'Active'})
            
     #This method will be used to uplift a service option when state in new
    @api.multi
    def uplift_service_option(self):
        serv_recs = self.env['vb.service_option'].search([('id','in', self._context.get('active_ids'))])
        if serv_recs.comments != None:
            dec_val = '*'*50
            temp_val = serv_recs.comments + "\n" + dec_val + "\n" + self.note
            self.note = temp_val
            serv_recs.write({'state':'Active','uplifted_by':self._uid,'comments':self.note, 'uplifted_date':datetime.now()})
            
     # Method for Refreshing web service cache        
    @api.multi
    def refresh_web_service_cache(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RefreshCache')])
        if not url_rec:
            raise Warning("There is no record in vb.config for RefreshCache 1")
        url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if url==False:
            raise Warning('Please provide the IP to connect with webservice for RefreshCache 1')
        data = dumps({})
        headers={'Content-type': 'application/json'}
        content = requests.get(url,data=data,headers=headers)    
        if content.json().get('code')!=200 or content ==False:
            raise Warning('Can not connect with web service for RefreshCache 1')
        else:
            if url_rec.parm3:
                url2 = str(url_rec.parm3)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                if url2 == False:
                    raise Warning('Please provide the IP to connect with webservice for RefreshCache 2')
                data = dumps({})
                headers={'Content-type': 'application/json'}
                content = requests.get(url2,data=data,headers=headers)  
                if content.json().get('code')!=200 or content ==False:
                    raise Warning('Can not connect with web service for RefreshCache 2') 
                else:
                    final_view = self.env['ir.model.data'].get_object_reference('vb_base', 'refresh_websvc_cache_completion_wizard_view')[1]
                    message = str('The Refresh process has been completed.')
                    return {
                        'name':'Process successful.',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'view_id': final_view,
                        'target': 'new',
                        'context': {'default_message': message},
                        }
            else:
                raise Warning(_("IP for the second server is not provided in parm3 \ncode: RefreshCache"))
            