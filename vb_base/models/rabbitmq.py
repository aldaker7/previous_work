
import pika
from openerp import models

class rabbitmq_server(models.Model):

    _name = 'rabbitmq.server'
    
    def get_rabbitmq_channel(self):
        mq_server_rec = self.env['vb.config'].search([('code','=','MQServer')])
        credentials = pika.PlainCredentials(mq_server_rec.parm3, mq_server_rec.parm4)
        connection = pika.BlockingConnection(pika.ConnectionParameters(mq_server_rec.parm1, int(mq_server_rec.parm2), '/', credentials))
        channel = connection.channel()
        channel.queue_declare(queue=mq_server_rec.parm5)
        return channel
