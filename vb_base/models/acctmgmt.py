# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Core models
#
# Author: Daniel Tan
# Last updated: 15-Aug-2017

from openerp import models, fields, api, tools, _
from datetime import datetime, date
from openerp.exceptions import ValidationError
import string, re
from openerp.http import request
from dateutil.relativedelta import relativedelta
from datetime import  datetime,date, timedelta
from library_test import *


# Model for COMPANY ACCOUNT BALANCE table
# This table stores the aggregate account balances at the company level
# 09-Oct-2016 Added by DT

class company_acct_bal(models.Model):
    _name = 'vb.company_acct_bal'
    _description = 'Company account balances'

    # fields
    company_id = fields.Many2one(comodel_name='res.company', string='Company',ondelete='restrict',index=True)
    assigned_limit = fields.Float(string='Assigned limit',digits=(15,2),default=0,)
    threshold = fields.Float(string='Threshold',digits=(5,2),default=0)
    # 31-Aug-2017
    eff_shareholder_fund = fields.Float(string='Effective shareholders fund',digits=(15,2),default=0)
    tot_max_finance_amt = fields.Float(string='Total max finance amount',digits=(15,2),default=0)
    single_max_finance_amt = fields.Float(string='Single max finance amount',digits=(15,2),default=0)
    single_cust_exposure = fields.Float(string='Single customer exposure',digits=(15,2),default=0)
    single_asset_exposure = fields.Float(string='Single asset exposure',digits=(15,2),default=0)

# Model for CUSTOMER table
# There is ONE record for each unique customer
# A customer can be an individual, a business entity (aka company) or a trader
# Constraint: There should not be any duplicate national ID (if entered) for an individual or BizRegNo for an business entity
# Targeted number of records: 1-2M within next 3 years

class customer(models.Model):
    _name = 'vb.customer'
    _description = 'Customer'
    _order = 'name'

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if self._context.get('incomplete_doc_cash'):
            lst = []
            recs = self.env['vb.customer_acct'].search([('state','=','IncompInfo'),('resubmit','=',False),('acct_type','=',"Cash")])
            for i in recs:
                lst.append(i.customer_id.id)
            cust_recs = self.env['vb.customer'].browse(lst)
            return cust_recs
        elif self._context.get('incomplete_doc_contra'):
            lst = []

            recs = self.env['vb.customer_acct'].search([('state','=','IncompInfo'),('resubmit','=',False),('acct_type','=',"Contra")])
            for i in recs:
                lst.append(i.customer_id.id)
            cust_recs = self.env['vb.customer'].browse(lst)
            return cust_recs
        elif self._context.get('incomplete_doc'):
            lst = []

            recs = self.env['vb.customer_acct'].search([('state','=','IncompInfo'),('resubmit','=',False)])
            for i in recs:
                lst.append(i.customer_id.id)
            cust_recs = self.env['vb.customer'].browse(lst)
            return cust_recs
        if self._context.get('login_first_time'):
            lst = []
            recs = self.env['vb.customer_login'].search([('initial_login','=',True),('login_domain','=','FE'),('state','=',"Active")])
            for i in recs:
                lst.append(i.customer_id.id)
            cust_recs = self.env['vb.customer'].browse(lst)
            return cust_recs
        return super(customer, self).search(args, offset, limit, order,count=count)


    @api.model
    def create(self,vals):
        if self.banks_ids:
#             for rec in self.env['vb.customer_bank'].search([('customer_id','=',self.id)]):
#                 rec.write({'bank_acct_name':self.name})
            vals['email1'] = vals['email1'].lower()
        else:
            vals['email1'] = vals['email1'].lower()
        return super(customer, self).create(vals)

    #this method for telling about required fields inside the trees
    @api.constrains('address_ids','banks_ids','jobs_ids','nok_ids')
    def get_required(self):
        if self.address_ids:
            for rec in self.address_ids:
                if rec.address_type == False:
                    raise  ValidationError ("Please fill in the Address type in Addresses")
                else:
                    if rec.address1 == False:
                        raise  ValidationError ("Please fill in the Address in Addresses")
                    else:
                        if rec.city == False:
                            raise  ValidationError ("Please fill in the City in Addresses")
                        else:
                            if rec.country_id == False:
                                raise ValidationError ('Please fill in the Country in Addresses')
                            else:
                                if rec.postcode == False:
                                    raise ValidationError ("Please fill in the Postal code in Addresses")
        if self.banks_ids:
            for rec in self.banks_ids:
                if rec.bank_id == False:
                    raise ValidationError("Please fill in Bank in Bank account")
                else:
                    if rec.bank_acct_no == False:
                        raise ValidationError('Please fill in Bank account number in Bank account')

    #this function for checking on repeated default or primary rec
    @api.constrains('address_ids','banks_ids')
    def get_default_address(self):
        if self.address_ids:
            default_count = 0
            for rec in self.env['vb.customer_address'].search([('customer_id','=',self.id)]):
                if rec.default_rec == True:
                    default_count += 1
            if default_count >= 2 :
                raise ValidationError('Sorry there is more than one default address in Addresses')
        if self.banks_ids:
            default_count_bank = 0
            for rec in self.env['vb.customer_bank'].search([('customer_id','=',self.id)]):
                if rec.primary_acct == True:
                    default_count_bank += 1
            if default_count_bank >= 2 :
                raise ValidationError('Sorry there is more than one Primary account in Bank account')
#
    #validation for customer Email
    @api.constrains("email1","email2","mobile_phone1","mobile_phone2","date_of_birth")
    def customer_field_validation(self):
        for rec in self:
            if rec.email1:
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,}|[0-9]{1,3})(\\]?)$", rec.email1) == None:
                    raise ValidationError ("Invalid email address. Please enter a valid primary email address!")

            if rec.email2:
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,}|[0-9]{1,3})(\\]?)$", rec.email2) == None:
                    raise ValidationError ("Invalid email address. Please enter a valid alternate email address!")

            if rec.mobile_phone1:
                if re.match("^\+(?:[0-9] ?){6,14}[0-9]$", rec.mobile_phone1) == None:
                    raise ValidationError ("Invalid mobile number. Please enter a valid primary mobile number!")

            if rec.mobile_phone2:
                if re.match("^\+(?:[0-9] ?){6,14}[0-9]$", rec.mobile_phone2) == None:
                    raise ValidationError ("Invalid mobile number. Please enter a valid alternate mobile number!")

            if rec.date_of_birth:
                d1 = datetime.strptime(rec.date_of_birth,'%Y-%m-%d')
                d2 = datetime.now()
                age = (d2 - d1).days + 1
                if age < 6570:
                    raise ValidationError ("Age of person must be 18 years and above.")

        return True

    # This function will reduce the size of any attached picture to 128x128px suitable for Kanban views
    @api.depends('picture')
    def _reduce_customer_picture(self):
        for rec in self:
            rec.picture = tools.image_resize_image_medium(rec.picture)

#     # Returns the default customer class
    @api.model
    def _get_default_class(self):
        rec = self.env['vb.common_code'].search([('code_type','=','CustClass'),('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return
#
    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    @api.onchange('national_id_type_id')
    def get_name_required(self):
        if self.national_id_type_id.name == 'Passport':
            self.id_expiry_check = True
        else:
            self.id_expiry_check = False

    @api.multi
    def write(self,vals):
        new_vals={}
        result = super(customer,self).write(vals)
        for rec in self:
            if rec.email1:
                v = rec.email1.lower()
                new_vals.update({'email1':v})
            if rec.name:
                acct_rec = self.env['vb.customer_acct'].search([('customer_id','=',rec.id)])
                for i in acct_rec:
                    i.update({'name':rec.name})
            result = super(customer,self).write(new_vals)
        return result

    @api.multi
    def name_get(self):
        result=[]

        for rec in self:
            x = str(request.__dict__)
            if 'action' in x:
                action_customer_overview = request.env['ir.model.data'].get_object('vb_dashboard', 'actions_customer_overview1')
                y = str(x.split('action')[1])
                action_id =  filter(lambda x:x.isdigit(),y.split(',')[0])
                if action_customer_overview.id == int(action_id):
                    self.get_cust_since(rec.id)
            self.env['vb.customer_bank'].get_document_stmt(rec.id)
            result.append((rec.id,u"%s" % (rec.name)))
        return result

    @api.multi
    def get_cust_since(self,customer_id):
        rec = self.env['vb.customer'].search([('id','=',customer_id)])
        if rec:
            if rec.date_of_birth:
                s = datetime.strptime(rec.date_of_birth,'%Y-%m-%d')
                final = str(s.strftime("%Y-%m-%d"))+' ('+str(relativedelta(datetime.now(), s).years)+' Years)'
                if rec.date_activated:
                    s1=datetime.strptime(rec.date_activated.split('.')[0],"%Y-%m-%d %H:%M:%S")
                    final1 = str(s1.strftime('%Y-%m-%d %H:%M:%S'))+'('+str(relativedelta(datetime.now(), s1).years)+' Years,'+str(relativedelta(datetime.now(), s1).months)+' Months,'+str(relativedelta(datetime.now(), s1).days)+' Days)'
                    self._cr.execute('Update vb_customer set customer_since=%s, birth_years=%s, write_uid=%s where id=%s',(final1,final,1,customer_id))
                else:
                    self._cr.execute('Update vb_customer set birth_years=%s, write_uid=%s where id=%s',(final,1,customer_id))

    @api.depends('jobs_ids')
    def _compute_industry(self):
        for rec in self:
            job_rec = self.env['vb.customer_job'].search([('customer_id','=',rec.id),('active','=',True)],limit=1)
            if job_rec.industry_id:
                industry = self.env['vb.common_code'].search([('id','=',job_rec.industry_id.id),('active','=',True)],limit=1)
                if industry:
                    rec.industry = industry.name

    # fields

    name = fields.Char(size=60,string='Customer name',index=True)
    # This is the customer code which will form part of the account number when customer opens account.
    code = fields.Char(size=20, string='Customer code',index=True,
                       help='This provides the master account number for this customer.  All accounts for this customer will have a sub-number appended to this.')
    title_id = fields.Many2one(comodel_name='vb.common_code',string='Title Id',ondelete='restrict',domain="[('code_type','=','PersonTitle')]")
    title = fields.Char(string='Title',related='title_id.name',store=True,readonly=True)
    formal_title = fields.Char(string='Formal title',help='Formal title used by dignitaries')
    address_ids = fields.One2many(comodel_name='vb.customer_address',inverse_name='customer_id', string='Customer address',
                                  help='This point to a list of addresses associated with this customer.')
    email1 = fields.Char(size=40,string='Primary email',index=True,
                         help='This is the primary email address that the system will use to communicate with this customer.')
    email2 = fields.Char(size=40,string='Alternate email')
    mobile_phone1 = fields.Char(size=20,string='Primary mobile no')
    mobile_phone2 = fields.Char(size=20,string='Alternate mobile no')
    home_phone = fields.Char(size=20,string='Home phone')
    work_phone = fields.Char(size=20,string='Work phone')
    work_phone_ext = fields.Char(string='Extension')
    main_phone = fields.Selection([
        ('Mobile1', 'Primary mobile'),
        ('Mobile2', 'Alternate mobile'),
        ('Home', 'Home'),
        ('Work', 'Work'),
        ], string='Primary phone', default='Mobile1',help='This field is used to indicate which phone will be used as main phone to contact customer')
    date_of_birth = fields.Date(string='Date of birth')
    cust_age = fields.Char(string='No. Of Years')
    country_of_birth_id = fields.Many2one(comodel_name='res.country',string='Country of birth',default=_get_default_country,ondelete='restrict')
    gender = fields.Selection([
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('N/A', 'Not applicable'),
        ], string='Gender', default='Male')
    marital_status = fields.Selection([
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Divorced', 'Divorced'),
        ('Widowed', 'Widowed'),
        ], string='Marital status', default='Single')
    employment_status = fields.Selection([
        ('SelfEmployed', 'Self Employed'),
        ('Employed', 'Employed'),
        ('UnEmployed', 'Unemployed'),
        ('Student', 'Student'),
        ('Retired', 'Retired'),
        ], string='Employment status', default='Employed')
    customer_class_id = fields.Many2one(comodel_name='vb.common_code',string='Customer classification',ondelete='restrict',
                                        default=_get_default_class, domain="[('code_type','=','CustClass')]")
    customer_type = fields.Selection([
        ('Individual', 'Individual'),
        ('BizOrg', 'Business organization'),
        ('Trader', 'Trader'),
        ('Staff', 'Staff'),
        ], string='Customer type', default='Individual')
    bumi_status = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No')
        ], string='Bumiputera', default='No')
    # The following field is for a business organization only
    bizregno = fields.Char(size=20,string='Business registration number',help='For companies - this will be the business registration number')
    # The following two fields only if customer type is Trader
    trader_profile_id = fields.Many2one(comodel_name='vb.trader_profile', string='Trader profile',ondelete='restrict')
    trader_code = fields.Char(size=20,string='Trader code',help='Use in legacy system to identify the trader.  Useful when migrating data to this system.')
    #
    nationality_id = fields.Many2one(comodel_name='res.country',string='Country of nationality',default=_get_default_country,ondelete='restrict')
    residency_id = fields.Many2one(comodel_name='res.country',string='Country of residency',default=_get_default_country,ondelete='restrict')
    national_id_no = fields.Char(size=20,string='Primary ID number',index=True)
    national_id_type_id = fields.Many2one(comodel_name='vb.common_code',string='Primary ID type',ondelete='restrict',domain="[('code_type','=','IdType')]")
    national_id_expiry = fields.Date(string='Primary ID expiry')
    other_id_no = fields.Char(size=20,string='Other ID number',index=True)
    other_id_type_id = fields.Many2one(comodel_name='vb.common_code',string='Other ID type',ondelete='restrict',domain="[('code_type','=','IdType')]")
    other_id_expiry = fields.Date(string='Other ID expiry date')
    race_id = fields.Many2one(comodel_name='vb.common_code',string='Race',ondelete='restrict',domain="[('code_type','=','Race')]")
    picture = fields.Binary(string='Picture',help='Attached picture will automatically be re-sized to 128x128px')
    picture_small = fields.Binary(string='Picture small',compute='_reduce_customer_picture',store=True)
    comments = fields.Text(string='Comments')
    date_signed_up = fields.Datetime(string='Date/time signed up',default=lambda self: fields.datetime.now())
    date_activated = fields.Datetime(string='Date/time activated')
    activated_by = fields.Many2one(comodel_name='res.users',string='Activated by')
    date_suspended = fields.Datetime(string='Date/time suspended')
    suspended_by = fields.Many2one(comodel_name='res.users',string='Suspended by')
    reason_suspended_id = fields.Many2one(comodel_name='vb.common_code', string='Reason for suspension',ondelete='restrict')
    date_uplifted = fields.Datetime(string='Date/time suspension uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by')
    date_dormant = fields.Datetime(string='Date/time dormant')
    last_activity_date = fields.Datetime(string='Last activity date')

    acct_activity_cash_ids=fields.One2many(comodel_name='vb.customer_acct_activity', inverse_name='customer_id', string="Account Activity Cash",domain=[('acct_id.acct_type', '=', 'Cash')])
    acct_activity_margin_ids=fields.One2many(comodel_name='vb.customer_acct_activity', inverse_name='customer_id', string="Account Activity Margin",domain=[('acct_id.acct_type', '=', 'Margin')])
    acct_activity_contra_ids=fields.One2many(comodel_name='vb.customer_acct_activity', inverse_name='customer_id', string="Account Activity Contra",domain=[('acct_id.acct_type', '=', 'Contra')])
    acct_msg_cash_ids=fields.One2many(comodel_name='vb.msg_log', inverse_name='customer_id', string="Communication Cash",domain=['|',('acct_id.acct_type', '=', 'Cash'),('acct_id','=',False)])
    acct_msg_margin_ids=fields.One2many(comodel_name='vb.msg_log', inverse_name='customer_id', string="Communication Margin",domain=['|',('acct_id.acct_type', '=', 'Margin'),('acct_id','=',False)])
    acct_msg_contra_ids=fields.One2many(comodel_name='vb.msg_log', inverse_name='customer_id', string="Communication Contra",domain=['|',('acct_id.acct_type', '=', 'Contra'),('acct_id','=',False)])
    acct_doc_cash_ids=fields.One2many(comodel_name='vb.document', inverse_name='customer_id', string="Documents Cash",domain=['|',('acct_id.acct_type', '=', 'Cash'),('acct_id','=',False)])
    acct_doc_margin_ids=fields.One2many(comodel_name='vb.document', inverse_name='customer_id', string="Documents Margin",domain=['|',('acct_id.acct_type', '=', 'Margin'),('acct_id','=',False)])
    acct_doc_contra_ids=fields.One2many(comodel_name='vb.document', inverse_name='customer_id', string="Documents Contra",domain=['|',('acct_id.acct_type', '=', 'Contra'),('acct_id','=',False)])
    acct_bal_cash_ids=fields.One2many(comodel_name='vb.customer_acct_bal', inverse_name='customer_id', string="Account Balance Cash",domain=[('acct_id.acct_type', '=', 'Cash')])
    acct_bal_margin_ids=fields.One2many(comodel_name='vb.customer_acct_bal', inverse_name='customer_id', string="Account Balance Margin",domain=[('acct_id.acct_type', '=', 'Margin')])
    acct_bal_contra_ids=fields.One2many(comodel_name='vb.customer_acct_bal', inverse_name='customer_id', string="Account Balance Contra",domain=[('acct_id.acct_type', '=', 'Contra')])

    sec_que_ids=fields.One2many(comodel_name='vb.customer_sec_question', inverse_name='customer_id', string="Security Questions")
    acct_ids = fields.One2many(comodel_name='vb.customer_acct', inverse_name='customer_id', string="Accounts")
    info_ids = fields.One2many(comodel_name='vb.customer_add_info', inverse_name='customer_id', string="Additional info")
    jobs_ids = fields.One2many(comodel_name='vb.customer_job', inverse_name='customer_id', string="Occupation")
    nok_ids = fields.One2many(comodel_name='vb.customer_nok', inverse_name='customer_id', string="Next of kin")
    login_ids = fields.One2many(comodel_name='vb.customer_login', inverse_name='customer_id', string="Login")
    banks_ids = fields.One2many(comodel_name='vb.customer_bank', inverse_name='customer_id', string="Bank account")
    # This points to the default trader handling this customer.  This default is overridden at the account level.
    trader_id = fields.Many2one(comodel_name='vb.customer', string='Trader',ondelete='set null',index=True,domain="[('customer_type','=','Trader')]",
                                help='This is the default trader for all accounts under this customer.')
    state = fields.Selection([
        ('Basic', 'Basic signup only'),
        ('New', 'New signup'),
        ('Rejected', 'Rejected'),
        ('Active', 'Activated'),
        ('Suspended', 'Suspended'),
        ('Dormant', 'Dormant'),
        ], string='Status', default='New',help="This shows the current status of the record")
    stage = fields.Selection([
        ('Signup', 'Signup'),
        ('Rejected', 'Rejected'),
        ('Activated', 'Activated'),
        ('Suspended', 'Suspended'),
        ('Uplifted', 'Uplifted'),
        ('Dormant', 'Dormant'),
        ], string='Stage', default='Signup',help="This shows the stage where the customer is within the sign-up process.")
    # customer will use login name to login to services.  Login password stored in separate file and depending on login domain
    login_name = fields.Char(size=100,string='Login name',index=True)
    # Internal fields - not directly exposed to user
    email1_verified = fields.Boolean(string='Primary email verified',default=False)
    email2_verified = fields.Boolean(string='Alternate email verified',default=False)
    email1_date_verified = fields.Datetime(string='Date primary email verified',default=False)
    email2_date_verified = fields.Datetime(string='Date alternate email verified',default=False)
    mobile1_verified = fields.Boolean(string='Mobile number 1 verified',default=False)
    mobile2_verified = fields.Boolean(string='Mobile number 2 verified',default=False)
    mobile1_date_verified = fields.Datetime(string='Date mobile number 1 verified',default=False)
    mobile2_date_verified = fields.Datetime(string='Date mobile number 2 verified',default=False)
    active = fields.Boolean(string='Active',default=True)
    # 08-Aug-2016 DT
    signup_ip = fields.Text(string='Signup IP address')
    signup_date = fields.Datetime(string='When first signup')
    id_expiry_check = fields.Boolean(string='ID expiry check',default=False)
    questionaire_score = fields.Integer(string='Questionaire scoring')
    # 20-Jan-2017 DT
    info_source_id = fields.Many2one(comodel_name='vb.common_code',string='Info source',domain="[('code_type','=','InfoSource')]")
    info_source_subid = fields.Many2one(comodel_name='vb.common_code',string='Info source sub',domain="[('code_type','=','InfoSource')]")
    info_source_specs = fields.Char(string='Info source specs')
    education_inst = fields.Char(string='Educational institution')
    birth_years = fields.Char()
    customer_since = fields.Char()
    is_cash = fields.Boolean(string='Is Cash',default=False)
    is_margin = fields.Boolean(string='Is Margin',default=False)
    is_contra = fields.Boolean(string='Is Contra',default=False)
    # 23-Mar-2017 DT
    other_title = fields.Char(string='Other title',help='Enter if not in title list')
#     Added by Nandha to use loyalty point referer id
    referrer_id = fields.Many2one(comodel_name='vb.customer', string='Referer Id',ondelete='cascade',index=True)
    referrer_code = fields.Char(string='Referrer Code')
    #Added by Nandha (Irfan needed this fields for loyalty)
    source_id = fields.Char(string='Source Id')
    source = fields.Char(string='Source')
    signup_channel = fields.Char(string='Signup Channel')
    # 10-Sep-2017
    extra_info_done = fields.Boolean(string='Extra information completed',default=False)
    extra_info_done_date = fields.Datetime(string='Extra information completed on')
    # 25-Oct-2017
    crs_tax_status = fields.Selection([
        ('Local', 'Malaysia tax resident'),
        ('LocalForeign', 'Msia and non-Malaysia tax resident'),
        ('Foreign', 'Non-Malaysia tax resident'),
        ], string='CRS tax status')
    crs_ids = fields.One2many(comodel_name='vb.customer_crs', inverse_name='customer_id', string='List of Tax ID numbers')
    ramci_kyc_date = fields.Datetime(string='Date KYC extract done')
    ramci_nrvb_date = fields.Datetime(string='Date NRVB extract done')
    ramci_iriss_date = fields.Datetime(string='Date IRISS extract done')
    questionaire_date = fields.Datetime(string='Last update of questionaire')
    riskquestion_ids = fields.One2many(comodel_name='vb.customer_questionaire', inverse_name='customer_id', string="Questionaire")
    incomplete_by = fields.Many2one('res.users',string="Incomplete email sent by")
    industry = fields.Char(string='Industry', store=True, compute=_compute_industry)
    # 28-Mar-2018
    is_staff = fields.Selection([('Yes','Yes'),('No','No')],default='No')

    # SQL constraint
    # 05-Aug-2016 DT: added additional constraint - by national ID

    _sql_constraints = [('unique_customer_code','UNIQUE(code,active)','Another customer with the same code already exist'),
                        ('unique_customer_name','UNIQUE(customer_type,name,date_of_birth,nationality_id,active)','Another customer with the same name and date of birth already exist'),
                        ('unique_national_id','UNIQUE(customer_type,nationality_id,national_id_type_id,national_id_no,active)','Another customer with the same name national ID already exist'),
                        ('unique_customer_email','UNIQUE(customer_type,email1,active)','Another customer with the same primary email already exist'),
                        ('unique_customer_mobile','UNIQUE(customer_type,mobile_phone1,active)','Another customer with the same mobile phone 1 already exist'),
                        ('unique_customer_login','UNIQUE(login_name,active)','Another customer with the same login name already exist')]

# Model for CUSTOMER_INBOX table
# Added 01-Dec-2016 DT

class customer_inbox(models.Model):
    _name = 'vb.customer_inbox'
    _description = 'Customer inbox'

    # field
    customer_id = fields.Many2one(comodel_name='vb.customer',string='Customer',ondelete='cascade',index=True)
    msg_date = fields.Datetime(string='Message date/time')
    msg_subject = fields.Char(string='Subject')
    msg_content = fields.Html(string='Content')
    msg_type = fields.Char(string='Message type') # List defined in vb_common_code
    msg_class = fields.Char(string='Message class') # List defined in vb_common_code
    sender_address = fields.Char(string='Sender address')
    email_address = fields.Char(string='Email address')
    state = fields.Selection([
        ('Read', 'Read'),
        ('Trash', 'Trashed'),
        ('UnRead', 'Un-read'),
        ], string='Status')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account')
    trash_date = fields.Datetime(string='Trash date/time')
    read_date = fields.Datetime(string='Read date/time')
    # 13-Jan-2017
    active = fields.Boolean(string='Active',default=True)
    # 06-Sep-2017
    msg_log_id = fields.Integer('Message log ID')
    notify_type = fields.Selection([
        ('Notify', 'Notify'),
        ('Push', 'Push'),
        ], string='Notify type',default='Notify')

    date_start = fields.Datetime(string='Date/time start')
    date_expire = fields.Datetime(string='Date/time expire')

# Model for CUSTOMER ATTRIBUTE table
# This model is to store the additional information about the customer (those to be added after LIVE)

class customer_add_info(models.Model):
    _name = 'vb.customer_add_info'
    _description = 'Customer additional info'
    _order = 'customer_id,list_seq'
    _rec_name = 'customer_id'

    # field
    attribute_id = fields.Many2one(comodel_name='vb.common_code',string='Info type',ondelete='restrict',domain="[('code_type','=','CustAttr')]")
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    info_value = fields.Char(string='Value')
    info_flag = fields.Boolean(string='Flag')
    info_text = fields.Text(string='Additional text')
    list_seq = fields.Integer(string='List order')
    active = fields.Boolean(string='Active',default=True)

# Model for CUSTOMER DEVICE SETUP table
# This model is to store the various settings on the device which the customer uses.
# Each setting is store in a string with the value pair format of 'aaa=bbb<,xxx=yyy>'
# The client app will need to use string functions to extract the individual settings.

class customer_dev_setup(models.Model):
    _name = 'vb.customer_dev_setup'
    _description = 'Customer device setup'

    # field
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    device_code = fields.Char(string='Device code',help='This field uniquely identifies the smart device.')
    code_type = fields.Char(string='Code type')
    code = fields.Char(string='Code key')
    name = fields.Char(string='Name')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    active = fields.Boolean(string='Active',default=True)

# Model for CUSTOMER BANK table
# This model is to store the information about the customer bank accounts
# This is important as it allows system to identify the customer or customer account when customer makes a deposit.
# Constraint: There should not be any duplicate bank account for the same bank_id.
# Targeted number of records: Average 1 bank account per customer

class customer_bank(models.Model):
    _name = 'vb.customer_bank'
    _description = 'Customer bank'
    _order = 'customer_id,bank_acct_no'
    _rec_name = "bank_name"

    @api.model
    def _get_default_bank(self):
        bank_default = self.env['vb.common_code'].search([('default_rec','=',True),('code_type','=','Bank')],limit=1)
        if bank_default:
            return bank_default.id
        return

    # This function will compute the default the valueres. of the customer bank account name (which is bank name + account no).
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    @api.multi
    def name_get(self):
        super(customer_bank, self).name_get()
        result=[]
        for rec in self:
            result.append((rec.id,u"%s (%s)" % (rec.bank_acct_no,rec.bank_id.name)))
        return result

    @api.multi
    def get_document_stmt(self,customer_id):
        if customer_id:
            document_rec = self.env['vb.document'].search([('customer_id','=',customer_id),('name','like','%BANK%')])
            if document_rec:
                for i in document_rec:
                    query= "Update vb_customer_bank set bank_statement=%s where customer_id=%s"
                    self._cr.execute(query,(i.id,customer_id))
                    break

#

    # field
    bank_id = fields.Many2one(comodel_name='vb.common_code',string='Bank',ondelete='restrict',domain="[('code_type','=','Bank')]", default=_get_default_bank)
    bank_name = fields.Char(size=100,string='Primary bank name',related='bank_id.name',readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    bank_acct_name = fields.Char(size=100,string='Bank account name',store= True)
    bank_acct_no = fields.Char(size=20,string='Bank account number')
    bank_branch = fields.Char(size=100,string='Bank branch')
    primary_acct = fields.Boolean(string='Primary bank account',default=False,help='Customer with more than one bank account, indicate which is the primary account to use.')
    active = fields.Boolean(string='Active',default=True)
    remark = fields.Char(string='Remarks',size=100)
    bank_statement = fields.Many2one(comodel_name='vb.document',string='Document')
    secondary_acct_name = fields.Char(size=100,string='Secondary bank name')
    # SQL constraint
    # 13-Jan-2017 - We trying now to use unique partial index to implement unique constraint
    # _sql_constraints = [('unique_acct_no','UNIQUE(bank_id,bank_acct_no,active)','Another bank account already exist with this number')]

# Model for CUSTOMER JOB table
# This model store the information about the customer jobs.  We need this information to determine if the customer
# is employed in any of the companies within the group.
# Targeted number of records: Average 1 job per customer

# Last amended: 05-Aug-2016

class customer_job(models.Model):
    _name = 'vb.customer_job'
    _description = 'Customer job'
    _order = 'customer_id,date_start desc'
    _rec_name = "customer_id"

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.active == True:
                rec.active = False;
            else:
                rec.active = False

    # shows the state dropdown or field editable dependent of Country selection
    @api.onchange('country_id')
    def _onchange_country_id(self):
        self.state_id = ''
        self._compute_hide_state()

    def _compute_hide_state(self):
        rec = self.env['res.country.state'].search([('country_id', '=', self.country_id.id)], limit=1)
        if rec:
            self.hide_state_id = False
        else:
            self.hide_state_id = True

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    employer = fields.Char(string='Employer name',)
    employer_biztype = fields.Char(string='Employer business type')
    employer_bizregno = fields.Char(string='Employer registration number',help='This will be the employer business registration number')
    occupation_id = fields.Many2one(comodel_name='vb.common_code',string='Occupation',ondelete='restrict',domain="[('code_type','=','Occupation')]")
    designation = fields.Char(string='Designation')
    date_start = fields.Date(string='Date started')
    date_end = fields.Date(string='Date until')
    last_salary = fields.Float(string='Last drawn salary')
    last_salary_asat = fields.Date(string='Last drawn salary as at')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency of salary',ondelete='restrict')
    note = fields.Text(string='Note')
    address1 = fields.Char(size=100,string='Address 1')
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=40,string='City')
    postcode = fields.Char(size=20,string='Postal code')
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    state_name = fields.Char(string='Name of state')
    country_id = fields.Many2one(comodel_name='res.country', string='Country',ondelete='restrict')
    phone = fields.Char(size=20,string='Employer general telephone')
    email = fields.Char(size=40,string='Employer general email')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)
    industry_id = fields.Many2one(comodel_name='vb.common_code',string='Industry',ondelete='restrict',domain="[('code_type','=','Industry')]")
    industry_info = fields.Char(string='Industry Info')
    display_address = fields.Char(string='Display address')
    # 15-Aug-2017 - V2
    annual_income = fields.Float(string='Annual income',digits=(9,2),default=0)
    current_job_flag = fields.Boolean(string='Current job',default=False)
    contact_name = fields.Char(size=100,string='Contact person name')
    contact_phone = fields.Char(size=20,string='Contact person phone')
    contact_email = fields.Char(size=40,string='Contact person email')
    # added 20-Oct-2017
    hide_state_id = fields.Boolean(string='Hide', compute="_compute_hide_state", store=False)

# Model for CUSTOMER NEXT OF KIN table
# This model store the information about the customer next of kin
# Targeted number of records: Average 2 next of kin per customer

# Last amended: 05-Aug-2016

class customer_nok(models.Model):
    _name = 'vb.customer_nok'
    _description = 'Customer next of kin'
    _order = 'customer_id'
    _rec_name = "customer_id"

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    @api.onchange('title')
    def _onchange_title(self):
        for rec in self:
            if rec.title:
                if rec.title != 'Other':
                    rec.formal_title = string.capitalize(rec.title)

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    # shows the state dropdown or field editable dependent of Country selection
    @api.onchange('employer_country_id')
    def _onchange_employer_country_id(self):
        self.employer_state_id = ''
        self._compute_hide_state()

    def _compute_hide_state(self):
        rec = self.env['res.country.state'].search([('country_id', '=', self.employer_country_id.id)], limit=1)
        if rec:
            self.hide_employer_state_id = False
        else:
            self.hide_employer_state_id = True

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    name = fields.Char(size=100,string='Next of kin name')
    # 23-Aug-2016 DT: added title_id so that values for this field is user-defined
    title_id = fields.Many2one(comodel_name='vb.common_code',string='Title',ondelete='restrict',domain="[('code_type','=','PersonTitle')]")
    title = fields.Char(string='Title',related='title_id.name',store=True,readonly=True)
    formal_title = fields.Char(string='Formal title',help='Enter if not in title list')
    relationship_type = fields.Selection([
        ('Spouse', 'Spouse'),
        ('Son', 'Son'),
        ('Daughter', 'Daughter'),
        ('Brother', 'Brother'),
        ('Sister', 'Sister'),
        ('Father', 'Father'),
        ('Mother', 'Mother'),
        ('Relative', 'Relative'),
        ], string='Relationship type')
    nationality_id = fields.Many2one(comodel_name='res.country',string='Country of nationality',default=_get_default_country,ondelete='restrict')
    national_id_type_id = fields.Many2one(comodel_name='vb.common_code',string='Primary ID type',ondelete='restrict',domain="[('code_type','=','IdType')]")
    national_id_no = fields.Char(size=20,string='Primary ID number')
    national_id_expiry = fields.Date(string='Primary ID expiry')
    other_id_no = fields.Char(size=20,string='Other ID number')
    other_id_type_id = fields.Many2one(comodel_name='vb.common_code',string='Other ID type',ondelete='restrict',domain="[('code_type','=','IdType')]")
    other_id_expiry = fields.Date(string='Other ID expiry date')
    race_id = fields.Many2one(comodel_name='vb.common_code',string='Race',ondelete='restrict',domain="[('code_type','=','Race')]")
    date_of_birth = fields.Date(string='Date of birth')
    country_of_birth_id = fields.Many2one(comodel_name='res.country',string='Country of birth',default=_get_default_country,ondelete='restrict')
    address1 = fields.Char(size=100,string='Address 1')
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=40,string='City')
    postcode = fields.Char(size=20,string='Postal code')
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    country_id = fields.Many2one(comodel_name='res.country', string='Country',default=_get_default_country,ondelete='restrict')
    phone = fields.Char(size=20,string='Telephone')
    employer = fields.Char(string='Employer name')
    employer_biztype = fields.Char(string='Employer business type')
    employer_bizregno = fields.Char(string='Employer registration number',help='This will be the employer business registration number')
    occupation_id = fields.Many2one(comodel_name='vb.common_code',string='Occupation',ondelete='restrict',domain="[('code_type','=','Occupation')]")
    gender = fields.Selection([
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other'),
        ], string='Gender', default='Male')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)
    state_name = fields.Char(string='Name of state')
    display_address = fields.Char(string='Display address')
    industry_id = fields.Many2one(comodel_name='vb.common_code',string='Industry',ondelete='restrict',domain="[('code_type','=','Industry')]")
    # 15-Aug-2017 - V2
    employer_phone = fields.Char(size=20,string='Employer phone')
    employer_email = fields.Char(size=40,string='Employer email')
    employer_address1 = fields.Char(size=100,string='Employer address 1')
    employer_address2 = fields.Char(size=100,string='Employer address 2')
    employer_address3 = fields.Char(size=100,string='Employer address 3')
    employer_city = fields.Char(size=40,string='Employer city')
    employer_postcode = fields.Char(size=20,string='Employer postal code')
    employer_state_id = fields.Many2one(comodel_name='res.country.state', string='Employer state',ondelete='restrict')
    employer_country_id = fields.Many2one(comodel_name='res.country', string='Employer country',ondelete='restrict')
    employer_contact = fields.Char(size=40,string='Employer contact person')
    #03-Oct-2017
    employment_status = fields.Selection([
        ('SelfEmployed', 'Self Employed'),
        ('Employed', 'Employed'),
        ('UnEmployed', 'Unemployed'),
        ('Student', 'Student'),
        ('Retired', 'Retired'),
        ], string='Employment status', default='Employed')
    employer_state_name = fields.Char(size=40,string='Employer state name')
    # added on 20-Oct-2017
    hide_employer_state_id = fields.Boolean(string='Hide', compute="_compute_hide_state", store=False)

# Model for CUSTOMER ADDRESS table
# Each customer may have zero to many addresses.  However, there will be one address that will be tagged as the default to be
# used by the system to generate documents (e.g. contract notes, etc)
# Targeted number of records: Average 2 addresses per customer

class customer_address(models.Model):
    _name = 'vb.customer_address'
    _description = 'Customer address'
    _rec_name = 'address_type'
    _order = 'customer_id,list_seq'

    @api.onchange('country_id')
    def change_country_state(self):
        if self.country_code != 'MY':
            self.state_id = False
        else:
            self.state_name = ''

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.active == True:
                rec.active = False;
            else:
                rec.active = False

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    address_type = fields.Selection([
        ('Permanent', 'Permanent'),
        ('Residential', 'Residential'),
        ('Work', 'Work'),
        ('Mailing', 'Mailing'),
        ('Other', 'Other'),
        ], string='Address type',default='Permanent')
    address1 = fields.Char(size=100,string='Address 1',)
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=40,string='City',)
    postcode = fields.Char(size=20,string='Postal code',)
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    state_name = fields.Char(size=100,string='Name of state')
    country_id = fields.Many2one(comodel_name='res.country',default=_get_default_country, string='Country',ondelete='restrict')
    country_code = fields.Char(string='Country code', related='country_id.code',readonly=True)
    display_address = fields.Char(string='Display address')
    default_rec = fields.Boolean(string='Default address',default=False,help='If customer has more than one address, this field indicates which is the default address')
    geolocation = fields.Char(string='Geo location',help='This can be a GPS location or some map reference')
    geopicture = fields.Binary(string='Picture of geolocation',help='This is the picture of the location.  Can be screen capture of map.')
    list_seq = fields.Integer(string='List order')
    active = fields.Boolean(string='Active',default=True)
    comments = fields.Text(string='Comments')

# Model for CUSTOMER SERVICE table
# Each customer may have zero to many service subscription.
# Unique constraints to be implemented externally since we cannot have same customer having 2 or more active
# subscription for the same service
# 05-Dec-2016 DT Added new

class customer_svc_sub(models.Model):
    _name = 'vb.customer_svc_sub'
    _description = 'Customer service subscription'
    _order = 'customer_id'
    _rec_name = 'customer_name'

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    customer_name = fields.Char(string='Customer name',related='customer_id.name',readonly=True)
    svc_sub_id = fields.Many2one(comodel_name='vb.service_option',string='Service subscription',ondelete='restrict')
    svc_name = fields.Char(string='Service name',related='svc_sub_id.name',readonly=True)
    subscription_date = fields.Date(string='Subscription date')
    last_renewal_date = fields.Date(string='Last renewal date')
    next_renewal_date = fields.Date(string='Next renewal date')
    curr_start_date = fields.Date(string='Current subscription start date')
    curr_end_date = fields.Date(string='Current subscription end date')
    subscription_amt = fields.Float(string='Monthly subscription amount',digits=(9,2),default=0)
    auto_renewal = fields.Boolean(string='Auto-renewal')
    last_charge_date = fields.Date(string='Date last charged/billed')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True)
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    list_seq = fields.Integer(string='List order')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Rejected', 'Rejected'),
        ('Active', 'Active'),
        ('Expired', 'Expired'),
        ('Suspended', 'Suspended'),
        ], string='Status', default='New')
    approve_reject_date = fields.Datetime(string='Date/time approved/rejected')
    approve_reject_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected by')
    suspended_date = fields.Datetime(string='Date/time suspended')
    suspended_by = fields.Many2one(comodel_name='res.users',string='Cancelled by')
    cancelled_date = fields.Datetime(string='Date/time suspended')
    cancelled_by = fields.Many2one(comodel_name='res.users',string='Cancelled by')
    uplifted_date = fields.Datetime(string='When uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by')
    comments = fields.Text(string='Comments')
    #added 19-Sept-2017
    cust_svc_sub_trans_id = fields.One2many(comodel_name='vb.customer_svc_sub_tran',inverse_name='cust_svc_sub_id', string='Customer Service Subscription transactions')

class customer_svc_sub_tran(models.Model):
    _name = 'vb.customer_svc_sub_tran'
    _description = 'Customer service subscription transactions'

    # fields
    cust_svc_sub_id = fields.Many2one(comodel_name='vb.customer_svc_sub', string='Customer service subscription')
    customer_name = fields.Char(string='Customer name',related='cust_svc_sub_id.customer_id.name',readonly=True)
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency')
    currency_name = fields.Char(string='Currency name',related='currency_id.name',readonly=True)
    tran_refno = fields.Char(size=20,string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    tran_type = fields.Char(string='Transaction type')
    #    ('Opening', 'Opening'),
    #    ('Charge', 'Charge'),
    #    ('Credit', 'Credit'),
    #    ('Renewal','Renewal'),
    #    ('New','New'),
    tran_mode = fields.Char(string='Payment mode')
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    curr_start_date = fields.Date(string='Current subscription start date')
    curr_end_date = fields.Date(string='Current subscription end date')
    other_refno = fields.Char(size=30,string='Other reference')
    bank_refno = fields.Char(size=30,string='Bank reference')
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code')
    date_cancelled = fields.Datetime(string='Date/time cancelled')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    reason_void_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for void',ondelete='restrict',domain="[('code_type','=','ReasonCancel')]")
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonReject')]")
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry')
    bank_acct_id = fields.Many2one(comodel_name='vb.bankacct', string='Related bank account',readonly=True)
    bank_name = fields.Char(size=100,string='Bank name')
    bank_branch = fields.Char(string='Bank branch', )
    bank_acct_no = fields.Char(size=20,string='Bank account number',)
    card_number = fields.Char(string='Credit card number')
    card_expiry = fields.Char(string='Card expiry in MM/YY')
    name_on_card = fields.Char(string='Name on card')
    fpx_refno = fields.Char(size=30,string='FPX refno number')
    fpx_date = fields.Datetime(string='FPX date')
    approval_code = fields.Char(size=30,string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    response_code = fields.Char(size=30,string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_code = fields.Char(size=30,string='Payment code')
    internal_trx_no = fields.Integer(string='Internal transaction number',readonly=True,index=True)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Subscribed', 'Subscribed'),
        ('Breached', 'Breached'),
        ('Rejected', 'Rejected'),
        ('Failed', 'Failed'),
       ], string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage')
    # 03-Feb-2017
    withdraw_limit = fields.Float(string='Withdraw limit')
    breached_amt = fields.Float(string='Breached amount')

# Model for CUSTOMER ACCOUNT table
# Each customer may have zero to several accounts.  Each account is identified by an account number.
# Currently, there will be at least two types of account for each customer:
# 1) Cash Upfront account - All customer when opening a trading account will have this
# 2) Margin account - Customers who subsequently subscribe to the margin product will also have a margin account.
# An account cannot be used immediately by the customer until it is 'Activated'
# Constraint: account number must be unique
# Targeted number of records: Average 2 accounts per customer

class customer_acct(models.Model):
    _name = 'vb.customer_acct'
    _description = 'Customer account'
    _order = 'acct_no'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(customer_acct, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=False)
        context = self._context or {}
        active_action = request.__dict__.get('jsonrequest').get('params').get('action_id')
        action_riskcheck = request.env['ir.model.data'].get_object('vb_acctmgmt', 'action_risk_checking')
        if str(active_action) == str(action_riskcheck.id):
            rec = self.env['vb.customer_acct'].search([('state','=','New'),('active','=',True)])
            for i in rec:
                if bool(i.document_ids)!=True:
                    exist_acct = self.env['vb.customer_acct'].search([('customer_id','=',i.customer_id.id),('state','=','Active'),('id','!=',i.id)])
                    if exist_acct:
                        if i.acct_type == 'Cash' or i.customer_id.employment_status in ['Student','UnEmployed','Retired']:

                            query = "Update vb_customer_acct set is_doc_approved='t' where id=%s"%i.id
                            self._cr.execute(query)
                        else:

                            query = "Update vb_customer_acct set is_doc_approved='f' where id=%s"%i.id
                            self._cr.execute(query)
        return res



    @api.multi
    def get_bank_details(self):
        if self.bank_acct_id:
            self.bank_acct_no = self.env['vb.customer_bank'].search([('id','=',self.bank_acct_id.id)]).bank_acct_no

    @api.multi
    def update_approver(self,app1id,app2id,app3id,app4id,recid):
        rec = self.env['vb.customer_acct_riskcheck'].search([('riskcheck_id','=',recid)])
        if rec:
            for i in rec:
                if i.acct_id.state in ['IncompInfo','New','Rejected']:
                    i.update({'approver1':app1id,'approver2':app2id,'approver3':app3id,'approver4':app4id})

    # Returns the default account class
    @api.model
    def _get_default_class(self):
        rec = self.env['vb.common_code'].search([('code_type','=','AcctClass'),('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default market
    @api.model
    def _get_default_market(self):
        market = self.env['vb.market'].search([('default_rec','=',True)],limit=1)
        if market:
            return market.id
        return

    # Returns the default currency
    @api.model
    def _get_default_currency(self):
        rec = self.env['vb.currency'].search([('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    @api.multi
    def write(self,vals):
        #to save the current editing
        result=super(customer_acct,self).write(vals)
        count_doc=0
        count_all=0
        doc2_approve = False
        # The follow logic only applies if the State is New only.
        if self.state in ['New','IncompInfo'] :
            new_vals = {}
            failed = 0
            #variable
            checklist_count = 0
            passed_count = 0
            count_doc_cash = 0
            count_doc_contra = 0
            if self.acct_type == 'Cash':
                if self.rel_acct_id:
                    if self.customer_id.employment_status in ['UnEmployed','Retired','Student']:
                        for i in self.rel_acct_id:
                            for n in i.document_ids:
                                count_doc_contra+=1
                        for i in self.document_ids:
                            count_doc_cash+=1
                        if count_doc_cash == count_doc_contra:
                            doc2_approve = True
            # run thru checklist, count total item in list and count how many passed
#             if self.acct_type != 'Cash':
            self._cr.execute("Select * from vb_customer_acct_cedd where acct_id=%s"%self.id)
            cedd_state = self._cr.fetchall()
            for risk_rec in self.riskcheck_ids:
                if (risk_rec.state == 'Failed') and (risk_rec.approver1 or risk_rec.approver2 or risk_rec.approver3 or risk_rec.approver4):
                    risk_rec.update({'is_failed_approv': True})
                else:
                    risk_rec.update({'is_failed_approv': False})
#             cedd_query_state = "Select state from vb_customer_acct_cedd where acct_id=%s"%self.id
#             self._cr.execute(cedd_query_state)
            if bool(cedd_state)!= False:
                if self.facility_approved_amt > 0.0 and self.acct_type!='Cash':
                    if self.ramci_contra2 == True and self.is_cedd_done==True:
                        if cedd_state not in ['CDDRejected','EDDRejected']:
                            for risk_rec in self.riskcheck_ids:
#                                 if (risk_rec.state == 'Failed') and (risk_rec.approver1 or risk_rec.approver2):
#                                     risk_rec.update({'is_failed_approv': True})
#                                 else:
#                                     risk_rec.update({'is_failed_approv': False})
                                checklist_count = checklist_count + 1
                                if risk_rec.state in ['OK', 'ByPass']:
                                    passed_count = passed_count + 1
                                if risk_rec.state == 'Failed' :
                                    if risk_rec.code == 'Kyc02':
                                        passed_count = passed_count + 1
                                    else:
                                        failed = failed + 1
                                if failed > 0:
                                    new_vals.update({'risk_check_status': 'Failed'})
                                elif passed_count != checklist_count:
                                    new_vals.update({'risk_check_status': 'None'})
                                else:
#                                     if bool(self.env['res.users'].has_group('vb_base.vb_group_top_mgmt'))==True:
#                                         new_vals.update({'risk_check_status': 'Passed',
#                                                          'state':'Checked','approved_by':self._uid,'approved_date':date.today()})
#                                     else:
                                    new_vals.update({'risk_check_status': 'Passed'})
            if self.acct_type == "Cash":
                if self.ramci_contra2 == True and self.is_cedd_done==True:
                    if cedd_state not in ['CDDRejected','EDDRejected']:
                        for risk_rec in self.riskcheck_ids:
                            if (risk_rec.state == 'Failed') and (risk_rec.approver1 or risk_rec.approver2 or risk_rec.approver3 or risk_rec.approver4):
                                risk_rec.update({'is_failed_approv': True})
                            else:
                                risk_rec.update({'is_failed_approv': False})
                            checklist_count = checklist_count + 1
                            if risk_rec.state in ['OK', 'ByPass']:
                                passed_count = passed_count + 1
                            if risk_rec.state == 'Failed' :
                                if risk_rec.code == 'Kyc02':
                                    passed_count = passed_count + 1
                                else:
                                    failed = failed + 1
                            if failed > 0:
                                new_vals.update({'risk_check_status': 'Failed'})
                            elif passed_count != checklist_count:
                                new_vals.update({'risk_check_status': 'None'})
                            else:
#                                 if bool(self.env['res.users'].has_group('vb_base.vb_group_top_mgmt'))==True:
#                                     new_vals.update({'risk_check_status': 'Passed',
#                                                      'state':'Checked','approved_by':self._uid,'approved_date':date.today()})
#                                 else:
                                new_vals.update({'risk_check_status': 'Passed'})


            if self.rel_acct_id:
                pep_failed = 0
                new_rec = 0
                for i in self.riskcheck_ids:
                    if i.state == "New":
                        new_rec +=1
                        break
                    if i.state == "Failed":
                        if i.code == "Kyc02":
                            pep_failed +=1
                        else:
                            new_rec +=1

                if pep_failed >0 and new_rec == 0:
                    new_vals.update({'pep_failed':True})
                if new_rec > 0:
                    new_vals.update({'pep_failed':False})

            exist_acct = self.env['vb.customer_acct'].search([('customer_id','=',self.customer_id.id),('state','=','Active'),('id','!=',self.id)])
            if exist_acct and self.acct_type == 'Cash' :
                new_vals.update({'is_doc_approved':True})
            else:
                if self.document_ids:
                    new_doc = 0
                    for doc in self.document_ids:
                        if doc.doc_class_id.code == 'ACCTOPEN':
                            count_all+=1
                            if doc.state in ['Approved','Archived']:
                                count_doc+=1
                            elif doc.state == 'New':
                                new_doc +=1
                        if count_all == count_doc:
                            new_vals.update({'is_doc_approved':True})
                            if doc2_approve == True and self.rel_acct_id:
                                self.rel_acct_id.is_doc_approved = True
                        else:
                            new_vals.update({'is_doc_approved':False})
                            if new_doc > 0 :
                                new_vals.update({'is_new_doc':True})
                            else:
                                new_vals.update({'is_new_doc':False})
            result=super(customer_acct,self).write(new_vals)
        # To remove the HTML tags from question_text
        if self.riskquestion_ids:
            question_text_id = self.riskquestion_ids
            for i in question_text_id:
                cleanr = re.compile('<.*?>')
                cleantext = re.sub(cleanr, '', i.question_text)
                i.update({'question_text':cleantext})
        return result

    @api.onchange('riskcheck_ids')
    def check_cedd_done(self):
        if self.acct_type == 'Contra':
            if self.riskcheck_ids:
                #variable
                checklist_count = 0
                passed_count = 0
                for i in self.riskcheck_ids:
                    checklist_count = checklist_count + 1
                    if i.state in ['OK', 'ByPass']:
                        passed_count = passed_count + 1
                if checklist_count == passed_count:
                    raise ValidationError("Reminder: \n Don't forget to fill the CDD and EDD form")


    # This function will compute the default the value of the account name (which is customer name + account no).
    @api.multi
    def name_get(self):
        super(customer_acct, self).name_get()
        self.check_ramci_date()
        for i in self:
            if bool(i.document_ids) != True:
                exist_acct = self.env['vb.customer_acct'].search([('customer_id','=',i.customer_id.id),('state','=','Active'),('id','!=',i.id)])
                if bool(exist_acct):
                    for n in exist_acct:
                        if i.acct_type == 'Cash' :
                            query = "Update vb_customer_acct set is_doc_approved='t' where id=%s"%i.id
                            self._cr.execute(query)
        print (self._uid)
        print ("There is another User using this")
        if 'jsonrequest' in (request.__dict__):
            if 'params' in (request.__dict__).get('jsonrequest'):
                if 'kwargs' in (request.__dict__).get('jsonrequest').get('params'):
                    if 'context' in (request.__dict__).get('jsonrequest').get('params').get('kwargs'):
                        if 'params' in (request.__dict__).get('jsonrequest').get('params').get('kwargs').get('context'):
                            if 'action' in (request.__dict__).get('jsonrequest').get('params').get('kwargs').get('context').get('params'):
                                active_action = (request.__dict__).get('jsonrequest').get('params').get('action')
                                action_riskcheck = request.env['ir.model.data'].get_object('vb_acctmgmt', 'action_risk_checking')
                                if active_action == action_riskcheck.id:
                                   
                                    query = 'Update vb_customer_acct set flag=%s where id=%s'
                                    self._cr.execute(query,('f',self.id))
        result=[]
        for rec in self:
            self.env['vb.customer_bank'].get_document_stmt(rec.customer_id.id)
            if rec.acct_no:
                result.append((rec.id,u"%s (%s)" % (rec.acct_no,rec.customer_id.name)))
            else:
                result.append((rec.id,u"%s (%s)" % ('New',rec.customer_id.name)))
        ###############################################
        ##########To get the active action id##########
        ###############################################
        if 'params' in (request.__dict__).get('jsonrequest'):
            if 'kwargs' in (request.__dict__).get('jsonrequest').get('params'):
                if 'context' in (request.__dict__).get('jsonrequest').get('params').get('kwargs'):
                    if 'params' in (request.__dict__).get('jsonrequest').get('params').get('kwargs').get('context'):
                        if 'action' in (request.__dict__).get('jsonrequest').get('params').get('kwargs').get('context').get('params'):
                            active_action = (request.__dict__).get('jsonrequest').get('params').get('kwargs').get('context').get('params').get('action')
                            action_riskcheck = request.env['ir.model.data'].get_object('vb_acctmgmt', 'action_risk_checking')
                            if active_action == action_riskcheck.id:
                                recs = (request.__dict__).get('jsonrequest').get('params').get('args')
                                self._cr.execute("UPDATE vb_customer_acct set who_using=null where who_using=%s"%request.session.uid)
                                record_id =recs[0][0] if type(recs[0][0]) == int else (request.__dict__).get('jsonrequest').get('params').get('args')[0][0][2][0]
                                print record_id
                                rec = self.env['vb.customer_acct'].search([('id','in',[record_id] ),('who_using','=',False)])
                                if rec:
                                    query = "UPDATE vb_customer_acct set who_using=%s where id=%s"
                                    self._cr.execute(query,(self._uid,record_id))
                                else:
                                    raise ValidationError('Another user is viewing this account')
#                                     mod_obj = self.env['ir.model.data']
#                                     try:
#                                         form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_risk_check_test_form')[1]
#                                     except ValueError:
#                                         form_res = False
#                                     return{
#                                            'name': "Account Application Approval",
#                                            'view_type': 'form',
#                                            'view_mode':"[form]",
#                                            'view_id': False,
#                                            'res_model': 'vb.customer_acct',
#                                            'type': 'ir.actions.act_window',
#                                            'views': [(form_res, 'form')],
#                                            'domain': [('id','=', record_id)],
#                                             }
                                
        result=[]
        for rec in self:
            self.env['vb.customer_bank'].get_document_stmt(rec.customer_id.id)
            if rec.acct_no:
                result.append((rec.id,u"%s (%s)" % (rec.acct_no,rec.customer_id.name)))
            else:
                result.append((rec.id,u"%s (%s)" % ('New',rec.customer_id.name)))
        return result
    
    @api.model
    def test_print(self):
        print "YES IT IS WORKINGGGGGGGGGGGGGGGGGG", request.session.uid
        user_exist = self.env['vb.customer_acct'].search([('who_using','=',request.session.uid)])
        if user_exist :
            self._cr.execute("UPDATE vb_customer_acct set who_using=null where who_using=%s"%request.session.uid)

    # This function will set various defaults when customer ID changes
    @api.onchange('customer_id')
    def _onchange_customer_id(self):
        if self.customer_id:
            addr = self.env['vb.customer_address'].search([('customer_id','=',self.customer_id.id),('default_rec','=',True)],limit=1)
            if addr:
                self.address_id=addr.id
            bank = self.env['vb.customer_bank'].search([('customer_id','=',self.customer_id.id),('primary_acct','=',True)],limit=1)
            if bank:
                self.bank_acct_id=bank.id
            self.email = self.customer_id.email1
            self.trader_id = self.customer_id.trader_id
            self.name = self.customer_id.name

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    @api.multi
    @api.constrains('email','cds_no')
    def check_email(self):
        if self.email:
            if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", self.email) == None:
                raise ValidationError ("Invalid email address. Please enter a valid email address!")
        if self.cds_no:
            check_string = bool(re.search('[a-zA-Z]+',self.cds_no))
            if check_string == True :
                raise ValidationError('CDS number is invalid:\nNumeric values only provided ')
            elif len(str(self.cds_no)) > 12:
                raise ValidationError('CDS number is invalid:\n Max length is 12 digits ')

    # Compute age of the suspended
    @api.depends('date_suspended')
    def _compute_day_diff(self):
        for rec in self:
            if rec.date_suspended:
                d1 = datetime.strptime(rec.date_suspended,'%Y-%m-%d %H:%M:%S')
                d2 = datetime.now()
                d3 = lambda d2: fields.datetime.now()
                rec.age=abs((d3(d2) - d1).days + 1)

    @api.multi
    def get_group(self):
        for i in self:
#             if bool(self.env['res.users'].has_group('vb_base.vb_group_top_mgmt'))==True:
#                 i.flag=True
#             else:
            i.flag=False

    @api.multi
    def check_new_doc(self):
        for i in self:
            if i.document_ids:
                for doc in i.document_ids:
                    if doc.state == 'New':
                        i.update({'is_new_doc':True})
                        query="update vb_customer_acct set is_doc_approved='f' where id=%s"%i.id
                        self._cr.execute(query)
#     @api.multi
#     def hide_incomplete_button(self):
#         for i in self:
#
#             if i.rel_acct_id:
#                 if i.acct_type == 'Cash':
#                     i.update({'hide_incomplete':False})
#                 else:
#                     i.update({'hide_incomplete':True})
#             else:
#                 i.update({'hide_incomplete':False})
#         self.get_group()
    @api.multi
    def check_ramci_date(self):
        list_of_ramci_date = []
        for n in self:
            if n.customer_id:
                recs = self.env['vb.customer_acct'].search([('customer_id','=',n.customer_id.id),('id','!=',n.id),('state','=','Active')])
                if recs:
                    for i in recs:
                        list_of_ramci_date.append(i.customer_id.ramci_nrvb_date)
                    last_ramci_date = max(list_of_ramci_date)
                    three_m_before = str(datetime.now() + relativedelta(months=-3))
                    if last_ramci_date >= three_m_before:
                        query = "update vb_customer_acct set show_nrvb='t' where id=%s"%n.id
                        self._cr.execute(query)
                        if n.acct_type == 'Cash':
                            query1 = "update vb_customer_acct set ramci_contra1='t', ramci_contra2='t' where id=%s"%n.id
                            self._cr.execute(query1)
                        else:
                            query1 = "update vb_customer_acct set ramci_contra1='t' where id=%s"%n.id
                            self._cr.execute(query1)
                    else:
                        query = "update vb_customer_acct set show_nrvb='f' where id=%s"%n.id
                        self._cr.execute(query)
                else:
                    query = "update vb_customer_acct set show_nrvb='f' where id=%s"%n.id
                    self._cr.execute(query)
        self.get_group()



    # Fields
    name = fields.Char(size=60,string='Account name')
    acct_no = fields.Char(size=20,string='Account number',index=True)
    other_acct_no = fields.Char(size=20,string='Other account number',index=True)
    acct_class_id = fields.Many2one(comodel_name='vb.common_code',string='Product',ondelete='restrict',default=_get_default_class,domain="[('code_type','=','AcctClass')]",readonly=True)
    acct_type = fields.Char(string='Account type',related='acct_class_id.parm1',readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='restrict')
    branch_id = fields.Many2one(comodel_name='vb.branch',string='Branch',ondelete='restrict')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency',ondelete='restrict')
    market_id = fields.Many2one(comodel_name='vb.market',string='Market',ondelete='restrict',default=_get_default_market)
    email = fields.Char(size=40,string='Account email',index=True)
    date_opened = fields.Datetime(string='Date/time opened',default=lambda self: fields.datetime.now())
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonRej')]")
    date_activated = fields.Datetime(string='Date/time activated')
    activated_by = fields.Many2one(comodel_name='res.users',string='Activated by')
    date_suspended = fields.Datetime(string='Date/time suspended')
    reason_suspended_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for suspension',ondelete='restrict',domain="[('code_type','=','ReasonSus')]")
    suspended_by = fields.Many2one(comodel_name='res.users',string='Suspended by')
    closing_request_date = fields.Datetime(string='Date/time close requested')
    date_closed = fields.Datetime(string='Date/time closed')
    closed_by = fields.Many2one(comodel_name='res.users',string='Closed by')
    reason_closed_id = fields.Many2one(comodel_name='vb.common_code', string='Reason for closing',ondelete='restrict',domain="[('code_type','=','ReasonCls')]")
    date_uplifted = fields.Datetime(string='Date/time suspension uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by')
    reason_uplifted_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for uplift',ondelete='restrict',domain="[('code_type','=','ReasonUplift')]")
    comments = fields.Text(string='Comments')
    date_inactive = fields.Datetime(string='Date/time inactive')
    date_dormant = fields.Datetime(string='Date/time dormant')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved By',ondelete='no action')
    # Approved date - previous Date type - for V2 - changed to Datetime to be consistent
    approved_date = fields.Datetime('Approved Date')
    age = fields.Integer(string='Aging', compute='_compute_day_diff', readonly=True, store=False)
    last_activity_date = fields.Datetime(string='Last activity date')
    balance_ids = fields.One2many(comodel_name='vb.customer_acct_bal', inverse_name='acct_id', string="Balances")
    portfolio_ids = fields.One2many(comodel_name='vb.portfolio', inverse_name='acct_id', string="Portfolio")
    cds_no = fields.Char(size=15,string='CDS no')
    acctprof_id = fields.Many2one(comodel_name='vb.account_profile', string='Account profile',ondelete='set null',index=True,
                                  help='This is the account profile associated with this account.')
    tradeprof_id = fields.Many2one(comodel_name='vb.trade_profile', string='Trade profile',ondelete='set null',index=True,
                                   help='This is the trade profile associated with this account.')
    mrgnprof_id = fields.Many2one(comodel_name='vb.margin_profile', string='Margin profile',ondelete='set null',index=True,
                                  help='This is the margin profile associated with this account.')
    riskprof_id = fields.Many2one(comodel_name='vb.risk_profile', string='Risk profile',ondelete='set null',index=True,
                                  help='This is the risk profile associated with this account.')
    # This points to the trader handling this account.  If not empty, this Trader will override the default trader defined for the customer
    trader_id = fields.Many2one(comodel_name='vb.customer', string='Trader',ondelete='set null',index=True,domain="[('customer_type','=','Trader'),('state','=','Active')]",
                                help='If not specified for this account, the default trader at the customer level will be used.')
    address_id = fields.Many2one(comodel_name='vb.customer_address', string='Account address',ondelete='restrict',domain="[('customer_id','=',customer_id)]",
                                 help='This is the customer address associated with this account.  If not specified, the default customer address will be used.')
    bank_acct_id = fields.Many2one(comodel_name='vb.customer_bank', string='Bank account',ondelete='restrict',domain="[('customer_id','=',customer_id)]",
                                 help='This is the customer bank associated with this account.  If not specified, the default customer bank will be used.')
    bank_acct_no = fields.Char(string='Bank Account Number',compute=get_bank_details)
    active = fields.Boolean(string='Active',default=True)
    # 15-Aug-2017 - for V2, added more states
    state = fields.Selection([
        ('Basic', 'Basic signup only'),
        ('New', 'New signup'),
        ('IncompInfo', 'Conditional reject'),
        ('Rejected', 'Rejected'),
        ('CDDRejected', 'CDD Rejected'),        # v2
        ('EDDRejected', 'EDD Rejected'),        # v2
        ('Checked', 'Risk checked'),
        ('CDDChecked', 'CDD checked'),          # v2
        ('EDDChecked', 'EDD checked'),          # v2
        ('ApprovedFclt', 'Facility approved'),  # v2
        ('AcceptedFclt', 'Facility accepted'),  # v2
        ('SentOffer', 'Sent offer'),            # V2
        ('AcceptOffer', 'Accepted offer'),      # V2
        ('CdsFailed', 'CDS failed'),
        ('Active', 'Activated'),
        ('Suspended', 'Suspended'),
        ('PendingClose', 'Pending close'),
        ('Closed', 'Closed'),
        ('CloseFailed', 'Close failed'),
        ('Inactive', 'In-Active'),
        ('Dormant', 'Dormant'),
        ], string='Status',default='New')
    stage = fields.Selection([
        ('Signup', 'Signup'),
        ('Rejected', 'Rejected'),
        ('Activated', 'Activated'),
        ('Closed', 'Closed'),
        ('Inactive', 'Inactive/dormant'),
        ], string='Stage',default='Signup')
    signup_ip = fields.Text(string='Signup IP address')
    signup_date = fields.Datetime(string='Date/time first signup')
    riskcheck_ids = fields.One2many(comodel_name='vb.customer_acct_riskcheck', inverse_name='acct_id', string="Risk check")
    riskquestion_ids = fields.One2many(comodel_name='vb.customer_acct_questionaire', inverse_name='acct_id', string="Questionaire")
    is_riskcheck_exist = fields.Boolean(compute='risk_check_button',string='Is check',help='to check about riskcheck one2many if it is exist')
    trade_allowed = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Trade allowed', default='No')
    # 15-Sep-2016 AlDaker (changed by DT)
    risk_check_status = fields.Selection([
        ('Passed', 'Passed'),
        ('Failed', 'Failed'),
        ('None','None'),
        ], string='Risk check status')
    risk_check_date = fields.Datetime(string='Date/time checked')
    document_ids = fields.One2many(comodel_name='vb.document', inverse_name='acct_id', string="Documents Ids",domain=[('doc_class_id.code','=','ACCTOPEN')])
    document_ramci_ids = fields.One2many(comodel_name='vb.document', inverse_name='acct_id', string="Documents Ramci Ids",domain=[('doc_class_id.code','=','RAMCI')])
    cds_open_date = fields.Date(string='CDS open date')
    notified_on_active = fields.Boolean(string='Notified on active')
    notified_date = fields.Datetime(string='Date/time notified')
    email1 = fields.Char(string='Primary email',related='customer_id.email1',readonly=True)
    mobile_phone1 = fields.Char(string='Primary mobile no',related='customer_id.mobile_phone1',readonly=True)
    national_id_no = fields.Char(string='Primary ID number',related='customer_id.national_id_no',store=True,readonly=True)
    national_id_type = fields.Char(string='Primary ID type',related='customer_id.national_id_type_id.name',store=True,readonly=True)
    other_id_no = fields.Char(string='Other ID number',related='customer_id.other_id_no',readonly=True)
    other_id_type = fields.Char(string='Other ID type',related='customer_id.other_id_type_id.name',readonly=True)
    questionaire_score = fields.Integer(string='Questionaire scoring')
    title = fields.Char(string='Title', related='customer_id.title',readonly=True)
    nationality = fields.Char(string="Nationality",related='customer_id.nationality_id.name',readonly=True)
    # 09-Jan-2017
    card_tran_ids = fields.One2many(comodel_name='vb.card_tran', inverse_name='acct_id', string="Card transactions")
    card_tran_name = fields.Char('Card holder name')
    reason_incomplete = fields.Many2one("vb.common_code","Reason for Incomplete",domain = [('code_type','=', 'ReasonInc')])
    # 08-Feb-2017
    resubmit = fields.Boolean(string='Re-submit signup flag')
    resubmit_date = fields.Datetime(string='Re-submit date/time')
    # 13-Mar-2017
    initial_margin = fields.Float(string='Initial margin',digits=(15,2),default=0)
    maintenance_margin = fields.Float(string='maintenance margin',digits=(15,2),default=0)
    # 15-Mar-2017
    date_kibb_sent = fields.Datetime(string='Date/time sent to KIBB')
    date_kibb_rcvd = fields.Datetime(string='Date/time received from KIBB')
    is_doc_approved = fields.Boolean('is document approved',default=False)
    # 14-Apr-2017
    risk_check_to_approve = fields.Integer(string='Count of risk check requiring approval',default=0)
    flag = fields.Boolean(default=False,compute=get_group)
    is_new_doc = fields.Boolean(default=False,string='Is document new',compute=check_new_doc)
    # margin_ratio = fields.Float(string='Margin ratio',digits=(7,4),default=0)
    # Set to True if need to generate margin call
    margin_call_flag = fields.Boolean(string='Margin call flag')
    last_margin_eval = fields.Date(string='Date of last margin evaluation')
    last_margin_call = fields.Date(string='Date of last margin call')
    # The call count is reset when margin is normalized
    margin_call_count = fields.Float(string='Margin call count',digits=(3,0),default=0)
    finance_period = fields.Float(string='Finance period',digits=(3,0),default=0)
    offer_date = fields.Date(string='Letter of offer date')
    facility_date = fields.Date(string='Facility agreement date')
    facility_requested = fields.Float(string='Facility requested',digits=(15,2),default=0)
    facility_expiry_date = fields.Date(string='Facility expiry date')
    facility_guarantee = fields.Text(string='Facility guarantee')
    financier_id = fields.Many2one(comodel_name='vb.bankacct', string='Financing bank',ondelete='restrict',index=True)
    financing_limit = fields.Float(string='Financing limit',digits=(15,2),default=0)
    # 15-Aug-2017 - additional fields for V2.0
    cash_2pledge_amt = fields.Float(string='Cash amt to pledge',digits=(15,2),default=0)
    asset_2pledge_amt = fields.Float(string='Asset amt to pledge',digits=(15,2),default=0)
    facility_approved_amt = fields.Float(string='Facility approved amt',digits=(15,2),default=0)
    facility_approved_by = fields.Many2one('res.users',string='Facility approved by')
    facility_approved_date = fields.Datetime('Facility approved date')
    facility_letter_flag = fields.Boolean('Facility letter generate flag')
    facility_letter_date = fields.Datetime('Facility letter generated date')
    facility_accepted = fields.Boolean('Facility accepted')
    facility_accepted_date = fields.Datetime('Facility accepted date')
    facility_letter_uploaded = fields.Boolean('Signed facility letter uploaded')
    facility_letter_upload_date = fields.Datetime('Signed facility letter uploaded date')
    facility_letter_upload_id = fields.Many2one(comodel_name='vb.document', string='Signed facility letter',ondelete='set null',index=True)
    facility_approved_comments = fields.Text('Facility approved comments')
    facility_unutilised_amt = fields.Float(string='Facility unutilised facility amt',digits=(15,2),default=0)
    offer_letter_flag = fields.Boolean('Offer letter and agreement generate flag')
    offer_letter_date = fields.Datetime('Offer letter generated date')
    offer_letter_accepted = fields.Boolean('Offer letter accepted')
    offer_letter_accepted_date = fields.Datetime('Offer letter accept date')
    offer_letter_reminder_count = fields.Integer('Offer letter reminder count')
    offer_letter_reminder_date = fields.Datetime('Offer letter last reminder date')
    offer_letter_uploaded = fields.Boolean('Signed offer letter uploaded')
    offer_letter_upload_date = fields.Datetime('Signed offer letter uploaded date')
    offer_letter_upload_id = fields.Many2one(comodel_name='vb.document', string='Signed offer letter',ondelete='set null',index=True)
    offer_letter_comments = fields.Text('Offer letter comments')
    offer_agreement_accepted = fields.Boolean('Margin agreement accepted')
    offer_agreement_accepted_date = fields.Datetime('Margin agreement accept date')
    offer_agreement_uploaded = fields.Boolean('Signed margin agreement uploaded')
    offer_agreement_upload_date = fields.Datetime('Signed margin agreement uploaded date')
    offer_agreement_upload_id = fields.Many2one(comodel_name='vb.document', string='Signed offer agreement',ondelete='set null',index=True)
    offer_agreement_comments = fields.Text('Offer agreement comments')
    rel_acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Related cash account',ondelete='set null',index=True)
    auto_crt_cash = fields.Boolean(string='Auto create cash account if not found')
    pledge_ids = fields.One2many(comodel_name='vb.customer_acct_pledge', inverse_name='acct_id', string='Assets to pledged')
    list_seq = fields.Integer(string='List order')
    incomplete_by = fields.Many2one('res.users',string="Incomplete email sent by")
    followed_by = fields.Many2one(comodel_name='res.users',string='Followed by')
    # 10-Sep-2017
    extra_info_done = fields.Boolean(string='Extra information completed',default=False)
    extra_info_done_date = fields.Datetime(string='Extra information completed on')
    # 20-Sep-2017
    ramci_contra1 = fields.Boolean('When the first button has been pressed',default=False)
    ramci_contra2 = fields.Boolean('When the second button has been pressed',default=False)
    is_cedd_done = fields.Boolean('to avoid forgetting about CEDD',default=False)
    # 12-Oct-2017
    buy_suspended = fields.Boolean('Buy suspended',default=False)
    date_buy_suspended = fields.Datetime(string='Date/time buy suspended')
    reason_id_buy_suspended = fields.Many2one(comodel_name='vb.common_code',string='Reason for buy suspension',ondelete='restrict',domain="[('code_type','=','ReasonSus')]")
    buy_suspended_by = fields.Many2one(comodel_name='res.users',string='Buy suspended by')
    date_buy_suspended_lifted = fields.Datetime(string='Date/time buy suspension lifted')
    buy_suspended_lifted_by = fields.Many2one(comodel_name='res.users',string='Buy suspension lifted by')
    sell_suspended = fields.Boolean('Sell suspended',default=False)
    date_sell_suspended = fields.Datetime(string='Date/time sell suspended')
    reason_id_sell_suspended = fields.Many2one(comodel_name='vb.common_code',string='Reason for sell suspension',ondelete='restrict',domain="[('code_type','=','ReasonSus')]")
    sell_suspended_by = fields.Many2one(comodel_name='res.users',string='Sell suspended by')
    date_sell_suspended_lifted = fields.Datetime(string='Date/time sell suspension lifted')
    sell_suspended_lifted_by = fields.Many2one(comodel_name='res.users',string='Sell suspension lifted by')
    # Deprecated as of 25-Oct-2017 (DT) - Moved to vb_customer
    crs_ids = fields.One2many(comodel_name='vb.customer_acct_crs', inverse_name='acct_id', string='List of Tax ID numbers')
    crs_tax_status = fields.Selection([
        ('Local', 'Malaysia tax resident'),
        ('LocalForeign', 'Msia and non-Malaysia tax resident'),
        ('Foreign', 'Non-Malaysia tax resident'),
        ], string='CRS tax status', default='Local')
    nrvb_clicked_flag = fields.Boolean('Clicked NRVB/KYC',default=False)
    iriss_clicked_flag = fields.Boolean('Clicked IRISS',default=False)
    show_nrvb = fields.Boolean('Show NRVB',default=False)
#     hide_incomplete = fields.Boolean('Hide incomplete button',default=False,compute=hide_incomplete_button)
    cardholder_name = fields.Char('Card holder name')
    #Added by nandha (Dt:23-11-2017)
    potential_loss_thld_pct = fields.Float(string='Threshold for potential loss',digits=(15,2))
    approval_limit_thld_pct = fields.Float(string='Threshold for approval limit',digits=(15,2))
    pep_failed = fields.Boolean('Pep failed',default=False)
    requested_document_by = fields.Many2one(comodel_name='res.users',string='Requested by')
    is_cedd_reviewed = fields.Boolean('Cedd reviewed',help='This is for dashboard Senior managment (high risk)')
    is_staff = fields.Selection(related='customer_id.is_staff',default='No')
    employer_name = fields.Char(related='customer_id.jobs_ids.employer',limit=1,readonly=True)
    who_using = fields.Many2one(comodel_name='res.users',string='Who Using')
    # SQL constraint
    _sql_constraints = [('unique_acct_no','UNIQUE(acct_no,active)','Another account with the same account number already exist'),
                        ('unique_cds_no','UNIQUE(cds_no,active)','Another account with the same CDS number already exist')]

# Model for CUSTOMER ACCOUNT BAL table
# Each customer account will have zero or one account balance.
# The fields in this table originally came from vb.customer_acct but is separated to to anticipated heavy
# updates in these fields.

class customer_acct_bal(models.Model):
    _name = 'vb.customer_acct_bal'
    _description = 'Customer account balances'
    _rec_name = 'acct_name'

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    acct_no = fields.Char(size=20,string='Account number',related='acct_id.acct_no',readonly=True)
    acct_name = fields.Char(size=100,string='Account name',related='acct_id.name',readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    trade_limit = fields.Float(string='Trading limit',digits=(15,2),default=0)
    margin_limit = fields.Float(string='Available margin',digits=(15,2),default=0)
    margin_breached_amt = fields.Float(string='Margin breached amount',digits=(15,2),default=0)
    # Running balances
    order_buy_amt = fields.Float(string='Buy order amt',digits=(15,2),default=0)
    order_sell_amt = fields.Float(string='Sell order amt',digits=(15,2),default=0)
    matched_buy_amt = fields.Float(string='Matched buy order amt',digits=(15,2),default=0)
    matched_sell_amt = fields.Float(string='Matched sell order amt',digits=(15,2),default=0)
    uplifted_order_buy_amt = fields.Float(string='Uplifted buy order amt (for limit computation)',digits=(15,2),default=0)
    uplifted_order_sell_amt = fields.Float(string='Uplifted sell order amt (for limit computation)',digits=(15,2),default=0)
    uplifted_matched_buy_amt = fields.Float(string='Uplifted matched order amt (for limit computation)',digits=(15,2),default=0)
    uplifted_matched_sell_amt = fields.Float(string='Uplifted matched order amt (for limit computation)',digits=(15,2),default=0)
    trade_os_long_amt = fields.Float(string='Outstanding long position',digits=(15,2),default=0)
    trade_os_short_amt = fields.Float(string='Outstanding short position',digits=(15,2),default=0)
    trade_os_others_amt = fields.Float(string='Trade other outstanding',digits=(15,2),default=0)
    trade_os_long_loc = fields.Float(string='Outstanding long position (local)',digits=(15,2),default=0)
    trade_os_short_loc = fields.Float(string='Outstanding short position (local)',digits=(15,2),default=0)
    trade_os_others_loc = fields.Float(string='Trade other outstanding (local)',digits=(15,2),default=0)
    unrlsd_int_amt = fields.Float(string='Unrealised interest amt',digits=(15,2),default=0)
    unrlsd_int_loc = fields.Float(string='Unrealised local interest amt',digits=(15,2),default=0)
    cash_ledger_bal = fields.Float(string='Cash ledger balance',digits=(15,2),default=0)
    cash_available_bal = fields.Float(string='Cash available balance',digits=(15,2),default=0)
    cash_earmarked_amt = fields.Float(string='Cash earmarked amount',digits=(15,2),default=0)
    cash_collateral_amt = fields.Float(string='Cash collateral amount',digits=(15,2),default=0)
    cash_ledger_loc = fields.Float(string='Cash ledger loc balance',digits=(15,2),default=0)
    cash_available_loc = fields.Float(string='Cash available loc balance',digits=(15,2),default=0)
    cash_collateral_loc = fields.Float(string='Cash collateral loc amount',digits=(15,2),default=0)
    cash_earmarked_loc = fields.Float(string='Cash earmarked loc amount',digits=(15,2),default=0)
    portfolio_val = fields.Float(string='Portfolio holding value',digits=(15,2),default=0)
    portfolio_cost = fields.Float(string='Portfolio holding cost',digits=(15,2),default=0)
    portfolio_loc = fields.Float(string='Portfolio holding loc value',digits=(15,2),default=0)
    portfolio_loc_cost = fields.Float(string='Portfolio holding loc cost',digits=(15,2),default=0)
    portfolio_earmarked_amt = fields.Float(string='Portfolio earmarked amount',digits=(15,2),default=0)
    portfolio_earmarked_loc = fields.Float(string='Portfolio earmarked loc amount',digits=(15,2),default=0)
    portfolio_unrlsd_pl_amt = fields.Float(string='Portfolio unrealised profit/loss',digits=(15,2),default=0)
    portfolio_rlsd_pl_amt = fields.Float(string='Portfolio realised profit/loss',digits=(15,2),default=0)
    portfolio_unrlsd_pl_loc = fields.Float(string='Portfolio unrealised loc profit/loss',digits=(15,2),default=0)
    portfolio_rlsd_pl_loc = fields.Float(string='Portfolio realised loc profit/loss',digits=(15,2),default=0)
    # 21-Sep-2016 DT
    withdraw_limit = fields.Float(string='Withdraw limit',digits=(15,2),default=0)
    last_activity_date = fields.Datetime(string='Last activity date')
    trade_limit_asat_date = fields.Datetime(string='Trade limit as at date')
    withdraw_limit_asat_date = fields.Datetime(string='Withdrawal limit as at date')
    cash_pending_in = fields.Float(string='Cash pending in',digits=(15,2),default=0)
    cash_pending_out = fields.Float(string='Cash pending out',digits=(15,2),default=0)
    trade_pending_in = fields.Float(string='Settlement pending in',digits=(15,2),default=0)
    trade_pending_out = fields.Float(string='Settlement pending out',digits=(15,2),default=0)
    # 17-Oct-2016 DT
    trade_closing_amt = fields.Float(string='Trade closing balance',digits=(15,2),default=0)
    cash_ledger_closing_amt = fields.Float(string='Cash ledger closing balance',digits=(15,2),default=0)
    cash_earmark_closing_amt = fields.Float(string='Cash earmark closing balance',digits=(15,2),default=0)
    cash_collateral_closing_amt = fields.Float(string='Cash callateral closing balance',digits=(15,2),default=0)
    portfolio_closing_amt = fields.Float(string='Portfolio closing balance',digits=(15,2),default=0)
    last_closing_date = fields.Date(string='Last closing date')
    # Cummulative year-to-date amounts
    ytd_order_buy_amt = fields.Float(string='YTD Buy order amount',digits=(15,2),default=0)
    ytd_order_sell_amt = fields.Float(string='YTD Sell order amount',digits=(15,2),default=0)
    ytd_matched_buy_amt = fields.Float(string='YTD matched buy order amount',digits=(15,2),default=0)
    ytd_matched_sell_amt = fields.Float(string='YTD matched sell order amount',digits=(15,2),default=0)
    ytd_trade_buy_tran_amt = fields.Float(string='YTD trade buy amount',digits=(15,2),default=0)
    ytd_trade_sell_tran_amt = fields.Float(string='YTD trade sell amount',digits=(15,2),default=0)
    ytd_trade_contra_tran_amt = fields.Float(string='YTD trade contra amount',digits=(15,2),default=0)
    ytd_trade_brkg_fee = fields.Float(string='YTD brokerage fees',digits=(15,2),default=0)
    ytd_trade_other_fee = fields.Float(string='YTD other trade fees',digits=(15,2),default=0)
    ytd_trade_charges_amt = fields.Float(string='YTD trade misc charges amount',digits=(15,2),default=0)
    ytd_trade_interest_amt = fields.Float(string='YTD trade interest amount',digits=(15,2),default=0)
    ytd_trade_settle_amt = fields.Float(string='YTD trade settlement amount',digits=(15,2),default=0)
    ytd_cash_deposit_amt = fields.Float(string='YTD cash deposit amount',digits=(15,2),default=0)
    ytd_cash_withdraw_amt = fields.Float(string='YTD cash withdrawal amount',digits=(15,2),default=0)
    ytd_cash_charges_amt = fields.Float(string='YTD cash misc charges amount',digits=(15,2),default=0)
    ytd_cash_interest_amt = fields.Float(string='YTD cash interest amount',digits=(15,2),default=0)
    ytd_cash_transfer_amt = fields.Float(string='YTD cash transfer amount',digits=(15,2),default=0)
    # 10-Nov-2016
    cash_unrlsd_int_amt = fields.Float(string='Unrealised cash interest amt',digits=(15,2),default=0)
    cash_unrlsd_int_loc = fields.Float(string='Unrealised cash local interest amt',digits=(15,2),default=0)
    cash_last_int_date = fields.Date(string='Last cash interest date')
    last_int_date = fields.Date(string='Last trade interest date')
    # 05-Dec-2016
    portfolio_costx = fields.Float(string='Portfolio holding cost (exc fees)',digits=(15,2),default=0)
    portfolio_closing_amtx = fields.Float(string='Portfolio closing balance (exc fees)',digits=(15,2),default=0)
    trade_limit_closing = fields.Float(string='Trading limit closing',digits=(15,2),default=0)
    margin_limit_closing = fields.Float(string='Margin limit closing',digits=(15,2),default=0)
    withdraw_limit_closing = fields.Float(string='Withdraw limit closing',digits=(15,2),default=0)
    trade_mth_closing_amt = fields.Float(string='Trade closing balance last mth',digits=(15,2),default=0)
    cash_ledger_mth_closing_amt = fields.Float(string='Cash ledger closing balance last mth',digits=(15,2),default=0)
    cash_earmark_mth_closing_amt = fields.Float(string='Cash earmark closing balance last mth',digits=(15,2),default=0)
    cash_collateral_mth_closing_amt = fields.Float(string='Cash callateral closing balance last mth',digits=(15,2),default=0)
    portfolio_mth_closing_amt = fields.Float(string='Portfolio closing balance last mth',digits=(15,2),default=0)
    portfolio_mth_closing_amtx = fields.Float(string='Portfolio closing balance last mth (exc fees)',digits=(15,2),default=0)
    last_mth_closing_date = fields.Date(string='Closing date last mth')
    trade_limit_mth_closing = fields.Float(string='Trading limit last mth',digits=(15,2),default=0)
    margin_limit_mth_closing = fields.Float(string='Margin limit last mth',digits=(15,2),default=0)
    withdraw_limit_mth_closing = fields.Float(string='Withdraw limit last mth',digits=(15,2),default=0)
    trade_limit_factor = fields.Float(string='Trade limit factor',digits=(5,3),default=0)
    portfolio_mgn_amt = fields.Float(string='Portfolio marginable value',digits=(15,2),default=0)
    portfolio_mgn_closing = fields.Float(string='Portfolio marginable closing',digits=(15,2),default=0)
    portfolio_mgn_mth_closing = fields.Float(string='Portfolio marginable closing last mth',digits=(15,2),default=0)
    # 19-Jan-2016
    national_id_no = fields.Char(string='Primary ID number',related='acct_id.customer_id.national_id_no',readonly=True)
    other_id_no = fields.Char(string='Other ID number',related='acct_id.customer_id.other_id_no',readonly=True)
    cds_no = fields.Char(size=20,string='CDS number',related='acct_id.cds_no',readonly=True)
    other_acct_no = fields.Char(size=20,string='KIBB acct number',related='acct_id.other_acct_no',readonly=True)
    # 13-Mar-2017
    initial_margin = fields.Float(string='Initial margin',digits=(15,2),default=0)
    maintenance_margin = fields.Float(string='maintenance margin',digits=(15,2),default=0)
    margin_usage = fields.Float(string='Margin usage %',digits=(7,4),default=0)

    # 15-Aug-2017 - additional fields for V2.0
    product_type = fields.Char(string='Product type')
    acct_class_id = fields.Integer(string='Product ID')
    margin_ratio = fields.Float(string='Margin ratio',digits=(7,4),default=0)
    margin_ratio_uncapped = fields.Float(string='Margin ratio (uncapped)',digits=(7,4),default=0)
    cash_multiplier = fields.Float(string='Cash multiplier',digits=(7,4),default=0)
    cash_margin_amt = fields.Float(string='Cash margin amount',digits=(15,2),default=0)
    cash_margin_loc = fields.Float(string='Cash margin local amount',digits=(15,2),default=0)
    portfolio_val_capped = fields.Float(string='Portfolio holding value (capped)',digits=(15,2),default=0)
    portfolio_loc_capped = fields.Float(string='Portfolio holding loc value (capped)',digits=(15,2),default=0)
    portfolio_margin_amt = fields.Float(string='Portfolio margin amount',digits=(15,2),default=0)
    portfolio_margin_loc = fields.Float(string='Portfolio margin local amount',digits=(15,2),default=0)
    trade_os_ctra_loss_amt = fields.Float(string='Outstanding contra loss',digits=(15,2),default=0)
    trade_os_ctra_gain_amt = fields.Float(string='Outstanding contra gain',digits=(15,2),default=0)
    trade_os_ctra_loss_loc = fields.Float(string='Outstanding contra loss (local)',digits=(15,2),default=0)
    trade_os_ctra_gain_loc = fields.Float(string='Outstanding contra gain (local)',digits=(15,2),default=0)

    # For margin module
    margin_shortfall_amt = fields.Float(string='Margin shortfall',digits=(15,2),default=0)
    margin_shortfall_loc = fields.Float(string='Margin local shortfall',digits=(15,2),default=0)
    share_topup_amt = fields.Float(string='Share top-up amount',digits=(15,2),default=0)
    share_topup_loc = fields.Float(string='Share top-up local amount',digits=(15,2),default=0)
    cash_topup_amt = fields.Float(string='Cash top-up amount',digits=(15,2),default=0)
    cash_topup_loc = fields.Float(string='Cash top-up local amount',digits=(15,2),default=0)
    forcesell_amt = fields.Float(string='Force-sell amount',digits=(15,2),default=0)
    forcesell_loc = fields.Float(string='Force-sell local amount',digits=(15,2),default=0)

    # 06-Oct-2017 - additional fields for V2.0 contra module
    potential_loss_amt = fields.Float(string='Potential loss',digits=(15,2),default=0)
    cash_pickup_amt = fields.Float(string='Cash pickup amount',digits=(15,2),default=0)
    trade_pickup_amt = fields.Float(string='Trade pickup amount',digits=(15,2),default=0)
    paid_share_amt = fields.Float(string='Paid share value',digits=(15,2),default=0)
    eod_coll_amt = fields.Float(string='EOD collateral value',digits=(15,2),default=0)
    util_coll_amt1 = fields.Float(string='Utilised collateral value (orders)',digits=(15,2),default=0)
    util_coll_amt2 = fields.Float(string='Utilised collateral value (trades)',digits=(15,2),default=0)
    util_coll_amt2a = fields.Float(string='Utilised collateral value (trades2)',digits=(15,2),default=0)
    net_coll_amt = fields.Float(string='Net collateral value',digits=(15,2),default=0)
    potential_loss_amt2 = fields.Float(string='Potential loss (for pickup)',digits=(15,2),default=0)
    trade_os_dbsale_amt = fields.Float(string='Outstanding debit sales amt',digits=(15,2),default=0)


# Model for ACCOUNT RISK CHECKLIST table
# This table will provide risk related parameters and rules to be applied for customer accounts with this profile.
# Targeted number of records: Small - Around 10-20 records

class customer_acct_riskcheck(models.Model):
    _name = 'vb.customer_acct_riskcheck'
    _description = 'Customer account risk check'
    _order = 'acct_id,checked_date,riskcheck_id'
    _rec_name = 'name'

    @api.constrains('state')
    def check_approver(self):
        self.approver1 = self.riskcheck_id.approver1.id
        self.approver2 = self.riskcheck_id.approver2.id
        self.approver3 = self.riskcheck_id.approver3.id
        self.approver4 = self.riskcheck_id.approver4.id
    @api.multi
    def write(self,vals):
        if vals.get('state'):
            if self.approver1 or self.approver2:
                if int(self.approver1) != self._uid:
                    if int(self.approver2) != self._uid:
                        if int(self.approver3) != self._uid:
                            if int(self.approver4) != self._uid:
                                raise ValidationError("You can not edit riskcheck name "+str(self.name))
            vals['checked_date']=datetime.now()
        rec = self.env['vb.customer_acct_riskcheck'].search([('acct_id','=',self.acct_id.id)])
        failed = 0
        new = 0
        for i in rec:
            if i.state=='Failed':
                if i.code == 'Kyc02':
                    failed+=1

            elif i.state == "New":
                new += 1
                break
        if failed >0 and new ==0 :
            query = "update vb_customer_acct set pep_failed='t' where id=%s"%self.acct_id.id
            self._cr.execute(query)
        else:
            query = "update vb_customer_acct set pep_failed='f' where id=%s"%self.acct_id.id
            self._cr.execute(query)

        return super(customer_acct_riskcheck,self).write(vals)

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    riskcheck_id = fields.Many2one(comodel_name='vb.risk_profile_check', string='Risk profile checking',ondelete='set null')
    name = fields.Char(size=100,string='Risk check name')
    checked_by = fields.Many2one(comodel_name='res.users',string='Checked by')
    state = fields.Selection([
        ('New', 'New'),
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('ByPass', 'Bypass'),
       ], string='Status', default='New')
    # 05-Dec-2016 DT
    checked_date = fields.Datetime(string='Date/time checked')
    checked_ok = fields.Boolean(string='Checked OK')
    checked_bypass = fields.Boolean(string='Bypassed')
    checked_failed = fields.Boolean(string='Failed')
    result_obtained = fields.Integer('Result obtained',default=0)
    result_max = fields.Integer('Maximum result required',default=0,help='If beyond this max, then risk check should fail')
    result_min = fields.Integer('Minimum result required',default=0,help='If below this min, then risk check should fail')
    bypassed_by = fields.Many2one(comodel_name='res.users',string='Bypassed by')
    bypassed_date = fields.Datetime(string='Date/time pypassed')
    code = fields.Char(size=20,string='Risk check code')
    # deprecated 05-Dec-2016
    when_first_checked = fields.Datetime(string='Date/time first checked')
    when_last_checked = fields.Datetime(string='Date/time last checked')
    is_failed_approv = fields.Boolean(default=False)
    # 16-Feb-2017
    approver1 = fields.Char(string='Approver #1')
    approver2 = fields.Char(string='Approver #2')
    approver3 = fields.Char(string='Approver #3')
    approver4 = fields.Char(string='Approver #4')
    approver5 = fields.Char(string='Approver #5')
    list_seq = fields.Char(string='list sequence')


# Model for ACCOUNT QUESTIONAIRE RESPONSE table
# This table will provide a risk questionaires for a risk profile.  This questionaire will be copied to the customer
# account to store the actual customer response.

class customer_acct_questionaire(models.Model):
    _name = 'vb.customer_acct_questionaire'
    _description = 'Response to questionaire'
    _order = 'acct_id,set,section,question_no'

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    question_id = fields.Many2one(comodel_name='vb.risk_profile_question', string='Questionaire',ondelete='cascade')
    name = fields.Char(size=100,string='Description',related='question_id.name',readonly=True,store=True)
    set = fields.Char(string='Set',related='question_id.set',readonly=True,store=True)
    section = fields.Char(string='Section',related='question_id.section',readonly=True,store=True)
    question_no = fields.Integer(string='Question no',related='question_id.question_no',readonly=True,store=True)
    question_text = fields.Text(string='Question',related='question_id.question_text',readonly=True,store=True)
    response = fields.Char(string='Response to question',help="This is the customer response to the question")
    required = fields.Selection([
        ('Yes', 'Yes'),
        ('Conditional', 'Conditional'),
        ], string='Required response')
    desired_response = fields.Char(string='Desired response',related='question_id.desired_response',readonly=True,store=True)
    scoring = fields.Integer(string='Scoring')
    state = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('None', 'No response'),
       ], string='Status')
    rel_quest_id = fields.Many2one(comodel_name='vb.customer_acct_questionaire', string='Related questionaire',ondelete='set null',index=True)
    # 29-Sep-2017
    active = fields.Boolean(string='Active',default=True)

class customer_acct_cedd(models.Model):
    _name = 'vb.customer_acct_cedd'
    _description = 'Customer account CDD/EDD response'
    _order = 'acct_id'

    @api.multi
    def name_get(self):
        result=[]
        for rec in self:
            result.append((rec.id,u"%s" % (rec.acct_id.name)))
        return result

    @api.multi
    @api.onchange('approve_cdd','reject_cdd','approve_edd','reject_edd')
    def get_edd(self):
        #variable
        failed = 0
        checklist_count = 0
        passed_count = 0
        query = """Update vb_customer_acct_cedd set q01=%s,q02=%s,q03=%s,q04=%s,q05=%s,q06a=%s,q06b=%s,q06c=%s,
        q07=%s,q08=%s,q09=%s,q10=%s,q11=%s,q12=%s,q13=%s,q14=%s,cdd_checked_date=%s,cdd_checked_by=%s,show_edd=%s,
        q31a=%s,q31b=%s,q32=%s,q33=%s,q34a=%s,q34b=%s,q35a=%s,q35b=%s,q36=%s,q37=%s,edd_checked_date=%s,edd_checked_by=%s,state=%s
        where acct_id=%s"""
        if self.show_edd == True:
            self.update({'cdd_checked_date':datetime.now(),'cdd_checked_by':self._uid,'edd_checked_date':datetime.now(),'edd_checked_by':self._uid})
            self._cr.execute(query,(self.q01 or u"",self.q02 or u"",self.q03 or u"",self.q04 or u"",self.q05 or u"",self.q06a or u"",
                                self.q06b or u"",self.q06c or u"",self.q07 or u"",self.q08 or u"",self.q09 or u"",self.q10 or u"",
                                self.q11 or u"",self.q12 or u"",self.q13 or u"",self.q14 or u"",str(datetime.now()),
                                self._uid,self.show_edd or u"",self.q31a or u"",self.q31b or u"",self.q32 or u"",self.q33 or u"",
                                self.q34a or u"",self.q34b or u"",self.q35a or u"",self.q35b or u"",self.q36 or u"",self.q37 or u"",
                                str(datetime.now()),self._uid,self.state,self.acct_id.id))
            if self.approve_edd or self.approve_cdd:
                query3 = "Update vb_customer_acct set is_cedd_done='t' where id=%s"%self.acct_id.id
                self._cr.execute(query3)
#                 query_cedd = "update vb_customer_acct set is_cedd_done='t' where id=%s"%self.acct_id.id
#                 self._cr.execute(query_cedd)
            for risk_rec in self.acct_id.riskcheck_ids:
                if (risk_rec.state == 'Failed') and (risk_rec.approver1 or risk_rec.approver2 or risk_rec.approver3 or risk_rec.approver4):
                    risk_rec.update({'is_failed_approv': True})
                else:
                    risk_rec.update({'is_failed_approv': False})
                checklist_count = checklist_count + 1
                if risk_rec.state in ['OK', 'ByPass']:
                    passed_count = passed_count + 1
                if risk_rec.state == 'Failed' :
                    if risk_rec.code == 'Kyc02':
                        passed_count = passed_count + 1
                    else:
                        failed = failed + 1
                if failed > 0:
                    acct_query = "Update vb_customer_acct set risk_check_status='Failed' where id=%s"%self.acct_id.id
                    self._cr.execute(acct_query)
                elif passed_count != checklist_count:
                    acct_query = "Update vb_customer_acct set risk_check_status='None' where id=%s"%self.acct_id.id
                    self._cr.execute(acct_query)
                else:
                    if self.approve_cdd == True or self.approve_edd == True:
#                         if bool(self.env['res.users'].has_group('vb_base.vb_group_top_mgmt'))==True:

#                             if self.env['vb.customer_acct'].search([('id','=',self.acct_id.id)]).acct_type == "Cash":
#                                 acct_query = """Update vb_customer_acct set risk_check_status='Passed',state ='Checked',
#                                             approved_by=%s,approved_date=%s where id=%s"""
#                                 self._cr.execute(acct_query,(self._uid,str(date.today()),self.acct_id.id))
#                             else:
#                                 acct_query = """Update vb_customer_acct set risk_check_status='Passed' where id=%s"""%self.acct_id.id
#                                 self._cr.execute(acct_query)
    #                             self.acct_id.update({'risk_check_status': 'Passed',
    #                                              'state':'Checked','approved_by':self._uid,'approved_date':date.today()})
#                         else:
                        acct_query = "Update vb_customer_acct set risk_check_status='Passed',flag='f' where id=%s"%self.acct_id.id
                        self._cr.execute(acct_query)
        else:
            if (bool(self.q01) and bool(self.q02) and bool(self.q03) and bool(self.q04) and bool(self.q05) and bool(self.q06a) and bool(self.q06b) and bool(self.q06c) and bool(self.q07) and bool(self.q08) and bool(self.q09) and bool(self.q10) and bool(self.q11) and bool(self.q12))!=False:
                query1 = """Update vb_customer_acct_cedd set q01=%s,q02=%s,q03=%s,q04=%s,q05=%s,q06a=%s,q06b=%s,q06c=%s,
                        q07=%s,q08=%s,q09=%s,q10=%s,q11=%s,q12=%s,q13=%s,cdd_checked_date=%s,cdd_checked_by=%s,state=%s,approve_cdd=%s where acct_id=%s"""
                self._cr.execute(query1,(self.q01 or u"",self.q02 or u"",self.q03 or u"",self.q04 or u"",self.q05 or u"",self.q06a or u"",
                                    self.q06b or u"",self.q06c or u"",self.q07 or u"",self.q08 or u"",self.q09 or u"",self.q10 or u"",
                                    self.q11 or u"",self.q12 or u"",self.q13 or u"",str(datetime.now()),
                                    self._uid,self.state,self.approve_cdd,self.acct_id.id))
                if self.approve_cdd or self.approve_edd:
                    query4 = "Update vb_customer_acct set is_cedd_done='t' where id=%s"%self.acct_id.id
                    self._cr.execute(query4)
#                     if bool(self.env['res.users'].has_group('vb_base.vb_group_top_mgmt'))==True:
#                         if self.env['vb.customer_acct'].search([('id','=',self.acct_id.id)]).acct_type != "Cash":
#                             if self.env['vb.customer_acct'].search([('id','=',self.acct_id.id)]).facility_approved_amt > 0:
#                                 acct_query = """Update vb_customer_acct set risk_check_status='Passed',state ='Checked',
#                                                 approved_by=%s,approved_date=%s where id=%s"""
#                                 self._cr.execute(acct_query,(self._uid,str(date.today()),self.acct_id.id))
#                             else:
#                                 acct_query = """Update vb_customer_acct set risk_check_status='Passed' where id=%s"""%self.acct_id.id
#                                 self._cr.execute(acct_query)
#
#                         #in case it is cash
#                         else:
#                             acct_query = """Update vb_customer_acct set risk_check_status='Passed',state ='Checked',
#                                             approved_by=%s,approved_date=%s where id=%s"""
#                             self._cr.execute(acct_query,(self._uid,str(date.today()),self.acct_id.id))
#                     else:
                    self._cr.execute("Update vb_customer_acct set risk_check_status='Passed',flag='f' where id=%s"%self.acct_id.id)
#                 self.update({'cdd_checked_date':datetime.now(),'cdd_checked_by':self._uid})

#         else:
#             self.update({'cdd_checked_date':False,'cdd_checked_by':False})
    @api.onchange('q01','q02','q03','q04','q05','q06a','q06b','q06c','q07',
                  'q08','q09','q10','q11','q12','q31a','q31b','q32','q33',
                  'q34a','q34b','q35a','q35b','q36','q37','q14')
    def all_required(self):
        if (bool(self.q01) and bool(self.q02) and bool(self.q03) and bool(self.q04) and bool(self.q05) and bool(self.q06a) and bool(self.q06b) and bool(self.q06c) and bool(self.q07) and bool(self.q08) and bool(self.q09) and bool(self.q10) and bool(self.q11) and bool(self.q12))==False:
            self.update({'savable':False})
        else:
            if self.acct_id.acct_type == 'Cash' and  self.acct_id.rel_acct_id.id == False and self.acct_id.customer_id.state != 'Basic' and len(self.env['vb.customer_acct'].search([('customer_id','=',self.acct_id.customer_id.id)]))>1:
                if bool(self.q06a == 'No' and self.q06b == 'No' and self.q06c == 'No'):
                    if bool(self.q07 == 'Yes' or self.q08 == 'Yes' or self.q09 == 'Yes' or self.q10 == 'Yes' or self.q11 == 'Yes' or self.q12 == 'Yes'):
                        self.update({'savable':False,'show_edd':True,'q13':'High'})
                    else:
                        self.update({'savable':True,'show_edd':False,'q13':'Low'})
                else:
                    if bool(self.q07 == 'Yes' or self.q08 == 'Yes' or self.q09 == 'Yes' or self.q10 == 'Yes' or self.q11 == 'Yes' or self.q12 == 'Yes'):
                        self.update({'savable':False,'show_edd':True,'q13':'High'})
                    else:
                        self.update({'savable':True,'show_edd':False,'q13':'Low'})
            else:
                if bool(self.q06a == 'No' and self.q06b == 'No' and self.q06c == 'No'):
                    self.update({'savable':False,'show_edd':True,'q13':'High'})
                else:
                    if bool(self.q07 == 'Yes' or self.q08 == 'Yes' or self.q09 == 'Yes' or self.q10 == 'Yes' or self.q11 == 'Yes' or self.q12 == 'Yes'):
                        self.update({'savable':False,'show_edd':True,'q13':'High'})
                    else:
                        self.update({'savable':True,'show_edd':False,'q13':'Low'})

        if bool(self.q31a) and bool(self.q31b) and bool(self.q32) and bool(self.q33) and bool(self.q34a)and bool(self.q35a)and bool(self.q36):
            self.update({'savable':True})

#         else:
#             if self.approve_reject_cdd != 'ApproveCDD':
#                 if self.approve_reject_edd != 'ApproveEDD':
#                     if self.approve_reject_edd == 'New':
#                         if self.approve_reject_cdd == 'New':
#                             self.update({'state':'New','show_edd':True})
#                         else:
#                            self.update({'state':'CDDRejected','show_edd':True})
#                     else:
#                         self.update({'state':'EDDRejected','show_edd':True})
#                 else:
#                     self.update({'state':'EDDApproved','show_edd':True})
#             else:
#                 self.update({'state':'CDDApproved','show_edd':True})
#             if bool(self.q31a) and bool(self.q31b) and bool(self.q32) and bool(self.q33) and bool(self.q34a)and bool(self.q35a)and bool(self.q36):
#                 if self.q34a == 'Others':
#                     if bool(self.q34b) == False:
#                         self.update({'savable':False})
#                     else:
#                         if self.q35a=='Others':
#                             if bool(self.q35b) == False:
#                                 self.update({'savable':False})
#                             else:
#                                 self.update({'savable':True})
#                         else:
#                             self.update({'savable':True})
#
#                 if self.q35a=='Others':
#                     if bool(self.q35b) == False:
#                         self.update({'savable':False})
#                     else:
#                         if self.q34a == 'Others':
#                             if bool(self.q34b) == False:
#                                 self.update({'savable':False})
#                             else:
#                                 self.update({'savable':True})
#                         else:
#                             self.update({'savable':True})
#                 if self.q34a != 'Others' and self.q35a != 'Others':
#                     self.update({'savable':True})

    @api.onchange('approve_cdd','approve_edd','reject_edd','reject_cdd')
    def get_edd_sign(self):

        if self.approve_cdd:
            query= "Update vb_customer_acct_cedd set state='CDDApproved',approve_cdd='t',reject_cdd='f',reject_edd='f',approve_edd='f' where acct_id=%s"%self.acct_id.id
            self._cr.execute(query)
            self.update({'is_pressed':True})
        else:
            if self.approve_edd:
                query= "Update vb_customer_acct_cedd set state='EDDApproved',approve_cdd='f',reject_cdd='f',reject_edd='f',approve_edd='t' where acct_id=%s"%self.acct_id.id
                self._cr.execute(query)
                self.update({'is_pressed':True})
            else:
                if self.reject_cdd:
                    self.update({'state':'CDDRejected','show_edd':True,'reject_edd':False,'approve_cdd':False,'reject_edd':False,'reject_cdd':True})
                    query= "Update vb_customer_acct_cedd set state='CDDRejected',approve_cdd='f',reject_cdd='t',reject_edd='f',approve_edd='f',show_edd='t' where acct_id=%s"%self.acct_id.id
                    self._cr.execute(query)
                    self.acct_id.update({'is_cedd_done':False})
                    self.update({'is_pressed':True})
                elif self.reject_edd:
                    query= "Update vb_customer_acct_cedd set state='EDDRejected',approve_cdd='f',reject_cdd='f',reject_edd='t',approve_edd='f',show_edd='t' where acct_id=%s"%self.acct_id.id
                    self._cr.execute(query)
                    self.update({'is_pressed':True})
                    self._cr.execute("Update vb_customer_acct set is_cedd_done='t' where id=%s"%self.acct_id.id)
                    self.acct_id.update({'is_cedd_done':True})

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    q01 = fields.Selection([
        ('New','New'),
        ('Existing','Existing'),
        ], string='Type of customer')
    q02 = fields.Selection([
        ('<50k','< RM 50,000'),
        ('50k-100k','RM50,001 - RM100,000'),
        ('100k-200k','RM101,001 - RM200,000'),
        ('200k-500k','RM201,001 - RM500,000'),
        ('500k-1M','RM500,001 - RM1 mill'),
        ('1M-3M','RM1 mill - RM3 mill'),
        ('>3M','> RM3 mill'),
        ], string='Net worth')
    q03 = fields.Selection([
        ('Website','Website'),
        ('Newspaper','Newspaper'),
        ('SocialMedia','SocialMedia'),
        ('Seminar','Seminar'),
        ('Radio','Radio'),
        ('WordMouth','WordMouth'),
        ('Others','Others'),
        ], string='Sales channel')
    q04 = fields.Selection([
        ('LongTerm','LongTerm (> 5 years)'),
        ('MediumTerm','Medium Term (2-5 years)'),
        ('ShortTerm','Short term (< 2 year)'),
        ], string='Investment objective')
    q05 = fields.Selection([
        ('No','No'),
        ('<3yr','< 3 yr'),
        ('3-7yr','3 - 7 years'),
        ('8-10yr','8 - 10 years'),
        ('>10yr','> 10 years'),
        ], string='Securities trading experience')
    q06a = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Document check: NRIC')
    q06b = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Document check: Payslip/Income tax Return/Employment letter')
    q06c = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Document check: Customer bank statement')
    q07 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Customer is operating in high risk business')
    q08 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Customer is operating in high risk industries')
    q09 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Does the customer have the same correspondence address with any existing customers?')
    q10 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Customer is a PEP or closely associated PEP')
    q11 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Customer residing in/from high risk country')
    q12 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Applicant/beneficiaries name matched to the AML blacklist?')
    q13 = fields.Selection([
        ('Low','Low'),
        ('High','High'),
        ], string='Customer Risk Rating')
    q14 = fields.Char(string='CDD Remarks')
    q15 = fields.Char(string='Question 15 description')
    q16 = fields.Char(string='Question 16 description')
    q17 = fields.Char(string='Question 17 description')
    q18 = fields.Char(string='Question 18 description')
    q19 = fields.Char(string='Question 19 description')
    q20 = fields.Char(string='Question 20 description')
    q30 = fields.Char(string='Question 30 description')

    q31a = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='a) Website confirmation or RAM report')
    q31b = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='b) Paylip/ EA form/ Bank Statement/ Employment Letter')
    q32 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Are there any adverse/negative/remarks/news on the customer?')
    q33 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='Is the customer is able to describe the nature of his business/employment without any difficult?')
    q34a = fields.Selection([
        ('Investment','For Investment'),
        ('Speculation','For Speculation'),
        ('MixedTrading','For Mixed Trading'),
        ('Placement','To facilitate placement if shares'),
        ('Others','Others'),
        ], string='Customer purpose of opening account')
    q34b = fields.Char(string='Other purpose')
    q35a = fields.Selection([
        ('Aggressive','Aggressive'),
        ('Conservative','Conservative'),
        ('Recommended','Buy share recommended by the company'),
        ('Contra','Contra'),
        ('Mixed','Mixed, partically pickup or contra'),
        ('Others','Others'),
        ], string='Level and nature of trading activities:')
    q35b = fields.Char(string='Other nature of trading activity')
    q36 = fields.Selection([
        ('Yes','Yes'),
        ('No','No'),
        ], string='For trust/nominee accounts, have the ultimate beneficial owner(s) been obtained?')
    q37 = fields.Char(string='EDD remarks')
    q38 = fields.Char(string='Question 38 description')
    q39 = fields.Char(string='Question 39 description')
    q40 = fields.Char(string='Question 30 description')
    state = fields.Selection([
        ('New', 'New'),
        ('CDDApproved', 'CDD approved'),
        ('EDDApproved', 'EDD approved'),
        ('CDDRejected', 'CDD rejected'),
        ('EDDRejected', 'EDD rejected'),
        ], string='Status', default='New')
    active = fields.Boolean(string='Active',default=True)
    cdd_checked_date = fields.Datetime(string='Date/time CDD checked')
    cdd_checked_by = fields.Many2one(comodel_name='res.users',string='CDD checked by')
    cdd_rejected_date = fields.Datetime(string='Date/time CDD rejected')
    cdd_rejected_by = fields.Many2one(comodel_name='res.users',string='CDD rejected by')
    edd_check_flag = fields.Boolean(string='EDD check required',default=False)
    edd_checked_date = fields.Datetime(string='Date/time EDD checked')
    edd_checked_by = fields.Many2one(comodel_name='res.users',string='EDD checked by')
    edd_rejected_date = fields.Datetime(string='Date/time EDD rejected')
    edd_rejected_by = fields.Many2one(comodel_name='res.users',string='EDD rejected by')
    show_edd = fields.Boolean('CDD Fail',defalt=False)
    savable = fields.Boolean('Savable',default=False)
    approve_reject_cdd = fields.Selection([
                        ('New','New'),
                        ('ApproveCDD','Approve CDD'),
                        ('RejectCDD','Reject CDD'),
                        ], string='Approving/Rejecting CDD', default='New')
    approve_reject_edd = fields.Selection([
                        ('New','New'),
                        ('ApproveEDD','Approve EDD'),
                        ('RejectEDD','Reject EDD'),
                        ], string='Approving/Rejecting EDD', default='New')
    approve_cdd = fields.Boolean('CDD Pass',default=False)
    reject_cdd = fields.Boolean('CDD Fail',default=False)
    approve_edd = fields.Boolean('EDD Pass',default=False)
    reject_edd = fields.Boolean('EDD Fail',default=False)
    is_pressed = fields.Boolean("Is_pressed",default=False)
    email1 = fields.Char(string='Primary email',related='acct_id.customer_id.email1',store=True,readonly=True)
    customer_name = fields.Char(string='Customer name',related='acct_id.customer_id.name',store=True,readonly=True)
    national_id_no = fields.Char(string='Primary ID number',related='acct_id.customer_id.national_id_no',store=True,readonly=True)
    is_edd_completed = fields.Boolean(string='Complete EDD',default=False)
    is_edd_reviewed = fields.Boolean(string='Reviewed EDD',default=False)


# Model for ACCOUNT ATTRIBUTE table
# This model is to store the additional information about the customer account (those not really affecting processing)

class customer_acct_add_info(models.Model):
    _name = 'vb.customer_acct_add_info'
    _description = 'Customer account additional info'
    _order = 'customer_acct_id,list_seq'
    _rec_name = "customer_acct_id"
    # fields
    attribute_id = fields.Many2one(comodel_name='vb.common_code',string='Info type',ondelete='restrict',domain="[('code_type','=','AcctAttr')]")
    customer_acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    info_value = fields.Char(string='Value')
    info_flag = fields.Boolean(string='Flag')
    info_text = fields.Text(string='Additional text')
    list_seq = fields.Integer(string='List order')

# Model for ACCOUNT PLEDGE table
# This table will provide risk related parameters and rules to be applied for customer accounts with this profile.
# Targeted number of records: Small - Around 10-20 records

class customer_acct_pledge(models.Model):
    _name = 'vb.customer_acct_pledge'
    _description = 'Customer account pledge'
    _order = 'acct_id,id'

    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset',ondelete='restrict',index=True)
    pledge_qty = fields.Float(string='Pledge qty',digits=(9,0),default=0)
    pledge_amt = fields.Float(string='Pledge amt',digits=(15,2),default=0)
    pledge_qty = fields.Float(string='Pledge qty',digits=(9,0),default=0)
    pledged_flag = fields.Boolean(string='Already pledged')
    pledged_qty = fields.Float(string='Pledged qty',digits=(9,0),default=0)
    pledged_date = fields.Datetime(string='Pledged date/time')

# Model for COMPANY BANK ACCOUNT table
# each record in this table references to a bank account where all the customer cash deposits are maintained.
# Primary use of this table is to do reconciliation between the total of the customer cash balance and what's actually maintained
# in the bank account under the company name.
# Constraint: There should not be any duplicate bank account for the same bank_id.
# Targeted number of records: Very small - probably 1 or 2 accounts only

class bankacct(models.Model):
    _name = 'vb.bankacct'
    _description = 'Bank account'
    _order = 'bank_id,bank_acct_no'

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    @api.multi
    def name_get(self):
        super(bankacct, self).name_get()
        result=[]
        for rec in self:
            result.append((rec.id,u"%s (%s)" % (rec.bank_acct_no,rec.name)))
        return result

    # fields
    name = fields.Char(size=100,string='Bank account name')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency',ondelete='restrict')
    bank_id = fields.Many2one(comodel_name='vb.common_code', string='Bank',ondelete='restrict',domain="[('code_type','=','Bank')]")
    bank_branch = fields.Char(string='Bank branch', )
    bank_acct_no = fields.Char(size=20,string='Bank account number',)
    glacct_no = fields.Char(size=20,string='GL account')
    cash_acct_bal = fields.Float(string='Total associated cash account balance',digits=(15,2),default=0)
    bank_acct_bal = fields.Float(string='Bank balance',digits=(15,2),default=0)
    bank_acct_asat = fields.Date(string='Bank balance as at')
    financier = fields.Boolean(string='Financier')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Activated'),
        ('Inactive', 'Inactive'),
        ('Closed', 'Closed'),
        ], string='Status', default='New')

# Model for BANK ACCOUNT TRANSACTION table
# This table stores all the transactions for a particular company bank account.
# The types of transaction stored is defined by the 'tran_type' field.
# Targeted number of records: Very large - probably 200-500 records per account per day

class bankacct_tran(models.Model):
    _name = 'vb.bankacct_tran'
    _description = 'Bank account transaction'
    _rec_name = 'tran_refno'
    _order = 'bank_acct_id,tran_date'

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    # field
    bank_acct_id = fields.Many2one(comodel_name='vb.bankacct', string='Bank account',ondelete='restrict',index=True)
    bank_acct_no = fields.Char(string='Bank account number',related='bank_acct_id.bank_acct_no',readonly=True)
    currency_name = fields.Char(string='Currency name',related='bank_acct_id.currency_id.name',readonly=True)
    tran_refno = fields.Char(size=40,string='Transaction reference')
    other_refno = fields.Char(size=20,string='Other reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    # 'tran_type_id' field only used during data entry to display selection to determine 'tran_type'
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type')
    tran_type = fields.Char(string='Transaction type',related='tran_type_id.code',store=True,readonly=True)
    #    ('Opening', 'Opening'),
    #    ('Deposit', 'Deposit'),
    #    ('Withdraw', 'Withdrawal'),
    #    ('Charge', 'Charge'),
    #    ('Credit', 'Misc Credit'),
    tran_mode_id = fields.Many2one(comodel_name='vb.common_code', string='Payment mode',domain="[('code_type','=','TranMode')]")
    tran_mode = fields.Char(string='Payment mode',related='tran_mode_id.code',store=True,readonly=True)
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    reconciled = fields.Boolean(string='Confirmed')
    # Used only for debit or credit note transactions
    charge_id = fields.Many2one(comodel_name='vb.charge_code',string='Charge code',ondelete='restrict')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Status', default='New')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage', default='New')
    date_reconciled = fields.Datetime(string='Date/time confirmed')
    reconciled_by = fields.Many2one(comodel_name='res.users',string='Confirmed by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    comments = fields.Text(string='Comments')
    print_status = fields.Char(string='Print status')
    print_date = fields.Datetime(string='Date/time printed')
    match_date = fields.Datetime(string='Date/time matched')
    match_status = fields.Char(string='Matched status')

# Model for Credit Card Transactions table
# This table will provide credit card transaction details which are normally associated with account opening

class card_tran(models.Model):
    _name = 'vb.card_tran'
    _description = 'Credit card transaction'
    _rec_name = 'tran_refno'
    _order = 'acct_id,tran_date'

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True)
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Datetime(string='Transaction date',default=lambda self: fields.datetime.now())
    tran_type = fields.Char(string='Transaction type')
    tran_amt = fields.Float(string='Transaction amount',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amount',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction local GST',digits=(15,2),default=0)
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    issuing_bank = fields.Char(string='Issuing bank')
    issuing_country = fields.Char(string='Issuing country')
    issuing_country_id = fields.Many2one(comodel_name='res.country',string='Card issuing country')
    card_number = fields.Char(string='Credit card number')
    card_expiry = fields.Char(string='Card expiry in MM/YY')
    name_on_card = fields.Char(string='Name on card')
    approval_code = fields.Char(string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    currency = fields.Char(string='Currency',related='currency_id.name',store=True,readonly=True)
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency')
    response_code = fields.Char(string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_gateway = fields.Char(string='Payment gateway',help="This is a string representation of the payment gateway address")
    order_refno = fields.Char(string='Order reference')
    state = fields.Selection([
        ('New', 'New'),
        ('Pending', 'Pending'),
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('Posted', 'Posted'),
        ('Cancelled', 'Cancelled'),
        ('Refunded', 'Refunded'),
        ], string='Status')
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    payment_code = fields.Char(string='Payment code')
    payment_mode = fields.Char(string='Payment mode')
    bank_refno = fields.Char(string='Bank reference')
    other_refno = fields.Char(string='Other reference')
    matched_bank = fields.Char(string='Matched with bank')
    matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    matched_amt = fields.Float(string='Matched amount',digits=(15,2),default=0)
    bank_matching_amt = fields.Float(string='Bank matching amount',digits=(15,2),default=0)
    comments = fields.Text(string='Comments')
    print_status = fields.Char(string='Print status')
    print_date = fields.Datetime(string='Date/time printed')
    match_status = fields.Char(string='Matched status')
    cardholder_name = fields.Char(string='Cardholder name')

# Model for Customer Account Activity log table
# This table will provide activity log which are normally associated with account opening but also with suspension, closing, etc
# 05-Sep-2016 DT

class customer_acct_activity(models.Model):
    _name = 'vb.customer_acct_activity'
    _description = 'Activity log for customer account'
    _order = 'acct_id'

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer',string='Customer',ondelete='cascade',store=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    name = fields.Char(size=100,string='Activity description')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='set null',index=True)
    activity_type_id = fields.Many2one(comodel_name='vb.common_code', string='Activity type',domain="[('code_type','=','ActivityType')]")
    activity_type = fields.Char(string='Activity type')
    # Signup1
    # Signup2
    # Signup2a
    # Signup3
    # IncompInfo
    # RiskChecked
    # Rejected
    # CdsFailed
    # Activated
    # EmailVerified
    # Suspended
    # Uplifted
    # CloseRequested
    # Closed
    # CloseFailed
    activity_note = fields.Text(string='Note',help="Describe the activity and result")
    activity_ref = fields.Char(string='Reference',help='This may be document reference involved in this activity.')
    activity_date = fields.Datetime(string='Activity date',default=lambda self: fields.datetime.now())
    # Value of State will depend on type of activity
    state = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('None', 'None'),
        ], string='Status')
    # Value of Result will depend on type of activity (e.g. for credit card or risk check = Pass or Failed)
    result = fields.Char(string='Result')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)

# Model for TRADE TRANSACTION table
# This table stores all the trade and trade-settlement transactions for a particular customer account.
# The types of transaction stored is defined by the 'tran_type' field.
# This table also stores the outstanding balances for certain transactions such as contracts,bills, netting, etc
# Targeted number of records: Very large - probably 1000 records per account

class trade_tran(models.Model):
    _name = 'vb.trade_tran'
    _description = 'Trade transaction'
    _rec_name = 'tran_refno'
    _order = 'acct_id,tran_type,tran_date'

    @api.model
    def default_get(self, fields):
        res = super(trade_tran,self).default_get(fields)
        action_contra = request.env['ir.model.data'].get_object('vb_trade', 'action_contra_transactions')
        action_contra_create = request.env['ir.model.data'].get_object('vb_trade', 'action_trade_contra_create')
        if 'params' in (request.__dict__):
            if 'kwargs' in (request.__dict__).get('params'):
                if 'context' in (request.__dict__).get('params').get('kwargs'):
                    if 'params' in (request.__dict__).get('params').get('kwargs').get('context'):
                        if 'action' in (request.__dict__).get('params').get('kwargs').get('context').get('params'):
                            active_action = (request.__dict__).get('params').get('kwargs').get('context').get('params').get('action')
                            if active_action == action_contra.id or active_action == action_contra_create.id:
                                contra = self.env['vb.tran_type'].search([('code','=','Contra')])
                                if contra:
                                    res.update({'tran_type_id':contra[0].id})
                                else:
                                    res.update({'tran_type_id': False})
        return res

    @api.model
    def create(self,vals):
        vals['state']='New'
        return super(trade_tran,self).create(vals)

    @api.constrains('appby_ids',"tran_qty")
    def check_contra_bal(self):
        if self.appby_ids:
            sell_qty = 0.0
            buy_qty = 0.0
            for rec in self.appby_ids:

                # 12-03-18 DT: Insert here to check qty dont exceed balance less pending
                if abs(rec.appto_qty) > abs(rec.appto_bal_qty + rec.appto_pdg_qty + rec.appto_qty):
                    raise ValidationError('Contra qty greater than balance for '+rec.appto_refno)
                else:
                    rec.update({'appto_type':rec.appto_tran_id.tran_type})
                    if rec.appto_tran_id.tran_type == 'Sell':
                        sell_qty+=rec.appto_tran_qty
                    if rec.appto_tran_id.tran_type == 'Buy':
                        buy_qty+=rec.appto_tran_qty

            if self.tran_qty > min(buy_qty,abs(sell_qty)):
                raise Warning("Contra qty can not exceed "+str(min(buy_qty,abs(sell_qty))))

            if self.tran_qty <= 0:
                raise ValidationError('Transaction qty cannot be 0 or less')

    # This function will compute the default the value of the account name (which is customer name + account no).
    @api.multi
    def name_get(self):
        result=[]
        for rec in self:
            if rec.tran_refno:
                if rec.balance_amt != 0:
                    result.append((rec.id,u"%s(%s) %.2f" % (rec.tran_refno,rec.asset_id.name,rec.balance_amt)))
                else:
                    result.append((rec.id,u"%s(%s)" % (rec.tran_refno,rec.asset_id.name)))
            else:
                result.append((rec.id,u"%s %s (%s)" % ('New',rec.tran_type,rec.asset_id.name)))
        return result

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # This function will set the dlv_basis_id field when asset_id is changed
    @api.onchange('asset_id')
    def _set_dlv_basis_id(self):
        for rec in self:
            if rec.asset_id:
                rec.dlv_basis_id = rec.asset_id.dlv_basis_id

    # Method to get total charges
    @api.multi
    def _compute_total_charges(self):
        for record in self:
            record.total_charge = record.brkg_amt + record.stamp_amt + record.clearing_amt + record.tax_amt + record.chrge1_amt + record.chrge2_amt + record.chrge3_amt

    # Method to get total GST
    @api.multi
    def _compute_total_GST(self):
        for record in self:
            record.total_gst = record.brkg_gst + record.stamp_gst + record.clearing_gst + record.tax_gst + record.chrge1_gst + record.chrge2_gst + record.chrge3_gst

    @api.multi
    def _compute_total_qty(self):
        for rec in self:
            rec.total_qty = rec.balance_qty + rec.pending_qty

    # fields
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True, domain="[('state','=','Active')]")
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    customer_name = fields.Char(string='Customer name',related='acct_id.customer_id.name',readonly=True)
    currency_name = fields.Char(string='Currency name',related='acct_id.currency_id.name',readonly=True)
    market_name = fields.Char(string='Market name',related='acct_id.market_id.name',readonly=True)
    tran_refno = fields.Char(size=20,string='Transaction reference')
    other_refno = fields.Char(size=20,string='Other reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    # 'tran_type_id' field only used during data entry to display selection to determine 'tran_type'
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type')
    tran_type = fields.Char(string='Transaction code',related='tran_type_id.code',store=True)
    #   ('Opening', 'Opening'),
    #   ('Buy', 'Buy contract'),
    #   ('Sell', 'Sell contract'),
    #   ('Charge', 'Charge'),
    #   ('Credit', 'Credit'),
    #   ('TradeSett', 'Trade settlement'),
    #   ('Contra', 'Contra'),
    #   ('Interest', 'Interest'),
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset',ondelete='restrict',index=True)
    portfolio_asset_id = fields.Many2one(comodel_name='vb.asset', string='Portfolio asset',ondelete='restrict')
    trader_id = fields.Many2one(comodel_name='vb.customer', string='Trader',ondelete='set null',index=True,domain="[('customer_type','=','Trader')]",
                                help='This is the default trader for all account under this customer.')
    dlv_basis_id = fields.Many2one(comodel_name='vb.common_code', string='Delivery basis',ondelete='restrict',index=True,domain="[('code_type','=','DlvBasis')]")
    due_date = fields.Date(string='Settlement due date',help='Due date for settlement when both monetary settlement and delivery of shares is on same date.')
    due_date2 = fields.Date(string='Payment due date',help='Only for non-cash account when monetary settlement separate from delivery of shares')
    tran_mode_id = fields.Many2one(comodel_name='vb.common_code', string='Payment mode Id',domain="[('code_type','=','TranMode')]")
    tran_mode = fields.Char(string='Payment mode',related='tran_mode_id.code',store=True,readonly=True)
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_price = fields.Float(string='Price',digits=(13,6),default=0)
    tran_qty = fields.Float(string='Transaction qty',digits=(9,0),default=0)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    gross_amt = fields.Float(string='Gross amt',digits=(15,2),default=0)
    brkg_amt = fields.Float(string='Brokerage',digits=(11,2),default=0)
    brkg_gst = fields.Float(string='Brokerage GST',digits=(11,2),default=0)
    stamp_amt = fields.Float(string='Stamp duty',digits=(11,2),default=0)
    stamp_gst = fields.Float(string='Stamp duty GST',digits=(11,2),default=0)
    clearing_amt = fields.Float(string='Clearing fees',digits=(11,2),default=0)
    clearing_gst = fields.Float(string='Clearing fees GST',digits=(11,2),default=0)
    tax_amt = fields.Float(string='Govern tax',digits=(11,2),default=0)
    tax_gst = fields.Float(string='Govern tax GST',digits=(11,2),default=0)
    chrge1_amt = fields.Float(string='Other charges 1',digits=(11,2),default=0)
    chrge1_gst = fields.Float(string='Other charges 1 GST',digits=(11,2),default=0)
    chrge2_amt = fields.Float(string='Other charges 2',digits=(11,2),default=0)
    chrge2_gst = fields.Float(string='Other charges 2 GST',digits=(11,2),default=0)
    chrge3_amt = fields.Float(string='Other charges 3',digits=(11,2),default=0)
    chrge3_gst = fields.Float(string='Other charges 3 GST',digits=(11,2),default=0)
    commission_amt = fields.Float(string='Commission amt',digits=(11,2),default=0)
    commission_gst = fields.Float(string='Commission GST',digits=(11,2),default=0)
    rev_shr_amt = fields.Float(string='Revenue share',digits=(11,2),default=0)
    rev_shr_gst = fields.Float(string='Revenue share GST',digits=(11,2),default=0)
    sclevy_amt = fields.Float(string='SC levy amt',digits=(11,2),default=0)
    access_amt = fields.Float(string='Access fee amt',digits=(11,2),default=0)
    tran_loc = fields.Float(string='Contract local amt',digits=(15,2),default=0)
    gross_loc = fields.Float(string='Gross local amt',digits=(15,2),default=0)
    brkg_loc = fields.Float(string='Brokerage local',digits=(11,2),default=0)
    brkg_gst_loc = fields.Float(string='Brokerage GST local',digits=(11,2),default=0)
    stamp_loc = fields.Float(string='Stamp duty local',digits=(11,2),default=0)
    stamp_gst_loc = fields.Float(string='Stamp duty GST local',digits=(11,2),default=0)
    clearing_loc = fields.Float(string='Clearing fees local',digits=(11,2),default=0)
    clearing_gst_loc = fields.Float(string='Clearing fees GST local',digits=(11,2),default=0)
    tax_loc = fields.Float(string='Govern tax local',digits=(11,2),default=0)
    tax_gst_loc = fields.Float(string='Govern tax GST local',digits=(11,2),default=0)
    chrge1_loc = fields.Float(string='Other charges 1 local',digits=(11,2),default=0)
    chrge1_gst_loc = fields.Float(string='Other charges 1 GST local',digits=(11,2),default=0)
    chrge2_loc = fields.Float(string='Other charges 2 local',digits=(11,2),default=0)
    chrge2_gst_loc = fields.Float(string='Other charges 2 GST local',digits=(11,2),default=0)
    chrge3_loc = fields.Float(string='Other charges 3 local',digits=(11,2),default=0)
    chrge3_gst_loc = fields.Float(string='Other charges 3 GST local',digits=(11,2),default=0)
    commission_loc = fields.Float(string='Commission local',digits=(11,2),default=0)
    commission_gst_loc = fields.Float(string='Commission GST local',digits=(11,2),default=0)
    rev_shr_loc = fields.Float(string='Revenue share local',digits=(11,2),default=0)
    rev_shr_gst_loc = fields.Float(string='Revenue share GST local',digits=(11,2),default=0)
    sclevy_loc = fields.Float(string='SC levy local',digits=(11,2),default=0)
    access_loc = fields.Float(string='Access fee local',digits=(11,2),default=0)
    balance_qty = fields.Float(string='Balance qty',digits=(9,0),default=0)
    balance_amt = fields.Float(string='Balance amt',digits=(15,2),default=0)
    balance_loc = fields.Float(string='Balance local amt',digits=(15,2),default=0)
    pending_qty = fields.Float(string='Pending qty',digits=(9,0),default=0)
    pending_amt = fields.Float(string='Pending amt',digits=(15,2),default=0)
    pending_loc = fields.Float(string='Pending local amt',digits=(15,2),default=0)
    forex_gainloss = fields.Float(string='Forex gain/loss local amt',digits=(15,2),default=0)
    prev_int_amt = fields.Float(string='Previous unrealised interest amt',digits=(15,2),default=0)
    prev_int_loc = fields.Float(string='Previous unrealised interest local amt',digits=(15,2),default=0)
    prev_int_date = fields.Date(string='Previous unrealised interest last date')
    curr_int_amt = fields.Float(string='Current unrealised interest amt',digits=(15,2),default=0)
    curr_int_loc = fields.Float(string='Current unrealised interest local amt',digits=(15,2),default=0)
    curr_int_date = fields.Date(string='Current unrealised interest last date')
    # 'appto_tran_id' refers to the trade record in the 'trade_tran' model that the settlement record is applied to
    appto_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Applied to transaction',ondelete='restrict',index=True)
    appto_refno = fields.Char(string='Applied-to reference')
    appto_type = fields.Char(string='Applied-to transaction type')
    appto_forex = fields.Float(string='Settlement forex rate',digits=(11,5))
    appto_qty = fields.Float(string='Settlement quantity',digits=(9,0))
    appto_amt = fields.Float(string='Settlement amount',digits=(15,2))
    appto_loc = fields.Float(string='Settlement local amount',digits=(15,2))
    sett_cash_id = fields.Many2one(comodel_name='vb.cash_tran',string='Settlement transaction',ondelete='restrict',index=True)
    # Used only for debit or credit note transactions
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='restrict')
    order_ids = fields.One2many(comodel_name='vb.order_book', inverse_name='trade_tran_id', string='Related orders')
    appby_ids = fields.One2many(comodel_name='vb.trade_tran_app', inverse_name='trade_tran_id', string='Transaction settled by')
    # Unique transaction ID used to link transactions across models that occurred at same time
    internal_trx_no = fields.Integer(string='Internal transaction number',readonly=True,index=True)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Failed', 'Failed'),
        ('Pending', 'Pending'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ('Settled', 'Settled'),
        ], string='Stage', default='New')
    date_reconciled = fields.Datetime(string='Date/time confirmed')
    reconciled_by = fields.Many2one(comodel_name='res.users',string='Confirmed by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    reason_void_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for void',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    cost_price = fields.Float(string='Cost price',digits=(13,6),default=0)
    cost_pricex = fields.Float(string='Cost price (exc fees)',digits=(13,6),default=0)
    cost_amt = fields.Float(string='Cost amt',digits=(15,2),default=0)
    cost_amtx = fields.Float(string='Cost amt (exc fees)',digits=(15,2),default=0)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency')
    market_id = fields.Many2one(comodel_name='vb.market',string='Market')
    portfolio_id = fields.Many2one(comodel_name='vb.portfolio',string='Portfolio')
    rlsd_profitloss_amt = fields.Float(string='Realised P/L amt',digits=(15,2),default=0)
    rlsd_profitloss_amtx = fields.Float(string='Realised P/L amt (exc fees)',digits=(15,2),default=0)
    # 20-Nov-2016 DT
    trade_term = fields.Selection([
        ('Normal','Normal'),
        ('Buyin','Buyin'),
        ('ForceSell','ForceSell'),
        ('Assisted','Assisted'),
        ],string='Trade term')
    brkg_rate = fields.Float(string='Brokerage %',digits=(7,4),default=0)
    gst_rate = fields.Float(string='GST %',digits=(7,4),default=0)
    brkg_id = fields.Many2one(comodel_name='vb.charge_code', string='Regular brokerage charge',ondelete='restrict')
    clearing_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Clearing charge',ondelete='restrict')
    stamp_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Stamp duty charge',ondelete='restrict')
    tax_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Govn tax charge',ondelete='restrict')
    oth_chrg1_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 1',ondelete='restrict')
    oth_chrg2_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 2',ondelete='restrict')
    oth_chrg3_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 3',ondelete='restrict')
    matched_broker = fields.Char(string='Matched with broker')
    matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    matched_amt = fields.Float(string='Matched amt',digits=(15,2),default=0)
    matched_brkg = fields.Float(string='Matched brkg',digits=(11,2),default=0)
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    broker_matching_amt= fields.Float(string='Broker matching amt',digits=(15,2),default=0)
    broker_matching_qty = fields.Float(string='Broker matching qty',digits=(9,0),default=0)
    broker_matching_brkg= fields.Float(string='Broker matching brkg',digits=(11,2),default=0)
    comments = fields.Text(string='Comments')
    settle_flag = fields.Char(string='System use - trigger settlement')
    # Null
    # Settled
    # Yes
    contra_buy_amt = fields.Float(string='Contra buy amt',digits=(15,2),default=0)
    contra_sell_amt = fields.Float(string='Contra sell amt',digits=(15,2),default=0)
    contra_buy_loc = fields.Float(string='Contra buy loc',digits=(15,2),default=0)
    contra_sell_loc = fields.Float(string='Contra sell aloc',digits=(15,2),default=0)
    print_status = fields.Char(string='Print status')
    print_date = fields.Datetime(string='Date/time printed')
    # match_date = fields.Datetime(string='Date/time matched')
    match_status = fields.Char(string='Matched status')
    tran_type_name = fields.Char(string='Transaction type',related='tran_type_id.name',readonly=True)
    search_refno = fields.Char(string='Search reference', index=True)
    total_charge = fields.Float(string='Total charges', compute='_compute_total_charges')
    total_gst = fields.Float(string='Total GST', compute='_compute_total_GST')
    trading_board = fields.Char(string='Trading board')
    # 31-Aug-2017 - Additional fields for V2
    pickup_flag = fields.Boolean(string='Pickup flag',default=False)
    nocontra_flag = fields.Boolean(string='No contra flag',default=False)
    demand_ltr_flag = fields.Boolean(string='Letter of demand flag',default=False)
    last_demand_ltr_date = fields.Date(string='Date of last demand letter generated')
    demand_ltr_count = fields.Float(string='Demand letter count',digits=(3,0),default=0)
    # For V2
    exch_refno = fields.Char(string='Exchange reference')
    last_done_price = fields.Float(string='Last price',digits=(13,6),default=0)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2),default=100)
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    price_asat = fields.Datetime(string='Last price as at')
    mark2mrkt_amt = fields.Float(string='Mark to market amt',digits=(15,2),default=0)
    cap2mrkt_amt = fields.Float(string='Capped to market amt',digits=(15,2),default=0)
    potential_loss_amt = fields.Float(string='Potential loss',digits=(15,2),default=0)
    acct_class_id = fields.Integer(string='Product ID')
    product_type = fields.Char(string='Product type')
    marginable = fields.Boolean(string='Marginable assets',default=True)
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2))
    util_coll_amt = fields.Float(string='Utilised collateral value',digits=(15,2),default=0)
    forcesell_flag = fields.Boolean(string='Forcesell flag',default=False)
    # 15-Jan-2018
    kibb1_tran_amt = fields.Float(string='KIBB transaction amt',digits=(15,2),default=0)
    kibb1_brkg_amt = fields.Float(string='KIBB brokerage',digits=(11,2),default=0)
    kibb1_brkg_gst = fields.Float(string='KIBB brokerage GST',digits=(11,2),default=0)
    kibb1_balance_amt = fields.Float(string='KIBB balance amt',digits=(15,2),default=0)
    kibb1_pending_amt = fields.Float(string='KIBB pending amt',digits=(15,2),default=0)
    kibb2_tran_amt = fields.Float(string='KIBB2 transaction amt',digits=(15,2),default=0)
    kibb2_brkg_amt = fields.Float(string='KIBB2 brokerage',digits=(11,2),default=0)
    kibb2_brkg_gst = fields.Float(string='KIBB2 brokerage GST',digits=(11,2),default=0)
    kibb2_stamp_amt = fields.Float(string='KIBB2 Stamp duty',digits=(11,2),default=0)
    kibb2_stamp_gst = fields.Float(string='KIBB2 Stamp duty GST',digits=(11,2),default=0)
    kibb2_clearing_amt = fields.Float(string='KIBB2 Clearing fees',digits=(11,2),default=0)
    kibb2_clearing_gst = fields.Float(string='KIBB2 Clearing fees GST',digits=(11,2),default=0)
    kibb2_balance_amt = fields.Float(string='KIBB2 balance amt',digits=(15,2),default=0)
    gst_invno = fields.Char(size=20,string='GST tax invoice number')
    # 23-Feb-2018
    date_pickup = fields.Datetime(string='Date/time pick up')
    pickup_by = fields.Many2one(comodel_name='res.users',string='Pickup by')
    date_cancel_pickup = fields.Datetime(string='Date/time cancel pick up')
    cancel_pickup_by = fields.Many2one(comodel_name='res.users',string='Cancel pickup by')
    total_qty = fields.Float(string='Total Qty', compute='_compute_total_qty')
    # 18-Mar-2018
    pickup_qty = fields.Float(string='Pickup qty',digits=(9,0),default=0)
    pickup_amt = fields.Float(string='Pickup amt',digits=(15,2),default=0)
    # 2-May-2018
    pickup_bal_qty = fields.Float(string='Pickup bal qty',digits=(9,0),default=0)
    pickup_bal_amt = fields.Float(string='Pickup bal amt',digits=(15,2),default=0)

# Model for TRADE TRANSACTION INTEREST table
# This table stores all the cash related unrealised interest computation

class trade_tran_int(models.Model):
    _name = 'vb.trade_tran_int'
    _description = 'Trade transaction interest'
    _order = 'acct_id,tran_date'

    # field
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Trade account transaction')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True, domain="[('state','=','Active')]")
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    customer_name = fields.Char(string='Customer name',related='acct_id.customer_id.name',readonly=True)
    currency_name = fields.Char(string='Currency name',related='acct_id.currency_id.name',readonly=True)
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    tran_type = fields.Char(string='Transaction type')
    # Interest
    # Roll-over
    # Commitment fee
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction GST local',digits=(15,2),default=0)
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='restrict')
    trade_os_bal = fields.Float(string='Trade outstanding balance',digits=(15,2),default=0)
    int_rate = fields.Float(string='Interest rate',digits=(5,2),default=0)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Cancelled', 'Cancelled'),
        ('Approved', 'Approved'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
       ], string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage')
    date_cancelled = fields.Datetime(string='Date/time cancelled')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry')


# Model for TRADE TRANSACTION APPLY-TO table
# 15-Aug-2017
# Used to store the Buy/Sell trade records associated to a contra record.

class trade_tran_app(models.Model):
    _name = 'vb.trade_tran_app'
    _description = 'Trade transaction applied to'
    _rec_name = 'tran_refno'

    @api.onchange('appto_tran_id')
    def _compute_avg_price(self):
        if self.appto_tran_qty <> 0:
            self.appto_avg_price = self.appto_tran_amt / self.appto_tran_qty

    @api.onchange('appto_tran_id')
    def get_type(self):
        if self.appto_tran_id:
            if self.appto_tran_id:
                query = 'Select tran_type from vb_trade_tran where id =%s'%self.appto_tran_id.id
                self._cr.execute(query)
                sq=self._cr.fetchall()
                self.appto_type = sq[0][0]

    @api.onchange('appto_qty')
    def _compute_input_amt(self):
        if self.appto_tran_qty <> 0:
            sign = self.appto_tran_qty / abs(self.appto_tran_qty)
            self.appto_qty = abs(self.appto_qty) * sign

            if self.appto_bal_qty <> 0:
                self.appto_amt = self.appto_qty * (self.appto_bal_amt / self.appto_bal_qty)
                self.appto_kibb1_amt = self.appto_qty * (self.appto_kibb1_bal_amt / self.appto_bal_qty)
                self.appto_kibb2_amt = self.appto_qty * (self.appto_kibb2_bal_amt / self.appto_bal_qty)
                self.appto_loc = 0
            else:
                self.appto_amt = 0
                self.appto_kibb1_amt = 0
                self.appto_kibb2_amt = 0
                self.appto_loc = 0

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    # fields
    # 'trade_tran_id' refers to the settlement record in the 'trade_tran' model (and NOT the trade record).
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Trade account transaction',ondelete='cascade',index=True, domain= "[('id','!=',parent.id),('acct_id','=',parent.acct_id),('asset_id','=',parent.asset_id),('tran_type','in',['Buy','Sell']),('state','=','Posted'),('balance_qty','!=',0)]")
    tran_acct_no = fields.Char(size=20,string='Account number',related='trade_tran_id.acct_id.acct_no',readonly=True)
    tran_refno = fields.Char(size=20,string='Transaction reference',related='trade_tran_id.tran_refno',readonly=True)
    tran_date = fields.Date(string='Transaction date',related='trade_tran_id.tran_date',readonly=True)
    tran_forex = fields.Float(string='Transaction reference',related='trade_tran_id.tran_forex',readonly=True)
    # 'appto_tran_id' refers to the trade record in the 'trade_tran' model that the settlement record is applied to
    appto_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Applied to transaction',index=True,
                                    help='Points to the original transaction that is applied to.')
    appto_acct_no = fields.Char(size=20,string='Account number',related='appto_tran_id.acct_id.acct_no',readonly=True)
    appto_type = fields.Char(string='Applied-to type')
    appto_refno = fields.Char(string='Applied-to reference',related='appto_tran_id.tran_refno',store=True,readonly=True)
    appto_date = fields.Date(string='Applied-to date',related='appto_tran_id.tran_date',store=True,readonly=True)
    appto_tran_qty = fields.Float(string='Transaction qty',related='appto_tran_id.tran_qty',store=True,readonly=True)
    appto_tran_amt = fields.Float(string='Transaction amt',related='appto_tran_id.tran_amt',store=True,readonly=True)
    appto_bal_qty = fields.Float(string='Balance qty',related='appto_tran_id.balance_qty',store=True,readonly=True)
    appto_bal_amt = fields.Float(string='Balance amt',related='appto_tran_id.balance_amt',store=True,readonly=True)
    appto_avg_price = fields.Float(string='Avg price',digits=(11,4))
    appto_forex = fields.Float(string='Settlement forex rate',digits=(11,5),default=1)
    appto_qty = fields.Float(string='Settle qty',digits=(9,0),default=0)
    appto_amt = fields.Float(string='Settle amt',digits=(15,2),default=0)
    appto_loc = fields.Float(string='Settle local amount',digits=(15,2),default=0)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Status', default='New')
    # 24-Jan-2017
    appto_tran_loc = fields.Float(string='Transaction local amt',related='appto_tran_id.tran_loc',store=True,readonly=True)
    appto_pdg_qty = fields.Float(string='Pending qty',related='appto_tran_id.pending_qty',store=True,readonly=True)
    appto_pdg_amt = fields.Float(string='Pending amt',related='appto_tran_id.pending_amt',store=True,readonly=True)
    appto_pdg_loc = fields.Float(string='Pending local amt',related='appto_tran_id.pending_loc',store=True,readonly=True)
    # 21-August-2017 - V2
    appto_due_date = fields.Date(string='Applied-to due date',related='appto_tran_id.due_date',store=True,readonly=True)
    appto_kibb1_amt = fields.Float(string='KIBB1 settle amt',digits=(15,2),default=0)
    appto_kibb2_amt = fields.Float(string='KIBB2 settle amt',digits=(15,2),default=0)
    appto_kibb1_tran_amt = fields.Float(string='KIBB1 transaction amt',digits=(15,2),default=0)
    appto_kibb2_tran_amt = fields.Float(string='KIBB2 transaction amt',digits=(15,2),default=0)
    appto_kibb1_bal_amt = fields.Float(string='KIBB1 balance amt',related='appto_tran_id.kibb1_balance_amt',store=True,readonly=True)
    appto_kibb2_bal_amt = fields.Float(string='KIBB2 balance amt',related='appto_tran_id.kibb2_balance_amt',store=True,readonly=True)
    # 12-03-18 DT Added computed fields to display balance after deducting pending
    appto_balpdg_qty = fields.Float(string='Balance qty',compute='_compute_balpdg_qty')
    appto_balpdg_amt = fields.Float(string='Balance amt',compute='_compute_balpdg_amt')

    @api.multi
    @api.depends('appto_tran_id','appto_bal_qty','appto_pdg_qty')
    def _compute_balpdg_qty(self):
        for rec in self:
            rec.appto_balpdg_qty = rec.appto_bal_qty + rec.appto_pdg_qty

    @api.multi
    @api.depends('appto_tran_id','appto_bal_amt','appto_pdg_amt')
    def _compute_balpdg_amt(self):
        for rec in self:
            rec.appto_balpdg_amt = rec.appto_bal_amt + rec.appto_pdg_amt


# Model for CASH TRANSACTION table
# This table stores all the cash related transactions for a particular customer account (both for cash-upfront and margin account).
# The types of transaction stored is defined by the 'tran_type' field.
# Targeted number of records: Very large - probably 200-500 records per account

class cash_tran(models.Model):
    _name = 'vb.cash_tran'
    _description = 'Cash transaction'
    _rec_name='tran_refno'
    _order = 'acct_id,tran_date'

    #this method to get the default value
    @api.model
    def _get_default_currency(self):
        currency_default = self.env['vb.currency'].search([('default_rec','=',True)],limit=1)
        if currency_default:
            return currency_default.id
        return

    @api.model
    def _get_default_payment(self):
        rec = self.env['vb.common_code'].search([('code_type','=','TranMode'),('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return
#=================================================================
#================Withdraw Limit Checking==========================
#=================================================================
    @api.model
    def WithdrawLimit_validation(self,tran_amt,tran_type_id,acct_id,previous_tran_amt):
        print "1",tran_amt,tran_type_id,acct_id,previous_tran_amt
        tran_type = self.env['vb.tran_type']
        if tran_type_id == 'TrfOut':
            tran_type_id = tran_type.search([('code','=',tran_type_id),('model','=',"cash_tran")]).id
        if tran_type.search([('id','=',tran_type_id)]).code == 'Withdraw' or tran_type.search([('id','=',tran_type_id)]).code == 'TrfOut':
            withdraw_limit = self.env['vb.customer_acct_bal'].search([('acct_id','=',acct_id)]).withdraw_limit
            if previous_tran_amt == 0:
                if withdraw_limit < tran_amt:
                    raise ValidationError('The transaction amount is exceeding the withdraw limit \n Withdraw limit is '+str(withdraw_limit))
                elif tran_amt <= 0:
                    raise ValidationError("Transaction amount should be greater than 0")
            else:
                if withdraw_limit+previous_tran_amt < tran_amt:
                    raise ValidationError('The transaction amount is exceeding the withdraw limit \n Withdraw limit is '+str(withdraw_limit+previous_tran_amt))
                elif tran_amt <= 0:
                    raise ValidationError("Transaction amount should be greater than 0")
        else:
            return


    # When creating new record, set 'state' to 'New'
    @api.model
    def create(self,vals):
        vals['state']='New'

        active_action = (request.__dict__).get('params').get('kwargs').get('context').get('params').get('action')
        action_cash_tran = request.env['ir.model.data'].get_object('vb_cashmgmt', 'action_vb_cash_tran')
        # action_cash_transfer = request.env['ir.model.data'].get_object('vb_cashmgmt', 'action_vb_inter_cash_transfer')

        # action for Manage/Transaction/Cash Transaction
        if active_action == action_cash_tran.id:
            # update customer_id and other fields if criteria are met
            # (Withdraw and E-Payment) or (Deposit and Online)
            deposit = self.env['vb.tran_type'].search([('code', '=', 'Deposit'),('model','=','cash_tran')])
            withdraw = self.env['vb.tran_type'].search([('code', '=', 'Withdraw'),('model','=','cash_tran')])
            epayment = self.env['vb.common_code'].search([('code', '=', 'E-Payment'),('code_type','=','TranMode')])
            online = self.env['vb.common_code'].search([('code', '=', 'Online'),('code_type','=','TranMode')])

            if (vals['tran_type_id'] == withdraw[0].id and vals['tran_mode_id'] == epayment[0].id) or (vals['tran_type_id'] == deposit[0].id and vals['tran_mode_id'] == online[0].id):
                # update customer_id
                cust_acct = self.env['vb.customer_acct'].search([('id','=',vals['acct_id'])],limit=1)
                customer_id = cust_acct.customer_id
                # update fin_year and fin_period
                config_rec = self.env['vb.config'].search([('code_type','=','App'),('code','=','CurrFinPeriod')],limit=1)
                vals.update({'customer_id': customer_id.id, 'fin_year': config_rec.parm1, 'fin_period': config_rec.parm2})
        if vals['tran_amt'] and bool('tran_type_id' in vals or 'to_acct_id' in vals) and vals['acct_id']:
            tran_type_id_val = vals['tran_type_id'] if 'tran_type_id' in vals else 'TrfOut'
            self.WithdrawLimit_validation(vals['tran_amt'],tran_type_id_val,vals['acct_id'],0)

        result=super(cash_tran,self).create(vals)
        return result
    
    @api.multi
    def write(self,vals):
        tran_amt = self.tran_amt
        acct_id = self.acct_id
        tran_type_id = self.tran_type_id
        if 'tran_amt' in vals:
            tran_amt = vals['tran_amt']
        if 'acct_id' in vals:
            acct_id = vals['acct_id']
        if acct_id == self.acct_id:
            if 'state' in vals:
                if vals['state'] != 'Void':
                    self.WithdrawLimit_validation(tran_amt,tran_type_id.id,acct_id.id,self.tran_amt)
        result=super(cash_tran,self).write(vals)
        return result
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # This function will compute the default the value of the account name (which is customer name + account no).
    @api.multi
    def name_get(self):
        # super(cash_tran, self).name_get()
        result=[]
        for rec in self:
            if rec.tran_refno:
                result.append((rec.id,u"%s" % rec.tran_refno))
            else:
                result.append((rec.id,u"%s" % 'New'))
        return result

    @api.onchange('acct_id')
    def get_domain(self):
        for i in self:
            if i.acct_id:
                return {'domain':{'to_acct_id':[('customer_id','=',i.acct_id.customer_id.id),('id','!=',i.acct_id.id)]}}
            else:
                return {'domain': {'to_acct_id': [('customer_id', '=', False)]}}


    @api.model
    def default_get(self, fields):
        res = super(cash_tran, self).default_get(fields)
        action_cash_transfer = request.env['ir.model.data'].get_object('vb_cashmgmt', 'action_vb_inter_cash_transfer')
        active_action = (request.__dict__).get('params').get('kwargs').get('context').get('params').get('action')
        if active_action == action_cash_transfer.id:
            transfer_out = self.env['vb.tran_type'].search([('code', '=', 'TrfOut'),('model','=','cash_tran')])
            if transfer_out:
                res.update({'tran_type_id': transfer_out[0].id,'manual_entry':'Yes'})
            else:
                res.update({'tran_type_id': False,'manual_entry':False})
        return res

    # autopopulate the acct_class_id based on the selected account (e.g. 22-'Cash', 24-'Contra')
    @api.onchange('acct_id')
    def get_acct_class_id(self):
        self.acct_class_id = self.acct_id.acct_class_id.id

#     @api.constrains('tran_type','tran_amt')
#     def check_withdraw_limit(self):
#         if self.tran_type == 'Withdraw' or self.tran_type == 'TrfOut':
#             withdraw_limit = self.env['vb.customer_acct_bal'].search([('acct_id','=',self.acct_id.id)]).withdraw_limit
#             if withdraw_limit < self.tran_amt :
#                 raise ValidationError('The transaction amount is exceeding the withdraw limit \n Withdraw limit is '+str(withdraw_limit))
#             elif self.tran_amt <= 0:
#                 raise ValidationError("Transaction amount should be greater than 0")
    
                

    
    # field
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True, domain="[('state','in',['Active','Suspended','PendingClose','Inactive','Dormant'])]")
    acct_no = fields.Char(size=20,string='Account number',related='acct_id.acct_no',readonly=True)
    customer_name = fields.Char(size=100,string='Customer name',related='acct_id.customer_id.name',readonly=True)
    currency_name = fields.Char(size=100,string='Currency name',related='acct_id.currency_id.name',readonly=True)
    market_name = fields.Char(size=100,string='Market name',related='acct_id.market_id.name',readonly=True)
    tran_refno = fields.Char(size=20,string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    # 'tran_type_id' field only used during data entry to display selection to determine 'tran_type'
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type ID',domain="[('model','=','cash_tran')]")
    tran_type = fields.Char(string='Transaction type code',related='tran_type_id.code',store=True,readonly=True)
    #    ('Opening', 'Opening'),
    #    ('Deposit', 'Deposit'),
    #    ('Withdraw', 'Withdrawal'),
    #    ('Charge', 'Charge'),
    #    ('Credit', 'Credit'),
    #    ('CashSett', 'Settlement'),
    #    ('TrfIn', 'Transfer in'),
    #    ('TrfOut', 'Transfer out'),
    #    ('Interest', 'Interest'),
    tran_mode_id = fields.Many2one(comodel_name='vb.common_code', string='Payment mode',domain="[('code_type','=','TranMode')]",default=_get_default_payment)
    tran_mode = fields.Char(string='Payment type',related='tran_mode_id.code',store=True,readonly=True)
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    earmark_amt = fields.Float(string='Earmark amt',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    earmark_loc = fields.Float(string='Earmark local amt',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction GST local',digits=(15,2),default=0)
    other_refno = fields.Char(size=30,string='Other reference')
    bank_refno = fields.Char(size=30,string='Bank reference')
    # Used only for debit or credit note transactions
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='restrict')
    # The following fields are used only for buy/sell settlement against trade account - points to the related trade transactions for the settlement
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Applied to transaction (Settlement)',ondelete='restrict')
    appto_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Applied to transaction',ondelete='restrict')
    appto_refno = fields.Char(string='Applied-to reference',related='trade_tran_id.appto_refno',index=True,readonly=True)
    appto_type = fields.Char(string='Applied-to transaction type',related='trade_tran_id.appto_type',readonly=True)
    appto_forex = fields.Float(string='Settlement forex rate',digits=(11,5),related='trade_tran_id.appto_forex',readonly=True)
    appto_amt = fields.Float(string='Settlement amount',digits=(15,2),related='trade_tran_id.appto_amt',readonly=True)
    appto_loc = fields.Float(string='Settlement local amount',digits=(15,2),related='trade_tran_id.appto_loc',readonly=True)
    # Unique transaction ID used to link transaction across models that occurred at same time
    internal_trx_no = fields.Integer(string='Internal transaction number',readonly=True,index=True)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Failed', 'Failed'),
        ('Cancelled', 'Cancelled'),
        ('Rejected', 'Rejected'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
       ], string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage', default='New')
    date_cancelled = fields.Datetime(string='Date/time cancelled')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    date_reconciled = fields.Datetime(string='Date/time confirmed')
    reconciled_by = fields.Many2one(comodel_name='res.users',string='Confirmed by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    reason_void_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for void',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    cancelled_by = fields.Many2one(comodel_name='res.users',string='Cancelled by')
    reason_cancelled_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for cancellation',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    rqs_date = fields.Datetime(string='Request date')
    rqs_amt = fields.Float(string='Request amount',digits=(15,2),default=0)
    gw_approval_status = fields.Char(string='Approval status')
    gw_approval_date = fields.Datetime(string='Gateway approval date')
    gw_approval_refno = fields.Char(string='Gateway approval reference')
    gw_amount = fields.Float(string='Gateway amount',digits=(15,2),default=0)
    gw_code = fields.Char(string='Gateway code')
    bank_acct_id = fields.Many2one(comodel_name='vb.bankacct', string='Related bank account')
    bank_name = fields.Char(size=100,string='Bank name')
    bank_branch = fields.Char(string='Bank branch', )
    bank_acct_no = fields.Char(string='Bank account number',)
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    to_acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Transfer to account ID')
    to_acct_no = fields.Char(string='Transfer to account No',related='to_acct_id.acct_no',store=True,readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='restrict')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency',ondelete='restrict')
    market_id = fields.Many2one(comodel_name='vb.market',string='Market',ondelete='restrict')
    card_number = fields.Char(string='Credit card number')
    card_expiry = fields.Char(string='Card expiry in MM/YY')
    name_on_card = fields.Char(string='Name on card')
    fpx_refno = fields.Char(string='FPX refno number')
    fpx_date = fields.Datetime(string='FPX date')
    approval_code = fields.Char(string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    response_code = fields.Char(string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_code = fields.Char(string='Payment code')
    # 20-Oct-2016 DT: For storing response code from payment gateway (e.g. for FPX)
    value_date = fields.Date(string='Value date',default=lambda self: fields.date.today())
    value_given = fields.Boolean(string='Value given')
    # 21-Nov-2016 DT
    collateral_amt = fields.Float(string='Collateral amt',digits=(15,2),default=0)
    collateral_loc = fields.Float(string='Collateral local amt',digits=(15,2),default=0)
    matched_bank = fields.Char(string='Matched with bank')
    matched_date = fields.Datetime(string='Matched date')
    matched_amt = fields.Float(string='Matched amount',digits=(15,2),default=0)
    bank_matching_amt = fields.Float(string='Bank matching amount',digits=(15,2),default=0)
    # 01-Dec-2016 DT: Points to related documents for cash transaction
    document_ids = fields.One2many(comodel_name='vb.document', inverse_name='cash_tran_id', string="Documents",domain=[('doc_class_id.code','in',['CASHDEP'])])
    comments = fields.Text(string='Comments')
    # 18-Feb-2017
    print_status = fields.Char(string='Bulk payment print status')
    print_date = fields.Datetime(string='Date/time printed')
    # match_date = fields.Datetime(string='Date/time matched')
    match_status = fields.Char(string='Matched status')
    # 01-Mar-2017
    customer_bank_name = fields.Char(string='Customer bank',related='acct_id.bank_acct_id.bank_id.name',readonly=True)
    # 08-Mar-2017
    tran_type_name = fields.Char(string='Transaction type name',related='tran_type_id.name',readonly=True)
    # 13-Mar-2017
    bnm_code = fields.Char(string='BNM code',related='acct_id.bank_acct_id.bank_id.parm1',readonly=True)
    # For V2
    exch_refno = fields.Char(string='Exchange reference')
    pickup_flag = fields.Boolean(string='pickup flag',default=False)
    acct_class_id = fields.Integer(string='Product ID')
    product_type = fields.Char(string='Product type')
    # 29-Sep-2017
    cardholder_name = fields.Char(string='Cardholder name')

# Model for CASH TRANSACTION INTEREST table
# This table stores all the cash related unrealised interest computation

class cash_tran_int(models.Model):
    _name = 'vb.cash_tran_int'
    _description = 'Cash transaction interest'
    _order = 'acct_id,tran_date'

    # field
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True, domain="[('state','=','Active')]")
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    customer_name = fields.Char(string='Customer name',related='acct_id.customer_id.name',readonly=True)
    currency_name = fields.Char(string='Currency name',related='acct_id.currency_id.name',readonly=True)
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    tran_type = fields.Char(string='Transaction type')
    # Interest
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction GST local',digits=(15,2),default=0)
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='restrict')
    cash_ledger_bal = fields.Float(string='Cash ledger balance',digits=(15,2),default=0)
    int_rate = fields.Float(string='Interest rate',digits=(5,2),default=0)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Cancelled', 'Cancelled'),
        ('Approved', 'Approved'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
       ], string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage')
    date_cancelled = fields.Datetime(string='Date/time cancelled')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry')

# Model for CUSTOMER PORTFOLIO table
# For each asset owned by the customer, this table stores the current quantity and value of the asset
# in one of 6 pools.  Pool 1 is the available pool.  Pool 2 is the purchase but not available,
# pools 3 and 4 to be defined and pool 5 is the earmarked pool (cannot be used by customer).
# Each asset could be an share equity, a unit trust, a bond, unit trust, etc.
# Please note that the transaction details for a particular asset is stored in a separate table.
# Targeted number of records: Estimated 50-100 records per account

class portfolio(models.Model):
    _name = 'vb.portfolio'
    _description = 'Portfolio assets'
    _rec_name = 'acct_id'

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    @api.multi
    def get_net_buysell(self):
        for rec in self:
            rec.net_buysell = rec.shrpool2_cur_qty + rec.shrpool2s_cur_qty

#     def get_withdrawable_qty_contra(self):
#         for rec in self:
#             # rec._cr.execute("SELECT sp_get_transferable_qty( %s,%s); ", (str(rec.acct_id.id), str(rec.asset_id.id)))
#             # trfqty = rec._cr.fetchone()[0]
#             rec.withdrawable_qty_contra = rec.transferable_qty


    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            rec._cr.execute("SELECT sp_get_transferable_qty( %s,%s); ", (str(rec.acct_id.id), str(rec.asset_id.id)))
            trfqty = rec._cr.fetchone()[0]
            rec.withdrawable_qty_contra = trfqty
            result.append((rec.id, u"%s (%s)" % (rec.acct_id.acct_no,rec.acct_id.customer_id.name)))
        return result


    # fields
    name = fields.Char(size=100,string='Name')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='restrict',index=True)
    acct_class_name = fields.Char(size=100,string='Product',related='acct_id.acct_class_id.name',readonly=True)
    customer_name = fields.Char(size=100,string='Customer',related='acct_id.customer_id.name',readonly=True)
    market_code = fields.Char(size=20,string='Market code',related='acct_id.market_id.code',readonly=True)
    market_name = fields.Char(size=100,string='Market name',related='acct_id.market_id.name',readonly=True)
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset',ondelete='restrict',index=True)
    asset_code = fields.Char(size=20,string='Asset code',related='asset_id.asset_code',readonly=True)
    market_symbol = fields.Char(string='Market symbol',related='asset_id.market_symbol',readonly=True,store=True)
    # Pool 1 is the free pool.  Pool 2 is the purchase but not available,
    # pools 3 and 4 to be defined and pool 5 is the earmarked pool (cannot be used by customer).
    shrpool1_cur_qty = fields.Float(string='Free qty',digits=(9,0),default=0)
    shrpool1_pdg_qty = fields.Float(string='Free pending qty',digits=(9,0),default=0)
    shrpool1_cur_cost = fields.Float(string='Free current cost',digits=(15,2),default=0)
    shrpool1_closing_cost = fields.Float(string='Free closing cost',digits=(15,2),default=0)
    shrpool2_cur_qty = fields.Float(string='O/S buy qty',digits=(9,0),default=0)
    shrpool2_pdg_qty = fields.Float(string='O/S buy pending qty',digits=(9,0),default=0)
    shrpool2_cur_cost = fields.Float(string='O/S buy current cost',digits=(15,2),default=0)
    shrpool2_closing_cost = fields.Float(string='O/S buy closing cost',digits=(15,2),default=0)
    shrpool3_cur_qty = fields.Float(string='Share consol qty',digits=(9,0),default=0)
    shrpool3_pdg_qty = fields.Float(string='Share consol pending qty',digits=(9,0),default=0)
    shrpool3_cur_cost = fields.Float(string='Share consol current cost',digits=(15,2),default=0)
    shrpool3_closing_cost = fields.Float(string='Share consol closing cost',digits=(15,2),default=0)
    shrpool4_cur_qty = fields.Float(string='Earmark qty',digits=(9,0),default=0)
    shrpool4_pdg_qty = fields.Float(string='Earmark Pending qty',digits=(9,0),default=0)
    shrpool4_cur_cost = fields.Float(string='Earmark current cost',digits=(15,2),default=0)
    shrpool4_closing_cost = fields.Float(string='Earmark closing cost',digits=(15,2),default=0)
    shrpool5_cur_qty = fields.Float(string='Sharepool5 current qty',digits=(9,0),default=0)
    shrpool5_pdg_qty = fields.Float(string='Sharepool5 pending qty',digits=(9,0),default=0)
    shrpool5_cur_cost = fields.Float(string='Sharepool5 current cost',digits=(15,2),default=0)
    shrpool5_closing_cost = fields.Float(string='Sharepool5 closing cost',digits=(15,2),default=0)
    holding_qty = fields.Float(string='Holding qty',digits=(9,0),default=0)
    holding_amt = fields.Float(string='Holding amount',digits=(15,2),default=0)
    holding_loc = fields.Float(string='Holding amount (loc)',digits=(15,2),default=0)
    holding_cost = fields.Float(string='Holding cost',digits=(15,2),default=0)
    holding_costx = fields.Float(string='Holding cost (exc fees)',digits=(15,2),default=0)
    updated_asat = fields.Datetime(string='Updated as at')
    active = fields.Boolean(string='Active',default=True)
    asset_tran_ids = fields.One2many(comodel_name='vb.asset_tran',inverse_name='portfolio_id',string='Asset Transaction')
    available_qty = fields.Float(string='Available trade qty',digits=(9,0),default=0)
    withdrawable_qty = fields.Float(string='Withdrawable qty',digits=(9,0),default=0)
    earmark_qty = fields.Float(string='Total earmark qty',digits=(9,0),default=0)
    earmark_amt = fields.Float(string='Total earmark amount',digits=(15,2),default=0)
    earmark_loc = fields.Float(string='Total earmark amount (loc)',digits=(15,2),default=0)
    unrlsd_profitloss_amtx = fields.Float(string='Unrealised P/L amt (exc fees)',digits=(15,2),default=0)
    unrlsd_profitloss_locx = fields.Float(string='Unrealised P/L loc amt  (exc fees)',digits=(15,2),default=0)
    rlsd_profitloss_amtx = fields.Float(string='Realised P/L amt (exc fees)',digits=(15,2),default=0)
    rlsd_profitloss_locx = fields.Float(string='Realised P/L loc amt (exc fees)',digits=(15,2),default=0)
    unrlsd_profitloss_amt = fields.Float(string='Unrealised P/L amt',digits=(15,2),default=0)
    unrlsd_profitloss_loc = fields.Float(string='Unrealised P/L amt (loc)',digits=(15,2),default=0)
    rlsd_profitloss_amt = fields.Float(string='Realised P/L amt',digits=(15,2),default=0)
    rlsd_profitloss_loc = fields.Float(string='Realised P/L amt (loc)',digits=(15,2),default=0)
    last_price = fields.Float(string='Last price',digits=(11,4),default=0)
    price_asat = fields.Datetime(string='Last price as at')
    last_closing_date = fields.Date(string='Last closing date')
    closing_holding_qty = fields.Float(string='Yesterday holding qty',digits=(9,0),default=0)
    closing_holding_amt = fields.Float(string='Yesterday holding amount',digits=(15,2),default=0)
    closing_holding_cost = fields.Float(string='Yesterday holding cost',digits=(15,2),default=0)
    closing_holding_costx = fields.Float(string='Yesterday holding cost (exc fees)',digits=(15,2),default=0)
    cost_price = fields.Float(string='Cost price',digits=(11,4))
    cost_pricex = fields.Float(string='Cost price (exc fees)',digits=(11,4))
    matched_cds = fields.Char(string='Matched with CDS')
    matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    cds_matching_qty = fields.Float(string='CDS matching qty',digits=(9,0),default=0)
    match_status = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ], string='Matched status')
    last_cost_price = fields.Float(string='Last cost price',digits=(11,4))
    mth_closing_holding_qty = fields.Float(string='Last month holding qty',digits=(9,0),default=0)
    mth_closing_holding_amt = fields.Float(string='Last month holding amount',digits=(15,2),default=0)
    mth_closing_holding_cost = fields.Float(string='Last month holding cost',digits=(15,2),default=0)
    mth_closing_holding_costx = fields.Float(string='Last month holding cost (exc fees)',digits=(15,2),default=0)
    intra_holding_qty = fields.Float(string='Intra-day holding qty',digits=(9,0),default=0)
    intra_holding_amt = fields.Float(string='Intra-day holding amount',digits=(15,2),default=0)
    intra_holding_cost = fields.Float(string='Intra-day holding cost',digits=(15,2),default=0)
    intra_holding_costx = fields.Float(string='Intra-day holding cost (exc fees)',digits=(15,2),default=0)
    intra_unrlsd_profitloss_amt = fields.Float(string='Intra-day unrealised P/L amt',digits=(15,2),default=0)
    intra_unrlsd_profitloss_amtx = fields.Float(string='Intra-day unrealised P/L amt (exc fees)',digits=(15,2),default=0)
    intra_rlsd_profitloss_amt = fields.Float(string='Intra-day realised P/L amt',digits=(15,2),default=0)
    intra_rlsd_profitloss_amtx = fields.Float(string='Intra-day realised P/L amt (exc fees)',digits=(15,2),default=0)
    pdg_corpact_qty = fields.Float(string='Corporate action entitlement qty',digits=(9,0),default=0)
    pdg_corpact_amt = fields.Float(string='Corporate action entitlement amt',digits=(15,2),default=0)
    corpact_date = fields.Date(string='Corporate action ex-date')
    # 31-Aug-2017 - V2
    share_multiplier = fields.Float(string='Share multiplier',digits=(7,4))
    cds_computed_qty = fields.Float(string='CDS computed qty',digits=(9,0),default=0)
    shrpool1_closing_qty = fields.Float(string='Pool1 closing qty',digits=(9,0),default=0)
    shrpool2_closing_qty = fields.Float(string='Pool2 closing qty',digits=(9,0),default=0)
    shrpool3_closing_qty = fields.Float(string='Pool3 closing qty',digits=(9,0),default=0)
    shrpool4_closing_qty = fields.Float(string='Pool4 closing qty',digits=(9,0),default=0)
    shrpool5_closing_qty = fields.Float(string='Pool5 closing qty',digits=(9,0),default=0)
    shrpool1_closing_amt = fields.Float(string='Pool1 closing amount',digits=(15,2),default=0)
    shrpool2_closing_amt = fields.Float(string='Pool2 closing amount',digits=(15,2),default=0)
    shrpool3_closing_amt = fields.Float(string='Pool3 closing amount',digits=(15,2),default=0)
    shrpool4_closing_amt = fields.Float(string='Pool4 closing amount',digits=(15,2),default=0)
    shrpool5_closing_amt = fields.Float(string='Pool5 closing amount',digits=(15,2),default=0)
    shrpool1_cur_amt = fields.Float(string='Pool1 current amount',digits=(15,2),default=0)
    shrpool2_cur_amt = fields.Float(string='Pool2 current amount',digits=(15,2),default=0)
    shrpool3_cur_amt = fields.Float(string='Pool3 current amount',digits=(15,2),default=0)
    shrpool4_cur_amt = fields.Float(string='Pool4 current amount',digits=(15,2),default=0)
    shrpool5_cur_amt = fields.Float(string='Pool5 current amount',digits=(15,2),default=0)
    shrpool1_mgn_amt = fields.Float(string='Pool1 margin amount',digits=(15,2),default=0)
    shrpool2_mgn_amt = fields.Float(string='Pool2 margin amount',digits=(15,2),default=0)
    shrpool3_mgn_amt = fields.Float(string='Pool3 margin amount',digits=(15,2),default=0)
    shrpool4_mgn_amt = fields.Float(string='Pool4 margin amount',digits=(15,2),default=0)
    shrpool5_mgn_amt = fields.Float(string='Pool5 margin amount',digits=(15,2),default=0)
    shrpool2s_cur_qty = fields.Float(string='O/S sales qty',digits=(9,0),default=0)
    shrpool2s_pdg_qty = fields.Float(string='O/S sales pending qty',digits=(9,0),default=0)
    shrpool2s_cur_cost = fields.Float(string='O/S sales cost',digits=(15,2),default=0)
    shrpool2s_closing_cost = fields.Float(string='O/S sales closing cost',digits=(15,2),default=0)
    shrpool2s_closing_qty = fields.Float(string='O/S sales closing qty',digits=(9,0),default=0)
    shrpool2s_closing_amt = fields.Float(string='O/S sales closing amount',digits=(15,2),default=0)
    shrpool2s_cur_amt = fields.Float(string='O/S sales current amount',digits=(15,2),default=0)
    shrpool2s_mgn_amt = fields.Float(string='O/S sales margin amount',digits=(15,2),default=0)
    shrpool6_cur_qty = fields.Float(string='Sell against free qty',digits=(9,0),default=0)
    shrpool6_pdg_qty = fields.Float(string='Sell adjust qty',digits=(9,0),default=0)
    shrpool6_cur_cost = fields.Float(string='shrpool6_cur_cost',digits=(15,2),default=0)
    shrpool6_closing_cost = fields.Float(string='shrpool6_closing_cost',digits=(15,2),default=0)
    shrpool6_closing_qty = fields.Float(string='Last Sell against free qty',digits=(9,0),default=0)
    shrpool6_closing_amt = fields.Float(string='Paid shares closing amount',digits=(15,2),default=0)
    shrpool6_mgn_amt = fields.Float(string='Paid shares margin amount',digits=(15,2),default=0)
    shrpool6_cur_amt = fields.Float(string='Paid shares current amount',digits=(15,2),default=0)
    cr_numerator = fields.Float(string='Numerator',digits=(9,4))
    cr_denominator = fields.Float(string='Denominator',digits=(9,4))
    cr_ex_date = fields.Date(string='Ex-date')
    cr_lodge_date = fields.Date(string='Lodgement date')
    cr_entitle_date = fields.Date(string='Entitle date')
    cr_active = fields.Boolean(string='Corporate restructure active flag',default=False)
    holding_amt_capped = fields.Float(string='Holding amount (capped)',digits=(15,2),default=0)
    holding_mgn_amt = fields.Float(string='Holding margin amount',digits=(15,2),default=0)
    intra_holding_amt_capped = fields.Float(string='Intra-day holding amount (capped)',digits=(15,2),default=0)
    intra_holding_mgn_amt = fields.Float(string='Intra-day Holding margin amount',digits=(15,2),default=0)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2),default=100)
    price_cap = fields.Float(string='Price cap amount',digits=(11,5))
    marginable = fields.Boolean(string='Marginable assets',default=False)
    acct_class_id = fields.Integer(string='Product ID')
    product_type = fields.Char(string='Product type')
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0),default=0)
    max_buy_amt = fields.Float(string='Max qty to buy',digits=(15,2),default=0)
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0),default=0)
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2),default=0)
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0),default=0)
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2),default=0)
    buy_suspended = fields.Boolean('Buy suspended',default=False)
    sell_suspended = fields.Boolean('Sell suspended',default=False)
    # 15-Mar-2018
    transferable_qty = fields.Float(string='Transferable qty',digits=(9,0),default=0)
    # Computed fields - non-stored
    net_buysell = fields.Float(compute=get_net_buysell)
    withdrawable_qty_contra = fields.Float(string='Withdrawable qty',digits=(9,0),default=0,store=False)
    # 03-Apr-2018
    shrpool6_cur_qty1 = fields.Float(string='Bal of buy over sell',digits=(9,0),default=0)
    shrpool6_pdg_qty1 = fields.Float(string='Buy adjust qty',digits=(9,0),default=0)
    shrpool6_closing_qty1 = fields.Float(string='Last bal of buy over sell',digits=(9,0),default=0)

# Model for ASSET TRANSACTION table
# This table stores the details of all the portfolio related transactions for the various assets of the customer.
# Targeted number of records: Very large - probably 500 records per account

class asset_tran(models.Model):
    _name = 'vb.asset_tran'
    _description = 'Portfolio asset transactions'
    _rec_name = 'tran_refno'
    _order = 'portfolio_id,tran_refno'

    # This function will set the acct_id field when portfolio_id is changed
    @api.onchange('portfolio_id')
    def _set_acct_id(self):
        for rec in self:
            rec.acct_id = rec.portfolio_id.acct_id

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    # This function will compute the default display name
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.tran_refno:
                result.append((rec.id, u"%s" % rec.tran_refno))
            else:
                result.append((rec.id, u"%s" % 'New'))
        return result
    
    @api.constrains('tran_qty')
    def check_tran_qty(self):
        if self.tran_qty <1:
            raise ValidationError("Transaction qty should not be 0")

    # field
    portfolio_id = fields.Many2one(comodel_name='vb.portfolio',string='Portfolio',ondelete='restrict',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Customer account',ondelete='restrict',index=True,domain="[('state','in',['Active','PendingClose'])]")
    asset_name = fields.Char(size=100,string='Asset name',related='portfolio_id.asset_id.name',readonly=True)
    asset_code = fields.Char(size=20,string='Asset code',related='portfolio_id.asset_id.asset_code',readonly=True)
    market_symbol = fields.Char(string='Market symbol',related='portfolio_id.asset_id.market_symbol',readonly=True)
    tran_refno = fields.Char(size=20,string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',default=lambda self: fields.date.today())
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    shrpool = fields.Char(string='Share pool',help='Values from 1 to 4')
    # 'tran_type_id' field only used during data entry to display selection to determine 'tran_type'
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type Id')
    tran_type = fields.Char(string='Transaction type',related='tran_type_id.code',store=True)
    #    ('Opening', 'Opening'),
    #    ('Deposit', 'Deposit'),
    #    ('Withdraw', 'Withdrawal'),
    #    ('DepositCancel', 'CDS Deposit Cancel'),
    #    ('WithdrawCancel', 'CDS Withdrawal Cancel'),
    #    ('Buy', 'Buy'),
    #    ('Sell', 'Sell'),
    #    ('Adjust', 'Adjustment'),
    #    ('CorpAction', 'Corporate action'),
    corp_action = fields.Char(string='Corporate action type')
    tran_mode = fields.Char(string='Transaction mode')
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_qty = fields.Float(string='Transaction qty',digits=(9,0),default=0)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    tran_price = fields.Float(string='Avg price',digits=(11,4),default=0)
    # 15-Sep-2016 DT: For 'Sell' the cost price is the average cost
    cost_price = fields.Float(string='Cost price',digits=(11,4),default=0)
    cost_pricex = fields.Float(string='Cost price (exc fees)',digits=(11,4),default=0)
    # 15-Sep-2016 DT: For 'Sell' the cost amt = tran_qty * cost_price
    cost_amt = fields.Float(string='Cost amt',digits=(15,2),default=0)
    cost_amtx = fields.Float(string='Cost amt (exc fees)',digits=(15,2),default=0)
    other_refno = fields.Char(size=30,string='Other reference')
    # Deprecated for V2 - duplicate field in corp_action
    # corporate_action = fields.Char(string='Corporate action')
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='restrict')
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Trade account transaction',ondelete='restrict',index=True)
    # Unique transaction ID used to link transaction across models that occurred at same time
    internal_trx_no = fields.Integer(string='Internal transaction number',readonly=True,index=True)
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Failed', 'Failed'),
        ('Cancelled', 'Cancelled'),
        ('Rejected', 'Rejected'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], default='New',string='Status')
    stage = fields.Selection([
        ('New', 'New'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Stage', default='New')
    date_reconciled = fields.Datetime(string='Date/time confirmed')
    reconciled_by = fields.Many2one(comodel_name='res.users',string='Confirmed by')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by')
    date_void = fields.Datetime(string='Date/time void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by')
    reason_void_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for void',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    cancelled_by = fields.Many2one(comodel_name='res.users',string='Cancelled by')
    reason_cancelled_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for cancellation',ondelete='restrict',domain="[('code_type','=','ReasonTranCancel')]")
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer')
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency')
    market_id = fields.Many2one(comodel_name='vb.market',string='Market')
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset')
    rlsd_profitloss_amt = fields.Float(string='Realised P/L amt',digits=(15,2),default=0)
    rlsd_profitloss_amtx = fields.Float(string='Realised P/L amt (exc fees)',digits=(15,2),default=0)
    rlsd_profitloss_loc = fields.Float(string='Realised P/L loc amt',digits=(15,2),default=0)
    rlsd_profitloss_locx = fields.Float(string='Realised P/L loc amt (exc fees)',digits=(15,2),default=0)
    # 21-Nov-2016 DT: Only for transfer out - this is the related CDS/account transferred to
    to_cds_no = fields.Char(size=15,string='CDS no')
    to_cds_name = fields.Char(size=80,string='CDS name')
    to_acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Transfer to account')
    matched_cds = fields.Char(string='Matched with CDS',help='CDS reference that caused the deposit/withdraw')
    matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    cds_matching_qty = fields.Float(string='CDS matching qty',digits=(9,0),default=0)
    fpx_refno = fields.Char(size=30,string='FPX refno number')
    bank_refno = fields.Char(size=30,string='Bank reference')
    fpx_date = fields.Datetime(string='FPX date')
    approval_code = fields.Char(size=30,string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    response_code = fields.Char(size=30,string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_code = fields.Char(size=30,string='Payment code')
    payment_amt = fields.Float(size=30,string='Payment amt',digits=(15,2),default=0)
    document_ids = fields.One2many(comodel_name='vb.document', inverse_name='asset_tran_id', string="Documents")
    cds_trf_generated = fields.Boolean(string='CDS transfer generated')
    cds_trf_date = fields.Datetime(string='When CDS transfer generated')
    cds_trf_reason = fields.Char(string='CDS Transfer reason')
    cancelled_cds = fields.Char(string='Cancelled with CDS',help='CDS reference that caused the cancellation')
    comments = fields.Text(string='Comments')
    bank_acct_id = fields.Many2one(comodel_name='vb.bankacct', string='Related bank account')
    bank_name = fields.Char(size=100,string='Bank name')
    bank_branch = fields.Char(string='Bank branch', )
    bank_acct_no = fields.Char(string='Bank account number',)
    print_status = fields.Char(string='Print status')
    print_date = fields.Datetime(string='Date/time printed')
    match_status = fields.Char(string='Matched status')
    tran_type_name = fields.Char(string='Transaction type name',related='tran_type_id.name',readonly=True)
    date_cancelled = fields.Datetime(string='Date/time cancelled')
    customer_name = fields.Char(related='acct_id.name', string="Customer name")
    # For V2
    exch_refno = fields.Char(string='Exchange reference')
    acct_class_id = fields.Integer(string='Product ID')
    product_type = fields.Char(string='Product type')
    # 29-Sep-2017
    cardholder_name = fields.Char(string='Cardholder name')
    #23-July-2018
    holding_qty = fields.Float(string='Holding qty',related='portfolio_id.holding_qty',readonly=True)
    available_qty = fields.Float(string='Available trade qty',related='portfolio_id.available_qty',readonly=True)
    withdrawable_qty = fields.Float(string='Withdrawable qty',related='portfolio_id.withdrawable_qty',readonly=True)
    earmark_qty = fields.Float(string='Total earmark qty',related='portfolio_id.earmark_qty',readonly=True)
    shrpool6_cur_qty = fields.Float(string='sellqtyfromfreebalance',related='portfolio_id.shrpool6_cur_qty',readonly=True)
    shrpool1_cur_qty = fields.Float(string='Free balance',related='portfolio_id.shrpool1_cur_qty',readonly=True)
    shrpool1_pdg_qty = fields.Float(string='Shrpool1 pending qty',related='portfolio_id.shrpool1_pdg_qty',readonly=True)
    shrpool2_cur_qty = fields.Float(string='O/S buy qty',related='portfolio_id.shrpool2_cur_qty',readonly=True)
    shrpool2_pdg_qty = fields.Float(string='Tday matched buy qty',related='portfolio_id.shrpool2_pdg_qty',readonly=True)
    shrpool2s_cur_qty = fields.Float(string='O/S sell qty',related='portfolio_id.shrpool2s_cur_qty',readonly=True)
    shrpool2s_pdg_qty = fields.Float(string='Tday sell qty',related='portfolio_id.shrpool2s_pdg_qty',readonly=True)

# Model for ACCOUNT PROFILE table
# This table will provide account related parameters for customer accounts with this profile.
# Important use of this table is during G/L interface where we need to extract out the G/L account for the control accounts.
# Another use is during interest computation where this profile will store the information on the rates to be used.
# Targeted number of records: Small - Around 10-20 records

class account_profile(models.Model):
    _name = 'vb.account_profile'
    _description = 'Account profile'
    _order = 'name'

    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            obj = self.env['vb.account_profile'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError ("Sorry there is already existing default account profile")
                    break

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    # fields
    name = fields.Char(size=100,string='Profile name',index=True)
    code = fields.Char(size=20,string='Profile code',index=True)
    default_rec = fields.Boolean(string='Default profile',default=False)
    interest_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Db interest charge (Cash)',ondelete='restrict')
    interest_cr_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Cr interest charge (Cash)',ondelete='restrict')
    min_cash_bal_rqd = fields.Float(string='Minimum cash balance',default=0)
    # The following fields are for G/L interface
    glacct_no_cust = fields.Char(string='G/L code - trade debtor - customer')
    glacct_no_brok = fields.Char(string='G/L code - trade debtor - broker')
    glacct_no_dr = fields.Char(string='G/L code - other debtor')
    glacct_no_cr = fields.Char(string='G/L code - other creditor')
    # The following field indicates which company bank account is associated to this profile
    bank_acct_id = fields.Many2one(comodel_name='vb.bankacct', string='Associated bank account',ondelete='restrict',index=True,
                                   help='This is the related company bank account associated with this account. Use for reconciliation.')
    active = fields.Boolean(string='Active',default=True)
    # 14-Dec-2016
    glacct_no_trust = fields.Char(string='G/L code - cashbook trust')
    glacct_no_susp = fields.Char(string='G/L code - trade/cash suspense')
    glacct_no1 = fields.Char(string='G/L code - trade creditor - customer')
    glacct_no2 = fields.Char(string='G/L code - trade creditor - broker')
    glacct_no3 = fields.Char(string='G/L code - cashbook company')
    glacct_no4 = fields.Char(string='G/L code - brokerage earned')
    # 08-Feb-2017 DT
    trade_int_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Db interest charge (Trade)',ondelete='restrict')
    trade_int_cr_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Cr interest charge (Trade)',ondelete='restrict')
    # 29-March-2017
    glacct_no5 = fields.Char(string='G/L code - brokerage expense')
    glacct_no6 = fields.Char(string='G/L code - clearing fee')
    glacct_no7 = fields.Char(string='G/L code - stamp duty')
    glacct_no8 = fields.Char(string='G/L code - stock clearing')
    # 10-Sep-2017
    financing_days = fields.Integer(string='Financing period (days)')
    financing_mths = fields.Integer(string='Financing period (months)')
    # 06-Jan-2018
    trade_int1_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Db interest charge (Contra)',ondelete='restrict')
    trade_int1_cr_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Cr interest charge (Contra)',ondelete='restrict')
    trade_int2_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Db interest charge (Others)',ondelete='restrict')
    trade_int2_cr_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Cr interest charge (Others)',ondelete='restrict')
    # 21-Jan-2018
    glacct_no9 = fields.Char(string='G/L code - output GST')
    glacct_no10 = fields.Char(string='G/L code - Input GST')
    glacct_no11 = fields.Char(string='G/L code - extra no11')
    glacct_no12 = fields.Char(string='G/L code - extra no12')

    # SQL constraint
    _sql_constraints = [('unique_profile_code','UNIQUE(code,active)','Another profile with the same code already exist'),
                        ('unique_profile_name','UNIQUE(name,active)','Another profile with the same name already exist')]

# Model for MARGIN PROFILE table
# This table will provide margin related parameters and rules to be applied for customer accounts with this profile.
# Important use of this table is during roll-over fee computation where this profile will store the information on the rates to be used.
# Targeted number of records: Small - Around 10-20 records

class margin_profile(models.Model):
    _name = 'vb.margin_profile'
    _description = 'margin profile'
    _order = 'name'

    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            for rec in self.env['vb.account_profile'].search([('id','!=',self.id)]):
                if rec.default_rec == True:
                    raise ValidationError ("Sorry there is already existing default margin profile")
                    break

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # fields
    name = fields.Char(size=100,string='Profile name',index=True)
    code = fields.Char(size=20,string='Profile code',index=True)
    default_rec = fields.Boolean(string='Default profile',default=False)
    default_approve_limit = fields.Float(string='Default approval limit',digits=(15,2))
    rollover_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Rollover charge',ondelete='restrict',index=True)
    rollover_share_pct = fields.Float(string='Rollover sharing %',digits=(5,2))
    interest_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Debit interest charge',ondelete='restrict',index=True)
    interest_cr_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Credit interest charge',ondelete='restrict',index=True)
    commitment_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Commitment fee charge',ondelete='restrict',index=True)
    cost_of_fund_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Cost of funds charge',ondelete='restrict',index=True)
    facility_renewal_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Facility renewal charge',ondelete='restrict',index=True)
    margin_pct = fields.Float(string='Approved margin of finance %',digits=(7,4))
    max_margin_limit = fields.Float(string='Maximum margin limit',digits=(15,2))
    cash_multiplier = fields.Float(string='Cash multiplier',digits=(7,4))
    share_multiplier = fields.Float(string='Share multiplier',digits=(7,4))
    other_multiplier = fields.Float(string='Other multiplier',digits=(7,4))
    equity_ratio = fields.Float(string='Equity ratio',digits=(7,4))
    margin_call_tolerance_pct = fields.Float(string='Margin call tolerance %',digits=(7,4))
    margin_call_interval = fields.Float(string='Margin call interval',digits=(3,0))
    margin_call_count_max = fields.Float(string='Margin call count max',digits=(3,0))
    active = fields.Boolean(string='Active',default=True)

    # 01-Aug-2017
    non_share_multiplier = fields.Float(string='Non-share multiplier',digits=(7,4))
    financier_id = fields.Many2one(comodel_name='vb.bankacct', string='Financing bank',ondelete='restrict',index=True)
    unutilised_threshold_pct = fields.Float(string='Unutilised threshold %',digits=(7,4))
    unutilised_threshold_amt = fields.Float(string='Unutilised threshold amt',digits=(15,2))
    margin_call_pct = fields.Float(string='Margin call %',digits=(7,4))

    # To be deprecated (please DO NOT use) -  replace by same at ASSET level
    price_cap_pct = fields.Float(string='Price cap %',digits=(7,4))
    price_cap_amt = fields.Float(string='Price cap amt',digits=(9,4))
    max_cap_amt = fields.Float(string='Maximum cap amt',digits=(15,2))

    # SQL constraint
    _sql_constraints = [('unique_profile_code','UNIQUE(code,active)','Another profile with the same code already exist'),
                        ('unique_profile_name','UNIQUE(name,active)','Another profile with the same name already exist')]

# Model for RISK PROFILE table
# This table will provide risk related parameters and rules to be applied for customer accounts with this profile.
# Targeted number of records: Small - Around 10-20 records

class risk_profile(models.Model):
    _name = 'vb.risk_profile'
    _description = 'risk profile'
    _order = 'name'

    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            for rec in self.env['vb.risk_profile'].search([('id','!=',self.id)]):
                if rec.default_rec == True:
                    raise ValidationError ("Sorry there is already existing default risk profile")
                    break

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # fields
    name = fields.Char(size=100,string='Profile name',index=True)
    code = fields.Char(size=20,string='Profile code',index=True)
    default_rec = fields.Boolean(string='Default profile',default=False)
    active = fields.Boolean(string='Active',default=True)
    min_scoring = fields.Integer(string='Minimum scoring for questionaire')
    checklist_ids = fields.One2many(comodel_name='vb.risk_profile_check', inverse_name='risk_profile_id', string="Check items")
    questionaire_ids = fields.One2many(comodel_name='vb.risk_profile_question', inverse_name='risk_profile_id', string="Questionaires")

    # SQL constraint
    _sql_constraints = [('unique_profile_code','UNIQUE(code,active)','Another profile with the same code already exist'),
                        ('unique_profile_name','UNIQUE(name,active)','Another profile with the same name already exist')]

# Model for RISK PROFILE CHECKLIST table
# This table will provide a risk checklist for a risk profile.  This checklist will be copied to the customer
# account to store the actual checklist response.

class risk_profile_check(models.Model):
    _name = 'vb.risk_profile_check'
    _description = 'Risk profile checking list'
    _order = 'risk_profile_id,list_seq,name'

    @api.multi
    def write(self,vals):
        result = super(risk_profile_check,self).write(vals)
        if self.approver1 or self.approver2 or self.approver3 or self.approver4:
            self.approver1.id,self.id
            self.env['vb.customer_acct'].update_approver(self.approver1.id or False,self.approver2.id or False,self.approver3.id or False,self.approver4.id or False,self.id)
        else:
            self.env['vb.customer_acct'].update_approver(False,False,False,False,self.id)

    @api.onchange('list_seq')
    def get_seq(self):
        if self.code_id:
            recs =  self.env['vb.customer_acct_riskcheck'].search([('riskcheck_id','=',self._origin.id)])
            if recs:
                for rec in recs:
                    query = 'Update vb_customer_acct_riskcheck set list_seq=%s where riskcheck_id=%s'
                    self._cr.execute(query,(self.list_seq,self._origin.id))

    # fields
    risk_profile_id = fields.Many2one(comodel_name='vb.risk_profile', string='Risk profile',ondelete='cascade',index=True)
    code_id = fields.Many2one(comodel_name='vb.common_code', string='RAMCI code',ondelete='restrict',domain="[('code_type','=','Ramci')]")
    code = fields.Char(string='RAMCI code',related='code_id.code', store=True, readonly=True)
    name = fields.Char(size=100,string='Checking description',related='code_id.name', store=True, readonly=True)
    required = fields.Boolean(string='Required check')
    process_mode = fields.Selection([
        ('Manual', 'Manual'),
        ('Online', 'Online'),
       ], string='Mode', default='Manual')
    server_id = fields.Many2one(comodel_name='vb.config', string='Server',ondelete='restrict',domain="[('code_type','=','Svr')]")
    webservice_id = fields.Many2one(comodel_name='vb.config', string='Web service',ondelete='restrict',domain="[('code_type','=','WebSvc')]")
    list_seq = fields.Integer(string='List order')
    # 05-Dec-2016
    result_max = fields.Integer('Maximum result required',default=0,help='If beyond this max, then risk check should fail')
    result_min = fields.Integer('Minimum result required',default=0,help='If below this min, then risk check should fail')
    # 16-Feb-2017
    approver1 = fields.Many2one(comodel_name='res.users',string='Approver #1')
    approver2 = fields.Many2one(comodel_name='res.users',string='Approver #2')
    approver3 = fields.Many2one(comodel_name='res.users',string='Approver #3')
    approver4 = fields.Many2one(comodel_name='res.users',string='Approver #4')
    approver5 = fields.Many2one(comodel_name='res.users',string='Approver #5')


# Model for RISK PROFILE QUESTIONAIRE table
# This table will provide a risk questionaires for a risk profile.  This questionaire will be copied to the customer
# account to store the actual customer response.

class risk_profile_question(models.Model):
    _name = 'vb.risk_profile_question'
    _description = 'Risk profile questionaire'
    _order = 'risk_profile_id,set,section,question_no'

    @api.multi
    def write(self,vals):
        if vals.get('section'):
            if '.' in vals.get('section'):
                    x = vals.get('section').split('.')
                    vals['section']=x[0]
            if vals.get('question_no'):
                final=''
                final = str(vals.get('section')+'.'+str(vals.get('question_no')))
                vals['section']=final
            else:
                final=''
                final = str(vals.get('section')+'.'+str(self.question_no))
                vals['section']=final
        if vals.get('question_no'):
            if bool(vals.get('section'))!= True:
                final=''
                final = str(str(self.section)+'.'+str(vals.get('question_no')))
                vals['section']=final.upper()
        return super(risk_profile_question,self).write(vals)

    # fields
    risk_profile_id = fields.Many2one(comodel_name='vb.risk_profile', string='Risk profile',ondelete='cascade',index=True)
    name = fields.Char(size=100,string='Description')
    set = fields.Char(string='Set')
    section = fields.Char(string='Section')
    question_no = fields.Integer(string='Question no (within section)')
    question_text = fields.Text(string='Question')
    response_type = fields.Selection([
        ('YesNo', 'Yes/no'),
        ('TextInput', 'Text input'),
        ('Selection', 'Select list'),
        ('CheckBox', 'Checkbox'),
        ], string='Response type')
    required = fields.Selection([
        ('Yes', 'Yes'),
        ('Conditional', 'Conditional'),
        ], string='Required response')
    select_list = fields.Text(string='Select/Checkbox list',help="This is the select list separated by semi-colon")
    desired_response = fields.Char(string='Desired response_question',help="This is the desired response for Yes/No or Selection")
    scoring = fields.Integer(string='Scoring',help="This is the if desired response or selection is met")
    # 08-Sep-2016 DT:  The follow 2 fields applies if the required response is conditional
    cond_question_id = fields.Many2one(comodel_name='vb.risk_profile_question', string='Related question',ondelete='set null',index=True)
    cond_response = fields.Char(string='Conditional response',help="This is the conditional response for Yes/No or Selection")
    # 08-Oct-2016 DT:  Requested by MSIL
    layout = fields.Integer(string='Layout')

# Model for CUSTOMER LOGIN table
# This model is to store the login information for each customer.
# Login information is further separated by domain.  There would be at least one login domain (e.g. for member-site)
# Targeted number of records: Large - average 1 record per customer

class customer_login(models.Model):
    _name = 'vb.customer_login'
    _description = 'Customer login'
    _rec_name = 'login_domain'

    # fields
    # We allow for different front-end service to have different login password
    login_domain = fields.Char(string='Login domain')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    # login password will be encrypted according to the system parameter 'login_encrypt_method' in 'config' model.
    login_pwd = fields.Char(string='Login password', compute="generate_pass_pin", store= True)
    valid_until = fields.Datetime(string='When valid until', compute="generate_pass_pin", store= True)
    last_login = fields.Datetime(string='When last logged in')
    login_count = fields.Integer(string='Login count',default=0)
    ytd_login_count = fields.Integer(string='YTD login count',default=0)
    failed_login_count = fields.Integer(string='Failed login last count',default=0)
    failed_login_total = fields.Integer(string='Failed login total count',default=0)
    temp_pwd = fields.Char(string='Temporary password', compute="generate_pass_pin", store= True)
    temp_pwd_issued = fields.Datetime(string='Temporary password date', compute="generate_pass_pin", store= True)
    temp_pwd_expiry = fields.Datetime(string='Temporary password expiry', compute="generate_pass_pin", store= True)
    reset_date = fields.Datetime(string='When reset password requested')
    reset_count = fields.Integer(string='Number of times password resetted')
    blocked_date = fields.Datetime(string='When last blocked')
    blocked_note = fields.Text(string='Why blocked')
    blocked_count = fields.Integer(string='Number of times blocked')
    initial_login = fields.Boolean(string='Initial login required',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Blocked', 'Blocked'),
        ('Rejected', 'Rejected'),
        ('Reset', 'Reset requested'),
        ('Disabled', 'Disabled'),
        ('Locked','Locked')
        ], string='Status', default='Active')
    # 01-Dec-2016 DT
    disabled_date = fields.Datetime(string='When login disabled')
    disabled_by = fields.Many2one(comodel_name='res.users',string='Disabled by')
    # Enabling is nothing more then changing the state from 'Disabled' to 'Active'
    enabled_date = fields.Datetime(string='When login enabled')
    enabled_by = fields.Many2one(comodel_name='res.users',string='Enabled by')
    # For account level login (esp for T-Pin at account level)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    login_disabled = fields.Boolean(string='Login disabled')

    # SQL constraint
    _sql_constraints = [('unique_login_domain','UNIQUE(customer_id,login_domain,acct_id)','Another login domain for the same customer already exist')]

# Model for CUSTOMER SECURITY QUESTION table
# This model is to store the security questions for each customer.
# The questions are further separated by login_domain.

class customer_sec_question(models.Model):
    _name = 'vb.customer_sec_question'
    _description = 'Customer security questions'
    _order = 'customer_id,login_domain'

    # This function will compute the default the value of the account name (which is customer name + account no).
    @api.multi
    def name_get(self):
        super(customer_sec_question, self).name_get()
        result=[]
        for rec in self:
            result.append((rec.id,u"%s (%s)" % (rec.login_domain,rec.question_no)))
        return result

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    login_domain = fields.Char(string='Login domain')
    question_no = fields.Char(string='Question no')
    question_text = fields.Char(string='Security question')
    answer = fields.Char(string='Answer')
    valid_until = fields.Datetime(string='When valid until')
    active = fields.Boolean(string='Active',default=True)
    list_seq = fields.Integer(string='List order')

# Model for CUSTOMER LOGIN HISTORY table
# This model is to store the history of all login made by the customer
# Targeted number of records: Very large - average 100 records per customer

class customer_login_hist(models.Model):
    _name = 'vb.customer_login_hist'
    _description = 'Customer login history'
    _rec_name = 'login_name'

    # field
    parent_id = fields.Many2one(comodel_name='vb.customer_login', string='Customer login record',ondelete='set null',index=True)
    customer_name = fields.Char(string='Customer name',related='parent_id.customer_id.name',readonly=True)
    login_name = fields.Char(size=100,string='Login name')
    login_pwd = fields.Char(string='Login password')
    login_date = fields.Datetime(string='When logged in')
    login_type = fields.Selection([
        ('Normal', 'Normal login'),
        ('Initial', 'Initial login'),
        ('Temp', 'Login using temp password'),
        ], string='Login type')
    login_status = fields.Selection([
        ('Success', 'Login successful'),
        ('Failed', 'Login failed'),
        ('Blocked', 'Login blocked'),
        ], string='Login status',)
    login_msg = fields.Text(string='Login message')
    # Login location can be description or GPS coordinate
    # Use format 'GPS <longitude>,<latitude>'
    login_loc = fields.Text(string='Login location')
    # Use format 'IP <ip address>'
    login_ip = fields.Text(string='Login IP address')
    login_device = fields.Text(string='Login device')
    device_code = fields.Text(string='Device code')
    device_agent = fields.Text(string='Device agent')

# Model for BLACKLIST table
# There is ONE record for each unique person or entity
# A customer can be an individual, a business entity (aka company) or a trader
# Constraint: There should not be any duplicate national ID (if entered) for an individual or BizRegNo for an business entity
# Targeted number of records: 1K within next 3 years

# Model added 07-Aug-2016

class blacklist(models.Model):
    _name = 'vb.blacklist'
    _description = 'blacklist'
    _order = 'name'

    @api.multi
    @api.constrains('email','mobile_phone')
    def email_phone_valid(self):
        for rec in self:
            if rec.email:
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", rec.email) == None:
                    raise ValidationError ("Invalid email address. Please enter a valid email address!")
                if rec.mobile_phone:
                    if re.match("^\+(?:[0-9] ?){6,14}[0-9]$", rec.mobile_phone) == None:
                        raise ValidationError ("Invalid mobile number. Please enter a valid mobile number!")

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # fields
    name = fields.Char(size=100,string='Customer name',index=True)
    email = fields.Char(size=40,string='Email',index=True)
    mobile_phone = fields.Char(size=20,string='Mobile no')
    date_of_birth = fields.Date(string='Date of birth')
    gender = fields.Selection([
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('N/A', 'Not applicable'),
        ], string='Gender', default='Male')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='set null')
    nationality_id = fields.Many2one(comodel_name='res.country',string='Country of nationality',default=_get_default_country,ondelete='restrict')
    national_id_no = fields.Char(size=20,string='National ID number',index=True)
    race_id = fields.Many2one(comodel_name='vb.common_code',string='Race',ondelete='restrict',domain="[('code_type','=','Race')]")
    other_id_no = fields.Char(size=20,string='Other ID number',index=True)
    other_id_type_id = fields.Many2one(comodel_name='vb.common_code',string='Other ID type',ondelete='restrict',domain="[('code_type','=','IdType')]")
    other_id_expiry = fields.Date(string='Other ID expiry date')
    comments = fields.Text(string='Comments')
    bl_source = fields.Char(string='Blacklist source')
    date_active = fields.Datetime(string='When active')
    date_inactive = fields.Datetime(string='When inactive')
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
        ], string='Status', default='New',help="This shows the current status of the record")

    _sql_constraints = [('unique_entity_name','UNIQUE(name,nationality_id,date_of_birth)','Another entity with the same name and date of birth already exist'),
                        ('unique_entity_id','UNIQUE(name,nationality_id,national_id_no)','Another entity with the same nationl ID already exist')]

# Model for TRADE_TRAN_ASAT table
# This table stores the outstanding trade records (usually the BUY and SELL records) as at the end of
# a particular date (usually end of the month)
# Model added 24-Jan-2017

class trade_tran_asat(models.Model):
    _name = 'vb.trade_tran_asat'
    _description = 'Trade transaction oustanding as at EOM'
    _order = 'asat_date,acct_no'

    # fields
    asat_date = fields.Date(string='As at date')
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Trade record')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',index=True)
    acct_no = fields.Char(string='Account number')
    account_name = fields.Char(string='Account name')
    tran_refno = fields.Char(size=20,string='Transaction reference')
    tran_date = fields.Date(string='Transaction date')
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    tran_type = fields.Char(string='Transaction code')
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset')
    asset_name = fields.Char(string='Asset name')
    due_date = fields.Date(string='Settlement due date',help='Due date for settlement when both monetary settlement and delivery of shares is on same date.')
    due_date2 = fields.Date(string='Payment due date',help='Only for non-cash account when monetary settlement separate from delivery of shares')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_price = fields.Float(string='Price',digits=(11,4),default=0)
    tran_qty = fields.Float(string='Transaction qty',digits=(9,0),default=0)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    gross_amt = fields.Float(string='Gross amt',digits=(15,2),default=0)
    balance_qty = fields.Float(string='Balance qty',digits=(9,0),default=0)
    balance_amt = fields.Float(string='Balance amt',digits=(15,2),default=0)
    balance_loc = fields.Float(string='Balance local amt',digits=(15,2),default=0)
    forex_gainloss = fields.Float(string='Forex gain/loss local amt',digits=(15,2),default=0)
    prev_int_amt = fields.Float(string='Previous unrealised interest amt',digits=(15,2),default=0)
    prev_int_loc = fields.Float(string='Previous unrealised interest local amt',digits=(15,2),default=0)
    prev_int_date = fields.Date(string='Previous unrealised interest last date')
    curr_int_amt = fields.Float(string='Current unrealised interest amt',digits=(15,2),default=0)
    curr_int_loc = fields.Float(string='Current unrealised interest local amt',digits=(15,2),default=0)
    curr_int_date = fields.Date(string='Current unrealised interest last date')

# Model for CUSTOMER_ACCT_BAL_ASAT table
# This table stores the customer account balance records as at the end of
# a particular date (usually end of the month)
# Model added 24-Jan-2017

class customer_acct_bal_asat(models.Model):
    _name = 'vb.customer_acct_bal_asat'
    _description = 'Customer account balances as at EOM'
    _order = 'asat_date,acct_no'

    # fields
    asat_date = fields.Date(string='As at date')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',index=True)
    acct_no = fields.Char(size=20,string='Account number')
    acct_name = fields.Char(size=100,string='Account name')
    trade_limit = fields.Float(string='Trading limit',digits=(15,2),default=0)
    withdraw_limit = fields.Float(string='Withdraw limit',digits=(15,2),default=0)
    margin_limit = fields.Float(string='Margin limit',digits=(15,2),default=0)
    margin_breached_amt = fields.Float(string='Margin breached amount',digits=(15,2),default=0)
    trade_limit_asat_date = fields.Datetime(string='Trade limit as at date')
    withdraw_limit_asat_date = fields.Datetime(string='Withdrawal limit as at date')
    last_activity_date = fields.Datetime(string='Last activity date')
    # Running balances
    order_buy_amt = fields.Float(string='YTD Buy order amount',digits=(15,2),default=0)
    order_sell_amt = fields.Float(string='YTD Sell order amount',digits=(15,2),default=0)
    matched_buy_amt = fields.Float(string='YTD matched buy order amount',digits=(15,2),default=0)
    matched_sell_amt = fields.Float(string='YTD matched sell order amount',digits=(15,2),default=0)
    trade_os_long_amt = fields.Float(string='Outstanding long position',digits=(15,2),default=0)
    trade_os_short_amt = fields.Float(string='Outstanding short position',digits=(15,2),default=0)
    trade_os_others_amt = fields.Float(string='Trade other outstanding',digits=(15,2),default=0)
    trade_os_long_loc = fields.Float(string='Outstanding long position (local)',digits=(15,2),default=0)
    trade_os_short_loc = fields.Float(string='Outstanding short position (local)',digits=(15,2),default=0)
    trade_os_others_loc = fields.Float(string='Trade other outstanding (local)',digits=(15,2),default=0)
    unrlsd_int_amt = fields.Float(string='Unrealised interest amt',digits=(15,2),default=0)
    unrlsd_int_loc = fields.Float(string='Unrealised local interest amt',digits=(15,2),default=0)
    cash_ledger_bal = fields.Float(string='Cash ledger balance',digits=(15,2),default=0)
    cash_available_bal = fields.Float(string='Cash available balance',digits=(15,2),default=0)
    cash_earmarked_amt = fields.Float(string='Cash earmarked amount',digits=(15,2),default=0)
    cash_collateral_amt = fields.Float(string='Cash collateral amount',digits=(15,2),default=0)
    cash_ledger_loc = fields.Float(string='Cash ledger loc balance',digits=(15,2),default=0)
    cash_available_loc = fields.Float(string='Cash available loc balance',digits=(15,2),default=0)
    cash_collateral_loc = fields.Float(string='Cash collateral loc amount',digits=(15,2),default=0)
    cash_earmarked_loc = fields.Float(string='Cash earmarked loc amount',digits=(15,2),default=0)
    cash_pending_in = fields.Float(string='Cash pending in',digits=(15,2),default=0)
    cash_pending_out = fields.Float(string='Cash pending out',digits=(15,2),default=0)
    trade_pending_in = fields.Float(string='Settlement pending in',digits=(15,2),default=0)
    trade_pending_out = fields.Float(string='Settlement pending out',digits=(15,2),default=0)
    portfolio_val = fields.Float(string='Portfolio holding value',digits=(15,2),default=0)
    portfolio_cost = fields.Float(string='Portfolio holding cost',digits=(15,2),default=0)
    portfolio_loc = fields.Float(string='Portfolio holding loc value',digits=(15,2),default=0)
    portfolio_loc_cost = fields.Float(string='Portfolio holding loc cost',digits=(15,2),default=0)
    portfolio_earmarked_amt = fields.Float(string='Portfolio earmarked amount',digits=(15,2),default=0)
    portfolio_earmarked_loc = fields.Float(string='Portfolio earmarked loc amount',digits=(15,2),default=0)
    portfolio_unrlsd_pl_amt = fields.Float(string='Portfolio unrealised profit/loss',digits=(15,2),default=0)
    portfolio_rlsd_pl_amt = fields.Float(string='Portfolio realised profit/loss',digits=(15,2),default=0)
    portfolio_unrlsd_pl_loc = fields.Float(string='Portfolio unrealised loc profit/loss',digits=(15,2),default=0)
    portfolio_rlsd_pl_loc = fields.Float(string='Portfolio realised loc profit/loss',digits=(15,2),default=0)

# Model for PORTFOLIO_ASAT table
# This table stores the customer portfolio balance records as at the end of
# a particular date (usually end of the month)
# Model added 24-Jan-2017

class portfolio_asat(models.Model):
    _name = 'vb.portfolio_asat'
    _description = 'Portfolio assets as at EOM'
    _order = 'asat_date,acct_no'

    # fields
    asat_date = fields.Date(string='As at date')
    #name = fields.Char(size=100,string='Name')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',index=True)
    acct_no = fields.Char(size=20,string='Account number')
    acct_name = fields.Char(size=100,string='Account name')
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset',index=True)
    asset_name = fields.Char(size=20,string='Asset name')
    shrpool1_cur_qty = fields.Float(string='Free qty',digits=(9,0),default=0)
    shrpool1_pdg_qty = fields.Float(string='Free pending qty',digits=(9,0),default=0)
    shrpool1_cur_cost = fields.Float(string='Sharepool1 current cost',digits=(15,2),default=0)
    #shrpool1_closing_cost = fields.Float(string='Sharepool1 closing cost',digits=(15,2),default=0)
    shrpool2_cur_qty = fields.Float(string='O/S buy qty',digits=(9,0),default=0)
    shrpool2_pdg_qty = fields.Float(string='O/S buy pending qty',digits=(9,0),default=0)
    shrpool2_cur_cost = fields.Float(string='Sharepool2 current cost',digits=(15,2),default=0)
    #shrpool2_closing_cost = fields.Float(string='Sharepool2 closing cost',digits=(15,2),default=0)
    shrpool3_cur_qty = fields.Float(string='Earmark share consol qty',digits=(9,0),default=0)
    shrpool3_pdg_qty = fields.Float(string='Sharepool3 Pending qty',digits=(9,0),default=0)
    shrpool3_cur_cost = fields.Float(string='Sharepool3 current cost',digits=(15,2),default=0)
    #shrpool3_closing_cost = fields.Float(string='Sharepool3 closing cost',digits=(15,2),default=0)
    shrpool4_cur_qty = fields.Float(string='Sharepool4 qty',digits=(9,0),default=0)
    shrpool4_pdg_qty = fields.Float(string='Sharepool4 Pending qty',digits=(9,0),default=0)
    shrpool4_cur_cost = fields.Float(string='Sharepool4 current cost',digits=(15,2),default=0)
    #shrpool4_closing_cost = fields.Float(string='Sharepool4 closing cost',digits=(15,2),default=0)
    shrpool5_cur_qty = fields.Float(string='Sharepool5 Current qty',digits=(9,0),default=0)
    shrpool5_pdg_qty = fields.Float(string='Sharepool5 Pending qty',digits=(9,0),default=0)
    shrpool5_cur_cost = fields.Float(string='Sharepool5 current cost',digits=(15,2),default=0)
    #shrpool5_closing_cost = fields.Float(string='Sharepool5 closing cost',digits=(15,2),default=0)
    holding_qty = fields.Float(string='Holding qty',digits=(9,0),default=0)
    holding_amt = fields.Float(string='Holding amount',digits=(15,2),default=0)
    holding_loc = fields.Float(string='Holding amount (loc)',digits=(15,2),default=0)
    holding_cost = fields.Float(string='Holding cost',digits=(15,2),default=0)
    holding_costx = fields.Float(string='Holding cost (exc fees)',digits=(15,2),default=0)
    #updated_asat = fields.Datetime(string='Updated as at')
    #active = fields.Boolean(string='Active',default=True)
    available_qty = fields.Float(string='Available trade qty',digits=(9,0),default=0)
    withdrawable_qty = fields.Float(string='Withdrawable qty',digits=(9,0),default=0)
    unrlsd_profitloss_amtx = fields.Float(string='Unrealised P/L amt (exc fees)',digits=(15,2),default=0)
    unrlsd_profitloss_locx = fields.Float(string='Unrealised P/L loc amt  (exc fees)',digits=(15,2),default=0)
    rlsd_profitloss_amtx = fields.Float(string='Realised P/L amt (exc fees)',digits=(15,2),default=0)
    rlsd_profitloss_locx = fields.Float(string='Realised P/L loc amt (exc fees)',digits=(15,2),default=0)
    unrlsd_profitloss_amt = fields.Float(string='Unrealised P/L amt',digits=(15,2),default=0)
    unrlsd_profitloss_loc = fields.Float(string='Unrealised P/L amt (loc)',digits=(15,2),default=0)
    rlsd_profitloss_amt = fields.Float(string='Realised P/L amt',digits=(15,2),default=0)
    rlsd_profitloss_loc = fields.Float(string='Realised P/L amt (loc)',digits=(15,2),default=0)
    last_price = fields.Float(string='Last price',digits=(11,4),default=0)
    price_asat = fields.Datetime(string='Last price as at')
    # 30-May-2018 - V2
    #last_closing_date = fields.Date(string='Last closing date')
    #closing_holding_qty = fields.Float(string='Yesterday holding qty',digits=(9,0),default=0)
    #closing_holding_amt = fields.Float(string='Yesterday holding amount',digits=(15,2),default=0)
    #closing_holding_cost = fields.Float(string='Yesterday holding cost',digits=(15,2),default=0)
    #closing_holding_costx = fields.Float(string='Yesterday holding cost (exc fees)',digits=(15,2),default=0)
    cost_price = fields.Float(string='Cost price',digits=(11,4))
    cost_pricex = fields.Float(string='Cost price (exc fees)',digits=(11,4))
    # 30-May-2018 - V2
    #matched_cds = fields.Char(string='Matched with CDS')
    #matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    #matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    #cds_matching_qty = fields.Float(string='CDS matching qty',digits=(9,0),default=0)
    #match_status = fields.Selection([
    #    ('OK', 'OK'),
    #    ('Failed', 'Failed'),
    #    ], string='Matched status')
    #last_cost_price = fields.Float(string='Last cost price',digits=(11,4))
    #mth_closing_holding_qty = fields.Float(string='Last month holding qty',digits=(9,0),default=0)
    #mth_closing_holding_amt = fields.Float(string='Last month holding amount',digits=(15,2),default=0)
    #mth_closing_holding_cost = fields.Float(string='Last month holding cost',digits=(15,2),default=0)
    #mth_closing_holding_costx = fields.Float(string='Last month holding cost (exc fees)',digits=(15,2),default=0)
    #intra_holding_qty = fields.Float(string='Intra-day holding qty',digits=(9,0),default=0)
    #intra_holding_amt = fields.Float(string='Intra-day holding amount',digits=(15,2),default=0)
    #intra_holding_cost = fields.Float(string='Intra-day holding cost',digits=(15,2),default=0)
    #intra_holding_costx = fields.Float(string='Intra-day holding cost (exc fees)',digits=(15,2),default=0)
    #intra_unrlsd_profitloss_amt = fields.Float(string='Intra-day unrealised P/L amt',digits=(15,2),default=0)
    #intra_unrlsd_profitloss_amtx = fields.Float(string='Intra-day unrealised P/L amt (exc fees)',digits=(15,2),default=0)
    #intra_rlsd_profitloss_amt = fields.Float(string='Intra-day realised P/L amt',digits=(15,2),default=0)
    #intra_rlsd_profitloss_amtx = fields.Float(string='Intra-day realised P/L amt (exc fees)',digits=(15,2),default=0)
    #pdg_corpact_qty = fields.Float(string='Corporate action entitlement qty',digits=(9,0),default=0)
    #pdg_corpact_amt = fields.Float(string='Corporate action entitlement amt',digits=(15,2),default=0)
    #corpact_date = fields.Date(string='Corporate action ex-date')
    # 31-Aug-2017 - V2
    # share_multiplier = fields.Float(string='Share multiplier',digits=(7,4))
    # cds_computed_qty = fields.Float(string='CDS computed qty',digits=(9,0),default=0)
    # shrpool1_closing_qty = fields.Float(string='Pool1 closing qty',digits=(9,0),default=0)
    # shrpool2_closing_qty = fields.Float(string='Pool2 closing qty',digits=(9,0),default=0)
    # shrpool3_closing_qty = fields.Float(string='Pool3 closing qty',digits=(9,0),default=0)
    # shrpool4_closing_qty = fields.Float(string='Pool4 closing qty',digits=(9,0),default=0)
    # shrpool5_closing_qty = fields.Float(string='Pool5 closing qty',digits=(9,0),default=0)
    # shrpool1_mgn_amt = fields.Float(string='Pool1 margin amount',digits=(15,2),default=0)
    # shrpool2_mgn_amt = fields.Float(string='Pool2 margin amount',digits=(15,2),default=0)
    # shrpool3_mgn_amt = fields.Float(string='Pool3 margin amount',digits=(15,2),default=0)
    # shrpool4_mgn_amt = fields.Float(string='Pool4 margin amount',digits=(15,2),default=0)
    # shrpool5_mgn_amt = fields.Float(string='Pool5 margin amount',digits=(15,2),default=0)
    # shrpool2s_cur_qty = fields.Float(string='O/S sales qty',digits=(9,0),default=0)
    # shrpool2s_pdg_qty = fields.Float(string='O/S sales pending qty',digits=(9,0),default=0)
    # shrpool2s_cur_cost = fields.Float(string='O/S sales cost',digits=(15,2),default=0)
    # shrpool2s_closing_cost = fields.Float(string='O/S sales closing cost',digits=(15,2),default=0)
    # shrpool2s_closing_qty = fields.Float(string='O/S sales closing qty',digits=(9,0),default=0)
    # shrpool2s_mgn_amt = fields.Float(string='O/S sales amount',digits=(15,2),default=0)
    # shrpool6_cur_qty = fields.Float(string='Pool6 Current qty',digits=(9,0),default=0)
    # shrpool6_pdg_qty = fields.Float(string='Pool6 Pending qty',digits=(9,0),default=0)
    # shrpool6_cur_cost = fields.Float(string='Pool6 current cost',digits=(15,2),default=0)
    # shrpool6_closing_cost = fields.Float(string='Pool6 closing cost',digits=(15,2),default=0)
    # shrpool6_closing_qty = fields.Float(string='Pool6 closing qty',digits=(9,0),default=0)
    # shrpool6_mgn_amt = fields.Float(string='Pool5 margin amount',digits=(15,2),default=0)

# Model for CUSTOMER_CHANGE_RQS table
# Added 29-April-2017 DT

class customer_change_rqs(models.Model):
    _name = 'vb.customer_change_rqs'
    _description = 'Customer change request'
    _rec_name = 'customer_id'

    # field
    customer_id = fields.Many2one(comodel_name='vb.customer',string='Customer',ondelete='cascade',index=True)
    request_date = fields.Datetime(string='Request date/time')
    field_name = fields.Char(string='Field name')
    field_type = fields.Char(string='Field type')
    new_value = fields.Char(string='New field value')
    new_address1 = fields.Char(size=100,string='Address 1',)
    new_address2 = fields.Char(size=100,string='Address 2')
    new_address3 = fields.Char(size=100,string='Address 3')
    new_city = fields.Char(size=100,string='City',)
    new_postcode = fields.Char(size=20,string='Postal code',)
    new_state_id = fields.Many2one(comodel_name='res.country.state', string='State')
    new_country_id = fields.Many2one(comodel_name='res.country', string='Country')
    doc_id = fields.Many2one(comodel_name='vb.document',string='Document ID')
    state = fields.Selection([
        ('New', 'New'),
        ('Rejected', 'Rejected'),
        ('Approved', 'Approved'),
        ('Updated', 'Updated'),
        ], string='Status',)
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    date_updated = fields.Datetime(string='Date/time updated')
    active = fields.Boolean(string='Active',default=True)
    comments = fields.Text(string='Comments')
    old_value = fields.Char(string='Old field value')
    display_field_name  = fields.Char(string='Display field name')
    display_old_value = fields.Char(string='Display old field value')
    display_new_value = fields.Char(string='Display new field value')
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonRej')]")
    old_address1 = fields.Char(size=100,string='Address 1',)
    old_address2 = fields.Char(size=100,string='Address 2')
    old_address3 = fields.Char(size=100,string='Address 3')
    old_city = fields.Char(size=100,string='City',)
    old_postcode = fields.Char(size=20,string='Postal code',)
    old_state_id = fields.Many2one(comodel_name='res.country.state', string='State')
    old_country_id = fields.Many2one(comodel_name='res.country', string='Country')


# Model for CUSTOMER BANKNAME WHITELIST table for V2

class bank_acctname_whitelist(models.Model):
    _name = 'vb.bankacct_whitelist'
    _description = 'Customer bank account name whitelist'
    _order = 'customer_id'

    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;

    @api.multi
    def name_get(self):
        result=[]
        for rec in self:
            result.append((rec.id,u"%s" % (rec.customer_id.name)))
        return result

    # field
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    bank_acct_name = fields.Char(size=100,string='Bank account name')
    bank_acct_no = fields.Char(size=20,string='Bank account number')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Rejected', 'Rejected'),
        ('Approved', 'Approved'),
        ('Deleted', 'Deleted'),
        ], string='Status', default='New')
    # added 3/2/2018
    customer_code = fields.Char(string='Customer code',related='customer_id.code',readonly=True)
    remarks = fields.Char(string='Remarks')
    # added/modified 4-May-2018
    approve_by = fields.Many2one(comodel_name='res.users',string='Approved by')
    approve_date = fields.Datetime(string='Date/time approved')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by')
    rejected_date = fields.Datetime(string='Date/time rejected')
    reason_rejected_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for rejection',ondelete='restrict',domain="[('code_type','=','ReasonReject')]")
    reason_rejected = fields.Char(string='Reason rejected')

# Model for CUSTOMER CRS table

class customer_crs(models.Model):
    _name = 'vb.customer_crs'
    _description = 'Customer tax info'

    # field
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    country_id = fields.Many2one(comodel_name='res.country', string='Country of Tax',ondelete='restrict',index=True)
    tax_no = fields.Char(string='Tax Identification No')
    no_tax_flag = fields.Boolean(string='No TIN')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)

# Model for CUSTOMER QUESTIONAIRE RESPONSE table
# This table will provide the combined and updated responses to questionaires provided by the customer
# during account opening

class customer_questionaire(models.Model):
    _name = 'vb.customer_questionaire'
    _description = 'Response to questionaire at customer level'
    _order = 'customer_id,set,section,question_no'

    # fields
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    name = fields.Char(string='Description')
    set = fields.Char(string='Set')
    section = fields.Char(string='Section')
    question_no = fields.Integer(string='Question no')
    question_text = fields.Text(string='Question')
    response = fields.Char(string='Response to question')
    required = fields.Selection([
        ('Yes', 'Yes'),
        ('Conditional', 'Conditional'),
        ], string='Required response')
    scoring = fields.Integer(string='Scoring')
    state = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('None', 'No response'),
       ], string='Status')
    active = fields.Boolean(string='Active',default=True)

# Deprecated - can be removed

class customer_acct_crs(models.Model):
    _name = 'vb.customer_acct_crs'
    _description = 'Customer account tax info'

    # field
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer',ondelete='cascade',index=True)
    country_id = fields.Many2one(comodel_name='res.country', string='Country of Tax',ondelete='restrict',index=True)
    tax_no = fields.Char(string='Tax Identification No')
    no_tax_flag = fields.Boolean(string='No TIN')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)

class terms_n_conditions(models.Model):
    _name = 'vb.terms_n_conditions'
    _description = 'Terms and conditions'


    @api.onchange('state')
    def check_active_state(self):
        if self.state=='Active':
            #select * from vb_terms_n_conditions where state = 'Active'
            recs = self.env['vb.terms_n_conditions'].search([('state','=','Active')])
            if len(recs) >= 1:
                raise ValidationError("Only 1 document allowed to be in active status. Please change your status.")

    @api.constrains('state')
    def save_check_active_state(self):
        if self.state=='Active':
            #select * from vb_terms_n_conditions where state = 'Acitve'
            recs = self.env['vb.terms_n_conditions'].search([('state','=','Active')])
            if len(recs) >= 2:
                raise ValidationError("Only 1 document allowed to be in active status. Please change your status.")

    code = fields.Char(string='Code')
    code_type = fields.Char(string='Code type')
    name = fields.Char(string='Document Name')
    version = fields.Char(string='Version')
    file_location = fields.Char(string='File location')
    start_date = fields.Datetime(string='Start date')
    end_date = fields.Datetime(string='End date')
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Expired', 'Expired'),
       ], string='Status')
    active = fields.Boolean(string='Active',default=True)

class customer_terms_condition(models.Model):
    _name = 'vb.customer_terms_condition'
    _description = 'Customer terms and condition'

    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    date_accepted = fields.Datetime(string='Date accepted')
    tns_code = fields.Char(string='Transaction code')
    state = fields.Char(string='State')
    active = fields.Boolean(string='Active',default=True)
