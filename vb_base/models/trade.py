# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Trade related models
#
# Author: Daniel Tan
# Last updated: 18-August-2017 for V2
#
# ==========================================

from openerp import models, fields, api
from datetime import datetime, timedelta
from openerp.exceptions import ValidationError


# Model for ORDER BOOK table
# This table will store the details of the trade orders made by the customer.
# Whenever an order is received by the front-office (web, smart-device, etc), an order record will be created.
# Whenever the stock exchange sends a confirmation, the status of the order will be updated.
# Please note that matched orders received from stock exchange is stored in the order_matched model.
# Targeted number of records: Very large: Average 100 per customer
# Maintenance: Should purge old records very month

class order_book(models.Model):
    _name = 'vb.order_book'
    _description = 'Order book'
    _order = 'order_time desc'

    # This function will approve force selling in bulk depending on the selected checkbox
    def approve_bulk_force_selling_action_wizard(self, cr, uid, data, context):
        active_ids = context.get('active_ids')
        try:
            form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade', 'force_selling_bulk_approve_wiz_form')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'force_selling_bulk_approve_wiz_form'. Please contact IT Support"))
        return{
                    'name': "Force selling transaction",
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': form_id,
                    'context': {'active_ids': active_ids},
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    }

    # This function will reject force selling in bulk depending on the selected checkbox
    def reject_bulk_force_selling_action_wizard(self, cr, uid, data, context):
        active_ids = context.get('active_ids')
        try:
            form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade', 'force_selling_bulk_reject_wiz_form')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'force_selling_bulk_reject_wiz_form'. Please contact IT Support"))
        return{
                    'name': "Force selling transaction",
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': form_id,
                    'context': {'active_ids': active_ids},
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    }

    # This function will send force sell orders to exchange
    def sendto_exchange_force_selling_action_wizard(self, cr, uid, data, context):
        fmt = "%H:%M:%S"
        datefmt = "%Y-%m-%d"
        time_now = (datetime.now() + timedelta(hours=8)).strftime(fmt)
        date_now = (datetime.now() + timedelta(hours=8)).strftime(datefmt)
        weekno = (datetime.now() + timedelta(hours=8)).weekday()

        sql = "select name, holiday_type from vb_holiday where date_start = '%s'" % date_now
        rec = cr.execute(sql)
        holiday = cr.fetchone()

        # block orders if holiday
        if holiday:
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_msg_wiz_form')[1]
                message = "No force sell orders on %s %s" % (holiday[0], holiday[1])
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_msg_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'context': {'default_message': message},
                        }
        # block orders if weekends
        elif weekno > 5:
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_msg_wiz_form')[1]
                message = "No force sell orders on weekends"
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_msg_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'context': {'default_message': message},
                        }
        # block orders if before opening
        elif time_now < '09:05:00':
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_msg_wiz_form')[1]
                message = "Force selling will only be send to exchange after 9:05am"
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_msg_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        # 'context': {'active_ids': active_ids},
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'context': {'default_message': message},
                        }
        # block orders if between closing and pre-opening time (12:30pm - 2:30pm)
        elif (time_now >= '12:30:00') and (time_now <= '14:30:00'):
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_msg_wiz_form')[1]
                message = "No force sell orders are allowed during this period (12:30pm - 2:30pm)"
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_msg_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'context': {'default_message': message},
                        }
        # block orders if closing
        elif time_now > '16:45:00':
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_msg_wiz_form')[1]
                message = "Market closed, no force sell orders will be accepted"
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_msg_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'context': {'default_message': message},
                        }
        # send to exchange        
        else:
            try:
                form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade','force_selling_sendto_exchange_wiz_form')[1]
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'force_selling_sendto_exchange_wiz_form'. Please contact IT Support"))
            return{
                        'name': "Force selling transaction",
                        'view_type': 'form',
                        'view_mode': 'form',
                        'view_id': form_id,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        }

    # This function will compute the default the value of the order name (which is order number).
    @api.multi
    def name_get(self):
        result=[]
        for rec in self:
            if rec.order_no:
                result.append((rec.id,u"%s" % (rec.order_no)))
            else:
                result.append((rec.id,u"No order number"))
        return result

    #Function to allow only future datetime in expiry_date field
    @api.constrains('expiry_date')
    def expiry_date_validation(self):
        if self.order_type in ['New','Amo','Amend']:
            #Lambda datetime to get the server datetime
            d1 = datetime.now()
            d2 = lambda d1: datetime.now()
            #re-format the datetiem.now 
            d3 = d2(d1).strftime("%Y-%m-%d %H:%M")
            if self.expiry_date <= d3:
                raise ValidationError('Good until date must be future date')

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # field
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Customer account',ondelete='cascade',index=True, domain="[('state','=','Active'),('trade_allowed','=','Yes')]")
    acct_no = fields.Char(size=20,string='Account number')
    cust_name = fields.Char(size=80,string='Customer name',related='acct_id.customer_id.name',readonly=True)
    currency_symbol = fields.Char(size=10,string='Currency')
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Asset',ondelete='restrict',index=True)
    market_symbol = fields.Char(size=20,string='Market symbol')
    trader_id = fields.Many2one(comodel_name='vb.customer',string='Trader',domain="[('code_type','=','Trader')]")
    dlv_basis_id = fields.Many2one(comodel_name='vb.common_code',string='Delivery basis',domain="[('code_type','=','dlv_basis')]")
    order_no = fields.Char(size=20,string='Order number',index=True)
    market_refno = fields.Char(size=20,string='Market reference',index=True)
    order_time = fields.Datetime(string='Order date/time',default=lambda self: fields.datetime.now())
    time_to_release = fields.Datetime(string='Date/time to release',default=lambda self: fields.datetime.now(),help='This is the time when the order is to be released to OMS')
    # 'tran_type_id' field only used during data entry to display selection to determine 'tran_type'
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type')
    tran_type = fields.Char(string='Transaction code')
    #    ('New', 'New order'),
    #    ('Amo', 'After market order'),
    #    ('Cancel', 'Cancel order'),
    #    ('Amend', 'Amend order'),
    buy_sell = fields.Selection([
         ('Buy', 'Buy order'),
         ('Sell', 'Sell order'),
        ], string='Buy/Sell', default='Buy')
    order_type = fields.Selection([
        ('Limit', 'Limit order'),
        ('Market', 'Market order'),
        ('StopLimit', 'Stop limit order'),
        ], string='Order type', default='Limit')
    expiry_type = fields.Selection([
        ('Day', 'Good for the day'),
        ('GTD', 'Good until date'),
        ('GTC', 'Good until cancelled'),
        ], string='Expiry type', default='Day')
    trading_board = fields.Selection([
        ('Normal', 'Normal'),
        ('Odd', 'Odd'),
        ('Buyin', 'Buyin'),
        ], string='Trading board', default='Normal')
    expiry_date = fields.Datetime(string='Good until date/time',default=lambda self: fields.datetime.now())
    # The following fields used only when canceling / replace another order
    appto_order_id = fields.Many2one(comodel_name='vb.order_book',string='Apply to order Id',
                        domain="[('tran_type','in',['New','Amo']),('state','in',['New','New-Amo','Acknowledged','PartialMatched']),('stage', '=', 'Open'),('acct_id','=',acct_id),('asset_id','=',asset_id)]")
    appto_order_no = fields.Char(string='Apply to order no',related='appto_order_id.order_no',store=True,readonly=True)
    limit_price = fields.Float(string='Limit price',digits=(9,4),default=0)
    market_price = fields.Float(string='Market price',digits=(9,4),default=0)
    stop_price = fields.Float(string='Stop price',digits=(9,4),default=0)
    order_qty = fields.Float(string='Order qty',digits=(9,0),default=0)
    order_amt = fields.Float(string='Order amt', digits=(15,2),default=0)
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    matched_amt = fields.Float(string='Matched amt',digits=(15,2),default=0)
    input_src = fields.Selection([
        ('MbrSite', 'Member site'),
        ('SmartDev', 'Smart device'),
        ('SvcRep', 'Service rep'),
        ('System', 'System'),
        ], string='Input source', default='System')
    state = fields.Selection([
        ('New', 'New'),
        ('New-Amo', 'New-AMO'),
        ('Sent', 'Sent'),
        ('Acknowledged', 'Acknowledged'),
        ('CancelAcknowledged', 'Cancel Acknowledged'),
        ('ReplaceAcknowledged', 'Amend Acknowledged'),
        ('Rejected', 'Rejected'),
        ('PendingCancel', 'Pending cancel'),
        ('PendingReplace', 'Pending amend'),
        ('Cancelled', 'Cancelled'),
        ('PartialCancelled', 'Partial cancelled'),
        ('PartialMatched', 'Partial matched'),
        ('Matched', 'Matched'),
        ('Closed', 'Closed'),
        ('Failed', 'Failed'),
        ('ForcesellPending', 'Forcesell pending'),
        ('ForcesellRejected', 'Forcesell rejected'),
        ], string='Status', default='New')
    stage = fields.Selection([
        ('New', 'New'),
        ('Renew', 'Renew'),
        ('Open', 'Open'),
        ('Closed', 'Closed'),
        ], string='Stage', default='New')
    limitstatus = fields.Selection([
        ('Good', 'Within limit'),
        ('QtyBreach', 'Qty breached'),
        ('LimitBreach', 'Limit breached'),
        ], string='Limit status')
    limit_amt = fields.Float(string='Limit amount',digits=(15,2),default=0)
    breached_amt = fields.Float(string='Amount breached',digits=(15,2),default=0)
    available_qty = fields.Float(string='Qty available',digits=(15,2),default=0)
    breached_qty = fields.Float(string='Qty breached',digits=(15,2),default=0)
    active = fields.Boolean(string='Active',default=True)
    order_tran_ids = fields.One2many(comodel_name='vb.order_tran',inverse_name='order_id', string='Order transactions')    
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    brkg_amt = fields.Float(string='Brokerage',digits=(11,2),default=0)
    brkg_gst = fields.Float(string='Brokerage GST',digits=(11,2),default=0)
    stamp_amt = fields.Float(string='Stamp duty',digits=(11,2),default=0)
    stamp_gst = fields.Float(string='Stamp duty GST',digits=(11,2),default=0)
    clearing_amt = fields.Float(string='Clearing fees',digits=(11,2),default=0)
    clearing_gst = fields.Float(string='Clearing fees GST',digits=(11,2),default=0)
    tax_amt = fields.Float(string='Govern tax',digits=(11,2),default=0)
    tax_gst = fields.Float(string='Govern tax GST',digits=(11,2),default=0)
    chrge1_amt = fields.Float(string='Other charges 1',digits=(11,2),default=0)
    chrge1_gst = fields.Float(string='Other charges 1 GST',digits=(11,2),default=0)
    chrge2_amt = fields.Float(string='Other charges 2',digits=(11,2),default=0)
    chrge2_gst = fields.Float(string='Other charges 2 GST',digits=(11,2),default=0)
    chrge3_amt = fields.Float(string='Other charges 3',digits=(11,2),default=0)
    chrge3_gst = fields.Float(string='Other charges 3 GST',digits=(11,2),default=0)
    portfolio_asset_id = fields.Many2one(comodel_name='vb.asset',string='Portfolio asset')
    trade_term = fields.Selection([
        ('Normal','Normal'),
        ('Buyin','Buyin'),
        ('ForceSell','ForceSell'),
        ('Assisted','Assisted'),
        ],string='Trade term', default='Assisted')
    dup_flag = fields.Char(string='Recovery duplicate flag')
    matched_exch = fields.Char(string='Matched with exchange')
    matched_date = fields.Datetime(string='Matched date')
    matched_amt = fields.Float(string='Matched amt',digits=(15,2),default=0)
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    match_status = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ], string='Matched status')
    exch_matching_amt= fields.Float(string='Exchange matching amt',digits=(15,2),default=0)
    exch_matching_qty = fields.Float(string='Exchange matching qty',digits=(9,0),default=0)
    exch_exec_id = fields.Char(string='Exchange execute ID',index=True)
    cds_no = fields.Char(string='CDS no', related='acct_id.cds_no',readonly=True)
    acct_type = fields.Char(string='Account type',related='acct_id.acct_type',readonly=True)
    remarks = fields.Char(string='Remarks')
    exch_secord_id = fields.Char(string='Exchange secondary order ID')
    exch_reason = fields.Char(string='Exchange reason')
    exch_cancel = fields.Char(string='Exchange initiated cancellation')
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Related contract')
    prev_trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Previous related contract')
    previous_state = fields.Char(string='Previous state')
    sent2fo_flag = fields.Boolean(string='Send to FO')
    appto_exec_id = fields.Char(string='Apply to execute ID',related='appto_order_id.exch_exec_id',readonly=True)
    display_status = fields.Char(string='Display status')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer')
    uplifted_order_amt = fields.Float(string='Uplifted order amt', digits=(15,2),default=0)
    uplifted_matched_amt = fields.Float(string='Uplifted matched amt',digits=(15,2),default=0)
    uplifted_order_price = fields.Float(string='Uplifted order price',digits=(9,4),default=0)
    last_done_price = fields.Float(string='Last done price',digits=(9,4),default=0)
    sent_time = fields.Datetime(string='Sent time')
    org_order_qty = fields.Float(string='Original order qty', digits=(9,0),default=0)
    org_matched_qty = fields.Float(string='Original matched qty', digits=(9,0),default=0)
    org_order_amt = fields.Float(string='Original order amt', digits=(15,2),default=0)
    org_matched_amt = fields.Float(string='Original matched amt', digits=(15,2),default=0)
    org_uplifted_order_amt = fields.Float(string='Original uplifted order amt',digits=(15,2),default=0)
    org_uplifted_matched_amt = fields.Float(string='Original uplifted matched amt',digits=(15,2),default=0)
    order_channel = fields.Char(string='Order channel')
    # 15-Aug-2017 - Additional fields for V2
    pickup_flag = fields.Boolean(string='Pickup flag',default=False)
    approve_reject_date = fields.Datetime(string='Date/time approved/rejected forcesell')
    approve_reject_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected forcesell by')
    forcesell_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Force-sell contract')
    unique_key1 = fields.Char(string='Unique key 1',index=True)
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2))
    portfolio_id = fields.Many2one(comodel_name='vb.portfolio',string='Portfolio')
    util_coll_amt = fields.Float(string='Utilised collateral value',digits=(15,2),default=0)    

    # SQL constraint
    _sql_constraints = [('unique_order_no','UNIQUE(order_no,active)','Another order with same order number already exist'),
                        ('unique_key1','UNIQUE(unique_key1,active)','Another order with same key already exist')]    

# Model for ORDER TRANSACTION table
# This table will store the details of the transactions / activity related to orders

class order_tran(models.Model):
    _name = 'vb.order_tran'
    _description = 'Order transaction'
    _order = 'order_id,tran_date desc'
    _rec_name = 'order_no'
    
    # field
    order_id = fields.Many2one(comodel_name='vb.order_book',string='Order',ondelete='cascade',index=True)
    order_no = fields.Char(size=20,string='Order number', related="order_id.order_no", store=True,readonly=True)
    tran_date = fields.Datetime(string="Transaction date/time")
    market_refno = fields.Char(size=20,string='Market reference')
    matched_refno = fields.Char(size=20,string='Matched reference')
    price = fields.Float(string='Price',digits=(9,4),default=0)
    qty = fields.Float(string='Qty',digits=(9,0),default=0)
    amt = fields.Float(string='Amount',digits=(15,2),default=0)
    # One or more orders can be associated with a trade contract (in the case of contract consolidation)
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran', string='Related contract')
    trade_refno = fields.Char(size=20,string='Contract reference',related='trade_tran_id.tran_refno',readonly=True)
    tran_type = fields.Selection([
        ('RECEIVED', 'RcvdFrmCust'),
        ('SENT', 'Sent2Exch'),
        ('REJECTED', 'Rejected'),
        ('ACKNOWLEDGED', 'Acknowledged'),
        ('CANCEL_ACKNOWLEDGED', 'Cancel acknowledged'),
        ('REPLACE_ACKNOWLEDGED', 'Amend acknowledged'),
        ('CANCELLED', 'Cancelled'),
        ('CANCELORG', 'Cancel original'),
        ('REPLACED', 'Amended'),
        ('MATCHED', 'Matched'),
        ('CLOSED', 'Closed'),
        ('FAILED', 'Failed'),
        ('RESTATED', 'Re-stated'),
        ('RENEWED', 'Renewed'),
        ('EXPIRED', 'Expired'),
        ], string='Activity type')
    manual_entry = fields.Selection([
        ('Yes', 'Yes'),
        ('No', 'No'),
        ], string='Manual entry', default='Yes')
    buy_sell = fields.Selection(string='Buy/Sell',related='order_id.buy_sell')
    acct_no = fields.Char(size=20,string='Account number',related='order_id.acct_id.acct_no',readonly=True)
    acct_name = fields.Char(size=80,string='Account name',related='order_id.acct_id.name',readonly=True)
    market_symbol = fields.Char(size=20,string='Market symbol',related='order_id.asset_id.market_symbol',readonly=True)
    cancelling_id = fields.Many2one(comodel_name='vb.order_book',string='Cancelling order')
    cancelling_order_no = fields.Char(size=20,string='Cancelling order number', related="cancelling_id.order_no", store=True)
    matched_exch = fields.Char(string='Matched with exchange')
    matched_date = fields.Datetime(string='Matched date',default=lambda self: fields.datetime.now())
    matched_amt = fields.Float(string='Matched amt',digits=(15,2),default=0)
    matched_qty = fields.Float(string='Matched qty',digits=(9,0),default=0)
    match_status = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ], string='Matched status')
    exch_matching_amt= fields.Float(string='Exchange matching amt',digits=(15,2),default=0)
    exch_matching_qty = fields.Float(string='Exchange matching qty',digits=(9,0),default=0)
    possible_dup = fields.Char(string='Possible duplicate')
    # Values - null, Yes or Ignore
    fix_seqno = fields.Integer(string='FIX sequence number')
    exch_exec_id = fields.Char(string='Exchange execute ID',index=True)
    exch_orgexec_id = fields.Char(string='Exchange execute original ID')
    exch_status_msg = fields.Char(string='Exchange status message')
    exch_secord_id = fields.Char(string='Exchange secondary order ID')
    exch_ord_status = fields.Char(string='Exchange order status')
    exch_reason = fields.Char(string='Exchange reason')
    exch_cancel = fields.Char(string='Exchange initiated cancellation')
    due_date = fields.Date(string='Settlement due date')

# Model for TRADER PROFILE table
# This table will provide trader related parameters and rules to be applied for traders with this profile.
# Targeted number of records: Very small: Estimated 10 records only

class trader_profile(models.Model):
    _name = 'vb.trader_profile'
    _description = 'Trade profile'
    _order = 'name'

    @api.multi
    @api.onchange('default_rec')
    def change_default(self):
        obj = self.env['vb.trader_profile'].search([])
        if self.default_rec == True:
            for rec in obj:
                rec.write({'default_rec':False})
            self.write({'default_rec':True})

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # field
    name = fields.Char(size=80,string='Profile name')
    default_rec = fields.Boolean(string='Default profile',default=False)
    max_trade_limit = fields.Float(string='Maximum trading limit',digits=(15,2),default=0)
    commission_share = fields.Float(string='Commission share %',digits=(5,2),default=0)
    other_share = fields.Float(string='Other share %',digits=(5,2),default=0)
    glacct_no = fields.Char(size=20,string='G/L account - trader control a/c')
    active = fields.Boolean(string='Active',default=True)    

# Model for TRADE PROFILE table
# This table will provide trade related parameters and rules to be applied for customer accounts with this profile.
# Important use of this table is during trade processing and when contracts are generated. It will contain information
# to compute the various charges.
# Targeted number of records: Very small: Estimated 10 records only

class trade_profile(models.Model):
    _name = 'vb.trade_profile'
    _description = 'Trade profile'
    _order = 'name'
    
    # Returns the default market
    @api.model
    def _get_default_market(self):
        market = self.env['vb.market'].search([('default_rec','=',True)],limit=1)
        if market:
            return market.id
        return
    
    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            obj = self.env['vb.trade_profile'].search([('code','!=',self.code)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError ("Sorry there is already existing default trade profile")
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # field
    name = fields.Char(size=80,string='Profile name')
    code = fields.Char(size=20,string='Profile code')
    default_rec = fields.Boolean(string='Default profile',default=False)
    # If specified, this trade profile only applies to this specific market
    market_id = fields.Many2one(comodel_name='vb.market', string='Market',ondelete='restrict',
                                default=_get_default_market,help='Leave blank if profile applies to any markets')
    # Charge IDs to be used to compute the various charges for a trade
    brkg_norm_id = fields.Many2one(comodel_name='vb.charge_code', string='Regular brokerage charge',ondelete='restrict',index=True)
    brkg_buyin_id = fields.Many2one(comodel_name='vb.charge_code', string='Buy-in brokerage charge',ondelete='restrict',index=True)
    clearing_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Clearing charge',ondelete='restrict',index=True)
    stamp_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Stamp duty charge',ondelete='restrict',index=True)
    tax_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Govn svc tax charge',ondelete='restrict',index=True)
    oth_chrg1_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 1',ondelete='restrict',index=True)
    oth_chrg2_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 2',ondelete='restrict',index=True)
    oth_chrg3_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 3',ondelete='restrict',index=True)
    active = fields.Boolean(string='Active',default=True)    
    brkg_forcesell_id = fields.Many2one(comodel_name='vb.charge_code', string='Force selling brokerage charge',ondelete='restrict',index=True)
    brkg_assisted_id = fields.Many2one(comodel_name='vb.charge_code', string='Assisted brokerage charge',ondelete='restrict',index=True)
    buy_qty_limit = fields.Float(string='Buy qty limit per order',digits=(9,0),default=0)
    sell_qty_limit = fields.Float(string='Sell qty limit per order',digits=(9,0),default=0)
    brkg_sell_id = fields.Many2one(comodel_name='vb.charge_code', string='Regular sell brokerage charge',ondelete='restrict',index=True)
    brkg_assisted_sell_id = fields.Many2one(comodel_name='vb.charge_code', string='Assisted sell brokerage charge',ondelete='restrict',index=True)
    #Added by nandha -- 25-01-2017
    biglot_buy_qty_limit = fields.Float(string='Buy qty limit per order',digits=(9,0),default=0)
    biglot_sell_qty_limit = fields.Float(string='Buy qty limit per order',digits=(9,0),default=0)
    
    # SQL constraint
    _sql_constraints = [('unique_profile_code','UNIQUE(code,active)','Another trade profile with the same code already exist'),
                        ('unique_profile_name','UNIQUE(name,active)','Another trade profile with the same name already exist')]    

# Model for TRADE PROFILE RANGE table
# This table will provide additional information about applicable charges if range rule is applicable
# Targeted number of records: Very small: Estimated 10 records per trade profile

class trade_profile_range(models.Model):
    _name = 'vb.trade_profile_range'
    _description = 'Trade profile range'
    _rec_name = 'trade_profile_id'
    
    # field
    trade_profile_id = fields.Many2one(comodel_name='vb.trade_profile', string='Trade profile',ondelete='cascade',index=True)
    value_from = fields.Float(string='Value from',default=0)
    value_to = fields.Float(string='Value to',default=0)
    brkg_norm_id = fields.Many2one(comodel_name='vb.charge_code', string='Regular brokerage charge',ondelete='restrict',index=True)
    brkg_buyin_id = fields.Many2one(comodel_name='vb.charge_code', string='Buy-in brokerage charge',ondelete='restrict',index=True)
    clearing_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Clearing charge',ondelete='restrict',index=True)
    stamp_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Stamp duty charge',ondelete='restrict',index=True)
    tax_chrg_id = fields.Many2one(comodel_name='vb.charge_code', string='Govn tax charge',ondelete='restrict',index=True)
    oth_chrg1_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 1',ondelete='restrict',index=True)
    oth_chrg2_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 2',ondelete='restrict',index=True)
    oth_chrg3_id = fields.Many2one(comodel_name='vb.charge_code', string='Other charge 3',ondelete='restrict',index=True)
    brkg_forcesell_id = fields.Many2one(comodel_name='vb.charge_code', string='Force selling brokerage charge',ondelete='restrict',index=True)

