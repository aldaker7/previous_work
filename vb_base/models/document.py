# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Models for Document module
#
# Author: Daniel Tan
# Last updated: 15-August-2016
#
# ==========================================

import os
from openerp import models, fields, api
from openerp.exceptions import ValidationError
from datetime import datetime

# Model for CENTRAL DOCUMENT table
# Actual physical storage of documents for this table should be in a separate file store

class document(models.Model):
    _name = 'vb.document'
    _description = 'Central document store'
    _order = 'name'
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    #this method to get the default value
    @api.model
    def _get_default_doc_class(self):
        document_class = self.env['vb.common_code'].search([('default_rec','=',True),('code_type','=','DocClass')],limit=1)
        if document_class:
            return document_class.id
        return

    @api.constrains('file_name')
    def get_type_immediately(self):
        #taking the document 
        ext= self.env['vb.common_code'].search([('code_type','=','DocType')])
        count = 0
        if self.file_name != False:
            for rec in ext:
                filename, file_extension = os.path.splitext(self.file_name)
                if file_extension[1:].lower() == rec.code.lower():
                    count += 1
                    break
            if count == 0:
                raise ValidationError('You have entered improper file')
            if len(self.file_name) > 50:
                #it is 50 but last 4-5 char are reserved by .docx etc
                raise ValidationError ('Please check your file name length it should be less than 45 letters')
        else:
            if self.document == False:
                self.file_name=u''
#    method to key in only future dates only 
    @api.constrains("date_valid")
    def doc_date_valid(self):
        if self.date_valid:
            d1=datetime.now()
            d2=datetime.strptime(self.date_valid, '%Y-%m-%d')
            if d1 > d2:
                raise ValidationError('Document valid until date must be future date.')
        return True

#   method to match customer id with customer account.
    @api.onchange('customer_id')
    def acct_def_value(self):
        self.acct_id = u''

    # fields
    name = fields.Char(size=80,string='Document name',index=True)
    is_open_exist = fields.Char(related='doc_class_id.code',string='Is check',help='to check about riskcheck one2many if it is exist',store=True,readonly=True)
    doc_class_id = fields.Many2one(comodel_name='vb.common_code',string='Document class',ondelete='restrict',domain="[('code_type','=','DocClass')]",default=_get_default_doc_class)
    doc_type_id = fields.Many2one(comodel_name='vb.common_code',string='Document type',ondelete='restrict',domain="[('code_type','=','DocType')]")
    doc_version = fields.Char(size=20,string='Doc version')
    file_loc = fields.Char(string='File location',help='This is the name of the file attachment')
    file_name = fields.Char(string='File name attachment',help='This is the name of the file attachment')
    file_name_related = fields.Char(related='file_name', string='File name',help='This is a related field only to show data. The data of FileName is in file_name field.')
    notes = fields.Text(string='Notes and comments')
    date_valid = fields.Date(string='Valid until',help='This indicate until when the document content is valid.')
    # The next 3 fields are optional and only if document is associated to the customer or customer account level
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='set null',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='set null',index=True,domain="[('customer_id','=',customer_id)]")
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    # The next field is optional and only if document is associated to a cash transaction (e.g. deposit)
    cash_tran_id = fields.Many2one(comodel_name='vb.cash_tran', string='Cash transaction',ondelete='cascade',index=True, domain="[('acct_id', '=', acct_id)]")
    # The next field is optional and only if document is associated to an asset transaction (e.g. CDS transfer out)
    asset_tran_id = fields.Many2one(comodel_name='vb.asset_tran', string='Asset transaction',ondelete='cascade',index=True, domain ="[('acct_id','=',acct_id)]")
    submit_date = fields.Datetime(string='Date/time submitted')
    submit_by = fields.Many2one(comodel_name='res.users',string='Submitted by',ondelete='restrict')
    apprej_date = fields.Datetime(string='Date/time approved/rejected')
    apprej_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected by',ondelete='restrict')
    archived_date = fields.Datetime(string='Date/time archived')
    archived_by = fields.Many2one(comodel_name='res.users',string='Archived by',ondelete='restrict')
    document = fields.Binary('Document', attachment=True)
    permission = fields.Selection([
        ('Public', 'Public'),
        ('MemberAll', 'All members'),
        ('MemberInd', 'Individual member'),
        ('Internal', 'Internal user only'),
        ], string='Permission', default='Internal')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Checked'),
        ('Rejected', 'Rejected'),
        ('Archived', 'Archived'),
        ], string='Status', default='New')
    doc_date = fields.Date(string='Document date',help='For use with statements')
    # 13-Jan-2017 - These fields used
    publish_freq = fields.Selection([
        ('InvestIdea', 'Investment Idea'),
        ('Quarterly', 'Company Report'),
        ('TechAnalysis', 'Technical Analysis'),
        ('Daily', 'Daily'),
        ('Weekly', 'Sector Report'),
        ('Monthly', 'Economic Report'),
#         ('BiWeekly', 'Bi-Weekly'),        
#         ('BiMonthly', 'Bi-Monthly'),
#         ('HalfYearly', 'Half-Yearly'),
#         ('Yearly', 'Yearly'),
#         ('AsAndWhen', 'As and When'),
        ], string='Publication frequency', default='Monthly')
    publish_period = fields.Char(string='Publication period')
    publish_year = fields.Char(string='Publication year')
    report_by = fields.Char(string='Report by')
    report_type_id = fields.Many2one(comodel_name='vb.common_code',string='Publication report type',ondelete='restrict',domain="[('code_type','=','DocRptType')]")
    report_type = fields.Char(string='Report type',related='report_type_id.name',store=True,readonly=True)
    # 20-Jan-2017
    download_count = fields.Integer(string='Download/read count')
    # 24-Jan-2017
    doc_desc = fields.Char(string='Document description')
    rr_category_id = fields.Many2one(comodel_name='vb.common_code',string='Research report category',ondelete='restrict',domain="[('code_type','=','RRCategory')]")
    rr_category_name = fields.Char(string='Research report category name',related='rr_category_id.name',store=True,readonly=True)
    rr_company = fields.Char(string='Research report company name')
    rr_notes = fields.Text(string='Research report notes and comments')
    list_code = fields.Char(string="List code", index=True)
    list_seq = fields.Integer(string='List sequence')
    header1 = fields.Char(string="Header 1")
    header2 = fields.Char(string="Header 2")
    desc1 = fields.Char(string="Description 1")
    desc2 = fields.Char(string="Description 2")
    header1_html = fields.Html(string="Header HTML 1")
    header2_html = fields.Html(string="Header HTML 2")
    desc1_html = fields.Html(string="Description HTML 1")
    desc2_html = fields.Html(string="Description HTML 2")
    main_content_html = fields.Html(string='Main HTML content')
    # 01-Aug-2017 - for margin
    rel_doc_id = fields.Many2one(comodel_name='vb.document', string='Related document',ondelete='set null',index=True)    
