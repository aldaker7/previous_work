# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Base models
#
# Author: Daniel Tan
# Last updated: 23-September-2016
#
# ==========================================

from openerp import models, fields,api
# from datetime import datetime

# Model for COMMON_WIZARD transient table
# 29-Aug-2016 DT: New transient table to be used by most wizards

class common_wizard(models.TransientModel):

    _name = 'vb.common_wizard'

    # Returns the default calendar
    @api.model
    def _get_last_closing(self):
        rec = self.env['vb.config'].search([('code_type','=','App'),('code','=','LastClosingDate')])
        if rec:
            if rec.date1:
                return rec.date1
        return
    
    @api.model
    def _get_last_trading(self):
        rec = self.env['vb.config'].search([('code_type','=','App'),('code','=','LastTradingDate')])
        if rec:
            if rec.date1:
                return rec.date1
        return
    
    # fields for use on a range basis
    post_date = fields.Date(string="Post date",default=lambda self: fields.date.today())
    last_closing_date = fields.Date(string="Last closing date",default=_get_last_closing)
    last_trading_date = fields.Date(string="Last trading date",default=_get_last_trading)
    start_date1 = fields.Date(string="From date")
    end_date1 = fields.Date(string="To date")
    start_date2 = fields.Date(string="From date")
    end_date2 = fields.Date(string="To date")
    start_customer = fields.Char(string="From customer")
    end_customer = fields.Char(string="To customer")
    start_account = fields.Char(string="From account")
    end_account = fields.Char(string="To account")
    start_code = fields.Char(string="From code")
    end_code = fields.Char(string="To code")
    start_refno = fields.Char(string="From reference")
    end_refno = fields.Char(string="To reference")
    start_appno = fields.Char(string="From applied-to reference")
    end_appno = fields.Char(string="To applied-to reference")
    start_datetime = fields.Datetime(string="From when")
    end_datetime = fields.Datetime(string="To when")
#     cds_no = fields.Char(string='CDS no',copy=False)

    # fields for use on a single basis
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account')
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type')
    tran_type = fields.Char(string="Transaction type")
    appto_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Applied-to transaction type')
    appto_tran_type = fields.Char(string="Applied-to Transaction type")
    currency_id = fields.Many2one('vb.currency', 'Asset')
    market_id = fields.Many2one('vb.market', 'Asset')
    asset_id = fields.Many2one('vb.asset', 'Asset')
    
    product_ids = fields.Many2many('vb.common_code', 'wizard_commoncode_rel', 'wizard_id', 'commoncode_id', string='Code 1')
    
    code1_id = fields.Many2one(comodel_name='vb.common_code', string='Code 1')
    code2_id = fields.Many2one(comodel_name='vb.common_code', string='Code 2')
    code3_id = fields.Many2one(comodel_name='vb.common_code', string='Code 3')
    charge_id = fields.Many2one('vb.charge_code', 'Charge')
    user_id = fields.Many2one(comodel_name='res.users',string='User')
    state = fields.Char(string="Status")
    state1 = fields.Selection([
        #('Basic', 'Basic signup only'),
        #('New', 'New'),
        ('Active', 'Active'),
        ('Rejected', 'Rejected'),        
        ('Suspended', 'Suspended'),
        ('Dormant', 'Dormant'),
        ('Closed', 'Closed'),
        ('Archived-Rejected', 'Archived-Rejected'),
        #('SuspendedBuy', 'Suspended from buying'),
        #('SuspendedSell', 'Suspended from selling'),
        #('PendingClose', 'Pending close'),       
        #('Inactive', 'In-Active'),
        #('Uplifted', 'Uplifted'),
        
        ], string='Status')
    state2 = fields.Selection([
        ('New', 'New'),
        ('Reconciled', 'Reconciled'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ], string='Status')
    state3 = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('ByPass', 'By pass'),
        ], string='Status')
    #Added by Nandha 20-06-2017
    state4 = fields.Selection([
        ('pdf', 'PDF'),
        ('xls', 'XLS'),        
        ], string='Report Type', default='pdf')
    #Added by Hazarath 19-10-2017
    state6 = fields.Selection([
        ('xls', 'XLS'),
        ], string='Report Type', default='xls')
    #download_url = fields.Char('Download')
    file_name = fields.Char(string='File Name', size=64)
    file =  fields.Binary('File')
    reason_suspend=fields.Many2one("vb.common_code","Reason For Suspend", domain = [('code_type','=', 'ReasonSus')])
    reason_closed = fields.Many2one("vb.common_code","Reason For Close",domain = [('code_type','=', 'ReasonCls')])
    reason_uplift = fields.Many2one("vb.common_code","Reason For Uplift",domain = [('code_type','=', 'ReasonUplift')])
    reason_reject = fields.Many2one("vb.common_code","Reason For Reject",domain = [('code_type','=', 'ReasonAcctRej')])
    note = fields.Text(string='Note')
    disable_login = fields.Boolean(string='Disable login (if customer has 1 activated account)',default=False,help='This check box will check if the customer has only 1 activated account and if you checked it then the login will be disabled')
    reason_void = fields.Many2one("vb.common_code","Reason For void",domain = [('code_type','=', 'ReasonTranCancel')])
    reason_cancel = fields.Many2one("vb.common_code","Reason For cancel",domain = [('code_type','=', 'ReasonTranCancel')])
    reason_incomplete = fields.Many2one("vb.common_code","Reason For Incomplete",domain = [('code_type','=', 'ReasonInc')])
    # 19 June 2017
    reason_reject_1 = fields.Many2one("vb.common_code","Reason For Reject",domain = [('code_type','=', 'ReasonRej')])
    #Added 10-Aug-2017
    user_ids = fields.Many2many('res.users', 'wizard_users_rel', 'wizard_id', 'resusers_id', string='User')
    # added 11-Sept-2017 for List Of CDS Share Deposit/Withdrawal Report
    state5 = fields.Selection([
        ('New', 'New'),
        ('Failed', 'Failed'),
        ('Cancelled', 'Cancelled'),
        ('Rejected', 'Rejected'),
        ('Confirmed', 'Confirmed'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
    ], string='Status')
    # added for temp assets wizard form 11-Nov-2017
    batch_no = fields.Char(string='Batch no')
    product_type = fields.Many2one("vb.common_code","Product",domain = [('code_type','=', 'AcctClass')])
    # force selling report status
    forcesell_status = fields.Selection([
        ('All', 'All'),
        ('ForcesellPending', 'Pending Approval'),
        ('New-Amo', 'Pending Match'),
        ('Cancelled', 'Cancelled'),
        ('Rejected', 'Rejected'),
        ('Matched', 'Matched'),
        ('ForcesellRejected', 'Rejected-internal'),
    ], string='Status', default='All')
    # Cash Deposit Exception report
    cash_deposit_status = fields.Selection([
        ('New', 'New'),
        ('Void', 'Void'),
        ('Failed', 'Failed'),
        ('Rejected', 'Rejected'),
        ('Cancelled', 'Cancelled'),
    ], string='Status') 
    # List of Corporate Action Subscription Report     
    date_type  = fields.Selection([
        ('Subscribed', 'Subscribed Date'),
        ('KIBB','KIBB Closing Date'),
        ], string='Date Type', default='Subscribed')   
