import sys, traceback
import os
import base64
from openerp import models, api
from openerp.addons.jasperserver.rest import Client
from openerp.addons.jasperserver.exceptions import *
from openerp.addons.jasperserver.services import Report
from openerp.addons.jasperserver.exceptions import JsException
from openerp.exceptions import Warning
from openerp.addons.jasperserver.services import Resource
from openerp.addons.jasperserver.services import Resources
from datetime import datetime

class jasper_server(models.Model):
    _name = 'vb.jasper'
    
    def connect_jasper_server_client(self):
        '''This method returns you the Jasper Server Path directory.'''
        jasper_server_rec = self.env['vb.config'].search([('code','=','JasperServer'),('code_type','=','Svr')])[0]
        if not jasper_server_rec:
            raise Warning('The Jasper Configuration doesnt exist. Please set the Jasper Configuration in General > Others > System Config > Jasper Server')
        try:
            client = Client('http://'+jasper_server_rec.parm1+':'+ jasper_server_rec.parm2+'/jasperserver', jasper_server_rec.parm3, jasper_server_rec.parm4)
        except JsException:
            traceback.print_exc()
            raise Warning("Invalid Jasper Configuration. Please check System / System Parameters / Jasper Report Server parameters")
        return client
    
    def get_jasper_report_cds_transferout(self, client, report_name, params, filename):
        document_directory = self.env['vb.config'].search([('code','=','RepDir'),('code_type','=','Sys')])[0].parm1
        report = Report(client, document_directory)
        params = params or False
        report_result = report.run(report_name, output_format='pdf', params=params)
        return base64.b64encode(report_result)
    
    def get_jasper_report(self, client, report_name, params, filename):
        
        document_directory = self.env['vb.config'].search([('code','=','RepDir'),('code_type','=','Sys')])[0].parm1
        report = Report(client, document_directory)
        
        params = params or False   
        report_result = report.run(report_name, output_format='pdf', params=params)
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(report_result),'file_name':filename})
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }
    
    def get_jasper_report_csv(self, client, report_name, params, filename):
        
        document_directory = self.env['vb.config'].search([('code','=','RepDir'),('code_type','=','Sys')])[0].parm1
        report = Report(client, document_directory)
        
        params = params or False   
        report_result = report.run(report_name, output_format='csv', params=params)
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(report_result),'file_name':filename})
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }
    def get_jasper_report_xls(self, client, report_name, params, filename):
        
        document_directory = self.env['vb.config'].search([('code','=','RepDir'),('code_type','=','Sys')])[0].parm1
        report = Report(client, document_directory)
        
        params = params or False   
        report_result = report.run(report_name, output_format='xls', params=params)
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(report_result),'file_name':filename})
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }
