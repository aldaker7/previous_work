# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Base models
#
# Author: Daniel Tan
# Last updated: 15-August-2017
#
# ==========================================

from openerp import models, fields, api
from datetime import datetime
from openerp.exceptions import ValidationError, Warning

# Model for BRANCH table
# This table stores the information for each company branch, which can be a physical or a virtual (kiosk) branch.
# There will be at least ONE branch and this is the main branch.  From this main branch, the address will be the main company address.

class users(models.Model):
    _inherit = 'res.users'
    
    t_pin = fields.Char('T-pin')
    
class branch(models.Model):
    _name = 'vb.branch'
    _description = 'Company branch'
    _order = 'name'
    
    # Returns the default calendar
    @api.model
    def _get_default_calendar(self):
        rec = self.env['vb.calendar'].search([('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return
    
    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return

    @api.constrains('branch_type')
    def check_main_branch(self):
        if self.branch_type == 'Main':
            recs = self.env['vb.branch'].search([('id','!=',self.id)])
            for rec in recs:
                if rec.branch_type == 'Main':
                    raise ValidationError('There is already a main branch')
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
        
    # This function will derive a address field suitable for showing on display or report
    @api.depends('address1','address2','address3','city','postcode','state_id','country_id')
    def _get_branch_address(self):
        for rec in self:
            if rec.address1:
                addr = rec.address1
                if rec.address2:
                    addr = addr+u',\n'+rec.address2
                if rec.address3:
                    addr = addr+u',\n'+rec.address3
                if rec.city:
                    addr = addr+u',\n'+rec.city
                if rec.postcode:
                    addr = addr+u',\nPostcode '+rec.postcode
                if rec.state_id:
                    addr = addr+u',\n'+rec.state_id.name
                if rec.country_id:
                    addr = addr+u',\n'+rec.country_id.name
            else:
                addr = u''
                                                    
            rec.display_address = addr
            
    # fields
    name = fields.Char(size=100,string='Branch name',index=True)
    code = fields.Char(size=20,string='Branch code',index=True)
    branch_type = fields.Selection([
        ('Kiosk', 'Kiosk'),
        ('Branch', 'Physical branch'),
        ('Main', 'Main'),
        ], string='Branch type', default='Branch',help='There should only be on main branch.  The company address is obtained from the main branch.')
    mgr_name = fields.Char(size=100,string='Name of manager')
    mgr_mobile = fields.Char(size=20,string='Mobile number of manager')
    mgr_email = fields.Char(size=40,string='Email of manager')
    address1 = fields.Char(size=100,string='Address 1',)
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=100,string='City',)
    postcode = fields.Char(size=20,string='Postal code',)
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    country_id = fields.Many2one(comodel_name='res.country', string='Country',default=_get_default_country,ondelete='restrict')
    calendar_id = fields.Many2one(comodel_name='vb.calendar',string='Calendar',default=_get_default_calendar,ondelete='restrict')
    display_address = fields.Char(string='Display address',compute='_get_branch_address',store=True)
    phone1 = fields.Char(size=15,string='Primary phone')
    phone2 = fields.Char(size=15,string='Alternate phone')
    fax = fields.Char(size=15,string='Branch fax phone')
    active = fields.Boolean(string='Active',default=True)    
    geolocation = fields.Char(string='Geo location',help='This can be a GPS location or some map reference')
    geopicture = fields.Binary(string='Picture of geolocation',help='This is the picture of the location.  Can be screen capture of map.')
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Activated'),
        ('Inactive', 'Inactive'),
        ('Closed', 'Closed'),
        ], string='Status', default='New')
    # 20-Jan-2017
    website = fields.Char(string='Web site address')

    # SQL constraint
    _sql_constraints = [('unique_branch_code','UNIQUE(code,active)','Another branch with the same code already exist'),
                        ('unique_branch_name','UNIQUE(name,active)','Another branch with the same name already exist')]
    
# Model for CHARGE table
# This table store the various charges, the rates associated with the charges and the G/L account.
# The actual rates are defined in another table.

class charge_code(models.Model):
    _name = 'vb.charge_code'
    _description = 'Charge code'
    _order = 'list_seq,code'
    
    # Method for checking start range and end range
    @api.constrains('range_ids')
    def range_check(self):
        index =-1
        if self.range_ids:
            list_ids=[]
            for rec in self.range_ids:
                if rec.range_end < rec.range_start:
                    raise ValidationError("Your end range must be greater than start range.")    
            for i in self.range_ids:
                list_ids.append(i.id)
                index +=1
            obj= self.env['vb.charge_range']
            compare_index=0
            while index > compare_index:
                if obj.search([('id','=',list_ids[compare_index])]).range_end > obj.search([('id','=',list_ids[compare_index+1])]).range_start:
                    raise ValidationError("Start range of current entry should be greater than previous end range")
                else:
                    compare_index = compare_index +1
                    
    @api.constrains('rate_pct','input_rnd_amt','result_rnd_amt','charge_min','charge_max')
    def restrict_digits(self):
        if len(str(int(self.rate_pct))) > 3:
            raise ValidationError("No more than 3 chars in before the point ")
        if len(str(int(self.input_rnd_amt))) > 5:
            raise ValidationError("No more than 5 digits in rounding amt for input ")
        if len(str(int(self.result_rnd_amt))) > 5:
            raise ValidationError("No more than 5 digits in rounding amt ")
        if len(str(int(self.charge_min))) > 9:
            raise ValidationError("No  more than 9 in minimum amount ")
        if len(str(int(self.charge_max))) > 13:
            raise ValidationError("No more than 13 digits in maximum amount ")

    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            obj = self.env['vb.charge_code'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise Warning ("Sorry there is already existing default charge code")
                    break   
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
             
    # fields
    name = fields.Char(size=100,string='Description',index=True)
    code = fields.Char(size=20,string='Charge code',index=True)
    default_rec = fields.Boolean(string='Default charge code',default=False)
    grace_period = fields.Float(string='Grace period',digits=(3,0),default=0)
    grace_period_type = fields.Selection([
         ('Calendar', 'Calendar days'),
         ('Work','Working days'),
     ],string='Grace period type', default='Calendar')
    # Charges that is a revenue to the business may attract GST
    charge_type = fields.Selection([
         ('PassThru', 'Pass thru'),
         ('Revenue','Revenue'),
         ('GST','GST'),
         ('Interest','Interest'),
     ],string='Charge type', default='PassThru')
    # The following 3 fields define the G/L account numbers to be used during extraction of financial
    # values for G/L purpose.  Note that if glacct_no_dr or glacct_no_cr is not specified, then
    # glacct_no will be used.
    glacct_no = fields.Char(size=20,string='G/L code')
    glacct_no_shr = fields.Char(size=20,string='G/L code (share)')
    glacct_no_tax = fields.Char(size=20,string='G/L code (GST)')
    # The following 2 fields define the upper and lower charge amount.
    # If left blank, there will be no restriction
    charge_min = fields.Float(string='Minimum amount',digits=(11,2),default=0)
    charge_max = fields.Float(string='Maximum amount',digits=(15,2),default=0)
    # This math expression field is to be used only when support libraries (like JEP in Java) can be used.
    math_expression = fields.Char(string='Math expression',help='To be used when a math expression parser is available')
    charge_method = fields.Char(string='Charge method')
    # The following 2 fields specify how to round the result value before being used
    # to compute the charge amount
    input_rnd_amt = fields.Float(string='Rounding amt for input',digits=(15,2),default=1,
                                 help='Round the input value by')
    input_rnd_opt = fields.Selection([
                                ('Up', 'Round up'),
                                ('Down','Round down'),
                                ('HalfAdj','Half-adjust'),
                                ('None','None'),
                              ],string='Rounding opt for input', default='None')
    # The charge about can be compute using a fixed amount method or a percentage method.
    # If the rate percentage is filled, then system will assume a percentage method is applied.
    rate_pct = fields.Float(string='Rate %',digits=(7,4),default=0)
    rate_type = fields.Selection([
         ('Absolute', 'Absolute'),
         ('BLR','BLR'),
     ],string='Rate type', default='Absolute')
    fixed_amt = fields.Float(string='Fixed value',digits=(15,2),default=0)
    per_value_of = fields.Float(string='Per value of',digits=(15,2),default=1)
    # The following 2 fields define how to round the result charge value
    result_rnd_amt = fields.Float(string='Rounding amt for result',digits=(15,2),default=1)
    result_rnd_opt = fields.Selection([
                                ('Up', 'Round up'),
                                ('Down','Round down'),
                                ('HalfAdj','Half-adjust'),
                                ('CustomAdj','Custom-adjust'),
                #                ('None','None'),
                              ],string='Rounding opt for result')
    # The following 4 fields applies only if there is a new set of charge rates/rules that will apply
    # from a certain date onwards.
    # 23-Aug-2016 DT: added effective rate and new amount if look-up to new charge not required
    effective_date = fields.Datetime(string="Effective start of new rate/amt")
    new_rate = fields.Float(string="New rate",degitis=(7,4),default=0)
    new_fixed_amt = fields.Float(string="New fixed amount",degitis=(15,2),default=0)
    new_charge_id = fields.Many2one(comodel_name='vb.charge_code', string='New charge code',ondelete='restrict')
    # Charge sharing (usually applies for brokerage/commission charges)
    charge_share_pct = fields.Float(string='Charge sharing %',digits=(5,2),default=0)
    charge_share_max = fields.Float(string='Charge sharing maximum',digits=(15,2),default=0)
    # Other non-functional record fields
    active = fields.Boolean(string='Active',default=True)    
    list_seq = fields.Integer(string='List order')
    gst_charge_id = fields.Many2one(comodel_name='vb.charge_code', string='GST charge code',ondelete='restrict')
    gst_code = fields.Char(size=20,string='GST code')
    effective_end_date = fields.Datetime(string="Effective end new rate/amt")
    range_ids = fields.One2many(comodel_name='vb.charge_range', inverse_name='charge_id', string='Range values')
    range_type = fields.Selection([
         ('Range', 'Range'),
         ('Tier','Tier'),
     ],string='Range type', default='Range')

    # SQL constraint
    _sql_constraints = [('unique_charge_code','UNIQUE(code,active)','Another charge with the same code already exist'),
                        ('unique_charge_name','UNIQUE(name,active)','Another charge with the same name already exist')]

# Model for CHARGE RANGE table
# This table store the applicable rates or fix amt for a range of values for those charge codes whose
# charge_method is either Range or Tier
# Table added: 20-Nov-2016

class charge_range(models.Model):
    _name = 'vb.charge_range'
    _description = 'Charge range'
    _order = 'charge_id,range_no'
    
    # Range check method
    @api.onchange('range_end')
    def range_check(self):
        if self.range_end < self.range_start:
            raise ValidationError("Your end range must be greater than start range.")
    
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge code',ondelete='cascade',index=True)
    range_no = fields.Integer(string='Line no',digits=(3,0),default=1)
    range_start = fields.Float(string='Start of range',digits=(15,2))
    range_end = fields.Float(string='End of range',digits=(15,2))
    rate_pct = fields.Float(string='Rate %',digits=(7,4))
    fixed_amt = fields.Float(string='Fixed value',digits=(15,2))    
    new_rate = fields.Float(string='New rate %',digits=(7,4))
    new_fixed_amt = fields.Float(string='New fixed value',digits=(15,2))   

# Model for COMMON CODE table
# This table store various codes used through the system.The field 'code_type' shows the list of codes and
# this list will be extended from time to time
# Each code can also contain one or more parameters that the application can use further for processing.

class common_code(models.Model):
    _name = 'vb.common_code'
    _description = 'Common code table'
    _order = 'code_type,list_seq,code'

    @api.onchange('code1_id')
    def _onchange_code1(self):
        for rec in self:
            if rec.code1_id:
                rec.parm1 = rec.code1_id.code

    @api.onchange('code2_id')
    def _onchange_code2(self):
        for rec in self:
            if rec.code2_id:
                rec.parm2 = rec.code2_id.code

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            self.update({'code': self.country_id.code})

    @api.constrains('code')
    def check_code(self):
        for rec in self:
            if rec.code_type == 'Countries' and self.country_id.code != self.code:
                    raise ValidationError("Wrong country code")

    @api.constrains('parm5')
    def unique_code_source(self):
        if self.parm5:
            rec = self.env['vb.common_code'].search([('parm5','=',self.parm5)])
            if len(rec)>1:
                raise ValidationError("Source unique code is duplicated with another info source.")

    @api.model
    def create(self, vals):
        if vals.get("country_id", False):
            res = self.env['res.country'].search([('id', '=', vals['country_id'])], limit=1)
            vals['code'] = res.code
        res = super(common_code, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if vals.get("country_id", False):
            res = self.env['res.country'].search([('id', '=', vals['country_id'])], limit=1)
            vals['code'] = res.code
        res = super(common_code, self).write(vals)
        return res

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
            
    @api.constrains('default_rec')
    def check_default_rec(self):
        obj = self.env['vb.common_code']
        if self.code_type == 'AcctAttr':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','AcctAttr'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default account attribute")
                        break
        elif self.code_type == 'PersonTitle':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','PersonTitle'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default title")
                        break
        elif self.code_type == 'ReasonCls':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','ReasonCls'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default closing reason")
                        break
        elif self.code_type == 'ReasonSus':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','ReasonSus'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default suspension reason")
                        break
        elif self.code_type == 'ReasonUplift':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','ReasonUplift'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default uplift reason")
                        break
        elif self.code_type == 'IdType':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','IdType'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default ID type")
                        break
        elif self.code_type == 'CustClass':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','CustClass'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default customer class")
                        break
        elif self.code_type == 'DocClass':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','DocClass'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default document class")
                        break
        elif self.code_type == 'Race':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','Race'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default race")
                        break
        elif self.code_type == 'Occupation':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','Occupation'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default industries")
                        break
        elif self.code_type == 'Countries':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','Countries'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default countries")
                        break
        elif self.code_type == 'Businesses':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','Businesses'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default businesses")
                        break
        elif self.code_type == 'TranMode':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','TranMode'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default payment code")
                        break
        elif self.code_type == 'AssetClass':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','AssetClass'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError("Sorry there is already existing default asset class")
                        break
        elif self.code_type == 'ReasonUplift':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','ReasonUplift'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError ("There is already existing default reason for uplift")
                        break
        elif self.code_type == 'CustAttr':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','CustAttr'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError ("There is already existing default customer attribute")
                        break
        elif self.code_type == 'Bank':
            if self.default_rec == True:
                for rec in obj.search([('code_type','=','Bank'),('id','!=',self.id)]):
                    if rec.default_rec == True:
                        raise ValidationError ("There is already existing default Bank")
                        break
                    
        
    # fields
    name = fields.Char(size=100,string='Description',index=True)
    code = fields.Char(size=30,string='Code',index=True)
    code_type = fields.Char(string='Code type',index=True)
    #    ('AcctAttr', 'Additional account attributes'),
    #    ('AcctClass', 'Account classification'),
    #    ('AcctType', 'Account type'),
    #    ('AssetClass', 'Asset class'),
    #    ('Bank', 'Bank'),
    #    ('CustAttr', 'Additional customer attribute'),
    #    ('CustClass', 'Customer classification'),
    #    ('DlvBasis', 'Delivery basis'),
    #    ('DocClass', 'Document class'),
    #    ('DocType', 'Document type'),
    #    ('FrontEnd', 'Front-end service'),
    #    ('IdType', 'Identification type'),
    #    ('KbClass', 'Knowledge class'),
    #    ('NewsFeed', 'News feed'),
    #    ('LoyaltyPgm', 'Loyalty programme'),
    #    ('Occupation', 'Occupation'),
    #    ('Race', 'Race'),
    #    ('Reason', 'Reason'),
    #    ('ReasonCls', 'Closing reason'),
    #    ('ReasonSus','Suspension reason'),
    #    ('ReasonAcctRej','Reject reason'),
    #    ('ReasonTranCancel','Cancel reason'),
    #    ('Other','Other'),
    #    ('PersonTitle','Person title'),
    #    ('TranMode','Transaction mode'),
    #    ('CaseCat','Case category'),
    #    ('CaseActType','Case activity type'),
    note = fields.Text(string='Notes')
    default_rec = fields.Boolean(string='Default code',default=False)
    # The following 3 fields define the G/L account numbers to be used during extraction of financial
    # values for G/L purpose.  Note that if glacct_no_dr or glacct_no_cr is not specified, then
    # glacct_no will be used.
    glacct_no = fields.Char(size=20,string='G/L account')
    glacct_no_dr = fields.Char(size=20,string='G/L account debit side')
    glacct_no_cr = fields.Char(size=20,string='G/L account credit side')
    # Application defined parameter values.
    # How each of the parameters will be used will be determined within each application.
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    amt1 = fields.Float(string='Amount 1')
    amt2 = fields.Float(string='Amount 2')
    amt3 = fields.Float(string='Amount 3')
    amt4 = fields.Float(string='Amount 4')
    amt5 = fields.Float(string='Amount 5')
    date1 = fields.Datetime(string='Date 1')
    date2 = fields.Datetime(string='Date 2')
    date3 = fields.Datetime(string='Date 3')
    date4 = fields.Datetime(string='Date 4')
    date5 = fields.Datetime(string='Date 5')
    # 10-Aug-2016 DT - added two new fields
    code1_id = fields.Many2one(comodel_name='vb.common_code', string='Code 1',ondelete='set null')
    code2_id = fields.Many2one(comodel_name='vb.common_code', string='Code 2',ondelete='set null')
    # The following 5 fields only being used when code_type = 'acct_class'
    bankacct_id = fields.Many2one(comodel_name='vb.bankacct', string='Associated trust account',ondelete='set null',
                                   help='This is the related bank trust account associated with this code. Use for reconciliation.')
    acctprof_id = fields.Many2one(comodel_name='vb.account_profile', string='Account profile',ondelete='set null',
                                  help='This is the account profile associated with this code.')
    tradeprof_id = fields.Many2one(comodel_name='vb.trade_profile', string='Trade profile',ondelete='set null',
                                   help='This is the trade profile associated with this code.')
    mrgnprof_id = fields.Many2one(comodel_name='vb.margin_profile', string='Margin profile',ondelete='set null',
                                  help='This is the margin profile associated with this code.')
    riskprof_id = fields.Many2one(comodel_name='vb.risk_profile', string='Risk profile',ondelete='set null',
                                  help='This is the risk profile associated with this code.')
    market_id = fields.Many2one(comodel_name='vb.market', string='Market',ondelete='set null',
                                  help='This is the market associated with this code.')
    # 10-Apr-2017 - Added for setting cut-off time parameter in Bank setup
    bank_cutoff_time = fields.Char(string="Cut-off time")     
    # Other fields
    active = fields.Boolean(string='Active',default=True)    
    list_seq = fields.Integer(string='List order',default=999)
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
        ], string='Status', default='New')

    country_id = fields.Many2one(comodel_name='res.country', string='Country')
    country_name = fields.Char(related='country_id.name', string='Country name',readonly=True)
    high_risk = fields.Boolean(string='High Risk')
    # SQL constraint
    _sql_constraints = [('unique_lookup_code','UNIQUE(code_type,code,active)','Lookup code must be unique'),
                        ('unique_lookup_name','UNIQUE(code_type,name,active)','Lookup name must be unique')]

# Model for CALENDAR table
# This table store various calendars used by the system.  Each calendar will define the days of rest and will
# be associated to one for more holidays.  Also, each calendar is associated to a country and maybe even to a particular state.

class calendar(models.Model):
    _name = 'vb.calendar'
    _description = 'Calendar'
    _order = 'country_id,name'

    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec ==True:
            obj = self.env['vb.calendar'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError("Sorry there is already existing default calendar")
                    break

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return    
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
    
    # fields
    name = fields.Char(size=100,string='Calendar name',index=True)
    country_id = fields.Many2one(comodel_name='res.country', string='Country',default=_get_default_country,ondelete='restrict',index=True)
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict',
                               help='Leave blank if calendar applies to all states in country.')
    rest_on_mon = fields.Boolean(String='Rest on Monday',default=False)
    rest_on_tue = fields.Boolean(String='Rest on Tuesday',default=False)
    rest_on_wed = fields.Boolean(String='Rest on Wednesday',default=False)
    rest_on_thu = fields.Boolean(String='Rest on Thursday',default=False)
    rest_on_fri = fields.Boolean(String='Rest on Friday',default=False)
    rest_on_sat = fields.Boolean(String='Rest on Saturday',default=True)
    rest_on_sun = fields.Boolean(String='Rest on Sunday',default=True)
    holiday_ids = fields.One2many(comodel_name='vb.holiday', inverse_name='calendar_id', string="Holidays")
    default_rec = fields.Boolean(string='Default calendar',default=False)
    active = fields.Boolean(string='Active',default=True)        

    # SQL constraint
    _sql_constraints = [('unique_calendar','UNIQUE(name,active)','Another calendar with same name already exist')]
    
# Model for HOLIDAY table
# This table store holidays for each calendar.  There are two types of holiday - 'Public holiday' which is a national gazetted holiday
# and 'non-work day' which is holiday declared by the company. 

class calendar_holiday(models.Model):
    _name = 'vb.holiday'
    _description = 'Holiday'
    _order = 'calendar_id,date_start'
    
    # fields
    calendar_id = fields.Many2one(comodel_name='vb.calendar',string='Calendar',index=True,ondelete='cascade')
    name = fields.Char(size=100,string='Date description',required=True)
    date_start = fields.Date(string='Date start',index=True)
    date_end = fields.Date(string='Until')
    holiday_type = fields.Selection([
        ('NoWork', 'Non-work day'),
        ('Holiday', 'Full-day holiday'),
        ('HalfDay', 'Half-day holiday'),
        ], string='Holiday type', default='Holiday')
    _sql_constraints = [('unique_holiday','UNIQUE(calendar_id,date_start)','Holiday for a date must be unique')]

    # If end date entered, ensure it is not earlier that start date
    @api.constrains('date_start', 'date_end')
    def _check_rate_range(self):
        for rec in self:
            if rec.date_end:
                date_start = datetime.strptime(rec.date_start,'%Y-%m-%d')
                date_end = datetime.strptime(rec.date_end,'%Y-%m-%d')
                if date_end < date_start:
                    raise models.ValidationError('Entered end date cannot be earlier than start date!')
    
# Model for MARKET table
# This table store information for each market that is applicable to the system.  Each market is also linked to a currency
# as well as to a calendar (which defines the trading days).  Also, we store the various opening and closing times for each
# trading sessions for the day (Max 4 sessions).

class market(models.Model):
    _name = 'vb.market'
    _description = 'Market'
    _order = 'name'
    
    # Returns the default currency
    @api.model
    def _get_default_currency(self):
        rec = self.env['vb.currency'].search([('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default calendar
    @api.model
    def _get_default_calendar(self):
        rec = self.env['vb.calendar'].search([('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default country
    @api.model
    def _get_default_country(self):
        config_rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','DftCountry')],limit=1)
        if config_rec:
            country_rec = self.env['res.country'].search([('code','=',config_rec.parm1)],limit=1)
            if country_rec:
                return country_rec.id
        return
    
    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec ==True:
            obj = self.env['vb.market'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError("Sorry there is already existing default market")
                    break

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False    
    
    # fields
    name = fields.Char(size=100,string='Name of market',index=True)
    code = fields.Char(size=20,string='Market code',index=True)
    market_type = fields.Selection([
        ('Exchange', 'Exchange'),
        ('Otc', 'Over-the-counter'),
        ], string='Market type', default='Exchange')
    address1 = fields.Char(size=100,string='Address 1',)
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=100,string='City',)
    postcode = fields.Char(size=20,string='Postal code',)
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    country_id = fields.Many2one(comodel_name='res.country',string='Country',default=_get_default_country,ondelete='restrict')
    phone1 = fields.Char(size=15,string='Primary phone')
    phone2 = fields.Char(size=15,string='Alternate phone')
    fax = fields.Char(size=15,string='Branch fax phone')
    geolocation = fields.Char(string='Geo location',help='This can be a GPS location or some map reference')
    geopicture = fields.Binary(string='Picture of geolocation',help='This is the picture of the location.  Can be screen capture of map.')
    mgr_name = fields.Char(size=100,string='Name of manager')
    mgr_mobile = fields.Char(size=20,string='Mobile number of manager')
    mgr_email = fields.Char(size=40,string='Email of manager')
    currency_id = fields.Many2one(comodel_name='vb.currency',default=_get_default_currency,string='Currency',ondelete='restrict',index=True)
    calendar_id = fields.Many2one(comodel_name='vb.calendar',default=_get_default_calendar,string='Calendar',ondelete='restrict',index=True)
    asset_ids = fields.One2many(comodel_name='vb.asset', inverse_name='market_id', string="Market assets")
    default_rec = fields.Boolean(string='Default market',default=False)
    sessions_per_day = fields.Integer(string='Sessions per day')
    sess1_open = fields.Char(string='Session 1 open (hh:mm)')
    sess1_close = fields.Char(string='Session 1 close (hh:mm)')
    sess2_open = fields.Char(string='Session 2 open (hh:mm)')
    sess2_close = fields.Char(string='Session 2 close (hh:mm)')
    sess3_open = fields.Char(string='Session 3 open (hh:mm)')
    sess3_close = fields.Char(string='Session 3 close (hh:mm)')
    sess4_open = fields.Char(string='Session 4 open (hh:mm)')
    sess4_close = fields.Char(string='Session 4 close (hh:mm)')
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
        ('Halt', 'Trading halt'),
        ], string='Market status', default='New')
    halt_until = fields.Datetime(string='Halt until',help='If trading is halted, specify until what time. If left blank, halt indefinitely.')
    # 20-Jan-2017
    EOD_time = fields.Char(string='EOD time')
    AMO_time = fields.Char(string='AMO time')
    website = fields.Char(string='Web site address')

    # SQL constraint
    _sql_constraints = [('unique_market','UNIQUE(name,active)','Another market with same name already exist')]

# Model for ASSET table
# This table store the various assets applicable to a market.  Also known as stock, unit trust, etc, each asset has a market symbol
# and is associate to various prices.  Each asset is also related to an asset class as well as an asset profile.

class asset(models.Model):
    _name = 'vb.asset'
    _description = 'Market asset'
    _order = 'name'
    
    # Returns the default market
    @api.model
    def _get_default_market(self):
        market = self.env['vb.market'].search([('default_rec','=',True)],limit=1)
        if market:
            return market.id
        return

    # Returns the default asset class
    @api.model
    def _get_default_class(self):
        rec = self.env['vb.common_code'].search([('code_type','=','AssetClass'),('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default delivery basis
    @api.model
    def _get_default_dlv_basis(self):
        rec = self.env['vb.common_code'].search([('code_type','=','DlvBasis'),('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default asset profile
    @api.model
    def _get_default_profile(self):
        rec = self.env['vb.asset_profile'].search([('default_rec','=',True)],limit=1)
        if rec:
            return rec.id
        return

    # Returns the default index
    #@api.model
    #def _get_default_index(self):
    #    rec = self.env['vb.common_code'].search([('code_type','=','Index'),('default_rec','=',True)],limit=1)
    #    if rec:
    #        return rec.id
    #    return

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # Method to get the default name of an asset as this sequence"(code)symbol-name"
    @api.multi
    def name_get(self):
        super(asset, self).name_get()
        result=[]
        for rec in self:
            result.append((rec.id,u"%s (%s)" % (rec.name,rec.asset_code)))
        return result
    
    @api.onchange('asset_class_id')
    def check_acct_class_id(self):
        asset_class_rec = self.env['vb.common_code'].search([('code', '=', 'WARR')])
        for rec in self:
            if rec.asset_class_id.id == asset_class_rec.id:
                rec.update({'is_warrant':True})
            else:
                rec.update({'is_warrant':False})

    @api.multi
    def write(self,vals):

        # checking for the expiry days in 'Allocation days' page
        # must asset_prof_daysb4exp1 > asset_prof_daysb4exp2
        # must asset_prof_daysb4exp2 > asset_prof_daysb4exp3
        if vals.get('asset_prof_daysb4exp1'):
            exp1 = vals.get('asset_prof_daysb4exp1')
        elif vals.get('asset_prof_daysb4exp1') == 0:
            exp1 = vals.get('asset_prof_daysb4exp1')
        else:
            exp1 = self.asset_prof_daysb4exp1

        if vals.get('asset_prof_daysb4exp2'):
            exp2 = vals.get('asset_prof_daysb4exp2')
        elif vals.get('asset_prof_daysb4exp2') == 0:
            exp2 = vals.get('asset_prof_daysb4exp2')
        else:
            exp2 = self.asset_prof_daysb4exp2

        exp3 = vals.get('asset_prof_daysb4exp3') if vals.get('asset_prof_daysb4exp3') else self.asset_prof_daysb4exp3

        if exp1 < exp2:
            raise ValidationError("(Days before expiry #1) must not be less than (Days before expiry #2)")
        elif exp2 < exp3:
            raise ValidationError("(Days before expiry #2) must not be less than (Days before expiry #3)")
        # end of checking

        return super(asset,self).write(vals)

    # field
    name = fields.Char(size=100,string='Name of asset',index=True)
    market_id = fields.Many2one(comodel_name='vb.market',default=_get_default_market,string='Market',ondelete='restrict',index=True)
    market_symbol = fields.Char(size=20,string='Market symbol',index=True,help='This is for normal trade board')
    market_sector = fields.Char(size=20,string='Market sector')
    market_board = fields.Char(size=20,string='Market board')
    total_issued = fields.Float(string='Total issued units',digits=(11,0))
    isin = fields.Char(string='ISIN',index=True)
    asset_code = fields.Char(size=20,string='Asset code',index=True)
    asset_class_id = fields.Many2one(comodel_name='vb.common_code',string='Asset class',default=_get_default_class,ondelete='restrict',index=True,domain="[('code_type','=','AssetClass')]")
    asset_profile_id = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile',default=_get_default_profile,ondelete='restrict',index=True)
    std_lot_size = fields.Float(string='Standard lot size', default=100, digits=(7,0))
    ceiling_price = fields.Float(string='Ceiling price',digits=(11,5))
    asset_symbol_ids = fields.One2many(comodel_name='vb.asset_symbol', inverse_name='asset_id', string="Other symbols")
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
        ('Expired', 'Expired'),
        ('Delisted', 'Delisted'),
        ('Suspended', 'Suspended'),
        ], string='Asset status', default='New')   
    parent_asset_id = fields.Many2one(comodel_name='vb.asset',string='Parent asset',ondelete='restrict',index=True,domain="[('parent_asset_id','=',False)]")
    dlv_basis_id = fields.Many2one(comodel_name='vb.common_code',string='Delivery basis',default=_get_default_dlv_basis,ondelete='restrict',index=True,domain="[('code_type','=','DlvBasis')]")
    maturity_date = fields.Date(string='Maturity date')
    last_done_price = fields.Float(string='Last price',digits=(11,4),default=0)
    opening_price = fields.Float(string='Daily opening price',digits=(11,5))
    high_price = fields.Float(string='Daily high price',digits=(11,5))
    low_price = fields.Float(string='Daily low price',digits=(11,5))
    volume = fields.Integer(string='Volume')
    price_asat = fields.Datetime(string='Last price as at')
    expired_date = fields.Date(string='Expired date')
    suspended_date = fields.Date(string='Suspended date')
    delisted_date = fields.Date(string='Delisted date')
    is_warrant = fields.Boolean(default=False)
    full_name = fields.Char(size=100,string='Full name of asset')
    # 15-Aug-2017 - V2    
    cr_numerator = fields.Float(string='Numerator',digits=(9,4))
    cr_denominator = fields.Float(string='Denominator',digits=(9,4))
    cr_ex_date = fields.Date(string='Ex-date')
    cr_lodge_date = fields.Date(string='Lodgement date')
    cr_entitle_date = fields.Date(string='Entitle date')
    cr_active = fields.Boolean(string='Corporate restructure active flag')
    corp_action_ind = fields.Char(string='Corporate action indicators')
    asset_prod_ids = fields.One2many(comodel_name='vb.asset_prod', inverse_name='asset_id', string="Asset products")
    asset_acct_ids = fields.One2many(comodel_name='vb.asset_acct', inverse_name='asset_id', string="Asset accounts")
    # 21-Sep-2017
    # The following fields will be DEPRECATED (not used now - use the same at the asset_product level)
    marginable = fields.Boolean(string='Marginable assets',default=False)
    collaterable = fields.Boolean(string='Collaterable assets',default=False)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    compute_stamp = fields.Selection([
        ('No', 'No'),
        ('Yes', 'Yes'),
        ], string='Compute stamp',help='Leave blank if depending on other factors')    
    # 28-Mar-2018
    index_id = fields.Many2one(comodel_name='vb.common_code',string='Index',ondelete='restrict',index=True,domain="[('code_type','=','Index')]")
    # 12-Apr-2018
    asset_prof_id1 = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile #1',default=_get_default_profile,ondelete='restrict',index=True)
    asset_prof_id2 = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile #2',default=_get_default_profile,ondelete='restrict',index=True)
    asset_prof_id3 = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile #3',default=_get_default_profile,ondelete='restrict',index=True)
    asset_prof_daysb4exp1 = fields.Float(string='Days before expiry #1',digits=(3,0))
    asset_prof_daysb4exp2 = fields.Float(string='Days before expiry #2',digits=(3,0))
    asset_prof_daysb4exp3 = fields.Float(string='Days before expiry #3',digits=(3,0))
    # 16-Apr-2018
    asset_prof_daymth1 = fields.Selection([('Day', 'Day'),('Month', 'Month')], string='Day/Month #1')    
    asset_prof_daymth2 = fields.Selection([('Day', 'Day'),('Month', 'Month')], string='Day/Month #2')    
    asset_prof_daymth3 = fields.Selection([('Day', 'Day'),('Month', 'Month')], string='Day/Month #3')  
    asset_prof_acct_ids = fields.One2many(comodel_name='vb.asset_profile_acct', inverse_name='asset_profile_id', string="Asset profile accounts")
    nostamp_start_date = fields.Date('Start date')
    nostamp_end_date = fields.Date('End date')

    # SQL constraint
    _sql_constraints = [('unique_market_symbol','UNIQUE(market_symbol,active)','Another asset with same symbol already exist'),
                        ('unique_asset_code','UNIQUE(asset_code,active)','Another asset with same code already exist')]

    
# Model for ASSET PRODUCT table
# This table stores the various risk related information for an asset at the product level

class asset_product(models.Model):
    _name = 'vb.asset_prod'
    _description = 'Asset by product'

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Asset',ondelete='cascade',index=True)
    acct_class_id = fields.Many2one(comodel_name='vb.common_code',string='Product',ondelete='restrict',index=True)
    product_type = fields.Char(string='Product type',related='acct_class_id.parm1',store=True,readonly=True)
    # product_type = fields.Char(string='Product type')
    marginable = fields.Boolean(string='Marginable assets',default=True)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2),default=1)
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0))
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2))
    override_flag = fields.Boolean(string='Override profile',default=False)
    ceiling_price = fields.Float(string='Ceiling price',digits=(11,5))
    propogate_flag = fields.Boolean(string='Propogate change',default=False)
    #Added by nandha (dt:23:11:2017)
    price_drop_pct_thld = fields.Float(string='Threshold price drop',digits=(15,2))
    max_buy_thld_pct = fields.Float(string='Threshold Max Buy',digits=(15,2))
    max_sell_thld_pct = fields.Float(string='Threshold Max Sell',digits=(15,2))

# Model for ASSET ACCOUNT table
# This table stores the various risk related information for an asset at the product level

class asset_account(models.Model):
    _name = 'vb.asset_acct'
    _description = 'Asset by account'

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    @api.constrains('acct_id')
    @api.multi
    def check_accts(self):
        recs = self.env['vb.asset_acct'].search([('acct_id','=',self.acct_id.id),('asset_id','=',self.asset_id.id)])
        if len(recs) > 1:
            raise ValidationError('Duplicated customer account for Contra/Margin not allowed')
    
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Asset',ondelete='cascade',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Account',ondelete='restrict',index=True)
    marginable = fields.Boolean(string='Marginable assets',default=True)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2),default=1)
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    override_flag = fields.Boolean(string='Override profile',default=False)
    #Added by Daniel (25-May-2018)
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0))
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2))
    propogate_flag = fields.Boolean(string='Propogate change',default=False)
    max_buy_thld_pct = fields.Float(string='Threshold for max buy',digits=(15,2))
    max_sell_thld_pct = fields.Float(string='Threshold for max sell',digits=(15,2))
    _sql_constraints = [('vb_asset_acct_unique','UNIQUE(acct_id,asset_id)','Duplicated customer account for Contra/Margin not allowed')]
# Model for ASSET PRICE table
# This table store the price history for each asset.
# A snapshoot of the asset prices at fixed frequency (e.g. twice a day) will be taken from the front-office system.

class asset_price(models.Model):
    _name = 'vb.asset_price'
    _description = 'Market asset price'
    _order = 'asset_id,price_date desc'
    _rec_name = 'asset_id'
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Asset',ondelete='restrict',index=True)
    market_symbol = fields.Char(string='Market symbol',related='asset_id.market_symbol',readonly=True)
    opening_price = fields.Float(string='Daily opening price',digits=(11,5))
    high_price = fields.Float(string='Daily high price',digits=(11,5))
    low_price = fields.Float(string='Daily low price',digits=(11,5))
    last_done_price = fields.Float(string='Last done price',digits=(11,5))
    volume = fields.Integer(string='Volume')
    last_updated = fields.Datetime(string='Last updated',default=lambda self: fields.datetime.now())
    # Not used
    price_date = fields.Date(string='Price date',default=lambda self: fields.date.today())

# Model for ASSET SYMBOL table
# This table store the sysmbol used by newsfeed vendor for each asset.
# Added: 05-Aug-2016

class asset_symbol(models.Model):
    _name = 'vb.asset_symbol'
    _description = 'Market asset symbol'
    _order = 'asset_id,newsfeed_id'
    _rec_name = 'asset_id'
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    asset_id = fields.Many2one(comodel_name='vb.asset', string='asset',ondelete='restrict',index=True)
    asset_code = fields.Char(size=20,string='Asset code',related='asset_id.asset_code',store=True,readonly=True)
    newsfeed_id = fields.Many2one(comodel_name='vb.common_code',string='News feed',ondelete='restrict',domain="[('code_type','=','NewsFeed')]")
    # This is the asset symbol used by the vendor and mapped to the asset
    asset_symbol = fields.Char(size=20,string='Symbol',index=True)
    trading_board = fields.Char(string='Trading board')
    
# Model for ASSET PROFILE table
# This table will provide related parameters for a particular set of assets especially those related to margin

class asset_profile(models.Model):
    _name = 'vb.asset_profile'
    _description = 'Asset profile'
    
    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            obj = self.env['vb.asset_profile'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError("Sorry there is already existing default asset profile")
                    break
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
    
    # fields
    name = fields.Char(size=100,string='Profile name')
    code = fields.Char(size=20,string='Profile code')
    default_rec = fields.Boolean(string='Default profile',default=False)
    active = fields.Boolean(string='Active',default=True)       
    # 21-Sep-2017
    asset_prof_prod_ids = fields.One2many(comodel_name='vb.asset_profile_prod', inverse_name='asset_profile_id', string="Asset profile products")
    asset_prof_acct_ids = fields.One2many(comodel_name='vb.asset_profile_acct', inverse_name='asset_profile_id', string="Asset profile accounts")
    # The following fields will be DEPRECATED (not used now - use the same at the asset_profile_product level)
    marginable = fields.Boolean(string='Marginable assets',default=False)
    collaterable = fields.Boolean(string='Collaterable assets',default=False)
    price_cap_pct = fields.Float(string='Price cap %',digits=(7,4))
    price_cap_amt = fields.Float(string='Price cap amount',digits=(11,5))
    margin_pct = fields.Float(string='Margin of finance %',digits=(7,4))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2))
    max_margin_limit = fields.Float(string='Maximum margin limit',digits=(15,2))

class asset_profile_product(models.Model):
    _name = 'vb.asset_profile_prod'
    _description = 'Asset profile by product'

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    asset_profile_id = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile',ondelete='cascade',index=True)
    acct_class_id = fields.Many2one(comodel_name='vb.common_code',string='Product ID',ondelete='restrict',index=True)
    product_type = fields.Char(string='Product type',related='acct_class_id.parm1',store=True,readonly=True)
    # product_type = fields.Char(string='Product type')
    marginable = fields.Boolean(string='Marginable assets',default=True)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2),default=1)
    max_cap_adj_pct = fields.Float(string='Maximum cap adj %',digits=(5,2))
    max_cap_adj_price = fields.Float(string='Maximum cap adj price',digits=(11,5))
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0))
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2))
    propogate_flag = fields.Boolean(string='Propogate change',default=False)
    #Added by nandha (dt: 27-11-2017)
    price_drop_pct_thld = fields.Float(string='Threshold price drop',digits=(15,2))

class asset_profile_account(models.Model):
    _name = 'vb.asset_profile_acct'
    _description = 'Asset profile by account'

    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")

    asset_profile_id = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile',ondelete='cascade',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Account',ondelete='restrict',index=True)
    marginable = fields.Boolean(string='Marginable assets',default=True)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2),default=1)
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0))
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2))
    propogate_flag = fields.Boolean(string='Propogate change',default=False)
    #Added by nandha (Dt:23:11;2017)
    max_buy_thld_pct = fields.Float(string='Threshold for max buy',digits=(15,2))
    max_sell_thld_pct = fields.Float(string='Threshold for max sell',digits=(15,2))
    

# Model for CURRENCY table
# This table store the information for each currency that will be used in the system.

class currency(models.Model):
    _name = 'vb.currency'
    _description = 'Currency'
    _order = 'name'
    
    @api.constrains('default_rec')
    def check_default_rec(self):
        if self.default_rec == True:
            obj = self.env['vb.currency'].search([('id','!=',self.id)])
            for rec in obj:
                if rec.default_rec == True:
                    raise ValidationError("Sorry there is already existing default currency")
                    break
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
    
    # fields
    name = fields.Char(size=100,string='Name of currency',index=True)
    country_id = fields.Many2one(comodel_name='res.country', string='Country',ondelete='restrict',index=True)
    symbol = fields.Char(size=10,string='Currency symbol',index=True)
    symbol_pos = fields.Selection([
            ('After','After Amount'),
            ('Before','Before Amount')
    ],string='Symbol Position', default='Before', help="Determines where the currency symbol should be placed after or before the amount.")
    rate_ids = fields.One2many(comodel_name='vb.currency_rate', inverse_name='currency_id', string="Currency rates")
    curr_buy_rate = fields.Float(string='Current buy rate',digits=(11,5))
    curr_sell_rate = fields.Float(string='Current sell rate',digits=(11,5))
    curr_spot_rate = fields.Float(string='Current spot rate',digits=(11,5))
    # 23-Aug-2016 DT: Change field type for rate_date to datetime since there is possibility for 2 rates prevailing in a day
    # looking at the volatility of forex these days
    curr_rate_date = fields.Datetime(string='Current rate as at')
    default_rec = fields.Boolean(string='Default currency',default=False)
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Inactive', 'Inactive'),
        ], string='Currency status', default='New')

    # SQL constraint
    _sql_constraints = [('unique_currency_name','UNIQUE(name,active)','Another currency with same name already exist'),
                        ('unique_currency_symbol','UNIQUE(symbol,active)','Another currency with same symbol already exist')]
    
# Model for CURRENCY RATE table
# This table store the rate history for each currency.
# This table is updated each time the latest rates are being updated into the currency table.

class currency_rate(models.Model):
    _name = 'vb.currency_rate'
    _description = 'Currency rate'
    _rec_name = 'currency_id'

    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # fields
    currency_id = fields.Many2one(comodel_name='vb.currency', string='Currency',ondelete='cascade',index=True)
    # 23-Aug-2016 DT: Change field type for rate_date to datetime since there is possibility for 2 rates prevailing in a day
    # looking at the volatility of forex these days
    rate_date = fields.Datetime(string='Rate as at',index=True)
    buy_rate = fields.Float(string='Current buy rate',digits=(11,5))
    sell_rate = fields.Float(string='Current sell rate',digits=(11,5))
    spot_rate = fields.Float(string='Current spot rate',digits=(11,5))

class service_option(models.Model):
    _name = 'vb.service_option'
    _description = 'Service options'
    _rec_name = 'name'
    
    @api.model
    def create(self,vals):
        vals['state']='New'
        result=super(service_option,self).create(vals)
        return result

    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # method to get Activate wizard for service option
    @api.multi
    def get_activate_srvop_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_base', 'activate_service_opt_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'activate_service_opt_wizard'. Please contact IT Support"))
        return{
                    'name': "Activate service option",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
    
    # method to get Suspend wizard for service option
    @api.multi
    def get_suspend_srvop_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_base', 'suspend_service_opt_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'suspend_service_opt_wizard'. Please contact IT Support"))
        return{
                    'name': "Suspend service option",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
    
    # method to get Uplift wizard for service option
    @api.multi
    def get_uplift_srvop_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_base', 'uplift_service_opt_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'uplift_service_opt_wizard'. Please contact IT Support"))
        return{
                    'name': "Uplift service option",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
            
    # fields
    name = fields.Char(string='Service name')
    charge_id = fields.Many2one(comodel_name='vb.charge_code', string='Charge',ondelete='restrict',index=True)
    # User needs to define the charge_id before a service option needs to be created
    charge_basis = fields.Selection([
        ('FixedSub', 'Fixed monthly subscription'),
        ('MthTradeVal', 'Monthly trade value'),
        ('MthTargetVal', 'Monthly trade target'),
        ], string='Charging basis',default='FixedSub')
    svc_duration = fields.Integer(string='Service duration (in months)',default=0)
    monthly_amt = fields.Float(string='Monthly subscription amt',digits=(9,2),default=0)
    mth_target = fields.Float(string='Monthly trade target',digits=(15,2),default=0,help='Monthly trade target to achieve to enjoy free service')
    note = fields.Text(string='Notes')
    start_date = fields.Date(string='Start date',help='When service is made available for subscription')
    end_date = fields.Date(string='End date',help='When service stop being made available for subscription.')
    charge_date = fields.Date(string='Date start charging',help='Date start charging - blanks means can charge from start')
    free_service_mth = fields.Integer(string='Free service months',help='Number of months free service from start of subscription')
    free_service_day = fields.Integer(string='Free service days',help='Number of months free service from start of subscription')
    active = fields.Boolean(string='Active',default=True)
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Activated'),
        ('Suspended', 'Suspended'),
        ], string='Status')
    activated_date = fields.Datetime(string='Date/time activated')
    activated_by = fields.Many2one(comodel_name='res.users',string='Activated by',ondelete='restrict')
    suspended_date = fields.Datetime(string='Date/time suspended')
    suspended_by = fields.Many2one(comodel_name='res.users',string='Suspended by',ondelete='restrict')
    uplifted_date = fields.Datetime(string='Date/time uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by',ondelete='restrict')
    comments = fields.Text(string='Comments')
    # 13-Jan-2017
    code = fields.Char(string='Service code', index=True)
    free_service_upon_activate = fields.Boolean(string='Free service upon activation')
        
class dashbrd_counter(models.Model):
    _name = 'vb.dashbrd_counter'
    _description = 'Dashboard board counters'
    
    # fields
    product_type            = fields.Char(string='Product type',index=True)
    group_code              = fields.Char(string='Group code',index=True)
    # Account opening counters
    rqs_uid                 = fields.Integer(string='uid')
    acop_incomp_signup      = fields.Integer(string='Incomplete signup',default=0)
    acop_incomp_app         = fields.Integer(string='Incomplete application',default=0)
    acop_pdg_email_vrfy     = fields.Integer(string='Pending Email Verification more than 1 Day',default=0)
    acop_pdg_1st_login      = fields.Integer(string='Pending First Time Login',default=0)
    acop_app_resubmit       = fields.Integer(string='Application Re-submit',default=0)
    acop_pdg_doc_check      = fields.Integer(string='Pending Document Check',default=0)
    acop_pdg_ramci_check    = fields.Integer(string='Pending RAMCI Check',default=0)
    acop_pdg_smgmt_appr     = fields.Integer(string='Pending Senior Management Approval',default=0)
    acop_pdg_opn_cds_ac     = fields.Integer(string='Pending Open CDS Account',default=0)
    acop_cds_opn_fail       = fields.Integer(string='CDS Account Open Fail',default=0)
    acop_2day_signup        = fields.Integer(string='Today signup (credit pass)',default=0)
    acop_2day_activated     = fields.Integer(string='Today Activated A/C (Until 6.00pm with CDS No.)',default=0)
    acop_2day_rejected      = fields.Integer(string='Today Rejected A/C',default=0)
    acop_week_signup        = fields.Integer(string='Weekly signup (credit pass)',default=0)
    acop_week_activated     = fields.Integer(string='Weekly Activated A/C (with CDS No.)',default=0)
    acop_week_rejected      = fields.Integer(string='Weekly Rejected A/C',default=0)
    acop_mth_signup         = fields.Integer(string='Monthly signup (credit pass)',default=0)
    acop_mth_activated      = fields.Integer(string='Monthly Activated A/C (with CDS No.)',default=0)
    acop_mth_rejected       = fields.Integer(string='Monthly Rejected A/C',default=0)
    acop_extra1             = fields.Integer(string='Account opening - extra counter 1',default=0)
    acop_extra2             = fields.Integer(string='Account opening - extra counter 2',default=0)
    acop_extra3             = fields.Integer(string='Account opening - extra counter 3',default=0)
    # Account status counters
    acst_2day_suspended     = fields.Integer(string='Today Suspended Accounts',default=0)
    acst_suspended_gt1day   = fields.Integer(string='Account Suspended More Than 1 Day',default=0)
    acst_inactive_gt1yr     = fields.Integer(string='Account Inactive More Than 1 Day',default=0)
    acst_pdg_close          = fields.Integer(string='Account Pending To Close',default=0)
    acst_dormant_gt3yr      = fields.Integer(string='Account Dormant (3 years)',default=0)
    acst_week_closed        = fields.Integer(string='Weekly Closed A/C',default=0)
    acst_mth_closed         = fields.Integer(string='Monthly Closed A/C',default=0)
    acst_ac_chg_appr        = fields.Integer(string='A/C change approval',default=0)
    acst_extra1             = fields.Integer(string='Account status - extra counter 1',default=0)
    acst_extra2             = fields.Integer(string='Account status - extra counter 2',default=0)
    acst_extra3             = fields.Integer(string='Account status - extra counter 3',default=0)
    acct_pndg_close_kibb = fields.Integer(string='KIBB to close account',default=0)
    acct_close_kibb_2day = fields.Integer(string='KIBB today closed account',default=0)
    # Cash management counters
    cash_pdg_online_dep    = fields.Integer(string='Pending Fund Deposit (Online)',default=0)
    cash_pdg_offline_dep    = fields.Integer(string='Pending Fund Deposit (Offline)',default=0)
    cash_pdg_withdrwl       = fields.Integer(string='Pending Fund Withdrawal',default=0)
    cash_pdg_withdrwl_cimb  = fields.Integer(string='Pending Fund Withdrawal - Bulk (CIMB)',default=0)
    cash_pdg_withdrwl_ncimb = fields.Integer(string='Pending Fund Withdrawal - Bulk (NonCIMB)',default=0)
    cash_pdg_trf            = fields.Integer(string='Pending Inter-account Transfer',default=0)
    cash_2day_cds_payable   = fields.Integer(string='Today CDS payable fees',default=0)
    cash_2day_ca_subs       = fields.Integer(string='Today CA subscriptions',default=0)
    cash_extra1             = fields.Integer(string='Cash mgmt - extra counter 1',default=0)
    cash_extra2             = fields.Integer(string='Cash mgmt - extra counter 2',default=0)
    cash_extra3             = fields.Integer(string='Cash mgmt - extra counter 3',default=0)
    # Portfolio management counters
    port_pdg_trf            = fields.Integer(string='Pending CDS Withdrawal/Transfer',default=0)
    port_extra1             = fields.Integer(string='Portfolio mgmt - extra counter 1',default=0)
    port_extra2             = fields.Integer(string='Portfolio mgmt - extra counter 2',default=0)
    port_extra3             = fields.Integer(string='Portfolio mgmt - extra counter 3',default=0)
    # Settlement counters
    sett_poss_buyin         = fields.Integer(string='Possible buyin',default=0)
    sett_poss_forcesell     = fields.Integer(string='Possible forcesell',default=0)
    sett_buyin              = fields.Integer(string='Buyin',default=0)
    sett_forcesell          = fields.Integer(string='Forcesell',default=0)
    sett_extra1             = fields.Integer(string='Settlement - extra counter 1',default=0)
    sett_extra2             = fields.Integer(string='Settlement - extra counter 2',default=0)
    sett_extra3             = fields.Integer(string='Settlement - extra counter 3',default=0)
    # Corporate action counters
    corp_require_sub        = fields.Integer(string='Corporate Action Requiring Subscription',default=0)
    corp_sub_rcvd           = fields.Integer(string='Corporate Action Subscription Received',default=0)
    corp_pndg_can           = fields.Integer(string='Cancellation pending for approval',default=0)
    corp_extra1             = fields.Integer(string='Corporate action - extra counter 1',default=0)
    corp_extra2             = fields.Integer(string='Corporate action - extra counter 2',default=0)
    corp_extra3             = fields.Integer(string='Corporate action - extra counter 3',default=0)
    # Management action counters
    mgmt_forcesell_pdg      = fields.Integer(string='Forcesell pending approval',default=0)
    mgmt_forcesell_2day     = fields.Integer(string='Forcesell generated today',default=0)
    mgmt_extra1             = fields.Integer(string='Management action - extra counter 1',default=0)
    mgmt_extra2             = fields.Integer(string='Management action - extra counter 2',default=0)
    mgmt_extra3             = fields.Integer(string='Management action - extra counter 3',default=0)
    # Risk management counters
    risk_limit_xceed        = fields.Integer(string='Limit breached',default=0)
    risk_threshld_xceed_com = fields.Integer(string='Threshold Limit Breached (company level)',default=0)
    risk_threshld_xceed_ac  = fields.Integer(string='Threshold Limit Breached (account level)',default=0)
    risk_pl_threshld_xceed  = fields.Integer(string='Potential Loss Threshold Breached',default=0)
    risk_t4_buy             = fields.Integer(string='T+4 Position (Buy)',default=0)
    risk_od_ctra_loss_bill  = fields.Integer(string='Overdue contra losses and debit bills',default=0)
    risk_extra1             = fields.Integer(string='Risk management - extra counter 1',default=0)
    risk_extra2             = fields.Integer(string='Risk management - extra counter 2',default=0)
    risk_extra3             = fields.Integer(string='Risk management - extra counter 3',default=0)
    # Helpdesk counters
    hdsk_open_ops           = fields.Integer(string='Open Cases - ops team',default=0)
    hdsk_open_cust_svc      = fields.Integer(string='Open Cases - cust service team',default=0)
    hdsk_open_2day          = fields.Integer(string='Today Open Cases',default=0)
    hdsk_open_gt1day        = fields.Integer(string='Open Cases More Than 1 day',default=0)
    hdsk_tot_new            = fields.Integer(string='Total New Cases',default=0)
    hdsk_tot_closed         = fields.Integer(string='Total Cases Closed',default=0)
    hdsk_extra1             = fields.Integer(string='Help desk - extra counter 1',default=0)
    hdsk_extra2             = fields.Integer(string='Help desk - extra counter 2',default=0)
    hdsk_extra3             = fields.Integer(string='Help desk - extra counter 3',default=0)
    hdsk_tot_new_assignto   = fields.Integer(string='Total new cases assigned to',default=0)
    hdsk_tot_closed_assignto= fields.Integer(string='Total closed cases assigned to',default=0)
    hdsk_open_2day_assignto = fields.Integer(string='Today open cases assigned to',default=0)    
    
class temp_asset(models.Model):
    _name = 'vb.temp_asset'
    _description = 'Portfolio asset transactions'

    def get_temp_asset_wizard(self, cr, uid, data, context):
        try:
            form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_portfolio', 'temp_asset_wiz_form')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot lhdsk_tot_new_assigntoocate required 'temp_asset_wiz_form'. Please contact IT Support"))
        return{
                    'name': "Are you sure to move assets?",
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': form_id,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    }

    # field
    name = fields.Char(size=100,string='Asset name')
    asset_code = fields.Char(size=20,string='Asset code')
    asset_class = fields.Char(size=40,string='Asset Class')
    expired_date = fields.Char(size=100,string='Expiry date')
    share_multiplier = fields.Float(string='Share Multiplier',digits=(11,5))
    high_price = fields.Float(string='Daily high price',digits=(11,5))
    low_price = fields.Float(string='Daily low price',digits=(11,5))
    index_name = fields.Char(string='Index name')
    marginable = fields.Boolean(string='Marginable assets',default=False)
    collaterable = fields.Boolean(string='Collaterable assets',default=False)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    batch_no =   fields.Char(size=20,string='Batch Number')
    process_status =  fields.Selection([
        ('New', 'New'),
        ('Ok', 'OK'),
        ('Failed', 'Failed'),
        ], string='New')
    process_date = fields.Datetime(string='Date/time processed')
    process_remarks = fields.Text(string='Remarks')
    market_board = fields.Char(string='Market Board')
    asset_profile_code = fields.Char(string='Asset profile')
