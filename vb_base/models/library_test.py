import re
from openerp.exceptions import ValidationError
from datetime import datetime, date

def check_email(email):
    if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) == None:
        raise ValidationError ("Invalid email address. Please enter a valid email address!") 

