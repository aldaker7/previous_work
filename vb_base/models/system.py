# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# System related models
#
# Author: Daniel Tan
# Last updated: 15-August-2017
#
# ==========================================

from openerp import models, fields, api
from openerp.http import request #Library to get ip address

# Model for CONFIG table
# This table store various configuration record used through the system.
# Although similar in structure to the Common Code table, this is used primarily in configuration and
# the access rights of this table will be more restrictive.

class config(models.Model):
    _name = 'vb.config'
    _description = 'Configuration table'
    _rec_name = 'name'
    _order = 'code_type,code'
    
    # field
    name = fields.Char(string='Description',required=True,index=True)
    code_type = fields.Char(string='Code type',required=True,index=True,default='Sys')
    # Possible values: 'Sys','App','WebSvc','LoginDomain'
    code = fields.Char(string='Code',required=True,index=True)
    # Possible values of code
    # --------------------------------------------
    # System config list (Sys): e.g.
    #  1. fixed_salt = Login MD5 fixed salt
    #
    # Application config list (App): e.g.
    #  1. base_int_rate = Base interest rate
    #  2. interest_type = Interest computation method - ABS=Absolute, BR=Base rate
    #  3. sell_insufficient_holding = Allow user to sell even if holding not sufficient at moment of sales
    #  4. cash_dep_multiplier = This is the cash deposit before trading limit
    #
    # Web service request mapping list (WebSvc): e.g.
    #  1. code = request IDs from external source
    #  2. parm1 = URL
    #  3. parm2 = Request ID
    note = fields.Text(string='Notes')
    default_rec = fields.Boolean(string='Default code',default=False)
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    parm6 = fields.Char(string='Parameter 6')
    parm7 = fields.Char(string='Parameter 7')
    parm8 = fields.Char(string='Parameter 8')
    parm9 = fields.Char(string='Parameter 9')
    parm1_desc = fields.Char(string='Description for parameter 1')
    parm2_desc = fields.Char(string='Description for parameter 2')
    parm3_desc = fields.Char(string='Description for parameter 3')
    parm4_desc = fields.Char(string='Description for parameter 4')
    parm5_desc = fields.Char(string='Description for parameter 5')
    parm6_desc = fields.Char(string='Description for parameter 6')
    parm7_desc = fields.Char(string='Description for parameter 7')
    parm8_desc = fields.Char(string='Description for parameter 8')
    parm9_desc = fields.Char(string='Description for parameter 9')
    amt1 = fields.Float(string='Amount 1')
    amt2 = fields.Float(string='Amount 2')
    amt3 = fields.Float(string='Amount 3')
    amt4 = fields.Float(string='Amount 4')
    amt5 = fields.Float(string='Amount 5')
    amt1_desc = fields.Char(string='Description for amount 1')
    amt2_desc = fields.Char(string='Description for amount 2')
    amt3_desc = fields.Char(string='Description for amount 3')
    amt4_desc = fields.Char(string='Description for amount 4')
    amt5_desc = fields.Char(string='Description for amount 5')
    date1 = fields.Datetime(string='Date 1')
    date2 = fields.Datetime(string='Date 2')
    date3 = fields.Datetime(string='Date 3')
    date4 = fields.Datetime(string='Date 4')
    date5 = fields.Datetime(string='Date 5')
    date1_desc = fields.Char(string='Description for date 1')
    date2_desc = fields.Char(string='Description for date 2')
    date3_desc = fields.Char(string='Description for date 3')
    date4_desc = fields.Char(string='Description for date 4')
    date5_desc = fields.Char(string='Description for date 5')
    # 20-Nov-2016 DT
    int1 = fields.Integer(string='Integer 1')
    int2 = fields.Integer(string='Integer 2')
    int3 = fields.Integer(string='Integer 3')
    int4 = fields.Integer(string='Integer 4')
    int5 = fields.Integer(string='Integer 5')
    int1_desc = fields.Char(string='Description for integer 1')
    int2_desc = fields.Char(string='Description for integer 2')
    int3_desc = fields.Char(string='Description for integer 3')
    int4_desc = fields.Char(string='Description for integer 4')
    int5_desc = fields.Char(string='Description for integer 5')

    _sql_constraints = [('unique_config_code','UNIQUE(code_type,code)','Config code must be unique')]
        
# Model for CONFIG table
# This table store various configuration record used through the system.
# Although similar in structure to the Common Code table, this is used primarily in configuration and
# the access rights of this table will be more restrictive.

class tran_type(models.Model):
    _name = 'vb.tran_type'
    _description = 'Transaction type'
    _order = 'model,list_seq,code'
    
    # field
    name = fields.Char(string='Description',required=True)
    model = fields.Char(string='Model',required=True,index=True)
    view = fields.Char(string='View')
    code = fields.Char(string='Code',required=True)
    ref_format = fields.Char(string='Format for reference')
    default_rec = fields.Boolean(string='Default code',default=False)
    list_seq = fields.Integer(string='List order')
        
# Model for MSG_TEMPLTE table
# This table store message templates that are used during sending of messages to customer.

class msg_template(models.Model):
    _name = 'vb.msg_template'
    _description = 'Message template'
    _order = 'list_seq,code'
    
    # field
    name = fields.Char(string='Description')
    code = fields.Char(string='Code',index=True)
    default_rec = fields.Boolean(string='Default code',default=False)
    subject = fields.Char(string='Subject')
    template = fields.Html(string='Template')
    list_seq = fields.Integer(string='List order')
    # 17-Mar-2017 DT
    msg_class_id = fields.Many2one(comodel_name='vb.common_code',string='Message class Id',ondelete='restrict',domain="[('code_type','=','MsgClass')]")
    msg_class = fields.Char(string='Message class',related='msg_class_id.code',store=True,readonly=True)

    _sql_constraints = [('unique_template_code','UNIQUE(code)','Another template with the same code already exist')]
        
# Model for MSG_LOG table
# This table store message templates that are used during sending of messages to customer.

class msg_log(models.Model):
    _name = 'vb.msg_log'
    _description = 'Message log'
    _rec_name = 'msg_mode'
    
    @api.multi
    def get_template_name(self):
        for i in self:
            if i.template_code:
                i.update({'template_name': self.env['vb.msg_template'].search([('code','=',i.template_code)]).subject})
            
    # field
    msg_create_time = fields.Datetime(string='Message created date/time')
    msg_send_time = fields.Datetime(string='Message send date/time')
    # Mode = Email (currently)
    msg_mode = fields.Char(string='Message mode')
    # Depending on msg_mode, if Email, then email address
    destination = fields.Char(string='Destination address')
    # Code in vb.msg_tmplate class
    template_code = fields.Char(string='Code')
    # This is the original template with the 'variables' replaced with actual values
    template_output = fields.Html(string='Template out')
    parm_json = fields.Char(string='Parameters in JSON string')
    # Server address can be in IP <nnn.nnn.nnn.nnn> or host name
    server_addr = fields.Char(string='Server address')
    # The next field is the response code returned by the server that's sending out the message
    server_response = fields.Char(string='Server response')
    comments = fields.Char(string='Comments')
    state = fields.Selection([
        ('OK', 'Message send successfully'),
        ('Failed', 'Message not sent'),
        ('ConnectError', 'Cannot connect to server'),
        ('success', 'Message send successfully'),
        ], string='Status')
    # 08-Sep-2016 DT: These fields optionally associate the message to the customer account
    customer_id = fields.Many2one(comodel_name='vb.customer',string='Customer')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account')
    acct_no = fields.Char(comodel_name='vb.customer_acct', string='Customer account',related='acct_id.acct_no',store=True,readonly=True)
    template_name = fields.Char('Description',compute=get_template_name)

# Model for AUDIT LOG table
# This table store details of the field changes made to important tables in the system (e.g. Customer, Customer account, etc).
# Records in this table will be created only by the database triggers.
# Targeted number of records: Large - Estimated 1-2M
# Maintenance: need to archive monthly

class auditlog(models.Model):
    _name = 'vb.auditlog'
    _description = 'Audit log'
    
    # field
    dbtable = fields.Char(string='Database table')
    recordkey = fields.Char(string='Record key')
    recordname = fields.Char(string='Record name')
    recordcode = fields.Char(string='Record code')
    audit_type = fields.Selection([
        ('I', 'Insert'),
        ('U', 'Update'),
        ('D', 'Delete'),
        ], string='Audit type')
    fieldnm = fields.Char(string='Field name')
    value_before = fields.Char(string='Value before')
    value_after = fields.Char(string='Value after')
    user_id = fields.Many2one(comodel_name='res.users', string='Who')
    comments = fields.Char(string='Comments')

# Model for AUDIT CONFIG table
# This table store what table and what fields need to be audited for changes

class audit_tbl_config(models.Model):
    _name = 'vb.audit_tbl_config'
    _description = 'Audit table configuration'
    
    # field
    table_name = fields.Char(string='Database table')
    table_field = fields.Char(string='Field name')
    audit_flag = fields.Selection([
        ('Y','Yes'),
        ('N','No'),
        ],string='Audit flag',default='Y')

# Model for PROCESSING EXECUTION REQUEST table
# This table is used to keep track of request made to submit a stored procedure in the database.
# Store procedures perform various batch-style processing within the database.
# Targeted number of records: Medium - 10-20 records per day.
# Maintenance: need to archive every 3 months

class proc_exec_request(models.Model):
    _name = 'vb.procedure_exec_request'
    _description = 'Procedure execution request'
    
    # field
    name = fields.Char(string='Procedure name')
    procfn = fields.Char(string='Procedure function')
    rqs_time = fields.Datetime(string='Request time')
    rqsby_id = fields.Many2one(comodel_name='res.users', string='Requested by')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    parm6 = fields.Char(string='Parameter 6')
    parm7 = fields.Char(string='Parameter 7')
    parm8 = fields.Char(string='Parameter 8')
    parm9 = fields.Char(string='Parameter 9')
    rec_count = fields.Integer(string='Records count')
    rec_processed = fields.Integer(string='Records processed')
    rec_skipped = fields.Integer(string='Records skipped')
    retcd = fields.Char(string='Return code')
    state = fields.Char(string='Status')

# Model for capturing system / application log messages

class error_log(models.Model):
    _name = 'vb.error_log'
    _description = 'Error log'
    
    # field
    log_timestamp = fields.Datetime(string='Timestamp')
    # Code or ID of the function
    function_code = fields.Char(string='Function_code')
    # Name of the function
    function_name = fields.Char(string='Function_name')
    log_type = fields.Char(string='Log type')
    # Info, Warning, Error
    severity = fields.Char(string='Severity')
    # Low, Medium, High
    log_message = fields.Char(string='Message')
    # 04-Jan-2016
    log_category = fields.Char(string='category')
    # e.g. KIBB-Interface
    dbtable = fields.Char(string='Database table')
    recordid = fields.Integer(string='Record ID')
    batch = fields.Char(string='Batch')
    state = fields.Char(string='Status')

# Inherited model to view user logins 
# Added the user IP to record where the user login from.     

class res_users_log(models.Model):
    _inherit = 'res.users.log'
    
    # field
    write_date = fields.Datetime(string='Login date')
    write_uid = fields.Many2one(comodel_name='res.users', string='Login user')
    #Method to get the uid ip address[21-12-016-Developing Team]
    user_ip = fields.Text(string='User IP')
    
# Model inherited to get the ip address of UID login [D 21-12-016-developing team] 
class res_users(models.Model):
    _inherit= 'res.users'
     
    def _get_ipaddress(self, cr, uid, context=None):
        return request.httprequest.environ['REMOTE_ADDR'] 
#   use get_ip_address method to store the value insiide user_ip field in res_users_log table
#   Override last login method to get the ip address and user uid url while creating [D 21-12-016-Developing Team]
    def _update_last_login(self, cr, uid):
#        # only create new records to avoid any side-effect on concurrent transactions
#        # extra records will be deleted by the periodical garbage collection
        self.pool['res.users.log'].create(cr, uid, {'user_ip':self._get_ipaddress(self, cr, uid)}) # To store the user login local ip address..

# Model for proc queue table.
# This table is used to queue records that needs to be processed by scheduled jobs
# A good example will be to send email notification to customers when certain action
# have been performed.

class proc_queue(models.Model):
    _name = 'vb.proc_queue'
    _description = 'Process queue'
    
    # field
    dbtable = fields.Char(string='Database table')
    recordid = fields.Integer(string='Record ID')
    proc_type = fields.Char(string='Process type')
    # AcctOpenNotify
    # AcctPendingCloseNotify
    # AcctClosedNotify
    # AcctMthStmtNotify
    # HelpdeskNewCaseNotify
    # ... more to come ...
    proc_freq = fields.Char(string='Process frequency')
    # ASAP - as frequency as possible - 15 minutes
    # EOD
    # EOM
    rqs_time = fields.Datetime(string='Request time')
    rqsby_id = fields.Many2one(comodel_name='res.users', string='Requested by')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    text1 = fields.Text(string='Text 1')
    text2 = fields.Text(string='Text 2')
    state = fields.Selection([
        ('New', 'New'),
        ('Processed', 'Processed'),
        ('Failed', 'Failed'),
        ], string='Status')
    proc_time = fields.Datetime(string='Processed time')
    proc_by = fields.Many2one(comodel_name='res.users', string='Processed by')
    proc_text = fields.Char(string='Process text - if process failed')
    purge_flag = fields.Boolean(string='To be purged')

# Model for proc_lock table.
# This table is used to provide a locking mechanism to prevent certain processes from
# running repeatedly within a given period, whether the period is within the Day, Month or year.
    
class proc_lock(models.Model):
    _name = 'vb.proc_lock'
    _description = 'Process lock'
    
    # field
    proc_code = fields.Char(string='Procedure code', index=True)
    # same as procfn in vb_procedure_exec_request
    proc_freq = fields.Char(string='Process frequency')
    # Day / Month / Year
    proc_time = fields.Datetime(string='Processed time')
    proc_period = fields.Char(string='Processing period')
    # YYYY-MM-DD or YYYY-MM or YYYY
    state = fields.Selection([
        ('InProgress', 'In progress'),
        ('Processed', 'Processed'),
        ('Failed', 'Failed'),
        ], string='Status')

# Model for SYS-RECON table.
# This table is used for daily account balance reconciliation purposes
# Records in this model will be created by reconciliation SP
    
class sys_recon(models.Model):
    _name = 'vb.sys_recon'
    _description = 'System reconciliation'
    
    # field
    recon_date = fields.Date(string='Reconciliation date', index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',index=True)
    order_buy_amt = fields.Float(string='YTD Buy order amount',digits=(15,2))
    order_sell_amt = fields.Float(string='YTD Sell order amount',digits=(15,2))
    matched_buy_amt = fields.Float(string='YTD matched buy order amount',digits=(15,2))
    matched_sell_amt = fields.Float(string='YTD matched sell order amount',digits=(15,2))
    order_buy_amt1 = fields.Float(string='YTD Buy order amount',digits=(15,2))
    order_sell_amt1 = fields.Float(string='YTD Sell order amount',digits=(15,2))
    matched_buy_amt1 = fields.Float(string='YTD matched buy order amount',digits=(15,2))
    matched_sell_amt1 = fields.Float(string='YTD matched sell order amount',digits=(15,2))
    trade_os_long_amt = fields.Float(string='Outstanding long position',digits=(15,2))
    trade_os_short_amt = fields.Float(string='Outstanding short position',digits=(15,2))
    trade_os_others_amt = fields.Float(string='Trade other outstanding',digits=(15,2))
    trade_mtd_buy_bal = fields.Float(string='Trade MTD buy bal',digits=(15,2))
    trade_mtd_sell_bal = fields.Float(string='Trade MTD sell bal',digits=(15,2))
    trade_mtd_contra_bal = fields.Float(string='Trade contra bal',digits=(15,2))
    trade_mtd_charge_bal = fields.Float(string='Trade charge bal',digits=(15,2))
    trade_mtd_credit_bal = fields.Float(string='Trade credit bal',digits=(15,2))
    trade_mtd_other_bal = fields.Float(string='Trade other bal',digits=(15,2))
    trade_mtd_opening_amt = fields.Float(string='Trade MTD opening amt',digits=(15,2))
    trade_mtd_buy_amt = fields.Float(string='Trade MTD buy amt',digits=(15,2))
    trade_mtd_sell_amt = fields.Float(string='Trade MTD sell amt',digits=(15,2))
    trade_mtd_contra_amt = fields.Float(string='Trade MTD contra amt',digits=(15,2))
    trade_mtd_charge_amt = fields.Float(string='Trade MTD charge amt',digits=(15,2))
    trade_mtd_credit_amt = fields.Float(string='Trade MTD credit amt',digits=(15,2))
    trade_mtd_tradesett_amt = fields.Float(string='Trade MTD tradesett amt',digits=(15,2))
    trade_mtd_other_amt = fields.Float(string='Trade MTD other amt',digits=(15,2))
    trade_mtd_buy_amt1 = fields.Float(string='Trade MTD buy amt (new)',digits=(15,2))
    trade_mtd_sell_amt1 = fields.Float(string='Trade MTD sell amt (new)',digits=(15,2))
    trade_mtd_contra_amt1 = fields.Float(string='Trade MTD contra amt (new)',digits=(15,2))
    trade_mtd_charge_amt1 = fields.Float(string='Trade MTD charge amt (new)',digits=(15,2))
    trade_mtd_credit_amt1 = fields.Float(string='Trade MTD credit amt (new)',digits=(15,2))
    trade_mtd_tradesett_amt1 = fields.Float(string='Trade MTD tradesett amt (new)',digits=(15,2))
    trade_mtd_other_amt1 = fields.Float(string='Trade MTD other amt (new)',digits=(15,2))
    trade_mtd_buy_amt2 = fields.Float(string='Trade MTD buy amt (void)',digits=(15,2))
    trade_mtd_sell_amt2 = fields.Float(string='Trade MTD sell amt (void)',digits=(15,2))
    trade_mtd_contra_amt2 = fields.Float(string='Trade MTD contra amt (void)',digits=(15,2))
    trade_mtd_charge_amt2 = fields.Float(string='Trade MTD charge amt (void)',digits=(15,2))
    trade_mtd_credit_amt2 = fields.Float(string='Trade MTD credit amt (void)',digits=(15,2))
    trade_mtd_tradesett_amt2 = fields.Float(string='Trade MTD tradesett amt (void)',digits=(15,2))
    trade_mtd_other_amt2 = fields.Float(string='Trade MTD other amt (void)',digits=(15,2))
    trade_mtd_closing_amt = fields.Float(string='Trade MTD closing amt',digits=(15,2))
    cash_ledger_bal = fields.Float(string='Cash ledger balance',digits=(15,2))
    cash_available_bal = fields.Float(string='Cash available balance',digits=(15,2))
    cash_earmarked_amt = fields.Float(string='Cash earmarked amount',digits=(15,2))
    cash_collateral_amt = fields.Float(string='Cash collateral amount',digits=(15,2))
    cash_mtd_opening_amt = fields.Float(string='Cash MTD opening amt',digits=(15,2))
    cash_mtd_tradesett_amt = fields.Float(string='Cash MTD tradesett amt',digits=(15,2))
    cash_mtd_deposit_amt = fields.Float(string='Cash MTD deposit amt',digits=(15,2))
    cash_mtd_withdraw_amt = fields.Float(string='Cash MTD withdraw amt',digits=(15,2))
    cash_mtd_charge_amt = fields.Float(string='Cash MTD charge amt',digits=(15,2))
    cash_mtd_credit_amt = fields.Float(string='Cash MTD credit amt',digits=(15,2))
    cash_mtd_earmark_amt = fields.Float(string='Cash MTD earmark amt',digits=(15,2))
    cash_mtd_collateral_amt = fields.Float(string='Cash MTD collateral amt',digits=(15,2))
    cash_mtd_transfer_amt = fields.Float(string='Cash MTD transfer amt',digits=(15,2))
    cash_mtd_other_amt = fields.Float(string='Cash MTD other amt',digits=(15,2))
    cash_mtd_tradesett_amt1 = fields.Float(string='Cash MTD tradesett amt (new)',digits=(15,2))
    cash_mtd_deposit_amt1 = fields.Float(string='Cash MTD deposit amt (new)',digits=(15,2))
    cash_mtd_withdraw_amt1 = fields.Float(string='Cash MTD withdraw amt (new)',digits=(15,2))
    cash_mtd_charge_amt1 = fields.Float(string='Cash MTD charge amt (new)',digits=(15,2))
    cash_mtd_credit_amt1 = fields.Float(string='Cash MTD credit amt (new)',digits=(15,2))
    cash_mtd_earmark_amt1 = fields.Float(string='Cash MTD earmark amt (new)',digits=(15,2))
    cash_mtd_collateral_amt1 = fields.Float(string='Cash MTD collateral amt (new)',digits=(15,2))
    cash_mtd_transfer_amt1 = fields.Float(string='Cash MTD transfer amt (new)',digits=(15,2))
    cash_mtd_other_amt1 = fields.Float(string='Cash MTD other amt (new)',digits=(15,2))
    cash_mtd_tradesett_amt2 = fields.Float(string='Cash MTD tradesett amt (void)',digits=(15,2))
    cash_mtd_deposit_amt2 = fields.Float(string='Cash MTD deposit amt (void',digits=(15,2))
    cash_mtd_withdraw_amt2 = fields.Float(string='Cash MTD withdraw amt (void)',digits=(15,2))
    cash_mtd_charge_amt2 = fields.Float(string='Cash MTD charge amt (void)',digits=(15,2))
    cash_mtd_credit_amt2 = fields.Float(string='Cash MTD credit amt (void)',digits=(15,2))
    cash_mtd_earmark_amt2 = fields.Float(string='Cash MTD earmark amt (void)',digits=(15,2))
    cash_mtd_collateral_amt2 = fields.Float(string='Cash MTD collateral amt (void)',digits=(15,2))
    cash_mtd_transfer_amt2 = fields.Float(string='Cash MTD transfer amt (void)',digits=(15,2))
    cash_mtd_other_amt2 = fields.Float(string='Cash MTD other amt (void)',digits=(15,2))
    cash_mtd_closing_amt = fields.Float(string='Cash MTD closing amt',digits=(15,2))
    portfolio_val = fields.Float(string='Portfolio holding value',digits=(15,2))
    portfolio_cost = fields.Float(string='Portfolio holding cost',digits=(15,2))
    portfolio_earmarked_amt = fields.Float(string='Portfolio earmarked amount',digits=(15,2))
    portfolio_earmarked_loc = fields.Float(string='Portfolio earmarked loc amount',digits=(15,2))
    portfolio_unrlsd_pl_amt = fields.Float(string='Portfolio unrealised profit/loss',digits=(15,2))
    portfolio_rlsd_pl_amt = fields.Float(string='Portfolio realised profit/loss',digits=(15,2))
    portfolio_unrlsd_pl_loc = fields.Float(string='Portfolio unrealised loc profit/loss',digits=(15,2))
    portfolio_rlsd_pl_loc = fields.Float(string='Portfolio realised loc profit/loss',digits=(15,2))
    port_mtd_opening_amt = fields.Float(string='Portfolio MTD opening amt',digits=(15,2))
    port_mtd_buy_amt = fields.Float(string='Portfolio MTD buy amt',digits=(15,2))
    port_mtd_sell_amt = fields.Float(string='Portfolio MTD sell amt',digits=(15,2))
    port_mtd_deposit_amt = fields.Float(string='Portfolio MTD deposit amt',digits=(15,2))
    port_mtd_withdraw_amt = fields.Float(string='Portfolio MTD withdraw amt',digits=(15,2))
    port_mtd_earmark_amt = fields.Float(string='Portfolio MTD earmark amt',digits=(15,2))
    port_mtd_other_amt = fields.Float(string='Portfolio MTD other amt',digits=(15,2))
    port_mtd_closing_amt = fields.Float(string='Portfolio MTD closing amt',digits=(15,2))
    state = fields.Selection([
        ('OK', 'OK'),
        ('Failed', 'Failed'),
        ('New', 'New'),
        ], string='Status',default='New')

# Model for DEV_TEMPLATE table.
# This table is used to store the template for device preference settings.
# Used mainly at the front-end
    
class dev_template(models.Model):
    _name = 'vb.dev_template'
    _description = 'Device template'

    # field
    device_code = fields.Char(string='Device code',help='This field uniquely identifies the smart device.')
    code_type = fields.Char(string='Code type')
    code = fields.Char(string='Code key')
    name = fields.Char(string='Name')
    parm1 = fields.Char(string='Parameter 1')
    parm2 = fields.Char(string='Parameter 2')
    parm3 = fields.Char(string='Parameter 3')
    parm4 = fields.Char(string='Parameter 4')
    parm5 = fields.Char(string='Parameter 5')
    active = fields.Boolean(string='Active',default=True)    

# Model for LOGINNAME_LIST table.
# This table is used to store list of login names (or patterns) that are white or blacklisted listed.
    
class loginname_blacklist(models.Model):
    _name = 'vb.loginname_list'
    _description = 'Login name list'

    # field
    name = fields.Char(string='Name')
    list_type = fields.Selection([
        ('Blacklist', 'Blacklist'),
        ('Whitelist', 'Whitelist'),
        ], string='List type',default='Blacklist')
    active = fields.Boolean(string='Active',default=True)    

    