odoo.define('vb_base.hide',function(require){
    "use strict";

    var ListView = require('web.ListView');
    var SearchView = require('web.SearchView');

    ListView.include({
        view_loading: function(r) {
        var self = this;
        if ((this.options.action) && ('hide_checkboxes_treeview' in this.options.action.context))
        {
            this.options.selectable = false;
        }
        if ((this.options.action) && ('import_enable' in this.options.action.context))
        {
            this.options.import_enabled = true;
        }
        else
        {
            this.options.import_enabled = false;
        }
        return this.load_list(r);
        },
    });

    SearchView.include({
        init: function(parent, dataset, view_id, defaults, options) {
            if ((options.action) && ('hide_searchview' in options.action.context))
            {
                options.hidden = true;
            }
            this._super(parent, dataset, view_id, defaults, options);
        },
    });

});
