odoo.define('vb_base.web_disable_export',function(require){
        "use strict";
    var core = require('web.core');
    var _t = core._t;
    var SUPERUSER_ID = 1;
    var Sidebar = require('web.Sidebar');

    Sidebar.include({

        add_items: function(section_code, items) {
            // allow Export for admin user
            if (this.session.uid == SUPERUSER_ID) {
                this._super.apply(this, arguments);
            }
            else {
                var export_label = _t("Export");
                var new_items = items;
                if (section_code == 'other') {
                    new_items = [];
                    for (var i = 0; i < items.length; i++) {
                        if (items[i]['label'] != export_label) {
                            new_items.push(items[i]);
                        }
                    }
                }
                if (new_items.length > 0) {
                    this._super.call(this, section_code, new_items);
                }
            }
            
        },

    });

});

// Created by Ahmad aldaker