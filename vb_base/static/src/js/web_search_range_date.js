odoo.define("vb_base.search",function (require) {

    var core = require('web.core');
    var SearchView = require('web.SearchView');
    var ViewManager = require('web.ViewManager');
    var FormView = require('web.FormView');
    var Model = require('web.Model');

    var QWeb = core.qweb;
    var _t = core._t;

    SearchView.include({
        start: function() {
            var result = this._super();
            var self = this;
            self.search_on = "name";
            self.fields_selection = [];
            return result;
        },
        search_filter: function(){
        $(document).ready(function() { 
        
        
//        
            $('.navbar-inverse .navbar-nav li a').on('click', function(){
            $('.navbar-inverse .navbar-nav li a').removeClass('menu_css');
            $(this).addClass('menu_css');});
//             $(this).css({'color':'#660000'});
//         });
//         $('.navbar-inverse .navbar-nav li a').on('click', function(){
//             $('.navbar-inverse .navbar-nav li').find('a').css({'color':'white','background-color':'#660000'});
//             $(this).closest('li').find('a').css({'color':'#660000','background-color':'white'});
// //             $(this).css({'color':'#660000'5566});
//         });
        
             setInterval(function() {
                $('#all_required').animate( { color: 'red' }, 300)
                .animate( { color: '#660000' }, 300); 
                }, 1000);
            
             }); 
            var filter_domain = [];
            var self = this;
            var from_date_updated;
            var to_date_updated;
            var x;
            var y;
            var g;
            var from_d;
            var from_date_time;
            var sp;
            var to_d;
            var to_date_time;
            var sp1;
            var day;
            console.log("asdas",self.to_date,this);
            new Model("vb.customer_acct").call("test_print");
            
            if (self.from_date && self.to_date && self.field_name_selection) {
                if(jQuery.type(self.to_date)== jQuery.type(self.from_date)){
                    sp = self.from_date.split(' ');
                    from_d = sp[0];
                    from_date_time = sp[1];
                    if ( self.field_name_selection === 'tran_date'){
                        x = from_d.split('/');
                        console.log('EEEEEEEEE',from_date_time);
                        sp1 = self.to_date.split(' ');
                        to_d = sp1[0];
                        to_date_time = sp1[1];
                        y =to_d.split('/');
                        g = x[0];
                        x[0]=x[2];
                        x[2]=g;
                        console.log('x[2]',x[2]);
                        day = (x[2]).toString();
    
                        from_date_updated = x[0]+'-'+x[1]+'-'+day;
                        g = y[0];
                        y[0]=y[2];
                        y[2]=g;
                        to_date_updated = y[0]+'-'+y[1]+'-'+y[2];
                        filter_domain.push("[('" + self.field_name_selection + "', '>=', '" + from_date_updated +"'),"+"('" + self.field_name_selection + "', '<=', '" + to_date_updated +"')]");
                        }
                    else{
                        x = self.from_date.split('/');

                        y = self.to_date.split('/');
                        g = x[0];
                        x[0]=x[2];
                        x[2]=g-1;
                        if (x[2]===0){

                            if (x[1]==01 || x[1]==02 || x[1]==04 || x[1]==06 ||x[1]==08 || x[1]==09|| x[1]==11){
                                x[2] = 31;
                                x[1] = x[1]-1;
                                if (x[1]===0)
                                {
                                    
                                    x[1]=12;
                                    x[0] = x[0]-1;

                                }
                            }
                            else if (x[1]==05 || x[1]==07 || x[1]==10 ||x[1]==12)
                            {
                                x[2] = 30;
                                x[1] = x[1]-1;
                                
                            }
                            
                            else{
                                x[2] = 28;
                                x[1] = x[1]-1;
                            }
                        }
                        from_date_updated = x[0]+'-'+x[1]+'-'+x[2];
                        g = y[0];
                        y[0]=y[2];
                        y[2]=g;
                        to_date_updated = y[0]+'-'+y[1]+'-'+y[2];
                        console.log('SSSSSSSSSSSSSSSSSSS',from_date_updated)
                        filter_domain.push("[('" + self.field_name_selection + "', '>=', '" + from_date_updated +" "+"16:00:00"+"'),"+"('" + self.field_name_selection + "', '<=', '" + to_date_updated +" "+"15:59:59"+ "')]");
                        console.log('I AM HERE',from_date_updated +" "+"16:00:00")
                    }
                    

                }
//                 else{
//                     x = self.from_date.split('/');
//                     g = x[0];
//                     x[0]=x[2];
//                     x[2]=g;
//                     from_date_updated = x[0]+'-'+x[1]+'-'+x[2];
//                     filter_domain.push("[('" + self.field_name_selection + "', '>=', '" + from_date_updated + "')]");
//                     
//                 }
            }

            if (filter_domain.length) {
                var filter_or_domain = [];
                for (i = 0; i < filter_domain.length-1; i++) {
                    filter_or_domain.push("['|']");
                }
                return filter_or_domain.concat(filter_domain || []);
            }
            return false;
        },

        build_search_data: function () {
            var result = this._super();
            var filter_domain = this.search_filter();
            if (filter_domain)
                result['domains'] = filter_domain.concat(result.domains || []);
            return result;
        }
    });

    ViewManager.include({
        start: function() {
            var self = this;
            this._super().done(function(){
                if('enable_date_range_bar' in self.dataset.context){
                    self.dataset.call('fields_get', [false, {}]).done(function (fields) {
                        search_fields_string = self.dataset.context.search_fields
                        if (self.dataset.context.search_fields != null)
                        {
                            var fields_array =  search_fields_string.split(',')
                            console.log("fields_array", fields_array)
                            $.each(fields, function (value) {
                                $.inArray(value, fields_array)
                                if($.inArray(value, fields_array) != -1){
                                    console.log("value", value)
                                    fields[value].name = value;
                                    self.searchview.fields_selection.push(fields[value]);
                                }
                            })
                        }
                        else{
                             $.each(fields, function (value) {

                                 if(fields[value].type == "datetime" || fields[value].type == "date"){
                                     fields[value].name = value;
                                     self.searchview.fields_selection.push(fields[value]);
                                 }
                             })
                        }
                        if(self.searchview.fields_selection.length){
                            $(self.el).find('.field_selection_column').append((QWeb.render('field-selection', {widget: self})));
                            $(self.el).find(".search_to_date").datepicker({dateFormat: "dd/mm/yy"});
                            $(self.el).find(".search_from_date").datepicker({dateFormat: "dd/mm/yy"});
                            $(self.el).find(".search_from_date").change(function(){
                                var to_date = $(self.el).find(".search_to_date").val()
                                console.log('to date1: ' + to_date)
                                console.log('from date: ' + $(this).val());
                                if (to_date) {
                                    self.searchview.to_date =  to_date;
//                                     console.log("Hello",self.searchview.to_date.)
                                    self.searchview.from_date =  $(this).val();
                                    self.searchview.field_name_selection = $(self.el).find('#field_name_selection').val()
                                    
                                    $(this).toggleClass('enabled');
                                }
                            })

                            $(self.el).find(".search_to_date").change(function(){

                                var from_date = $(self.el).find(".search_from_date").val()
                                console.log('from date: ' + from_date)
                                console.log('to date: ' + $(this).val());
                                if (from_date) {
                                    self.searchview.from_date =  from_date;
                                    self.searchview.to_date =  $(this).val();
                                    self.searchview.field_name_selection = $(self.el).find('#field_name_selection').val()
                                    $(this).toggleClass('enabled');
                                }
                            })

                            $(self.el).find(".clear_filter").click(function () {
                                self.searchview.from_date =  false;
                                self.searchview.to_date =  false
                                $(this).toggleClass('enabled');
                                self.searchview.field_name_selection = false;
                                self.searchview.do_search();
                                $(self.el).find(".search_to_date").val("")
                                $(self.el).find(".search_from_date").val("")

                            });

                            $(self.el).find(".record_search").click(function () {
                                self.searchview.do_search();

                            });

                            $(self.el).find('#field_name_selection').change(function () {
                                self.searchview.field_name_selection = $(self.el).find('#field_name_selection').val();
                                var to_date = $(self.el).find(".search_to_date").val()
                                var from_date = $(self.el).find(".search_from_date").val()
                                if (to_date && from_date) {
                                    self.searchview.from_date =  from_date;
                                    self.searchview.to_date =  to_date;
                                    $(this).toggleClass('enabled');
                                }
                            })
                        }
                    });
                }else{
                    $(self.el).find('.search_filter').hide();
                }
            })
        },
        switch_mode: function(view_type, no_store, view_options) {
            var self = this;
            var view = this.views[view_type];
            var result = this._super(view_type, no_store, view_options);
            if (this.searchview && this.active_view != "form") {
                if (view != null)
                {
                    if(view.controller){
                        if ((view.controller.searchable === false || this.searchview.options.hidden)  || this.active_view == "form"){
                            $(self.el).find('.search_filter').hide();
                        }else
                            $(self.el).find('.search_filter').show();
                    }
                }
            }
            else{
                $(self.el).find('.search_filter').hide();
            }
            return result;
        },
    });
    FormView.include({
        load_defaults: function () {
            var self = this;
            if($(self.ViewManager.el).find('.search_filter'))
                $(self.ViewManager.el).find('.search_filter').hide();
            return this._super();
        },
    });
})