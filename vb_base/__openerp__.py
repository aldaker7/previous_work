# -*- coding: utf-8 -*-

{
    'name': 'VB Base',
    'version': '1.2',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['base'],
    'description': """

Virtual Broker Application - Base Module
======================================================

This module is referenced by all other modules in the VB System.  It contains
the core models required by other modules in the VBroker application.

Through this module, various application parameters, common data, setups
can be performed.

    """,
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/views_base.xml',
        'wizards/wizards_base.xml',
        'views/actions_base.xml',
        'views/menus_base.xml',
        'views/template.xml',
#        'data/setup_config.xml',
#        'data/setup_data.xml',
#        'data/setup_data2.xml',
#        'data/setup_user.xml',
#        'data/message_templates_data.xml',
#        'data/setup_state_asia1.xml',
#        'data/setup_state_asia2.xml',
#        'data/setup_state_asia3.xml'
    ],

    'qweb' : [
        'static/src/xml/web_search_range_date.xml',
        "static/src/xml/base.xml",
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
