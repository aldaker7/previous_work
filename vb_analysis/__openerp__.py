# -*- coding: utf-8 -*-

{
    'name': 'VB Analysis',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_trade'],
    'description': """

Virtual Broker Application - Analysis Module
======================================================

This module contains the views and functions for data analysis.

    """,
    'data': [
        'security/user_groups.xml',
        'views/views_analysis.xml',
        'views/menus_analysis.xml',
            ],

    'installable': True,
    'auto_install': False,
    'application': True,
}