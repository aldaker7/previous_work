# -*- encoding: utf-8 -*-

{
    'name': 'Vbroker Web Login Screen',
    'summary': 'The new configurable Vbroker Web Login Screen',
    'version': '1.2',
    'category': 'Website',
    'summary': """
The new configurable Vbroker Web Login Screen
""",
    'author': "VBroker Dev Team",
    'website': 'Virtual Broker',    
    'depends': [
    ],
    'description': """

Virtual Broker Application - Web Login Module
======================================================

This module contains the function for login screen

    """,
    'data': [
        'data/ir_config_parameter.xml',
        'templates/webclient_templates.xml',
        'templates/website_templates.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
}
