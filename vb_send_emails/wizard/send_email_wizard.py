from openerp import models, fields, api, _
from datetime import datetime,timedelta,date
from openerp.exceptions import Warning
import json
import string, random, base64, hashlib
import requests
from json import dumps
class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'
    
    @api.multi
    def send_email_manual(self):
        rec = self.env['vb.send_email'].search([('id','=',self._context.get('active_id'))])
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        for i in rec:
            if i.email_code == 'Cf1':
                mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
                if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                    common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                    return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
                    channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                    data= {
                    "custId":i.customer_id.id,
                    "acctId":i.acct_id,
                    "publisherApp":"backoffice",
                    "templateCode":"Cf1",
                    "messageParams":{"date":(d2(d1).strftime("%d-%m-%Y")) or '',
                                    "login_name":str(i.customer_id.login_name) or '',
                                    "date_opened":str(i.date_opened) or ''},
                    "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": i.customer_id.name or '',
                                 "email": i.customer_id.email1 or '',
                                 "title": i.customer_id.title or '',
                                }]
                    }
                    data_json=json.dumps(data)
                    channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
            elif i.email_code == 'TPW':
                print "YESS"
                url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','SendTpin')])
                url ="http://"+str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                if (url_rec.parm1 or url_rec.parm2 or url_rec.parm5)== False:
                    raise Warning('Please provide the params in system parameters code SendTpin')
                else:
                    data = dumps({ "loginName" : str(i.customer_id.login_name),
                           "accountNo" : str(i.acct_id.acct_no),
                           'tempPassword':str(self.env['vb.customer_login'].search([('id','=',i.customer_id.id),('login_domain','=','FE')]).login_pwd),
                         })
                    headers={'Content-type': 'application/json'}
                    content = requests.post(url,headers=headers,data=data)
                if content.json().get('code')!=200:
                    raise Warning(str(content.json().get('code'))+": "+content.json().get('message'))
                else:
                    raise Warning(content.json().get('code'))
            
        rec.update({'state':'Sent','sent_by':self._uid,'date_sent':datetime.now()})