# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Core models
#
# Author: Ahmad Aldaker
# Last updated: 8-Sep-2017

from openerp import models, fields, api, tools
from datetime import datetime, date
from openerp.exceptions import ValidationError
import string, re
from openerp.http import request
from dateutil.relativedelta import relativedelta


class send_email(models.Model):
    _name = 'vb.send_email'
    _description = 'Manual sending email'
    _rec_name = 'customer_id'
    

    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',index=True,domain="[('state','=','Active'),('customer_id','=',customer_id)]")
    email_id = fields.Many2one(comodel_name='vb.msg_template', string='Email type',index=True,domain=[('code','in',['TPW','Cf1'])])
    email_code = fields.Char(string='Email Code',related='email_id.code')
    state = fields.Selection([
            ('New', 'New'),
            ('Sent', 'Sent'),
            ], string='Status', default='New',help="This shows the current status of the record")
    date_sent = fields.Datetime(string='Date sent')
    sent_by = fields.Many2one(comodel_name='res.users',string='Sent by')
    
    
    @api.multi
    def send_email_manu(self):
        if bool(self.customer_id and self.email_id) == True:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_send_emails','send_manual_email_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':"Send email",
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            raise Warning("Please fill in all the required fields")