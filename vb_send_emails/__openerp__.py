# -*- coding: utf-8 -*-

{
    'name': 'VB Send email',
    'version': '1.2',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['base'],
    'description': """

Virtual Broker Application - Base Module
======================================================

This module is referenced by all other modules in the VB System.  It contains
the core models required by other modules in the VBroker application.

Through this module, various application parameters, common data, setups
can be performed.

    """,
    'data': [
        'views/view_send_email.xml',
        'views/action_send_email.xml',
        'views/menu_send_email.xml',
        'wizard/send_email_wizard.xml',
    ],


    'installable': True,
    'auto_install': False,
    'application': True,
}
