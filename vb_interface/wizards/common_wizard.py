from openerp import models, fields, api, _
from datetime import datetime
from openerp.exceptions import Warning
import new
from json import dumps
import json
import requests

class common_wizards(models.TransientModel):
    _inherit='vb.common_wizard'
    
    @api.multi
    def export_kibb(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','KibbExport1')])
        url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if url==False:
            raise Warning('Please provide the IP to connect with webservice')
        data = dumps({})
        headers={'Content-type': 'application/json'}
        content = requests.post(url,data=data,headers=headers) 
        if content.json().get('code')!=200 or content ==False:
            raise Warning('Can not connect with web service')      
        
    @api.multi
    def import_kibb(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','KibbImport1')])
        url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if url==False:
            raise Warning('Please provide the IP to connect with kibb webservice')
        data = dumps({})
        headers={'Content-type': 'application/json'}
        content = requests.post(url,data=data,headers=headers)       
        if content.json().get('code')!=200 or content ==False:
            raise Warning('Can not connect with kibb web service')
        
        # Calling FRA webservice
        url_rec1 = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','FRADataExchange')])
        url1 = str(url_rec1.parm1)+":"+str(url_rec1.parm2)+'/'+str(url_rec1.parm5)
        if url1==False:
            raise Warning('Please provide the IP to connect with FRA webservice')
        data = dumps({})
        headers={'Content-type': 'application/json'}
        content = requests.post(url1,data=data,headers=headers)       
        if content.json().get('code')!=200 or content ==False:
            raise Warning('Can not connect with FRA web service')
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'SOD_process_completion_wizard_view')[1]
            message = str('The SOD process has been completed successfully.')
            return {
                'name':'SOD process successful.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

    @api.multi
    def confirm_kibb_corp_action_dividend_pay(self):
        asset_recs = self.env['vb.kibb_corp_action_dividend_pay'].search([('id', 'in', self._context.get('active_ids'))])
        processed = 0
        not_processed = 0
        for rec in asset_recs:
            if rec.state == 'New':
                rec.write({'state': 'Posted', 'resolved_by': self._uid, 'date_resolved': datetime.now()})
                processed += 1
            else:
                not_processed += 1
        final_view = self.env['ir.model.data'].get_object_reference('vb_interface', 'kibb_corp_action_dividend_pay_processed_view')[1]
        message = str(processed) + ' records processed, ' + str(not_processed) + ' records not processed'
        return {
            'name':'Posted status',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'view_id': final_view,
            'target': 'new',
            'context': {'default_message': message},
            }
            
      
    # Method for EIKON upload        
    @api.multi
    def upload_eikon(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','EIKONupload')])
        url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if url==False:
            raise Warning('Please provide the IP to connect with webservice')
        data = dumps({})
        headers={'Content-type': 'application/json'}
        content = requests.post(url,data=data,headers=headers)       
        if content.json().get('code')!=200 or content ==False:
            raise Warning('Can not connect with web service')
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_interface', 'eikon_upload_process_completion_wizard_view')[1]
            message = str('The EIKON upload process has been completed successfully.')
            return {
                'name':'EIKON upload process successful.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }


    # Method to confirm Cancel Subscribe Corporate Action
    @api.multi
    def confirm_corp_action_cancel(self):
        rec = self.env['vb.kibb_corp_action_subs_conv_dtl'].search([('id','=',self._context.get('active_id')),('state','=','Subscribed')])
        if rec:
            rec.write({'state': 'PendingCancel','stage':'PendingCancel', 'cancel_by':self._uid, 'cancel_remark':self.note})
            # final_view = self.env['ir.model.data'].get_object_reference('vb_interface', 'corp_action_request_cancel')[1]
            # message = str('Request for cancellation done')
            # return {
            #     'name':'Corporation action request cancel done',
            #     'type': 'ir.actions.act_window',
            #     'view_type': 'form',
            #     'view_mode': 'form',
            #     'res_model': 'vb.common_wizard',
            #     'view_id': final_view,
            #     'target': 'new',
            #     'context': {'default_message': message},
            # }
        else:
            raise Warning('Record is not in Subscribed state')

    # Method to cancel subscribe corporate action
    @api.multi
    def approve_corp_action_cancel(self):
        rec = self.env['vb.kibb_corp_action_subs_conv_dtl'].search([('id','=',self._context.get('active_id')),('state','=','PendingCancel')])
        if rec:
            if int(rec['write_uid']) != self._uid:
                rec.write({'state': 'Cancelled','stage':'Cancelled', 'date_cancel':datetime.now()})
            else:
                raise Warning(_('You are not authorized to approve this cancellation!'))
        else:
            raise Warning('Record is not in PendingCancel state')
