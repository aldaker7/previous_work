# -*- coding: utf-8 -*-

{
    'name': 'VB Interface',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Interface Module
======================================================

This module contains models for interface to external parties

    """,
    'data': [
        'views/views_interface.xml',
        'views/actions_interface.xml',
        'views/menus_interface.xml',
        'wizards/interface_wizards.xml',
        'security/ir.model.access.csv',
    ],
    
    'installable': True,
    'auto_install': False,
    'application': True,
}
