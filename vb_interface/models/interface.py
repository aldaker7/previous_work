from openerp import models, fields,api
from datetime import datetime, date, timedelta


# This model will the various categories of cases that can occur.
# e.g. Inquiry, Issues, Assistant, etc

class kibb_acct_create_close(models.Model):
    _name = 'vb.kibb_acct_create_close'
    _description = 'KIBB Customer Account opening'
    _order = 'name'

    name = fields.Char(string='Customer name')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account')
    client_id = fields.Char(string='Client ID')
    # EBG account number
    product_type = fields.Char(string='Product type')
    # C=eCash UpFront D-eDayTrade88(nom) E=Prepaid F=ePremier88(nom) G=eDayTrade88(Direct)
    # H=ePremier88(direct)...
    id_type = fields.Char(string='ID type')
    # 1=NRIC 2=Passport 3=BizRegistration 4=Arm Forces/Others 5=OldNRIC
    id_number = fields.Char(string='ID number')
    id_expiry_date = fields.Date(string='ID number')
    nationality = fields.Char(string='Nationality')
    cds_number = fields.Char(string='CDS number')
    cds_open_date = fields.Date(string='CDS open date')
    kibb_account_name1 = fields.Char(string='KIBB account name 1')
    kibb_account_name2 = fields.Char(string='KIBB account name 2')
    kibb_account_name3 = fields.Char(string='KIBB account name 3')
    kibb_account_number = fields.Char(string='KIBB account number')
    ebg_insert_timestamp = fields.Datetime(string='EBG insert timestamp')
    kibb_fetch_status = fields.Char(string='KIBB fetch status')
    # T=True F=False
    kibb_fetch_timestamp = fields.Datetime(string='KIBB fetch timestamp')
    kibb_update_status = fields.Char(string='KIBB update status')
    # T=True F=False
    kibb_update_timestamp = fields.Datetime(string='KIBB update timestamp')
    ebg_fetch_status = fields.Char(string='EBG fetch status')
    # T=True F=False
    ebg_fetch_timestamp = fields.Datetime(string='EBG fetch timestamp')
    remarks = fields.Char(string='Remarks')
    # 31-Oct-2016 DT
    action_type = fields.Char(string='Action type')
    agent_fetch_status = fields.Char(string='Agent fetch status')
    agent_fetch_timestamp = fields.Datetime(string='Agent fetch timestamp')
    # 01-Dec-2016 DT
    error_flag = fields.Char(string='Error flag')
    ebg_remarks = fields.Char(string='EBG remarks')
    
class kibb_contract(models.Model):
    _name = 'vb.kibb_contract'
    _description = 'KIBB contract'
    
    # The fields in this model mostly derived from vb.trade_tran
    # We will need a trigger at vb.trade_tran to insert the record into this table when a Buy or Sell trade
    # is either Posted or made Void
    
    # fields
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to original trade',index=True)
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    delivery_type = fields.Char(string='Delivery type') # Use dlv_basis_id --> vb.common_code: parm2
    kibb_acct_no = fields.Char(string='KIBB Account number') # Use acct_id --> vb.customer_acct: other_acct_no
    trx_type = fields.Char(string='Buy or Sell') # Use tran_type: P=Buy  S=Sell 
    market = fields.Char(string='Market name') # Use asset_id --> vb.asset:market_id --> vb.market: name
    stock_code = fields.Char(string='Stock code') # Use asset_id --> vb.asset:market_symbol
    stock_name = fields.Char(string='Stock name') # Use asset_id --> vb.asset:name
    traded_currency = fields.Char(string='Traded currency') # Use hard-code value 'MYR'
    settlement_currency = fields.Char(string='Traded currency') # Use hard-code value 'MYR'
    quantity = fields.Float(string='Transaction qty') # Use tran_qty (should remove -ve sign for Sell)
    price = fields.Float(string='Price') # Use tran_price
    brkg_amt = fields.Float(string='Brokerage') # Use field with same name
    brkg_gst = fields.Float(string='Brokerage GST') # Use field with same name
    brkg_rate = fields.Float(string='Brokerage rate') # Hard code - set to ???
    brkg_gst_rate = fields.Float(string='Brokerage rate') # Hard code - set to 0.06 (for 6%)
    clearing_amt = fields.Float(string='Clearing fees') # Use field with same name
    clearing_gst = fields.Float(string='Clearing fees GST') # Use field with same name
    clearing_rate = fields.Float(string='Brokerage rate') # Hard code - set to ???
    clearing_gst_rate = fields.Float(string='Brokerage rate') # Hard code - set to 0.06 (for 6%)
    stamp_amt = fields.Float(string='Stamp duty') # Use field with same name
    sclevy_amt = fields.Float(string='SC levy amt') # Use field with same name
    access_amt = fields.Float(string='Access fee amt') # Use field with same name
    trx_net_amt = fields.Float(string='Transaction amt') # Use tran_amt
    total_gst_amt = fields.Float(string='Transaction amt') # Sum of brkg_gst + clearing_gst
    brkg_shr_amt = fields.Float(string='Revenue share') # Use rev_shr_amt
    brkg_shr_rate = fields.Float(string='Revenue share rate') # Hard code - set to ???
    trx_date = fields.Date(string='Transaction date') # Use tran_date in ISO yyyy-mm-dd
    delivery_due_date = fields.Date(string='Payment due date') # Use due_date in ISO yyyy-mm-dd
    payment_due_date = fields.Date(string='Payment due date') # Use due_date in ISO yyyy-mm-dd
    client_id = fields.Char(string='KIBB client ID') # ?????
    cds_no = fields.Char(string='CDS number') # Use acct_id --> vb.customer_acct:cds_no
    acct_no = fields.Char(string='Account number') # Use acct_id --> vb.customer_acct:acct_no
    trx_no = fields.Char(string='Transaction reference') # Use tran_refno
    contract_status = fields.Char(string='Transaction reference') # See note below
    # If trade state = 'Posted' and manual entry is NULL or No, set contract status to 'N'
    # If trade state = 'Posted' and manual entry is Yes, set contract status to 'A'
    # If trade state = 'Void', we need to create TWO records into this file - one with status 'N' for the original trade
    # and one with status 'C' for the cancellation trade (since KIBB treat these two as separate transactions).
    # If the tran_date is on different date can the date_void, then the two records are inserted in separate batch
    original_trx_no = fields.Char(string='Original TRX number') # Set to other_refno (only for cancellation or Amendment)
    kibb_trx_no = fields.Char(string='KIBB reference') # Set to blank - KIBB to update
    ebg_timestamp = fields.Datetime(string='EBG datetime') # Set to write_date
    kibb_timestamp = fields.Datetime(string='KIBB datetime') # Set to null - KIBB to update
    process_status = fields.Char(string='Processing status')
    # P = processed, E = Error, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    kibb2_tran_amt = fields.Float(string='KIBB2 transaction amt',digits=(15,2),default=0)
    kibb2_brkg_amt = fields.Float(string='KIBB2 brokerage',digits=(11,2),default=0)
    kibb2_brkg_gst = fields.Float(string='KIBB2 brokerage GST',digits=(11,2),default=0)
    kibb2_stamp_amt = fields.Float(string='KIBB2 Stamp duty',digits=(11,2),default=0)
    kibb2_stamp_gst = fields.Float(string='KIBB2 Stamp duty GST',digits=(11,2),default=0)
    kibb2_clearing_amt = fields.Float(string='KIBB2 Clearing fees',digits=(11,2),default=0)
    kibb2_clearing_gst = fields.Float(string='KIBB2 Clearing fees GST',digits=(11,2),default=0)

class kibb_contract_2(models.Model):
    _name = 'vb.kibb_contract_2'
    _description = 'KIBB contract 2'
    
    # fields
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    kibb_client_no = fields.Char(string='KIBB client account no')
    cds_no = fields.Char(string='CDS number')
    kibb_trx_no = fields.Char(string='KIBB contract no') 
    trx_type = fields.Char(string='Buy or Sell') 
    trx_date = fields.Date(string='Transaction date') 
    delivery_type = fields.Char(string='Delivery type')
    traded_currency = fields.Char(string='Traded currency') 
    settlement_currency = fields.Char(string='Traded currency')
    market = fields.Char(string='Market name')
    stock_code = fields.Char(string='Stock code')
    quantity = fields.Float(string='Transaction qty')
    price = fields.Float(string='Price')
    brkg_amt = fields.Float(string='Brokerage')
    brkg_gst = fields.Float(string='Brokerage GST')
    brkg_rate = fields.Float(string='Brokerage rate') 
    brkg_gst_rate = fields.Float(string='Brokerage rate') 
    clearing_amt = fields.Float(string='Clearing fees') 
    clearing_gst = fields.Float(string='Clearing fees GST') 
    clearing_rate = fields.Float(string='Brokerage rate') 
    clearing_gst_rate = fields.Float(string='Brokerage rate')
    stamp_amt = fields.Float(string='Stamp duty') 
    sclevy_amt = fields.Float(string='SC levy amt') 
    access_amt = fields.Float(string='Access fee amt') 
    trx_net_amt = fields.Float(string='Transaction amt') 
    total_gst_amt = fields.Float(string='Transaction amt') 
    delivery_due_date = fields.Date(string='Payment due date')
    payment_due_date = fields.Date(string='Payment due date') 
    # The following fields will be populated during the processing (not import) of this file
    tran_refno = fields.Char(string='EBG contract no')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account')
    acct_no = fields.Char(string='Account number') # Use acct_id --> vb.customer_acct:acct_no
    asset_id = fields.Many2one(comodel_name='vb.asset', string='Asset')
    dlv_basis_id = fields.Many2one(comodel_name='vb.common_code', string='Delivery basis')
    #
    ebg_timestamp = fields.Datetime(string='EBG datetime') # Set to write_date
    kibb_timestamp = fields.Datetime(string='KIBB datetime') # Set to null - KIBB to update
    process_status = fields.Char(string='Processing status')
    # P = processed, E = Error, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')

# This interface file is to receive the daily TRS from the stock exchange through Kenanga
# Each file uploaded for the day will have a unique batch number assigned to it.
# Only the Data record (Record Type='1') will be processed from the uploaded text file into this table
    
class kibb_score_trs(models.Model):
    _name = 'vb.kibb_score_trs'
    _description = 'KIBB SCORE TRS'
    
    seller_broker_no = fields.Char(string='Seller broker no')
    seller_broker_branch = fields.Char(string='Seller broker branch')
    seller_cds_no = fields.Char(string='Seller CDS number')
    trs_date = fields.Date(string='TRS date')
    trs_no = fields.Char(string='TRS number')
    buyer_cds_no = fields.Char(string='Buyer CDS number')
    buyer_broker_no = fields.Char(string='Buyer broker no')
    buyer_broker_branch = fields.Char(string='Buyer broker branch')
    side = fields.Char(string='Side: B=Buy  S=Sell')
    stock_code = fields.Char(string='Stock code') 
    quantity = fields.Float(string='Transaction qty')
    price = fields.Float(string='Price')
    lot_code = fields.Char(string='Lot code') 
    # E=10, A=100,  B=200, N=1000,  S=Others
    terminal_id = fields.Char(string='Terminal ID')
    order_no = fields.Char(string='Order no') 
    short_sell = fields.Integer(string='Short sell indicator')
    # 0 = not short sell   1 = Short sell
    full_ext_order_no = fields.Char(string='Full extend order no') 
    
    # extra fields for internal use only
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to original trade',index=True)
    order_tran_id = fields.Many2one(comodel_name='vb.order_tran',string='Link to original order match record',index=True)
    matched_status = fields.Char(string='Match status')
    # M = matched, Null = not matched
    matched_error = fields.Char(string='Match error message')
    when_processed = fields.Datetime(string='When processed')
    # 29-Dec-2016    
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True) # stock_code --> vb.asset:market_symbol
    # 02-Fec-2017    
    gross_amt = fields.Float(string='Gross amount', digits=(15,2))   

# This is the reconcilation summary file for SCORE TRS.
    
class kibb_score_trs_recon(models.Model):
    _name = 'vb.kibb_score_trs_recon'
    _description = 'KIBB SCORE TRS reconciliation'

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.batch:
                result.append((rec.id, u"%s (%s)" % (rec.batch, rec.acct_id.name if rec.acct_id.name else 'Empty')))
            else:
                result.append((rec.id, u"%s (%s)" % ('New', rec.acct_id.name if rec.acct_id.name else 'Empty')))
        return result
    
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True)
    score_buy_qty = fields.Float(string='SCORE buy qty',digits=(9,0))
    score_sell_qty = fields.Float(string='SCORE sell qty',digits=(9,0))
    score_buy_amt = fields.Float(string='SCORE buy amt',digits=(15,2))
    score_sell_amt = fields.Float(string='SCORE sell amt',digits=(15,2))
    order_buy_qty = fields.Float(string='Order buy qty',digits=(9,0))
    order_sell_qty = fields.Float(string='Order sell qty',digits=(9,0))
    order_buy_amt = fields.Float(string='Order buy amt',digits=(15,2))
    order_sell_amt = fields.Float(string='Order sell amt',digits=(15,2))
    diff_buy_qty = fields.Float(string='Diff buy qty',digits=(9,0))
    diff_sell_qty = fields.Float(string='Diff sell qty',digits=(9,0))
    diff_buy_amt = fields.Float(string='Diff buy amt',digits=(15,2))
    diff_sell_amt = fields.Float(string='Diff sell amt',digits=(15,2))
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('Reconciled', 'Reconciled'),
        ('UnReconciled', 'Un-reconciled'),
        ('Resolved', 'Resolved'),
       ], string='Status')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    diff_comments = fields.Text(string='Comments on why unreconciled')
    how_resolved = fields.Text(string='How resolved')

# This interface file is to receive the CDS shareholding transactions from Central Depository through Kenanga
# Each file uploaded for the day will have a unique batch number assigned to it.
# Only the Data record (Record Type='1') will be processed from the uploaded text file into this table
    
class kibb_cds_holding_tran(models.Model):
    _name = 'vb.kibb_cds_holding_tran'
    _description = 'KIBB CDS holding transactions'
    
    cds_no = fields.Char(string='CDS number') # This is the account number field in the input file
    tran_type = fields.Char(string='Transaction type')
    business_date = fields.Date(string='Business date')
    refno = fields.Char(string='Reference number')
    stock_code = fields.Char(string='Stock code') # Link to asset_code in vb_asset to get asset_id
    quantity = fields.Float(string='Quantity of shares')
    trans_detail1 = fields.Char(string='Transaction detail 1')
    trans_detail2 = fields.Char(string='Transaction detail 2')
    status_code = fields.Char(string='Status code') 
    home_branch = fields.Char(string='Home branch') 

    # extra fields for internal use only
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    portfolio_id = fields.Many2one(comodel_name='vb.portfolio',string='Link to customer portfolio',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True) 
    
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    # 02-Fec-2017    
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('Reconciled', 'Reconciled'),
        ('UnReconciled', 'Un-reconciled'),
        ('Resolved', 'Resolved'),
       ], string='Status')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    diff_comments = fields.Text(string='Comments on why unreconciled')
    how_resolved = fields.Text(string='How resolved')

class kibb_cds_holding_bal(models.Model):
    _name = 'vb.kibb_cds_holding_bal'
    _description = 'KIBB CDS holding balance'

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.batch:
                result.append((rec.id, u"%s (%s)" % (rec.batch, rec.acct_id.name if rec.acct_id.name else 'Empty')))
            else:
                result.append((rec.id, u"%s (%s)" % ('New', rec.acct_id.name if rec.acct_id.name else 'Empty')))
        return result

    cds_no = fields.Char(string='CDS number') # This is the account number field in the input file
    stock_code = fields.Char(string='Stock code') # Link to asset_code in vb_asset to get asset_id
    free_qty = fields.Float(string='Free balance')
    earmark_qty = fields.Float(string='Earmarked balance')
    unclear_qty = fields.Float(string='Unclear balance')
    suspended_qty = fields.Float(string='Suspended qty')
    payable_qty = fields.Float(string='Payable qty')
    receivable_qty = fields.Float(string='Receivable qty')

    # extra fields for internal use only
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    portfolio_id = fields.Many2one(comodel_name='vb.portfolio',string='Link to customer portfolio',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True) 

    # Balance at vb_portfolio at time of processing
    shrpool1_cur_qty = fields.Float(string='Sharepool1 Current qty',digits=(9,0),default=0)
    shrpool1_pdg_qty = fields.Float(string='Sharepool1 Pending qty',digits=(9,0),default=0)
    shrpool2_cur_qty = fields.Float(string='Sharepool2 Current qty',digits=(9,0),default=0)
    shrpool2_pdg_qty = fields.Float(string='Sharepool2 Pending qty',digits=(9,0),default=0)
    shrpool3_cur_qty = fields.Float(string='Sharepool3 Current qty',digits=(9,0),default=0)
    shrpool3_pdg_qty = fields.Float(string='Sharepool3 Pending qty',digits=(9,0),default=0)
    shrpool4_cur_qty = fields.Float(string='Sharepool4 Current qty',digits=(9,0),default=0)
    shrpool4_pdg_qty = fields.Float(string='Sharepool4 Pending qty',digits=(9,0),default=0)
    shrpool5_cur_qty = fields.Float(string='Sharepool5 Current qty',digits=(9,0),default=0)
    shrpool5_pdg_qty = fields.Float(string='Sharepool5 Pending qty',digits=(9,0),default=0)
    holding_qty = fields.Float(string='Holding qty',digits=(9,0),default=0)
    earmark_qty = fields.Float(string='Earmark qty',digits=(9,0),default=0)
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    # 22-Jan-2017
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('Reconciled', 'Reconciled'),
        ('UnReconciled', 'Un-reconciled'),
        ('Resolved', 'Resolved'),
       ], string='Status')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    diff_comments = fields.Text(string='Comments on why unreconciled')
    how_resolved = fields.Text(string='How resolved')

class kibb_cds_settlement(models.Model):
    _name = 'vb.kibb_cds_settlement'
    _description = 'KIBB CDS settlement (DSSSCRPS)'
    
    broker_no = fields.Char(string='Broker number')
    branch_no = fields.Char(string='Broker branch number')
    cds_no = fields.Char(string='CDS number') # This is the account number field in the input file
    stock_code = fields.Char(string='Stock code') # Link to asset_code in vb_asset to get asset_id
    trs_no = fields.Char(string='TRS number') # Link to asset_code in vb_asset to get asset_id
    contract_date = fields.Date(string='Contract date')
    quantity = fields.Float(string='Quantity of shares')
    price_traded = fields.Float(string='Quantity debited/credited')
    deduction_amt = fields.Float(string='Deduction amount')
    buyin_remarks = fields.Char(string='Buyin remarks')
    deduction_remarks = fields.Char(string='Deduction remarks')
    buyer_seller = fields.Integer(string='Buyer/Seller') # 1=Seller  2=Buyer

    # extra fields for internal use only
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to original trade',index=True)
    trade_sett_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to trade settlement',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True) 

    # trade and trade sett info from vb_trade_tran at time of processing
    tran_price = fields.Float(string='Price',digits=(11,4),default=0)
    tran_qty = fields.Float(string='Transaction qty',digits=(9,0),default=0)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    gross_amt = fields.Float(string='Gross amt',digits=(15,2),default=0)
    sett_qty = fields.Float(string='Settlement qty',digits=(9,0),default=0)
    sett_amt = fields.Float(string='Settlement amt',digits=(15,2),default=0)
    
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    # 08-Feb-2017
    record_type = fields.Char(string='Record type')

# This is the reconcilation summary file for CDS settlement
    
class kibb_cds_sett_recon(models.Model):
    _name = 'vb.kibb_cds_sett_recon'
    _description = 'KIBB CDS settlement reconcilation'

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.batch:
                result.append((rec.id, u"%s (%s)" % (rec.batch, rec.acct_id.name if rec.acct_id.name else 'Empty')))
            else:
                result.append((rec.id, u"%s (%s)" % ('New', rec.acct_id.name if rec.acct_id.name else 'Empty')))
        return result
    
    batch = fields.Char(string='Batch number',index=True) # Need to generate one batch for each day - running number - get from vb.config
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True)
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True)
    cds_buy_qty = fields.Float(string='CDS buy qty',digits=(9,0))
    cds_sell_qty = fields.Float(string='CDS sell qty',digits=(9,0))
    cds_buy_amt = fields.Float(string='CDS buy amt',digits=(15,2))
    cds_sell_amt = fields.Float(string='CDS sell amt',digits=(15,2))
    sett_buy_qty = fields.Float(string='Settle buy qty',digits=(9,0))
    sett_sell_qty = fields.Float(string='Settle sell qty',digits=(9,0))
    sett_buy_amt = fields.Float(string='Settle buy amt',digits=(15,2))
    sett_sell_amt = fields.Float(string='Settle sell amt',digits=(15,2))
    diff_buy_qty = fields.Float(string='Diff buy qty',digits=(9,0))
    diff_sell_qty = fields.Float(string='Diff sell qty',digits=(9,0))
    diff_buy_amt = fields.Float(string='Diff buy amt',digits=(15,2))
    diff_sell_amt = fields.Float(string='Diff sell amt',digits=(15,2))
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('Reconciled', 'Reconciled'),
        ('UnReconciled', 'Un-reconciled'),
        ('Resolved', 'Resolved'),
       ], string='Status')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    diff_comments = fields.Text(string='Comments on why unreconciled')
    how_resolved = fields.Text(string='How resolved')

class th_asset(models.Model):
    _name = 'vb.th_asset'
    _description = 'Thomson Reuters Asset Master'
    _order = 'name'
    
    # field
    name = fields.Char(size=100,string='Name of asset',index=True)
    market_code = fields.Char(string="Market code",index=True)
    market_symbol = fields.Char(size=20,string='Market symbol')
    market_sector = fields.Char(size=20,string='Market sector')
    market_board = fields.Char(size=20,string='Market board')
    total_issued = fields.Float(string='Total issued units',digits=(11,0))
    isin = fields.Char(string='ISIN',index=True)
    asset_code = fields.Char(size=20,string='Asset code',index=True)
    asset_class = fields.Char(size=20,string='Asset class',index=True)
    std_lot_size = fields.Float(string='Standard lot size',digits=(7,0))
    ric_code = fields.Char(size=20,string='RIC code',index=True)
    maturity_date = fields.Date(string='Maturity date')
    
class gl_interface_tran(models.Model):
    _name = 'vb.gl_interface_tran'
    _description = 'G/L Interface Transaction'
    _order = 'batch,ledger_type,acct_id'
    
    # field
    batch = fields.Char(string='Batch number',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct')
    charge_id = fields.Many2one(comodel_name='vb.charge_code')
    ledger_type = fields.Char(string='Ledger type')
    jrn_refno = fields.Char(string='Journal reference')
    jrn_desc = fields.Char(string='Journal description')
    # Trade / Cash
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Date(string='Transaction date')
    tran_type = fields.Char(string='Transaction type')
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    glacct_no = fields.Char(string='G/L account')
    amt_db = fields.Float(string='Debit amount',digits=(15,2))
    amt_cr = fields.Float(string='Credit amount',digits=(15,2))
    date_posted = fields.Datetime(string='When posted')
    internal_trx_no = fields.Integer(string='Internal transaction number')
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    # 09-Feb-2017
    appto_refno = fields.Char(string='Applied-to reference')
    appto_type = fields.Char(string='Applied-to type')
    glacct_type = fields.Char(string='G/L account type')
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran')
    cash_tran_id = fields.Many2one(comodel_name='vb.cash_tran')
    currency_code = fields.Char(string='MYOB currency code')
    forex = fields.Float(string='MYOB Forex',digits=(9,5))

class gl_interface_summ(models.Model):
    _name = 'vb.gl_interface_summ'
    _description = 'G/L Interface summary'
    _order = 'batch,ledger_type,jrn_desc,glacct_no'
    
    # field
    batch = fields.Char(string='Batch number or MYOB job number',index=True)
    ledger_type = fields.Char(string='Ledger type or MYOB category')
    glacct_no = fields.Char(string='G/L account')
    glacct_desc = fields.Char(string='G/L account description')
    fin_year = fields.Integer(string='Financial year')
    fin_period = fields.Integer(string='Financial period')
    amt_db = fields.Float(string='Debit amount include tax',digits=(15,2))
    amt_cr = fields.Float(string='Credit amount include tax',digits=(15,2))
    glacct_type = fields.Char(string='G/L account type')
    jrn_date = fields.Date(string='Journal date')
    jrn_refno = fields.Date(string='Journal number')
    jrn_desc = fields.Char(string='Journal description')
    db_cr_code = fields.Char(string='Debit credit code')
    memo = fields.Char(string='MYOB Memo')
    inclusive = fields.Char(string='MYOB Inclusive')
    amt_db_extax = fields.Float(string='MYOB Debit amount excluding tax',digits=(15,2))
    amt_cr_extax = fields.Float(string='MYOB Credit amount excluding tax',digits=(15,2))
    tax_code = fields.Char(string='MYOB tax code')
    currency_code = fields.Char(string='MYOB currency code')
    forex = fields.Float(string='MYOB Forex',digits=(9,5))

class kibb_corp_action_announce(models.Model):
    _name = 'vb.kibb_corp_action_announce'
    _description = 'KIBB Corporate Action announcement'
    _rec_name = 'asset_code'
    
    @api.multi
    def name_get(self):
        super(kibb_corp_action_announce, self).name_get()
        result=[]
        result.append((self.id,u"%s - %s" % (self.asset_code,self.circular_no)))
        return result

    batch = fields.Char(string='Batch number',index=True)
    asset_code = fields.Char(string='Asset code',help='Stock_no')
    term = fields.Char(string='Term')
    term_desc = fields.Char(string='Term description')
    div_rate = fields.Float(string='Dividend rate',digits=(13,2))
    pct_cent = fields.Char(string='Percentage',help='P=% C=cent')
    numerator = fields.Float(string='Numerator',digits=(18,4))
    denominator = fields.Float(string='Denominator',digits=(18,4))
    announce_date = fields.Date(string='Announcement date')
    ex_date = fields.Date(string='Ex-date')
    lodge_date = fields.Date(string='Lodgement date')
    scans_date = fields.Datetime(string='SCANS date')
    payment_date = fields.Date(string='Payment date')
    tax_rate = fields.Float(string='Tax rate',digits=(18,4))
    remarks = fields.Char(string='Remarks')
    seq_no = fields.Integer(string='Sequence number')
    circular_no = fields.Char(string='Circular number')
    unconditional_offer = fields.Char(string='Unconditional offer')
    int_rate = fields.Float(string='Interest rate',digits=(12,2))
    type_of_div = fields.Char(string='Type of dividend')
    year_end = fields.Date(string='Year ended')
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    asset_id = fields.Many2one(comodel_name='vb.asset') # help= 'Provided by trigger'
    # 18-Feb-2017
    corpaction_type = fields.Char(string='Corporate action type')
    # RightsIssueBought
    # RightsIssueEntitled
    # BonusIssue
    # ShareSplit
    # ShareDividend
    # TakeOverOffer
    # StockExpiryConv
    # DividendReinvestPlan
    
    # 14-Mar-2017
    market = fields.Char(string='Market',related='asset_id.market_id.name',readonly=True)
    # 13-Apr-2017
    kibb_date_created = fields.Datetime(string="KIBB created date")
    # 14-Apr-2017
    asset_name = fields.Char(string='Asset name')
    
class kibb_corp_action_subs_conv_hdr(models.Model):
    _name = 'vb.kibb_corp_action_subs_conv_hdr'
    _description = 'KIBB Corporate Action subscription and conversion header'

    batch = fields.Char(string='Batch number',index=True)
    asset_code1 = fields.Char(string='Primary asset code',help='Stock_no')
    asset_code2 = fields.Char(string='CA asset code') # Additional for rights, etc
    asset_name2 = fields.Char(string='CA asset name') # Additional for rights, etc
    asset_price2 = fields.Float(string='CA price',digits=(13,6)) # For rights
    entitle_type = fields.Char(string='Entitle type')
    # RightsIssueBought
    # RightsIssueEntitled
    # BonusIssue
    # ShareSplit
    # ShareDividend
    # TakeOverOffer
    # StockExpiryConv
    entitle_desc1 = fields.Char(string='Description 1')
    entitle_desc2 = fields.Char(string='Description 2')
    entitle_desc3 = fields.Char(string='Description 3')
    entitle_desc4 = fields.Char(string='Description 4')
    entitle_desc5 = fields.Char(string='Description 5')
    conv_ratio = fields.Char(string='Conversion ratio description')
    conv_price = fields.Float(string='Conversion price',digits=(13,6))
    circular_no = fields.Char(string='Exchange circular number')
    last_sales_date = fields.Datetime(string='Last date for sales')
    last_trf_date = fields.Datetime(string='Last date for transfer')
    reg_close_date = fields.Datetime(string='Registration closing date')
    kibb_close_date = fields.Datetime(string='KIBB closing date')
    listing_date = fields.Datetime(string='Listing date')
    ex_date = fields.Date(string='Ex date')
    entitled_date = fields.Date(string='Entitled date')
    delist_date = fields.Datetime(string='Delist date',help='For expiry and conversion')
    registrar_name = fields.Char(string='Registrar name')
    registrar_contact = fields.Char(string='Registrar contact')
    bank_draft_charges = fields.Float(string='Bank draft charges',digits=(9,2))
    bank_charges = fields.Float(string='Bank charges',digits=(9,2))
    transfer_fee = fields.Float(string='Transfer fee',digits=(9,2),help='For takeover')
    stamp_duty = fields.Float(string='Stamp duty',digits=(9,2))
    registrar_fee = fields.Float(string='Registrar file',digits=(9,2))
    handling_fee = fields.Float(string='EBG handling fee',digits=(9,2))
    kibb_bank_dtl1 = fields.Char(string='KIBB bank dtl1')
    kibb_bank_dtl2 = fields.Char(string='KIBB bank dtl2')
    asset_id1 = fields.Many2one(comodel_name='vb.asset') # Provided by trigger
    asset_id2 = fields.Many2one(comodel_name='vb.asset',help='Provided by trigger')
    total_records = fields.Integer(string='Total detail records')
    kibb_proc_date = fields.Datetime(string='KIBB process date')
    # For EBG only
    ebg_proc_date = fields.Datetime(string='EBG process date')
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    # 07-Jan-2017
    cash_dividend = fields.Float(string='Cash dividend',digits=(13,6))
    electable_portion = fields.Float(string='Electable portion',digits=(13,6))
    reinvest_price = fields.Float(string='Reinvest price',digits=(13,6))
    payment_date = fields.Datetime(string='Payment date',help='For DRP')
    offer_cash = fields.Char(string='Offer cash')
    offer_share = fields.Char(string='Offer share')
    kibb_handling_fee = fields.Float(string='KIBB handling fee',digits=(9,2))
    # 18-Jan-2017
    entitlement_type_name = fields.Char(string='Entitlement type name')
    # 24-Jan-2017
    asset_name1 = fields.Char(string='Primary asset name') 
    # 18-Feb-2017
    corpaction_type = fields.Char(string='Corporate action type')
    # RightsIssueBought
    # RightsIssueEntitled
    # BonusIssue
    # ShareSplit
    # ShareDividend
    # TakeOverOffer
    # StockExpiryConv
    # DividendReinvestPlan
    ebg_close_date = fields.Date(string='EBG closing date')    
    # 08-Mar-2017
    cevent_type = fields.Char(string='CA event type')
    asset_name1 = fields.Char(string='Asset name 1')
    
class kibb_corp_action_subs_conv_dtl(models.Model):
    _name = 'vb.kibb_corp_action_subs_conv_dtl'
    _description = 'KIBB Corporate Action subscription and conversion detail'
    
    @api.multi
    def name_get(self):
        super(kibb_corp_action_subs_conv_dtl, self).name_get()
        result=[]
        result.append((self.id,u"%s (%s)" % (self.acct_id.name,self.circular_no)))
        return result
    
    # Method to calculate EBG close date
    @api.multi
    def _calc_ebg_close_date(self):
        config_rec = self.env['vb.config'].search([('code','=','KibbCloseDate'),('code_type','=','App')])
        if bool(self.kibb_close_date) != False or '':
            self.ebg_close_date = datetime.strptime(self.kibb_close_date,'%Y-%m-%d %H:%M:%S') + timedelta(days=config_rec.int1)
        else:
            self.ebg_close_date = False

    # Method to Cancel Subscribe Corporate Action
    @api.multi
    def get_kibb_corp_action_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_interface', 'corp_action_cancel_wizard_view')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'confirm_corp_action_cancel_wizard_view'. Please contact IT Support"))
        return {
            'name': "Cancel subscribed Corporate Action.",
            'view_type': 'form',
            'view_mode': "[form]",
            'view_id': False,
            'res_model': 'vb.common_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'views': [(form_id, 'form')],
        }

    # Method to Approve Cancellation
    @api.multi
    def get_corp_action_approve_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_interface', 'corp_action_approve_cancel_wizard_view')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'confirm_corp_action_approve_cancel_wizard_view'. Please contact IT Support"))
        return {
            'name': "Confirm Corp approve cancel",
            'view_type': 'form',
            'view_mode': "[form]",
            'view_id': False,
            'res_model': 'vb.common_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'views': [(form_id, 'form')],
        }
            
    batch = fields.Char(string='Batch number',index=True)
    recno = fields.Integer(string='Record number',help='To be generated during file import - starting from 1')
    cds_no = fields.Char(string='CDS number')
    kibb_client_no = fields.Char(string='KIBB client no')
    acct_id = fields.Many2one(comodel_name='vb.customer_acct') # Provided by trigger
    holding_qty = fields.Float(string='Holding qty',digits=(10,0),default=0)
    entitled_qty = fields.Float(string='Entitled qty',digits=(10,0),default=0)
    bonus_qty1 = fields.Float(string='Bonus qty 1',digits=(10,0),default=0)
    bonus_qty2 = fields.Float(string='Bonus qty 2',digits=(10,0),default=0)
    accept_qty = fields.Float(string='Accept qty',digits=(10,0),default=0,help='Convert qty for conversion, partial qty for takeover')
    accept_ind = fields.Char(string='Accept indicator',help='Yes/No/Partial')
    amt_payable = fields.Float(string='Amount payable',digits=(15,2),default=0)
    bank_draft_charges = fields.Float(string='Bank draft charges',digits=(9,2))
    bank_charges = fields.Float(string='Bank charges',digits=(9,2))
    transfer_fee = fields.Float(string='Transfer fee',digits=(9,2),help='For takeover')
    stamp_duty = fields.Float(string='Stamp duty',digits=(9,2))
    registrar_fee = fields.Float(string='Registrar fee',digits=(9,2))
    handling_fee = fields.Float(string='EBG handling fee',digits=(9,2))
    excess_qty = fields.Float(string='Excess quantity applied',digits=(10,0),default=0)
    excess_amt_payable = fields.Float(string='Excess amount payable',digits=(15,2),default=0)
    excess_bank_charges = fields.Float(string='Excess bank chargese',digits=(9,2),default=0)
    total_fee = fields.Float(string='Total fee',digits=(15,2),default=0)
    remarks = fields.Char(string='Remarks')
    # 29-Dec-2016 DT: For storing response code from payment gateway (e.g. for FPX)
    bank_name = fields.Char(size=100,string='Bank name')
    bank_branch = fields.Char(string='Bank branch', )
    bank_acct_no = fields.Char(string='Bank account number',)
    bank_refno = fields.Char(size=30,string='Bank reference')
    other_refno = fields.Char(size=30,string='Other reference')
    card_number = fields.Char(string='Credit card number')
    card_expiry = fields.Char(string='Card expiry in MM/YY')
    name_on_card = fields.Char(string='Name on card')
    fpx_refno = fields.Char(string='FPX refno number')
    fpx_date = fields.Datetime(string='FPX date')
    approval_code = fields.Char(string='Tran approval code')
    approval_date = fields.Datetime(string='When tran approved')
    response_code = fields.Char(string='Tran response code')
    response_msg = fields.Char(string='Tran response cmsg')
    payment_code = fields.Char(string='Payment code')
    tran_mode = fields.Char(string='Payment mode')
    # e.g. FPX, Offline
    tran_desc = fields.Text(string='Description')
    tran_forex = fields.Float(string='Forex rate',digits=(11,5),default=1)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_gst = fields.Float(string='Transaction GST',digits=(15,2),default=0)
    tran_loc = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    tran_gst_loc = fields.Float(string='Transaction GST local',digits=(15,2),default=0)
    state = fields.Char(string='Status')
    # 04-Jan-2017
    hdr_id = fields.Many2one(comodel_name='vb.kibb_corp_action_subs_conv_hdr',string='Header record')
    # 07-Jan-2017 - additional field for DRP
    reinvest_qty = fields.Float(string='Reinvest quantity',digits=(10,0),default=0,help='Convert qty for conversion, patrial qty for takeover')
    elect_type = fields.Char(string='Elect type') # Valid code: SHARE/CASH
    div_payable = fields.Float(string='Dividend payable',digits=(15,2),default=0)
    # 09-Jan-2017
    withdraw_limit = fields.Float(string='Withdraw limit',digits=(15,2),default=0)
    breached_amt = fields.Float(string='Amount breached',digits=(15,2),default=0,copy=False)
    kibb_handling_fee = fields.Float(string='KIBB handling fee',digits=(9,2))
    # 13-Jan-2017 - For EBG only
    ebg_proc_date = fields.Datetime(string='EBG process date')
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    # 18-Jan-2017
    flag_submitted = fields.Char(string='Submitted flag')
    submitted_timestamp = fields.Datetime(string='Submitted time stamp')
    ebg_remarks = fields.Char(string='EBG remarks')
    # 24-Jan-2017
    currency_code = fields.Char(string='Currency code')
    circular_no = fields.Char(string='Circular number', related='hdr_id.circular_no',store=True,readonly=True)
    entitle_type = fields.Char(string='Entitle type', related='hdr_id.entitle_type',store=True,readonly=True)
    asset_id = fields.Char(string='Name of asset', related='hdr_id.asset_id1.name',readonly=True)
    asset_code = fields.Char(string='Asset code', related='hdr_id.asset_code1',store=True,readonly=True)
    kibb_close_date = fields.Datetime(string='KIBB closing date', related='hdr_id.kibb_close_date',store=True,readonly=True)
    reg_close_date = fields.Datetime(string='Registrar close date', related='hdr_id.reg_close_date',readonly=True)
    ex_date = fields.Date(string='Ex date', related='hdr_id.ex_date',readonly=True)
    entitled_date = fields.Date(string='Entitle date', related='hdr_id.entitled_date',readonly=True)
    offer_cash = fields.Char(string='Offer cash', related='hdr_id.offer_cash',readonly=True)
    offer_share = fields.Char(string='Offer share', related='hdr_id.offer_share',readonly=True)
    kibb_bank_dtl1 = fields.Char(string='KIBB bank detail1', related='hdr_id.kibb_bank_dtl1',readonly=True)
    kibb_bank_dtl2 = fields.Char(string='KIBB bank detail2', related='hdr_id.kibb_bank_dtl2',readonly=True)
    registrar_name = fields.Char(string='Registrar name', related='hdr_id.registrar_name',readonly=True)
    registrar_contact = fields.Char(string='Registrar contact', related='hdr_id.registrar_contact',readonly=True)
    cash_dividend = fields.Float(string='Cash dividend', related='hdr_id.cash_dividend',readonly=True)
    electable_portion = fields.Float(string='Electable portion',related='hdr_id.electable_portion',readonly=True)
    reinvest_price = fields.Float(string='Reinvest price',related='hdr_id.reinvest_price',readonly=True)
    conv_ratio = fields.Char(string='Conversion ratio description', related='hdr_id.conv_ratio',readonly=True)
    conv_price = fields.Float(string='Conversion price', related='hdr_id.conv_price',readonly=True)
    last_sales_date = fields.Datetime(string='Last date for sales', related='hdr_id.last_sales_date',readonly=True)
    last_trf_date = fields.Datetime(string='Last date for transfer', related='hdr_id.last_trf_date',readonly=True)
    delist_date = fields.Datetime(string='Delisted date',related='hdr_id.delist_date',readonly=True)
    market = fields.Char(string='Market',related='hdr_id.asset_id1.market_id.name',readonly=True)
    payment_date = fields.Datetime(string='Payment date',related='hdr_id.payment_date',readonly=True)
    # 18-Feb-2017
    corpaction_type = fields.Char(string='Corporate action type', related='hdr_id.corpaction_type',readonly=True)
    ebg_close_date = fields.Datetime(string='EBG closing date', compute= _calc_ebg_close_date)
    # 23-Feb-2017
    entitlement_type_name = fields.Char(string='Entitlement type name', related='hdr_id.entitlement_type_name')
    # 24-Feb-2017
    entitle_desc1 = fields.Char(string='Description 1', related='hdr_id.entitle_desc1',readonly=True)
    entitle_desc2 = fields.Char(string='Description 2', related='hdr_id.entitle_desc2',readonly=True)
    entitle_desc3 = fields.Char(string='Description 3', related='hdr_id.entitle_desc3',readonly=True)
    entitle_desc4 = fields.Char(string='Description 4', related='hdr_id.entitle_desc4',readonly=True)
    entitle_desc5 = fields.Char(string='Description 5', related='hdr_id.entitle_desc5',readonly=True)
    # 08-Mar-2017
    asset_name = fields.Char(string='Asset name')
    asset_price2 = fields.Float(string='Rights price',related='hdr_id.asset_price2',readonly=True) # For rights
    listing_date = fields.Datetime(string='Right listing date', related='hdr_id.listing_date',readonly=True)
    # 20-Mar-2017
    customer_name = fields.Char(string='Customer name', related='acct_id.customer_id.name',readonly=True)
    # 04-Apr-2017
    subscribed_timestamp = fields.Datetime(string='Subscribed time stamp')
    # 29-Sep-2017
    cardholder_name = fields.Char(string='Cardholder name')
    date_cancel = fields.Datetime(string='Cancelled time stamp')
    cancel_by = fields.Many2one(comodel_name='res.users',string='Cancelled by',ondelete='restrict')
    stage = fields.Selection([
        ('Subscribed', 'Subscribed'),
        ('PendingCancel', 'Pending Cancel'),
        ('Cancelled', 'Cancelled'),
        ], string='Stage', default='Subscribed',help="This shows the stage where the customer is within the sign-up process.")
    cancel_remark = fields.Text(string='Cancel remark')
    
class kibb_corp_action_dividend_pay(models.Model):
    _name = 'vb.kibb_corp_action_dividend_pay'
    _description = 'KIBB Corporate Action dividend pay distribution list'

    # Method to post
    @api.multi
    def get_kibb_corp_action_dividend_pay_post_confirm_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_interface', 'confirm_kibb_corp_action_dividend_pay_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'confirm_kibb_corp_action_dividend_pay_wizard'. Please contact IT Support"))
        return {
            'name': "Confirm KIBB Corporate Action dividend pay",
            'view_type': 'form',
            'view_mode': "[form]",
            'view_id': False,
            'res_model': 'vb.common_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'views': [(form_id, 'form')],
        }

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.batch:
                result.append((rec.id, u"%s (%s)" % (rec.batch, rec.acct_id.name if rec.acct_id.name else 'Empty')))
            else:
                result.append((rec.id, u"%s (%s)" % ('New', rec.acct_id.name if rec.acct_id.name else 'Empty')))
        return result
    
    batch = fields.Char(string='Batch number',index=True)    
    kibb_client_no = fields.Char(string='KIBB client no')
    cds_no = fields.Char(string='CDS number')
    stock_code = fields.Char(string='Stock code')
    div_desc = fields.Char(string='Dividend description')
    holding_qty = fields.Float(string='Holding qty',digits=(10,0),default=0)
    div_rate = fields.Float(string='Dividend rate',digits=(12,6),default=0)
    int_rate = fields.Float(string='Interest rate',digits=(12,6),default=0)
    tax_rate = fields.Float(string='Tax rate',digits=(12,6),default=0)
    income_tax = fields.Float(string='Income tax',digits=(12,6),default=0)
    par_value = fields.Float(string='Par value',digits=(6,3),default=0)
    gross_div_amt = fields.Float(string='Gross dividend amt',digits=(15,2),default=0)
    handling_fee = fields.Float(string='Handling fee',digits=(9,2),default=0)
    bank_charges = fields.Float(string='Bank charges',digits=(9,2),default=0)
    other_charges = fields.Float(string='Other charges',digits=(9,2),default=0)
    net_div_amt = fields.Float(string='Net dividend amt',digits=(15,2),default=0)
    kibb_handling_fee = fields.Float(string='KIBB handling fee',digits=(9,2))
    kibb_proc_date = fields.Datetime(string='KIBB process date')
    # For EBG only
    acct_id = fields.Many2one(comodel_name='vb.customer_acct') # Provided by trigger
    asset_id = fields.Many2one(comodel_name='vb.asset') # Provided by trigger
    cash_tran_id = fields.Many2one(comodel_name='vb.cash_tran') # Provided by trigger
    ebg_proc_date = fields.Datetime(string='EBG process date')
    process_status = fields.Char(string='Processing status')
    # P = processed, S = Skipped, null = not processed
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    state = fields.Selection([
        ('New', 'Newly imported'),
        ('Failed', 'Failed during import'),
        ('Posted', 'Posted'),
       ], string='Status')
    payment_instr = fields.Selection([
        ('Bank', 'Pay to bank a/c'),
        ('CashBal', 'Top-up cash balance'),
       ], string='Payment instruction',default='CashBal')
    bank_acct_id = fields.Many2one(comodel_name='vb.customer_bank') # Provided by trigger
    when_rcvd_instr = fields.Datetime(String='Date/time received instruction')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    resolved_comments = fields.Text(string='Comments on resolution')
    how_resolved = fields.Text(string='How resolved')
    
class kibb_trade_recon(models.Model):
    _name = 'vb.kibb_trade_recon'
    _description = 'KIBB Trade Reconciliation'

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.batch:
                result.append((rec.id, u"%s (%s)" % (rec.batch, rec.acct_id.name if rec.acct_id.name else 'Empty')))
            else:
                result.append((rec.id, u"%s (%s)" % ('New', rec.acct_id.name if rec.acct_id.name else 'Empty')))
        return result
    
    batch = fields.Char(string='Batch number',index=True)    
    kibb_client_no = fields.Char(string='KIBB client no')
    cds_no = fields.Char(string='CDS number')
    stock_code = fields.Char(string='Stock code') # Use asset_id --> vb.asset:market_symbol
    buy_sell_ind = fields.Char(string='Buy sell indicator')
    ebg_qty = fields.Float(string='EBG qty',digits=(10,0),default=0)
    kibb_qty = fields.Float(string='KIBB qty',digits=(10,0),default=0)
    diff_qty = fields.Float(string='Difference qty',digits=(10,0),default=0)
    kibb_proc_date = fields.Datetime(string='KIBB process date')
    # For EBG only
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to customer account',index=True) # Provided by trigger
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Link to asset',index=True) # Provided by trigger
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    # 02-Fec-2017    
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('Reconciled', 'Reconciled'),
        ('UnReconciled', 'Un-reconciled'),
        ('Resolved', 'Resolved'),
       ], string='Status')
    date_resolved = fields.Datetime(string='Date/time resolved')
    resolved_by = fields.Many2one(comodel_name='res.users',string='Resolved by',ondelete='restrict')
    diff_comments = fields.Text(string='Comments on why unreconciled')
    how_resolved = fields.Text(string='How resolved')
   
# This model stores the summary of each file downloaded from KIBB.  We can also use this
# to store for upload summary to KIBB if required (see load_type field).

class kibb_file_load_summary(models.Model):
    _name = 'vb.kibb_fileload_summary'
    _description = 'KIBB File Upload Download Summary'
    
    dbtable = fields.Char(string='Database table name', index=True)
    batch = fields.Char(string='Batch number',index=True)
    tran_date = fields.Date(string='Transaction date')
    load_type = fields.Selection([('Up','Upload'),('Down','Download')], string='Load type')
    when_loaded = fields.Datetime(string='Date/time uploaded/downloaded')
    when_processed = fields.Datetime(string='Date/time processed')
    rec_count = fields.Integer(string='Record count')
    rec_processed = fields.Integer(string='Records processed successfully')
    rec_processed_error = fields.Integer(string='Records processed with error')
    rec_error = fields.Integer(string='Records not processed due to error')
    comments = fields.Text(string='Comments')
    state = fields.Selection([
        ('InProgress', 'In progress'),
        ('Processed', 'Processed'),
        ('Failed', 'Failed'),
        ], string='Status')
    # The following fields can optionally be used to store totals for checking
    # to ensure we don't miss any records when processing
    totalamt1 = fields.Float(string='Total amt 1',digits=(15,2))
    totalamt2 = fields.Float(string='Total amt 2',digits=(15,2))
    totalqty1 = fields.Float(string='Total qty 1',digits=(13,0))
    totalqty2 = fields.Float(string='Total qty 1',digits=(13,0))
    checksum = fields.Integer(string='Checksum')
    
class file_upload(models.Model):
    _name = 'vb.file_upload'
    _description = 'File upload'
    
    code = fields.Char(string='File search code',index=True)
    name = fields.Char(string='Logical file name')
    file_name_orig = fields.Char(string='Original physical file name')
    file_name_new = fields.Char(string='New physical file name')
    file_loc = fields.Char(string='Physical file location')
    when_uploaded = fields.Datetime(string='Date/time uploaded')
    when_processed = fields.Datetime(string='Date/time processed')
    uploaded_by = fields.Many2one(comodel_name='res.users',string='Upoaded by',ondelete='restrict')
    processed_by = fields.Many2one(comodel_name='res.users',string='Processed by',ondelete='restrict')
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    data_src = fields.Char(string='Source of data')
    upload_count = fields.Integer(string='Upload count')
    processed_count = fields.Integer(string='Processed count')
    fail_count = fields.Integer(string='Fail count')
    state = fields.Selection([
        ('New', 'New'),
        ('Processed', 'Processed'),
        ('Rejected', 'Rejected'),
        ('Failed', 'Failed'),
        ], string='Status')

class asset_batch_update(models.Model):
    _name = 'vb.asset_batch_update'
    _description = 'Asset by product for update'

    batch = fields.Char(string='Batch number',index=True)
    product_type = fields.Char(string='Product type')
    asset_id = fields.Many2one(comodel_name='vb.asset',string='Asset',index=True)
    asset_name = fields.Char(string='Asset name',index=True)
    asset_code = fields.Char(string='Asset code',index=True)
    asset_profile_id = fields.Many2one(comodel_name='vb.asset_profile',string='Asset profile')
    marginable = fields.Boolean(string='Marginable assets',default=False)
    price_cap_pct = fields.Float(string='Price cap %',digits=(5,2))
    price_cap = fields.Float(string='Price cap',digits=(11,5))
    share_multiplier = fields.Float(string='Share multiplier',digits=(5,2))
    max_buy_qty = fields.Float(string='Max qty to buy',digits=(9,0))
    max_buy_amt = fields.Float(string='Max amount to buy',digits=(15,2))
    max_sell_qty = fields.Float(string='Max qty to sell',digits=(9,0))
    max_sell_amt = fields.Float(string='Max amount to sell',digits=(15,2))
    max_buy_sell_qty = fields.Float(string='Max net qty to buy/sell',digits=(9,0))
    max_buy_sell_amt = fields.Float(string='Max net amount to buy/sell',digits=(15,2))
    ceiling_price = fields.Float(string='Ceiling price',digits=(11,5))
    state = fields.Selection([
        ('New', 'New'),
        ('Processed', 'Processed'),
        ('Rejected', 'Rejected'),
        ('Error', 'Error'),
        ], string='Status')
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='Date/time processed')

class kibb_contra(models.Model):
    _name = 'vb.kibb_contra'
    _description = 'KIBB contra'
    
    # fields
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to original trade',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to account',index=True)
    batch = fields.Char(string='Batch number',index=True)
    acct_no = fields.Char(string='RT Account number') 
    kibb_acct_no = fields.Char(string='KIBB Account number') 
    cds_number = fields.Char(string='CDS number') 
    product_type = fields.Char(string='Product type') 
    market = fields.Char(string='Market name') 
    stock_code = fields.Char(string='Stock code')
    stock_name = fields.Char(string='Stock name')
    tran_refno = fields.Char(string='RT Contra reference no')
    kibb_tran_refno = fields.Char(string='KIBB Contra reference no')
    tran_type = fields.Char(string='Transaction type')
    tran_date = fields.Date(string='Transaction date')
    due_date = fields.Date(string='Due date')
    tran_qty = fields.Integer(string='Contra qty')
    tran_amt = fields.Float(string='Contra net amount')
    appto_type = fields.Char(string='Apply to transaction type')
    appto_refno = fields.Char(string='Apply to reference number')
    appto_date = fields.Date(string='Apply to transaction date')
    appto_qty = fields.Integer(string='Apply to qty')
    appto_amt = fields.Float(string='Apply to amount')
    sett_currency = fields.Char(string='Currency') 
    rt_timestamp = fields.Datetime(string='RT timestamp')
    kibb_timestamp = fields.Datetime(string='KIBB timestamp')
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')

class kibb_contra_sett(models.Model):
    _name = 'vb.kibb_contra_sett'
    _description = 'KIBB contra settlement'
    
    # fields
    trade_tran_id = fields.Many2one(comodel_name='vb.trade_tran',string='Link to original trade',index=True)
    batch = fields.Char(string='Batch number',index=True)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Link to account',index=True)
    acct_no = fields.Char(string='RT Account number') 
    kibb_acct_no = fields.Char(string='KIBB Account number') 
    cds_number = fields.Char(string='CDS number') 
    product_type = fields.Char(string='Product type') 
    market = fields.Char(string='Market name') 
    stock_code = fields.Char(string='Stock code')
    stock_name = fields.Char(string='Stock name')
    tran_refno = fields.Char(string='RT Contra reference no')
    kibb_tran_refno = fields.Char(string='KIBB Contra reference no')
    tran_type = fields.Char(string='Transaction type')
    tran_date = fields.Date(string='Transaction date')
    due_date = fields.Date(string='Due date')
    tran_qty = fields.Integer(string='Contra qty')
    tran_amt = fields.Float(string='Contra net amount')
    appto_type = fields.Char(string='Apply to transaction type')
    appto_refno = fields.Char(string='Apply to reference number')
    appto_date = fields.Date(string='Apply to transaction date')
    appto_qty = fields.Integer(string='Apply to qty')
    appto_amt = fields.Float(string='Apply to amount')
    sett_currency = fields.Char(string='Currency') 
    rt_timestamp = fields.Datetime(string='RT timestamp')
    kibb_timestamp = fields.Datetime(string='KIBB timestamp')
    process_status = fields.Char(string='Processing status')
    process_error = fields.Char(string='Process error message')
    when_processed = fields.Datetime(string='When processed')
    