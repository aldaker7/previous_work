from openerp import models, api, fields
from openerp.exceptions import ValidationError, Warning
import requests
from json import dumps
import json
from datetime import  datetime, date, timedelta
import datetime as dt

class trade_tran(models.Model):
    _inherit = 'vb.trade_tran'

    acct_class_code = fields.Char(string='Product ID',related='acct_id.acct_class_id.code',readonly=True)
    
    @api.multi
    def manual_settlement(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'manual_settlment_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Manual Settlement",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
    
    @api.onchange('acct_id')
    def get_related_asset(self):
        if self.tran_type == 'Contra':
            asset_lst=[]
            if self.acct_id:
                rec_trade = self.env['vb.trade_tran'].search([('acct_id','=',self.acct_id.id),('state','=','Posted')])
                print "1",rec_trade
                if rec_trade:
                    for i in rec_trade:
                        asset_trade_recs = self.env['vb.trade_tran'].search([('acct_id','=',self.acct_id.id),('asset_id','=',i.asset_id.id),('state','=','Posted')])
                        print "2",asset_trade_recs
                        buy,sell = 0,0
                        for x in asset_trade_recs:
#                             if x.asset_id.id in asset_lst:
#                                 break
#                             else:
                            print bool(not x.appto_tran_id and x.balance_qty != 0.0 and x.pickup_flag != True and (x.balance_qty + x.pending_qty)!=0.0)
                            if not x.appto_tran_id and x.balance_qty != 0.0 and x.pickup_flag != True and (x.balance_qty + x.pending_qty)!=0.0:
                                if x.tran_type == 'Buy':
                                    buy+=1
                                if x.tran_type == 'Sell':
                                    sell+=1
                        if (buy and sell) > 0:
                            print "Adding to buy and sell",asset_id
                            asset_lst.append(i.asset_id.id)
                        else:
                            pass
            final_asset_ids = list(set(asset_lst))
            return {'domain':{'asset_id':[('id','in',final_asset_ids)]}}
                
    @api.multi
    def buy_sell_overview(self):
        cab = self.env['vb.customer_acct_bal'].search([('acct_id','=',self.acct_id.id)])
        withdraw_limit = cab.withdraw_limit
        share_pickup_limit = cab.cash_available_bal - (
                             cab.trade_os_ctra_loss_amt + cab.unrlsd_int_amt + 
                             cab.potential_loss_amt + cab.trade_os_others_amt +
                             cab.trade_os_dbsale_amt) + (cab.cash_pending_out - 
                                                         cab.trade_pickup_amt)
        if self.tran_type:
            if self.tran_type in ['Buy'] and self.state=='Posted' and share_pickup_limit >= self.balance_amt and self.pickup_flag != True:
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'confirm_trade_tran_contra_wiz_form')[1]
                except ValueError:
                    form_id = False
                return{
                        'name': "Confirm Contra",
                        'view_type': 'form',
                        'view_mode':"[form]",
                        'view_id': False,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'views': [(form_id, 'form')],
                            }
            else:
                if self.tran_type not in ['Buy']:
                    raise Warning("It should be a buy transaction")
                elif self.state!='Posted':
                    raise Warning("Transaction should be Posted")
                elif share_pickup_limit < self.balance_amt:
                    raise Warning("share pick limit should be greater than balance amount")
                else:
                    raise Warning("This transaction already picked up")
        else:
            raise Warning('There is not type for this transaction')
    # This constraint is used primarily for Contra net record
    @api.onchange('acct_id')
    def get_asset(self):
        if self.acct_id:
            list1 = []
            portfolio_rec = self.env['vb.portfolio'].search([('acct_id','=',self.acct_id.id)])
            for i in portfolio_rec:
                list1.append(i.asset_id.name)
            print list1
            if list1:
                return {'domain':{'asset_id':[('name','in',list1)]}}
        else:
                return {'domain':{'asset_id':[('id','=',0)]}}
    @api.constrains('appby_ids')
    def get_validation(self):
        if self.appby_ids:
            appto_rec = self.appby_ids
            sum_sell = 0
            sum_buy = 0
            count1= 0
            count2= 0
            total_buy = 0
            total_sell= 0
            rec=''
            big_buy_date=''
            big_sell_date=''
            sum_total_amt = 0
            sum_total_amt1 = 0
            sum_total_amt2 = 0
            
            for i in appto_rec:
                if i.appto_type == 'Buy':
                    if big_buy_date == '' or big_buy_date < i.appto_date:
                        big_buy_date = i.appto_date
                    total_buy = i.appto_qty + sum_buy
                    sum_buy = total_buy
                    count1 +=1
                else:
                    if i.appto_type == 'Sell':
                        if big_sell_date == '' or big_sell_date > i.appto_date:
                            big_sell_date = i.appto_date
                        total_sell = abs(i.appto_qty) + sum_sell
                        sum_sell = total_sell
                        count2 +=1
                    
#                 if i.appto_type in ['Buy','Sell']:
#                     if i.appto_type  == 'Buy' and rec == 'Sell':
#                         raise ValidationError('All Buy should be before Sell') 
#                     elif i.appto_type  == 'Sell' and rec == 'Buy':
#                         if big_sell_date < big_buy_date:
#                             raise ValidationError ('Sell transaction date can not be less than Buy transaction date')
#                         else:
#                             rec=i.appto_type
#                     else:
#                         rec = i.appto_type

                i._compute_avg_price()
                i._compute_input_amt()                
                sum_total_amt += i.appto_amt
                sum_total_amt1 += i.appto_kibb1_amt
                sum_total_amt2 += i.appto_kibb2_amt

            self.tran_amt = sum_total_amt
            self.kibb1_tran_amt = sum_total_amt1
            self.kibb2_tran_amt = sum_total_amt2
                                
            if count1 >0 and count2>0:
                if total_buy != self.tran_qty or total_sell !=self.tran_qty:
                    raise ValidationError("The total of Buy qty and total of Sell qty should match the balance qty ")
            if count1 == 0 or count2 ==0:
                raise ValidationError('You must enter 2 records at least of trades with different types (Buy,Sell)')
    

#   Method to get trade statement
    @api.multi
    def get_Trade_Statement(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'trade_Statement_action_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "trade statement",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#   Method to get trade settlement
    @api.multi
    def get_Trade_Settlement(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'trade_settl_action_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "trade settlement",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#   Method to get pre-trade settlement
    @api.multi
    def get_Trade_pre_Settlement(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'Trade_pre_Settlement_action_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Pre-trade settlement",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#   Method to get order closing method
    @api.multi
    def get_order_closing(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'Trade_order_closing_action_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Order Closing",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#    Method to get post daily trades
    @api.multi
    def get_Post_daily_trades(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'Post_daily_trades_action_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Post daily trades",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to get post trade transaction
    @api.multi
    def get_post_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'post_trade_tran_manage_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Post trade transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
    @api.multi
    def get_settle_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'settle_trade_tran_manage_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Post trade transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#   Method to void trade transaction
    @api.multi
    def get_void_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'void_trade_tran_manage_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "void trade transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
    #Method to void trade transaction for contra
    @api.multi
    def get_void_wizard_contra(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'void_trade_tran_contra_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "void trade transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

class order_book(models.Model):
    _inherit = 'vb.order_book'
    
    
#   Method to show only portfolio asset when 'Sell' order has been given by CSR  
    @api.onchange('acct_id','buy_sell')
    def get_asset(self):
        portfolio_rec = self.env['vb.portfolio'].search([('acct_id','=',self.acct_id.id)])
        if self.buy_sell =='Sell':
            if portfolio_rec:
                g=[]
                for i in portfolio_rec:
                    g.append(i.asset_id.name)
                return {'domain':{'asset_id':[('name','in',g)]}}      
    
#   Method to get market price when order is market order  
    @api.onchange('order_type','buy_sell')
    def get_market_price(self):
        if self.order_type == 'Market':
            #         url = "http://172.18.6.105:8080/vbroker/api/v1/order/marketPrice"
            url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','MktPrice')])
            url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
            data = dumps({"marketSymbol" : str(self.asset_id.market_symbol),
                          "tradingBoard": str(self.trading_board)})
            
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)
            if content == False:
                raise Warning('Cannot get the market price.')
            else:
                multiplier_rec = self.env['vb.config'].search([('code_type','=','App'),('code','=','MrktOrderMultiplier')])
                result = float(content.json().get('response').get('data').get('orderPrice'))
                if self.buy_sell == "Sell":
                    final_result = result * multiplier_rec.amt2
                    self.update({'market_price':final_result})
                else:
                    final_result = result * multiplier_rec.amt1
                    self.update({'market_price':final_result})
    
    
#   Method to send order to web service after checking the validation and saved successfully
    @api.multi
    def send_order(self):
        #         url = "http://172.18.6.105:8080/vbroker/api/v1/order/newBO"
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','AsstOrder')])
        url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if self.cds_no == False or self.order_no == False:
            raise Warning("CDS number or Order number missing! You cannot send this order.")
        else:
            if self.tran_type == 'New':
                data = dumps({"loginName" : str(self.acct_id.customer_id.login_name),
                              "accountNo" : str(self.acct_id.acct_no),
                              "cdsNo": str(self.cds_no),
                              "orderNo": str(self.order_no),
                              "excOrderId" : "",
                              "orderSide": str(self.buy_sell),
                              "orderType": str(self.order_type),
                              "marketSymbol":str(self.asset_id.market_symbol),
                              "orderQty": self.order_qty,
                              "tradingBoard": str(self.trading_board),
                              "limitPrice": self.limit_price or "",
                              "expiryType": str(self.expiry_type)})
            else:
                 #         url = "http://172.18.6.105:8080/vbroker/api/v1/order/cancelBO"
                url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','AsstOrderCancel')])
                url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                data = dumps({"loginName" : str(self.acct_id.customer_id.login_name),
                              "accountNo" : str(self.acct_id.acct_no),
                              "cdsNo": str(self.cds_no),
                              "orderNo": str(self.order_no),
                              "appToOrderNo":str(self.appto_order_id),
                              "excOrderId" : str(self.appto_exec_id),
                              "orderSide": str(self.buy_sell),
                              "orderType": str(self.order_type),
                              "marketSymbol":str(self.asset_id.market_symbol),
                              "orderQty": self.order_qty})
         
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)
            if content ==False:
                self.update({'state':'Failed'})
                raise Warning('Failed to sent.')
            else:
                self.update({'sent2fo_flag':True})


#     method to approve force selling
    @api.multi
    def get_approve_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'approve_force_selling_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Force Selling transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     method to reject force selling
    @api.multi
    def get_reject_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'reject_force_selling_wiz_form')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Force Selling transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
    
    # This function will print force selling csv file
    def print_csv_force_selling_action_wizard(self, cr, uid, data, context):
        active_ids = context.get('active_ids')
        try:
            form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_trade', 'force_selling_print_csv_wiz_form')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'force_selling_print_csv_wiz_form'. Please contact IT Support"))
        return{
                    'name': "Force selling transaction",
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': form_id,
                    'context': {'active_ids': active_ids},
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    }