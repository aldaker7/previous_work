# -*- coding: utf-8 -*-

{
    'name': 'VB Order and Trade management',
    'version': '1.2',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_acctmgmt'],
    'description': """

Virtual Broker Application - Order and Trade Module
======================================================

This module contains the functions for Order and Trade Management.

    """,
    'data': [
        'wizards/wizards_trade.xml',
        'views/views_trade.xml',
        'views/actions_trade.xml',
        'views/menus_trade.xml',
        'views/template.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
