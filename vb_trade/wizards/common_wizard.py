from openerp import models, fields, api, _
from datetime import datetime
from openerp.exceptions import Warning, ValidationError
import new
import requests, traceback
from json import dumps
import base64

class common_wizards(models.TransientModel):
    _inherit='vb.common_wizard'

#   method to post daily trade transaction when state equal to New
    @api.multi
    def trade_statement(self):
        list_trade_stat= []
        final_list= []
        trade_stat_obj= self.env['vb.trade_tran'].search([('tran_date', '=',self.post_date),
                                                          ('state', '=', 'Posted'), 
                                                          ('tran_type', 'in', ['Buy','Sell'])])
        print"::::::::TRADEOBJ",trade_stat_obj
        for rec in trade_stat_obj:
            list_trade_stat.append(rec.acct_id.id)
        final_list = list(set(list_trade_stat))
        date = str(self.post_date)
        final_list.insert(0,date)

#   This is a wizard screen to allow user to confirm 
#   and start the End-Of-Day trade settlement.
    @api.multi
    def Trade_Settlement(self):
        proc_code = 'EOD_TRADE_SETT'+str(self.post_date)
        proc_lock = self.env['vb.proc_lock'].search([('proc_code','=',proc_code)])
        if proc_lock:
            raise Warning('This function already performed for this date.')
        else:
            proc_lock=self.env['vb.proc_lock'].create({'proc_code':proc_code,'proc_freq':'Day','proc_period':str(self.post_date),'state':'InProgress'})
            Trade_Settl_recs = self.env['vb.procedure_exec_request']
            rec_id = Trade_Settl_recs.create({'name':'SOD trade settlement', 'procfn':'SOD_TRADE_SETT','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'show_trade_count_wizard_view')[1]
            message = str(rec_id.rec_count) + " records read, " + str(rec_id.rec_processed) + " records processed, " + str(rec_id.rec_skipped) + " records failed."
            if rec_id.rec_processed > 0:
                proc_lock.write({'state':'Processed'})
            else:
                proc_lock.unlink()

            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

#   This is a wizard screen to allow user to confirm 
#   and start the End-Of-Day pre-trade settlement.
    @api.multi
    def Trade_pre_Settlement(self):
        proc_code = 'EOD_TRADE_SETT_PRE'+str(self.post_date)
        proc_lock = self.env['vb.proc_lock'].search([('proc_code','=',proc_code)])
        if proc_lock:
            raise Warning('This function already performed for this date.')
        else:
            proc_lock=self.env['vb.proc_lock'].create({'proc_code':proc_code,'proc_freq':'Day','proc_period':str(self.post_date),'state':'InProgress'})
            Trade_Pre_Settl_recs = self.env['vb.procedure_exec_request']
            rec_id = Trade_Pre_Settl_recs.create({'name':'SOD pre-trade settlement', 'procfn':'SOD_TRADE_SETT_PRE','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'show_trade_count_wizard_view')[1]
            message = str(rec_id.rec_count) + " records read, " + str(rec_id.rec_processed) + " records processed, " + str(rec_id.rec_skipped) + " records failed."
            if rec_id.rec_processed > 0:
                proc_lock.write({'state':'Processed'})
            else:
                proc_lock.unlink()

            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

#    Method to create record at vb.procedure_exec_request regarding to closing order
    @api.multi
    def order_closing(self):
        proc_code = 'EOD_ORDER_PROC_'+str(self.post_date)
        proc_lock = self.env['vb.proc_lock'].search([('proc_code','=',proc_code)])
        if proc_lock:
            raise Warning('This function already performed for this date.')
        else:
            proc_lock=self.env['vb.proc_lock'].create({'proc_code':proc_code,'proc_freq':'Day','proc_period':str(self.post_date),'state':'InProgress'})
            Trade_order_clos_recs = self.env['vb.procedure_exec_request']
            rec_id = Trade_order_clos_recs.create({'name':'EOD order processing', 'procfn':'EOD_ORDER_PROC','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'show_trade_count_wizard_view')[1]
            message = str(rec_id.rec_processed)+" records processed"
            if rec_id.rec_processed > 0:
                proc_lock.write({'state':'Processed'})
            else:
                proc_lock.unlink()

            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

#   method to create record at vb.procedure_exec_request regarding to post daily trades
    @api.multi
    def Post_daily_trades(self):
        proc_code = 'EOD_TRADE_POST_'+str(self.post_date)
        proc_lock = self.env['vb.proc_lock'].search([('proc_code','=',proc_code)])
        if proc_lock:
            raise Warning('This function already performed for this date.')
        else:
            proc_lock=self.env['vb.proc_lock'].create({'proc_code':proc_code,'proc_freq':'Day','proc_period':str(self.post_date),'state':'InProgress'})
            post_daily_trades_recs = self.env['vb.procedure_exec_request']
            rec_id = post_daily_trades_recs.create({'name':'EOD trade posting', 'procfn':'EOD_TRADE_POST','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'show_trade_count_wizard_view')[1]
            message = str(rec_id.rec_processed)+" records processed"
            if rec_id.rec_processed > 0:
                proc_lock.write({'state':'Processed'})
            else:
                proc_lock.unlink()

            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

#   method to post trade transaction when state equal to New
    @api.multi
    def post_trade_tran_manage(self):
        trade_tran_post_recs = self.env['vb.trade_tran'].search([('state', '=', 'New'),('id','=',self._context.get('active_id'))])
        if trade_tran_post_recs:
            for rec in trade_tran_post_recs:
                rec.write({'state':'Posted', 'stage':'Posted', 'posted_by':self._uid, 'date_posted':datetime.now()})
        else:
            raise Warning(_("The trade transaction state is not New"))
        return True
    
    @api.multi
    def settle_trade_tran_manage(self):
        trade_tran_post_recs = self.env['vb.trade_tran'].search([('state', '=', 'Posted'),('id','=',self._context.get('active_id'))])
        if trade_tran_post_recs:
                trade_tran_post_recs.write({'settle_flag':'Settled'})
        else:
            raise Warning(_("You can not settle this trade\n Please check the state it should be Posted"))

#     method to void trade transaction when state 
#     equal to Posted and set stage to Void
    @api.multi
    def void_trade_tran_manage(self):
        trade_tran_void_recs= self.env['vb.trade_tran'].search([('id', '=', self._context.get('active_id')),('state', '=', 'Posted')])
        if trade_tran_void_recs:
            if trade_tran_void_recs.tran_type not in  ['TradeSett','Contra']:
                if (trade_tran_void_recs.balance_amt == trade_tran_void_recs.tran_amt and trade_tran_void_recs.pending_amt==0) and ((trade_tran_void_recs.balance_qty == trade_tran_void_recs.tran_qty and trade_tran_void_recs.pending_qty==0)):
                    trade_tran_void_recs.write({'state':'Void', 'stage':'Void', 'void_by':self._uid, 'date_void':datetime.now()})
                else:
                    raise Warning(_("Transaction already Contra or settled."))
            else:
                if trade_tran_void_recs.tran_type == "TradeSett":
                    trade_tran_void_recs.write({'state':'Void', 'stage':'Void', 'void_by':self._uid, 'date_void':datetime.now()})
        else:
            raise Warning(_("The transaction already voided."))
        return True
    
    @api.multi
    def void_trade_tran_contra(self):
        trade_tran_void_recs= self.env['vb.trade_tran'].search([('id', '=', self._context.get('active_id')),('state','!=','Void')])
        if trade_tran_void_recs:
            if trade_tran_void_recs.balance_amt != trade_tran_void_recs.tran_amt and trade_tran_void_recs.pending_amt!=0:
                raise Warning('Transaction already settled.')
            else:
                trade_tran_void_recs.write({'state':'Void', 'stage':'Void', 'void_by':self._uid, 'date_void':datetime.now()})
        else:
            raise Warning(_("The transaction already Voided"))

#     method to reconcile trade transaction when state equal 
#     to Void and set stage and state to Reconcile
    @api.multi
    def reconcile_trade_tran_manage(self):
        trade_tran_reconcile_recs= self.env['vb.trade_tran'].search([('id', 'in', self._context.get('active_id')),('state', '=', 'Reconcile')])
        if trade_tran_reconcile_recs:
            trade_tran_reconcile_recs.write({'state':'Reconcile', 'stage':'Reconcile', 'reconciled_by':self._uid, 'date_reconciled':datetime.now()})
        else:
            raise Warning(_("The trade transaction state is not Reconcile"))
    @api.multi
    def buy_sell_trade_contra(self):
        rec = self.env['vb.trade_tran'].search([('id','=',self._context.get('active_id'))])
        if rec.tran_type == "Buy":
            if int(rec.balance_qty + rec.pending_qty + rec.pickup_bal_qty) > 0:
                if rec.pickup_flag == True and (rec.balance_qty == (rec.pickup_bal_qty + rec.pending_qty)):
                    raise Warning("It's already confirmed or fully picked up")
                else:
                    rec.update({'pickup_flag':True, 'date_pickup':datetime.now(), 'pickup_by':self._uid})
            else:
                raise Warning("Balance qty should be greater than zero")
        elif rec.tran_type == "Sell":
            raise Warning("It should be a buy transaction")

    # method to approve the force selling, state = 'New-Amo'
    @api.multi
    def approve_force_selling(self):
        force_sell_pending_rec = self.env['vb.order_book'].search([('state', '=', 'ForcesellPending'), ('id', '=', self._context.get('active_id'))])
        if force_sell_pending_rec:
            for rec in force_sell_pending_rec:
                rec.write({'state': 'New-Amo', 'approve_reject_by': self._uid, 'approve_reject_date': datetime.now()})
        else:
            raise Warning(_("The force selling transaction state is not Forcesell pending"))
        return True

    # method to reject the force selling, stage = 'closed'
    @api.multi
    def reject_force_selling(self):
        force_sell_pending_rec = self.env['vb.order_book'].search([('state', '=', 'ForcesellPending'), ('id', '=', self._context.get('active_id'))])
        if force_sell_pending_rec:
            for rec in force_sell_pending_rec:
                rec.write({'state': 'ForcesellRejected', 'stage': 'Closed', 'approve_reject_by': self._uid, 'approve_reject_date': datetime.now()})
        else:
            raise Warning(_("The force selling transaction state is not Forcesell pending"))
        return True

    @api.multi
    def approve_bulk_force_selling(self, context):
        force_sell_pending_rec = self.env['vb.order_book'].search([('state', '=', 'ForcesellPending'), ('id', 'in', context.get('active_ids'))])
        if force_sell_pending_rec:
            count = 0
            for rec in force_sell_pending_rec:
                rec.write({'state': 'New-Amo', 'approve_reject_by': self._uid, 'approve_reject_date': datetime.now()})
                count += 1
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_bulk_successful_wizard')[1]
            message = "%s force sell orders have been approved successfully." % str(count)
            return {
                'name': 'Force selling',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_unsuccessful_wizard')[1]
            message = "Force selling can only be approved if status is 'Forceselling pending'. Please check selected order."
            return {
                'name': 'Force selling',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }

    @api.multi
    def reject_bulk_force_selling(self, context):
        force_sell_pending_rec = self.env['vb.order_book'].search([('state', '=', 'ForcesellPending'), ('id', 'in', context.get('active_ids'))])
        if force_sell_pending_rec:
            count = 0
            for rec in force_sell_pending_rec:
                rec.write({'state': 'ForcesellRejected', 'stage': 'Closed', 'approve_reject_by': self._uid,'approve_reject_date': datetime.now()})
                count += 1
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_bulk_successful_wizard')[1]
            message = "%s force sell orders have been rejected successfully." % str(count)
            return {
                'name': 'Force selling',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_unsuccessful_wizard')[1]
            message = "Force selling can only be rejected if status is 'Forceselling pending'. Please check selected order."
            return {
                'name': 'Force selling',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }

    @api.multi
    def sendto_exchange_force_selling(self, context):
        # sit_url = "http://172.18.6.205:8080/vbroker/api/v1/order/place-amo-forcesell"
        approved_rec = self.env['vb.order_book'].search([('state', '=', 'New-Amo'), ('trade_term', '=', 'ForceSell'), ('stage','in',['New','Renew'])])
        if approved_rec:
            count = len(list(approved_rec))
            url_rec = self.env['vb.config'].search([('code_type', '=', 'WebSvc'), ('code', '=', 'ForceSell2Exchange')])
            if url_rec:
                url = str(url_rec.parm1) + ':' + str(url_rec.parm2) + '/' + str(url_rec.parm5)
                data = dumps({})
                headers = {'Content-type': 'application/json'}
                try:
                    content = requests.post(url, data=data, headers=headers)
                    if content == False:
                        raise Warning('Error in sending to Exchange')
                    else:                
                        final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_bulk_successful_wizard')[1]
                        message = "%s approved force sell orders have been sent to exchange." % str(count)
                        return {
                            'name': 'Force selling',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'view_id': final_view,
                            'target': 'new',
                            'context': {'default_message': message},
                        }
                except Exception as err:
                    print err
                    traceback.print_exc()
                    raise Warning("Wrong connection value in system parameters.")
            else:
                raise Warning("No 'ForceSell2Exchange' code was found in system parameters.")
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_unsuccessful_wizard')[1]
            message = "No force sell orders has been approved recently, hence no orders sent to exchange"
            return {
                'name': 'Force selling',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }

    # Trade Transaction print xls button
    @api.multi
    def get_print_trade_tran(self):
        ids = self.env['vb.trade_tran'].search([('id','in',self._context.get('active_ids'))])

        data = []
        for id in ids:
            data.append([id.acct_id.acct_no,
                         id.acct_id.name,
                         id.tran_date,
                         id.date_posted,
                         id.tran_type,
                         id.charge_id.code,
                         id.tran_refno,
                         id.asset_id.name,
                         id.tran_qty,
                         id.tran_price,
                         id.tran_amt,
                         id.balance_qty,
                         id.balance_amt,
                         id.due_date,
                         id.state
                       ])

        header = ("Customer account,Customer name,Tran date,Date/Time posted,Tran,Charge code,Reference,Asset,Tran qty,Price,Tran amt,Bal qty,Bal amt,Due date,Status")

        count =0
        text1=''
        for i in data:
            for k in i:
                text1 += str(k or '').replace(',','')
                count +=1
                if count == 15: # columns
                    text1+='\n'
                    count = 0
                else:
                    text1+=','
        
        final_text = header +'\n'+text1
        filename = "Trade Transaction.csv"
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text),'file_name':filename})

        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }


    # Force Selling print CSV
    @api.multi
    def get_print_force_selling(self):
        ids = self.env['vb.order_book'].search([('id','in',self._context.get('active_ids'))])

        data = []
        for id in ids:
            data.append([id.acct_id.acct_no,
                         id.acct_id.name,
                         id.tran_type,
                         id.asset_id.name,
                         id.order_type,
                         id.buy_sell,
                         id.order_no,
                         id.appto_order_no,
                         id.expiry_type,
                         id.expiry_date,
                         id.available_qty,
                         id.limit_amt,
                         id.dlv_basis_id.name,
                         id.order_qty,
                         id.limit_price,
                         id.trading_board,
                         id.state,
                         id.remarks
                       ])

        header = ("Customer account,Customer name,Tran type,Asset,Order type,Buy/Sell,Order no,Apply to order no,Expiry type,Expiry date,Available qty,Limit amt,Delivery basis,Order qty,Limit price,Trading board,State,Remarks")

        count =0
        text1=''
        for i in data:
            for k in i:
                text1 += str(k or '').replace(',','')
                count +=1
                if count == 18: # columns
                    text1+='\n'
                    count = 0
                else:
                    text1+=','
        
        final_text = header +'\n'+text1
        filename = "Force sell.csv"
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text),'file_name':filename})

        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }

    @api.multi
    def manual_settlement_wiz(self):
        try:
            rec = self.env['vb.trade_tran'].search([('id','=',self._context.get('active_ids'))])
            sp = "Select sp_trade_sett_by_ref(%s,%s,%s)"
            self._cr.execute(sp,(self._uid,rec.acct_id.id,rec.tran_refno))
        except ValueError:
            raise Warning(ValueError)
        final_view = self.env['ir.model.data'].get_object_reference('vb_trade', 'force_selling_unsuccessful_wizard')[1]
        message = "Transaction has been settled successfully "
        return {
            'name': 'Manual Settlement',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'view_id': final_view,
            'target': 'new',
            'context': {'default_message': message},
        }
