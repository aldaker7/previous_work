import os
from openerp import models, api , _ , fields
from datetime import  datetime, date, timedelta
from openerp.exceptions import Warning
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import string, random, base64, hashlib
import requests
from json import dumps
import json
import os.path
from openerp.addons.web import http
from openerp.http import request
from openerp.addons.web.controllers.main import content_disposition
import tempfile
from os import chdir
import webbrowser
from sys import argv
from collections import Counter
from string import punctuation
from operator import methodcaller
import ast
import openerp
from openerp.addons.web.controllers.main import Home as main

# class for customer buttons only,
# button separated from acctmgmt.py
#  for no lose data purpose.
class customer(models.Model):
    _inherit = 'vb.customer'

    promo_code = fields.Char(string='Promotion code')

    # this button used at Customers List form,
    # for a specific customer based on the form acctno.
    @api.multi
    def get_uplift_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'uplift_customer_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Uplift Customer",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     method to get Suspend wizard for customer
#     based on the customer acct_id
    @api.multi
    def get_suspend_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'customer_suspend_wizard_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Suspend Customer",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     method to get Reject wizard for customer
#     based on the record acct_id.
    @api.multi
    def get_reject_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'customer_reject_wizard_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Reject Customer",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

        #method to get Activate wizard for customer
    @api.multi
    def get_activate(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'customer_activate_wizard_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Activate Customer",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

    @api.multi
    def forget_login(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'forget_login_form_view')[1]
        except ValueError:
            form_id = False
        return{
               'name': "Send login info",
               'view_type': 'form',
               'view_mode':"form",
               'view_id': form_id,
               'res_model': 'vb.common_wizard',
               'type': 'ir.actions.act_window',
               'target':'new'
               }

    @api.multi
    def incomplete_form_customer(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','incomplete_customer_form_view1')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Incomplete customer signup',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def get_customer_details(self):
        mod_obj = self.env['ir.model.data']
        try:
            form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_form')[1]
        except ValueError:
            form_res = False
        return{
               'name': "Customer Details",
               'view_type': 'form',
               'view_mode':"[form]",
               'view_id': form_res,
               'res_model': 'vb.customer',
               'type': 'ir.actions.act_window',
                'views': [(form_res, 'form')],
               'res_id': self.id,
               }

class customer_acct(models.Model):
    _inherit = 'vb.customer_acct'

    #Method to get account cash details
    @api.multi
    def call_cash_transaction_view(self):
        mod_obj = self.env['ir.model.data']
        try:
            tree_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_tree1')[1]
            form_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_form1')[1]
            search_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_search1')[1]
        except ValueError:
            form_res = tree_res = search_res = False
        return{
               'name': "Transactions Cash Details",
               'view_type': 'form',
               'view_mode':"[tree,form]",
               'view_id': False,
               'res_model': 'vb.cash_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_res, 'tree'), (form_res, 'form')],
               'domain': [('acct_id','=', self.id),('state','in',['Posted','Void'])],
               'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
               'search_view_id':[(search_res)],
               }

    #Method to get Transactions Trade Details
    @api.multi
    def call_trade_transaction_view(self):
        mod_obj = self.env['ir.model.data']
        try:
            tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
            form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
            search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
        except ValueError:
            form_res = tree_res = search_res = False
        return{
               'name': "Transactions Trade Details",
               'view_type': 'form',
               'view_mode':"[tree,form]",
               'view_id': False,
               'res_model': 'vb.trade_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_res, 'tree'), (form_res, 'form')],
               'domain': [('acct_id','=',self.id),('state','in',['Posted','Void']),('tran_type', '!=', 'Opening')],
               'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
               'search_view_id':[(search_res)],
               }

    #Method to get Asset Transactions Details
    @api.multi
    def call_asset_transaction_view(self):
        mod_obj = self.env['ir.model.data']
        try:
            tree_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_tree1')[1]
            form_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_form1')[1]
            search_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_search1')[1]
        except ValueError:
            tree_res = False
        return{
               'name': "Asset Transaction Details",
               'view_type': 'form',
               'view_mode':'[tree,form]',
               'view_id': False,
               'res_model': 'vb.asset_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_res, 'tree'), (form_res, 'form')],
               'domain': [('portfolio_id.acct_id','=',self.id),('state','in',['Posted','Void']),('tran_type', '!=', 'Opening')],
               # 'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
               'search_view_id':[(search_res)],
               }

    # this button object method use to activate
    # an account when it state equal to new.
    @api.multi
    def get_activate_account_wizard(self):
        if self.state == 'Checked':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'activate_account_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Activate account',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            self.cds_no = u''
            raise Warning('Please approve this account before activate it.')

    @api.multi
    def get_pending_close_account(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'pending_close_account_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
#     @api.multi
#     def get_all_activate_acct_wizard(self):
#         try:
#             form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'activate_checked_account_wizard_view')[1]
#         except ValueError:
#             form_id = False
#         return {
#                     'type': 'ir.actions.act_window',
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'res_model': 'vb.common_wizard',
#                     'views': [(form_id, 'form')],
#                     'view_id': form_id,
#                     'target': 'new',
#                 }

    # this button object method use to close
    # an account when it state equal to pending-close.
    @api.multi
    def get_close_account_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'closed_account_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Pending Close',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    # this button object method use to reject
    # an account when it state equal to new.
    @api.multi
    def get_reject_account_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'reject_account_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Application Rejection',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }


    # this button object method use to suspend
    # an account when it state equal to active.
    @api.multi
    def get_suspend_account_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'suspend_account_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def get_suspend_account_pending_close_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'suspend_account_pending_close_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def get_customer_details(self):
        mod_obj = self.env['ir.model.data']
        try:
            form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_form')[1]
        except ValueError:
            form_res = False
        return{
               'name': "Customer Details",
               'view_type': 'form',
               'view_mode':"[form]",
               'view_id': form_res,
               'res_model': 'vb.customer',
               'type': 'ir.actions.act_window',
                'views': [(form_res, 'form')],
               'res_id': self.customer_id.id,

               }

    # this button object method use to uplift
    # an account when it state equal to suspended or credit hold.
    @api.multi
    def get_uplift_account_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'uplift_account_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def upload_doc(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'upload_documets_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }


    @api.multi
    def show_risks_profile(self):
        if self.show_nrvb == True:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'ramci_second_time_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            stage1 = False
            stage2 = False
    #         if self.acct_type == 'Cash':
    #             self.update({'is_cedd_done': True})
    #         url = "http://172.18.6.105:8080/vbroker/api/v1/ramci"
            url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RamciInfo')])
            url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
            data = dumps({"loginName" : str(self.customer_id.login_name),
                          "acctId" : str(self.id),
                          "customerName": self.customer_id.name,
                          "idNo": str(self.national_id_no),
                          "idType" : str(self.national_id_type),
                          "country": str(self.customer_id.nationality_id.code),
                          "dob": str(self.customer_id.date_of_birth),
                          "mobileNo":str(self.mobile_phone1),
                          "emailAddress": str(self.email1),
                          "lastKnownAddress": ""})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)
            if int(content.json().get('code'))!=200 or content ==False:
                raise Warning(_(content.json().get('message')))
                
            risk_list = []
            quest_list=[]
            if not self.acct_class_id:
                raise Warning("Product has not been specified for this Account")
            else:
                acct_class_rec= self.acct_class_id
                if not acct_class_rec.riskprof_id:
                    raise Warning("Risk profile for the Product cannot be located")
                else:
                    riskprof_rec= acct_class_rec.riskprof_id
                    if not riskprof_rec.checklist_ids:
                        raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                    else:
                        checklist_state= riskprof_rec.checklist_ids
                        quest_recs= riskprof_rec.questionaire_ids
            query ="Delete from vb_customer_acct_riskcheck Where acct_id =%s"%self.id
            self._cr.execute(query)
            if bool(quest_recs)!=True:
            #to get the risk_checklist_ids recs from the specific product
                for rec in quest_recs:
                    quest_dict=(0,0,{'question_id':rec.id,'response':rec.desired_response,'scoring':rec.scoring})
                    quest_list.append(quest_dict)
                #to get the questionaire_ids recs from the specific product
                for rec in checklist_state:
                    if content.json().get('result'):
                        for i in content.json().get('result'):
                            if str(rec.code) == str(i):
                                if bool(int(content.json().get('result').get(str(rec.code))) >= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'OK','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                elif bool(int(content.json().get('result').get(str(rec.code))) == -1 or int(content.json().get('result').get(str(rec.code))) == -1):
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'OK','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                else:
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'Failed','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                    else:
                        raise Warning ("This account doesn't have any risk check list")
                self.write({'riskcheck_ids':risk_list,'riskquestion_ids':quest_list})
            else:
                for rec in checklist_state:
                    if content.json().get('result'):
                        for i in content.json().get('result'):
                            if str(rec.code) == str(i):
                                if bool(int(content.json().get('result').get(str(rec.code))) >= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                    risk_dict = (0,0,{'name' : rec.name,'state':'OK','riskcheck_id':rec.id,'result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                elif bool(int(content.json().get('result').get(str(rec.code)))>= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,
                                                      'state':'OK','result_min':rec.result_min,
                                                      'result_max':rec.result_max,
                                                      'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                else:
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'Failed','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                    else:
                        raise Warning ("This account doesn't have any risk check list")
                self.write({'riskcheck_ids':risk_list})
                if self.acct_type == 'Cash':
                    query = "update vb_customer_acct set ramci_contra1='t', ramci_contra2='t' where id=%s"%self.id
                    self._cr.execute(query)
                else:
                    query = "update vb_customer_acct set ramci_contra1='t' where id=%s"%self.id
                    self._cr.execute(query)
    #         card_rec = self.env['vb.card_tran'].search([('acct_id','=',self.id)])
    #         if card_rec:
    #             for card in card_rec:
    #                 if card.cardholder_name and card.state in ['Posted','Ok']:
    #                     query=''
    #                     query = 'UPDATE vb_customer_acct SET card_tran_name=%s Where id=%s'
    #                     self._cr.execute(query,(card.cardholder_name,self.id))
            if int(content.json().get('code'))==200:
                stage1 = True
                query_cust = "Update vb_customer set ramci_kyc_date=%s,ramci_nrvb_date=%s where id=%s"
                self._cr.execute(query_cust,(datetime.now(),datetime.now(),self.customer_id.id))
            url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/'+str(url_rec.parm6)
            data = dumps({ "loginName" : self.customer_id.login_name,
                          "acctId" : str(self.id)})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)
            doc_rec = self.env['vb.document']
            x =content.text.split(',')
            stage2 = True
            if stage1== True and stage2 == True:
                self.update({'nrvb_clicked_flag':True})
            list = []
            doc_list =[]
            doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm2
            file_loc = doc_directory +'RAMCI/'
            if os.path.exists(file_loc)== False:
                os.makedirs(file_loc)
            for rec in self.env['vb.document'].search([('acct_id','=',self.id),('customer_id','=',self.customer_id.id)]):
                doc_list.append(rec.file_name)
            for i in x:
                index = x.index(i)
                if not self.rel_acct_id:
                    if 'KYCPDF=' in i:
                        file_name_kyc = str('KYCPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_kycpdf = str(file_loc + file_name_kyc)
                        rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=',"KYC report")])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)

                        if file_name_kyc not in doc_list or bool(doc_list) == False:
                            kycpdf = base64.b64decode(x[index].split('KYCPDF=')[1])
                            f = open(file_path_kycpdf,'wb')
                            f.write(kycpdf)
                            f.close()
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',self.customer_id.id,
                                                self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                    elif 'IRISSPDF=' in i:
                        file_name_iriss = str('IRISSPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_irisspdf = str(file_loc+file_name_iriss)
                        check_doc2 = self.env['vb.document'].search([('acct_id','=',self.id),('name','=','IRISS report')])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)

                        if file_name_iriss not in doc_list or bool(doc_list) == False:
                            irisspdf = base64.b64decode(x[index].split('IRISSPDF=')[1])
                            y = open(file_path_irisspdf,'wb')
                            y.write(irisspdf)
                            y.close()
                            query='''INSERT INTO vb_document (file_loc,name, doc_version,file_name,is_open_exist,
                                    customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_irisspdf,'IRISS report',1.0,file_name_iriss,'RAMCI',self.customer_id.id,
                                            self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                            'Internal','New',datetime.now(),datetime.now(),self._uid,self._uid,True))

                    elif 'NRVBPDF' in i:
                        file_name_nrvb=str('NRVBPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_nrvbpdf = str(file_loc + file_name_nrvb)
                        rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=','NRVB report')])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)
                        if file_name_nrvb not in doc_list or bool(doc_list) == False:
                            nrvbpdf = base64.b64decode(x[index].split('NRVBPDF=')[1])
                            g = open(file_path_nrvbpdf,'wb')
                            g.write(nrvbpdf)
                            g.close()
                            check_doc1 = self.env['vb.document'].search([('acct_id','=',self.id),('file_name','=',file_name_nrvb),('file_loc','=',file_path_nrvbpdf)])
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',self.customer_id.id,
                                                    self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                    'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                else:
                        if 'KYCPDF=' in i:
                            file_name_kyc = str('KYCPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                            file_path_kycpdf = str(file_loc + file_name_kyc)
                            rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=',"KYC report")])
                            if rec_check:
                                for i in rec_check:
                                    query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                    self._cr.execute(query)

                            if file_name_kyc not in doc_list or bool(doc_list) == False:
                                kycpdf = base64.b64decode(x[index].split('KYCPDF=')[1])
                                f = open(file_path_kycpdf,'wb')
                                f.write(kycpdf)
                                f.close()
                                query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                                 customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                  VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                self._cr.execute(query,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',self.customer_id.id,
                                                    self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                    'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                                if self.state == 'New':
                                    query2='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                                     customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                      VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                    self._cr.execute(query2,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',self.customer_id.id,
                                                        self.rel_acct_id.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                        'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                        elif 'IRISSPDF=' in i:
                            file_name_iriss = str('IRISSPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                            file_path_irisspdf = str(file_loc+file_name_iriss)
                            check_doc2 = self.env['vb.document'].search([('acct_id','=',self.id),('name','=','IRISS report')])
                            if rec_check:
                                for i in rec_check:
                                    query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                    self._cr.execute(query)

                            if file_name_iriss not in doc_list or bool(doc_list) == False:
                                irisspdf = base64.b64decode(x[index].split('IRISSPDF=')[1])
                                y = open(file_path_irisspdf,'wb')
                                y.write(irisspdf)
                                y.close()
                                query='''INSERT INTO vb_document (file_loc,name, doc_version,file_name,is_open_exist,
                                        customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                self._cr.execute(query,(file_path_irisspdf,'IRISS report',1.0,file_name_iriss,'RAMCI',self.customer_id.id,
                                                self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                'Internal','New',datetime.now(),datetime.now(),self._uid,self._uid,True))
                                #if self.state == 'New':
                                #    query2='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                                #             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                #              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                #    self._cr.execute(query2,(file_path_irisspdf,'IRISS report',1.0,file_name_iriss,'RAMCI',self.customer_id.id,
                                #                            self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                #                            'Internal','New',datetime.now(),datetime.now(),self._uid,self._uid,True))


                        elif 'NRVBPDF' in i:
                            file_name_nrvb=str('NRVBPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                            file_path_nrvbpdf = str(file_loc + file_name_nrvb)
                            rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=','NRVB report')])
                            if rec_check:
                                for i in rec_check:
                                    query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                    self._cr.execute(query)
                            if file_name_nrvb not in doc_list or bool(doc_list) == False:
                                nrvbpdf = base64.b64decode(x[index].split('NRVBPDF=')[1])
                                g = open(file_path_nrvbpdf,'wb')
                                g.write(nrvbpdf)
                                g.close()
                                check_doc1 = self.env['vb.document'].search([('acct_id','=',self.id),('file_name','=',file_name_nrvb),('file_loc','=',file_path_nrvbpdf)])
                                query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                                 customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                  VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                self._cr.execute(query,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',self.customer_id.id,
                                                        self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                        'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                                if self.state == 'New':
                                   query2='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                                            customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                             VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                                   self._cr.execute(query2,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',self.customer_id.id,
                                                           self.rel_acct_id.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                           'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
            del doc_list[:]




    @api.multi
    def risk_check_button(self):
        count_failed=0
        for i in self:
            for doc in i.document_ids:
                if doc.state not in ["Approved", "Archived"]:
                    count_failed+=1
                    break
            if not i.riskcheck_ids or count_failed>0:

                i.is_riskcheck_exist = False
            else:
                i.is_riskcheck_exist = True

    @api.multi
    def suspend_buy(self):
        if self.state == 'Active':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'suspend_buy_form_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Suspend buy trades',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }

    @api.multi
    def suspend_sell(self):
        if self.state == 'Active':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'suspend_sell_form_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Suspend sell trades',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }

    @api.multi
    def uplift_buy(self):
        if self.state == 'Active':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'uplift_buy_form_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Uplift buy trades',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }

    @api.multi
    def uplift_sell(self):
        if self.state == 'Active':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'uplift_sell_form_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Uplift sell trades',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }

#

    @api.multi
    def incomplete_form_accounts(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','incomplete_accounts_form_view1')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Incomplete account',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
    @api.multi
    def approve_RMCI(self):
        count_bypass = 0
        for rec in self.riskcheck_ids:
            if rec.state == 'ByPass':
                count_bypass +=1
        if count_bypass == 0:
            if self.acct_type != 'Cash':
                if self.facility_approved_amt > 0.0:
                    self._cr.execute("Select state from vb_customer_acct_cedd where acct_id=%s"%self.id)
                    cedd_state = self._cr.fetchone()[0]
                    if cedd_state == 'EDDRejected':
                        if self.env['res.users'].has_group('vb_base.vb_group_operation') or self.env['res.users'].has_group('vb_base.vb_group_operation_mgr'):

                            try:
                                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_form_view')[1]
                            except ValueError:
                                form_id = False
                            return {
                                        'name':'Approving application',
                                        'type': 'ir.actions.act_window',
                                        'view_type': 'form',
                                        'view_mode': 'form',
                                        'res_model': 'vb.common_wizard',
                                        'views': [(form_id, 'form')],
                                        'view_id': form_id,
                                        'target': 'new',
                                    }
                        else:
                            raise Warning("Only Operations and Operations Managers can approve this document")
                    else:
                        try:
                            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_form_view')[1]
                        except ValueError:
                            form_id = False
                        return {
                                    'name':'Approving application',
                                    'type': 'ir.actions.act_window',
                                    'view_type': 'form',
                                    'view_mode': 'form',
                                    'res_model': 'vb.common_wizard',
                                    'views': [(form_id, 'form')],
                                    'view_id': form_id,
                                    'target': 'new',
                                }
                else:
                    raise Warning("Approve limit amount should be greater than 0")
            else:
                self._cr.execute("Select state from vb_customer_acct_cedd where acct_id=%s"%self.id)
                cedd_state = self._cr.fetchone()[0]
                if cedd_state == 'EDDRejected':
                    if self.env['res.users'].has_group('vb_base.vb_group_operation') or self.env['res.users'].has_group('vb_base.vb_group_operation_mgr'):
                        try:
                            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_form_view')[1]
                        except ValueError:
                            form_id = False
                        return {
                                    'name':'Approving application',
                                    'type': 'ir.actions.act_window',
                                    'view_type': 'form',
                                    'view_mode': 'form',
                                    'res_model': 'vb.common_wizard',
                                    'views': [(form_id, 'form')],
                                    'view_id': form_id,
                                    'target': 'new',
                                }
                    else:

                        raise Warning("Only Operations and Operations Managers can approve this document")
                else:
                    try:
                        form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_form_view')[1]
                    except ValueError:
                        form_id = False
                    return {
                                'name':'Approving application',
                                'type': 'ir.actions.act_window',
                                'view_type': 'form',
                                'view_mode': 'form',
                                'res_model': 'vb.common_wizard',
                                'views': [(form_id, 'form')],
                                'view_id': form_id,
                                'target': 'new',
                            }
        else:
            self._cr.execute("Select state from vb_customer_acct_cedd where acct_id=%s"%self.id)
            cedd_state = self._cr.fetchone()[0]
            if cedd_state == 'EDDRejected':
                if self.env['res.users'].has_group('vb_base.vb_group_operation') or self.env['res.users'].has_group('vb_base.vb_group_operation_mgr'):
                    try:
                        form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_bypass_form_view')[1]
                    except ValueError:
                        form_id = False
                    return {
                                'name':'Approving customer account',
                                'type': 'ir.actions.act_window',
                                'view_type': 'form',
                                'view_mode': 'form',
                                'res_model': 'vb.common_wizard',
                                'views': [(form_id, 'form')],
                                'view_id': form_id,
                                'target': 'new',
                            }
                else:
                    raise Warning("Only Operations and Operations Managers can approve this document")
            else:
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_RMCI_form_view')[1]
                except ValueError:
                    form_id = False
                return {
                            'name':'Approving application',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(form_id, 'form')],
                            'view_id': form_id,
                            'target': 'new',
                        }

# to search about account that date open more than 24 hours
    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if self._context.get('before_24hrs'):
            #the time will save in UTC time and it will save in database as UTC time not as System time because it will save it like this in database:
            #DEFAULT_SERVER_DATETIME_FORMAT instead of %Y-%M- %D %H %M %S
            before24hrs= (datetime.today() - timedelta(hours = 24)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            arg_domain = ('date_opened','<=',before24hrs)
            args.append(arg_domain)
        return super(customer_acct, self).search(args, offset, limit, order,count=count)

    @api.multi
    def send_email_wbsvc(self,login_n,acct_n,temp_pwd):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','SendTpin')])
        url =   "http://"+str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if (url_rec.parm1 or url_rec.parm2 or url_rec.parm5)== False:
            raise Warning('Please provide the params in system parameters code SendTpin')
        else:
            data = dumps({ "loginName" : str(login_n),
                           "accountNo" : str(acct_n),
                           'tempPassword':temp_pwd,
                         })
            headers={'Content-type': 'application/json'}
            content = requests.post(url,headers=headers,data=data)
            if content.json().get('code')!=200:
                return str(content.json().get('code'))+": "+content.json().get('message')
            else:
                return content.json().get('code')

    # @api.model
    # def send_email_auto_helpdesk(self):
    #     get_case_escalate_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.helpdesk_case'),('proc_type','=','EscalateCaseNotify'),('state','=','New'),('proc_freq','=','ASAP')])
    #     get_case_new_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.helpdesk_case'),('proc_type','=','HelpDeskNewCaseNotify'),('state','=','New'),('proc_freq','=','ASAP')])
    #     d1 = datetime.now()
    #     d2 = lambda d1: datetime.now()
    #     if get_case_escalate_id:
    #         for id in get_case_escalate_id:
    #             rec = self.env['vb.helpdesk_case'].search([('id','=',id.recordid)])
    #             if rec:
    #                 mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
    #                 if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
    #                     id.write({'proc_text':'Error in MQ server, system params are not specified'})
    #                 else:
    #                     channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
    #                     data= {
    #                        "publisherApp":"backoffice",
    #                        "templateCode":"HDESC",
    #                        "messageParams":{'date':str( d2(d1).strftime("%d-%m-%Y")) or '',},
    #                        "recipients":[{
    #                                      "recipientNo":"1",
    #                                      "recipientType":"normal",
    #                                      "name": id.parm2 or '',
    #                                      "email": id.parm1 or '',
    #                                      "title": id.parm3 or '',
    #                                     }]
    #                        }
    #                     data_json=json.dumps(data)
    #                     channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
    #                     id.update({'state':'Processed','proc_time':datetime.now()})
    #     if get_case_new_id:
    #         for id in get_case_new_id:
    #             rec = self.env['vb.helpdesk_case'].search([('id','=',id.recordid)])
    #             mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
    #             if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
    #                 id.write({'proc_text':'Error in MQ server, system params are not specified'})
    #             else:
    #                 if bool(rec.customer_id) == True:
    #                     channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
    #                     data= {
    #                         'acctId':rec.acct_id.id,
    #                         'custId':rec.customer_id.id,
    #                        "publisherApp":"backoffice",
    #                        "templateCode":"HDNNFY",
    #                        "messageParams":{'date':str( d2(d1).strftime("%d-%m-%Y")) or '',
    #                                         "ticket_no":rec.case_ref or '',
    #                                         'date_created':rec.create_date or '',
    #                                         'created_by':rec.create_uid.login or '',
    #                                         'case_cat':rec.category_id.name or '',},
    #                        "recipients":[{
    #                                      "recipientNo":"1",
    #                                      "recipientType":"normal",
    #                                      "name": id.parm2 or '',
    #                                      "email": id.parm1 or '',
    #                                      "title": id.parm3 or '',
    #                                     }]
    #                        }
    #                     data_json=json.dumps(data)
    #                     channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
    #                     id.update({'state':'Processed','proc_time':datetime.now()})
    #                 else:
    #                     channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
    #                     data= {
    #                        "publisherApp":"backoffice",
    #                        "templateCode":"HDNNFY",
    #                        "messageParams":{'date':str( d2(d1).strftime("%d-%m-%Y")) or '',
    #                                         "ticket_no":rec.case_ref or '',
    #                                         'date_created':rec.create_date or '',
    #                                         'created_by':rec.create_uid.login or '',
    #                                         'case_cat':rec.category_id.name or '',},
    #                        "recipients":[{
    #                                      "recipientNo":"1",
    #                                      "recipientType":"normal",
    #                                      "name": id.parm2 or '',
    #                                      "email": id.parm1 or '',
    #                                      "title": id.parm3 or '',
    #                                     }]
    #                        }
    #                     data_json=json.dumps(data)
    #                     channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
    #                     id.update({'state':'Processed','proc_time':datetime.now()})


#     @api.model
#     def send_email_auto(self):
#         get_refund_email_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.card_tran'),('proc_type','=','FreeCDSRegRefoundEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_acctopen_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctOpenNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_acctclose_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctClosedNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_acctpendclose_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctPendingCloseNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_acctstatment_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctMthStmtNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cashdepoist_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.cash_tran'),('proc_type','=','CashDepositEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cashwithdraw_client_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.cash_tran'),('proc_type','=','CashWithdrawEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cdstrfout1_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.asset_tran'),('proc_type','=','CDSTrfOutEmailCustomer1'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cdstrfout2_id = self.env['vb.proc_queue'].search([('dbtable','=','vb.asset_tran'),('proc_type','=','CDSTrfOutEmailCustomer2'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_verify = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','EmailVerifyNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_incomplete = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','IncompleteInfoNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_signup = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','EmailSignUpNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_suspend = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctSuspendNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_uplift = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','AcctSuspendUpliftNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cancel_cash_deposit = self.env['vb.proc_queue'].search([('dbtable','=','vb.cash_tran'),('proc_type','=','CashWithdrawCancelEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_opening_rejected = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer_acct'),('proc_type','=','OpeningRejectedNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_insuf_avail_qty = self.env['vb.proc_queue'].search([('dbtable','=','vb.trade_tran'),('proc_type','=','InsufficientAvailQtyNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_insuf_avail_cash_qty = self.env['vb.proc_queue'].search([('dbtable','=','vb.trade_tran'),('proc_type','=','InsufficientCashBalNotify'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_dividend_customer = self.env['vb.proc_queue'].search([('dbtable','=','vb.kibb_corp_action_dividend_pay'),('proc_type','=','DividendPayEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_corp_subs = self.env['vb.proc_queue'].search([('dbtable','=','vb.kibb_corp_action_subs_conv_dtl'),('proc_type','=','CorporateActionSubsNotify1'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_corp_subs_customer = self.env['vb.proc_queue'].search([('dbtable','=','vb.kibb_corp_action_subs_conv_dtl'),('proc_type','=','CorporateActionSubsNotify2'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_cds_failed = self.env['vb.proc_queue'].search([('dbtable','=','vb.asset_tran'),('proc_type','=','CDSTrfOutCancelEmailCustomer'),('state','=','New'),('proc_freq','=','ASAP')])
#         get_email_af_verify = self.env['vb.proc_queue'].search([('dbtable','=','vb.customer'),('proc_type','=','Email1Verified'),('state','=','New'),('proc_freq','=','ASAP')])
#         d1 = datetime.now()
#         d2 = lambda d1: datetime.now()
#         if get_email_af_verify:
#             for id in get_email_af_verify:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
# #                             'acctId':rec.id,
# #                             'custId':rec.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"EAV1",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")) or '',},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cds_failed:
#             for id in get_cds_failed:
#                 rec = self.env['vb.asset_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"CDFN",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")) or '',
#                                             "faccNo":str(rec.acct_id.acct_no) or '',
#                                             "faccName":str(rec.acct_id.name) or '',
#                                             "taccno":str(rec.to_cds_no) or '',
#                                             "taccName":str(rec.to_cds_name) or '',
#                                             "stockCode":str(rec.acct_id.acct_class_id.code) or '',
#                                             "stockName":str(rec.acct_id.acct_class_id.name) or '',
#                                             "stockQty":str(int(rec.tran_qty)) or '',
#                                             'ref_no':str(rec.tran_refno) or '',
#                                             'reason_code':str(rec.cds_trf_reason),
#                                             'request_date':rec.tran_date or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         #re-format the datetiem.now
#         if get_email_signup:
#             for id in get_email_signup:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                                 'acctId':id.recordid,
#                                 'custId':rec.customer_id.id,
#                                "publisherApp":"backoffice",
#                                "templateCode":"Cf1",
#                                "messageParams":{"date":(d2(d1).strftime("%d-%m-%Y")) or '',
#                                                 "date_opened":str(rec.date_opened) or ''},
#                                "recipients":[{
#                                              "recipientNo":"1",
#                                              "recipientType":"normal",
#                                              "name": id.parm2 or '',
#                                              "email": id.parm1 or '',
#                                              "title": id.parm3 or '',
#                                             }]
#                                         }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_corp_subs_customer:
#             for id in get_corp_subs_customer:
#                 rec = self.env['vb.kibb_corp_action_subs_conv_dtl'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"CASNC",
#                            "messageParams":{'entitle_type':rec.entitle_type or '',
#                                             'closedate':str(self.env['vb.kibb_corp_action_subs_conv_hdr'].search([('id','=',rec.hdr_id.id)]).reg_close_date) or '',
#                                             'assetname':rec.asset_name or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")) or '',},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",

#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_dividend_customer:
#             for id in get_dividend_customer:
#                 rec = self.env['vb.kibb_corp_action_dividend_pay'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"DPEC",
#                            "messageParams":{'accNo':str(rec.acct_id.acct_no) or '',
#                                             'stockName':(rec.asset_id.name) or '',
#                                             'tranAmount':"{0:,.2f}".format(rec.net_div_amt) or '',

#                                                 },
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "date":str( d2(d1).strftime("%d-%m-%Y ")),
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_corp_subs:
#             for id in get_corp_subs:
#                 rec = self.env['vb.kibb_corp_action_subs_conv_dtl'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"CASEC",
#                            "messageParams":{'entitle_type':rec.entitle_type or '',
#                                             'closedate':str(rec.ebg_close_date) or '',
#                                             'assetname':rec.asset_name or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")) or '',},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",

#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_insuf_avail_qty:
#             for id in get_insuf_avail_qty:
#                 rec = self.env['vb.trade_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"INSQTY",
#                            "messageParams":{
#                                             'stock_name':rec.asset_id.name or '' ,
#                                             'quantity':str(rec.tran_qty) or '',
#                                             'ref_no':str(rec.tran_refno) or '',
#                                             'cash_balance':str(self.env['vb.customer_acct_bal'].search([('acct_id','=',rec.acct_id.id)]).cash_available_bal) or '',
#                                             'amount_due':rec.balance_amt or '',
#                                             'due_date': str(rec.due_date) or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "date":str( d2(d1).strftime("%d-%m-%Y ")),
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_insuf_avail_cash_qty:
#             for id in get_insuf_avail_cash_qty:
#                 rec = self.env['vb.trade_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"INSCQTY",
#                            "messageParams":{'stock_code':str(rec.asset_id.asset_code) or '' ,
#                                             'stock_name':str(rec.asset_id.name) or '',
#                                             'ref_no':str(rec.tran_refno) or '',
#                                             'share_quantity':str(rec.appto_qty) or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")) or '',},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_refund_email_id:
#             for id in get_refund_email_id:
#                 rec = self.env['vb.card_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"Cdsf1",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y ")),
#                                             "tranAmount":"{0:,.2f}".format(rec.tran_amt) or '',},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cancel_cash_deposit:
#             for id in get_cancel_cash_deposit:
#                 rec = self.env['vb.cash_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         acct_bal = self.env['vb.customer_acct_bal'].search([('acct_id','=',rec.acct_id.id)])
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"CWCE",
#                            "messageParams":{},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "date":str( d2(d1).strftime("%d-%m-%Y ")),
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_email_incomplete:

#             for rec in get_email_incomplete:
#                 acct = self.env['vb.customer_acct'].search([('id','=',rec.recordid)])
#                 if acct:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         rec.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.recordid,
#                             'custId':acct.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"IEN1",
#                            "messageParams":{'login_name':str(acct.customer_id.login_name) or '',
#                                             "comment":str(acct.comments) or '',
#                                             "reason_incomplete":str(acct.reason_incomplete.name) or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")),},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": rec.parm2 or '',
#                                          "email": rec.parm1 or '',
#                                          "title": rec.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         rec.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_email_suspend:
#             for rec in get_email_suspend:
#                 acct = self.env['vb.customer_acct'].search([('id','=',rec.recordid)])
#                 if acct:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         rec.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.recordid,
#                             'custId':acct.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"As2",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")),
#                                             'acctNo':str(acct.acct_no),
#                                             "accn":str(acct.name or ''),
#                                             "date_suspended":acct.date_suspended or '',
#                                             "reason_suspension":str(acct.reason_suspended_id.name) or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": rec.parm2 or '',
#                                          "email": rec.parm1 or '',
#                                          "title": rec.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         rec.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_email_uplift:
#             for rec in get_email_uplift:
#                 acct = self.env['vb.customer_acct'].search([('id','=',rec.recordid)])
#                 if acct:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         rec.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.recordid,
#                             'custId':acct.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"As1",
#                            "messageParams":{"accno":str(acct.acct_no or ''),
#                                             'acctname':acct.name or '',
#                                             "date_suspended":str(acct.date_suspended) or '',
#                                             "date_uplifted":str(acct.date_uplifted) or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")),},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": rec.parm2 or '',
#                                          "email": rec.parm1 or '',
#                                          "title": rec.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         rec.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_opening_rejected:
#             for rec in get_opening_rejected:
#                 acct = self.env['vb.customer_acct'].search([('id','=',rec.recordid)])
#                 if acct:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         rec.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.recordid,
#                             'custId':acct.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"Cf3",
#                            "messageParams":{"login_name":str(acct.acct_type) or '',
#                                             "date":str(d2(d1).strftime("%d-%m-%Y")),
#                                             'product_name':acct.acct_type or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": rec.parm2 or '',
#                                          "email": rec.parm1 or '',
#                                          "title": rec.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         rec.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_email_verify:
#             for rec in get_email_verify:
#                 acct = self.env['vb.customer_acct'].search([('id','=',rec.recordid)])
#                 if acct:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         rec.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             "inboxFlag" : "True",
#                             'acctId':rec.recordid,
#                             'custId':acct.customer_id.id,
#                            "publisherApp":"backoffice",
#                            "templateCode":"Aa3",
#                            "messageParams":{"login_name":str(acct.customer_id.login_name) or '',
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")) or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": rec.parm2 or '',
#                                          "email": rec.parm1 or '',
#                                          "title": rec.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         rec.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_acctopen_id:
#             for id in get_acctopen_id:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                         cust_log_obj = self.env['vb.customer_login'].search([('customer_id','=',rec.customer_id.id)])
#                         temp_pass=''
#                         if cust_log_obj:
#                             for log_recs in cust_log_obj:
#                                 cus_rec = self.env['vb.customer_login'].search([('customer_id','=',rec.customer_id.id),('login_domain','=','FE'),('temp_pwd','=',False),('state','=','Active')])
#                                 if cus_rec:
#                                     if log_recs.login_domain == 'FE':
#                                         tem_password =''
#                                         temp_pass = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
#                                         for i in temp_pass:
#                                             if i in ['I','i','L','l','0','O','o','1']:
#                                                 tem_password = temp_pass.replace(i,'A')
#                                                 temp_pass = tem_password
#                                         log_recs.write({
#                                                          'temp_pwd': temp_pass,
#                                                          'login_pwd': base64.b64encode(hashlib.md5(hashlib.md5('ebglobal' +temp_pass).digest()).digest()),
#                                                          'valid_until':  datetime.strptime(d2(d1).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") + timedelta(days=7),
#                                                          'temp_pwd_issued':  d2(d1).strftime("%Y-%m-%d %H:%M:%S"),
#                                                          'temp_pwd_expiry':  datetime.strptime(d2(d1).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") + timedelta(days=7)
#                                                          })
#                     else:
#                         cust_log_obj = self.env['vb.customer_login'].search([('customer_id','=',rec.customer_id.id)])
#                         temp_pass=''
#                         tem_password =''
#                         temp_pass = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
#                         for i in temp_pass:
#                             if i in ['I','i','L','l','0','O','o','1']:
#                                 tem_password = temp_pass.replace(i,'A')
#                                 temp_pass = tem_password

#                         if rec.customer_id.email1_verified == True:
#                             if cust_log_obj:
#                                 for log_recs in cust_log_obj:
#                                     cus_rec = self.env['vb.customer_login'].search([('customer_id','=',rec.customer_id.id),('login_domain','=','FE'),('state','=','Active')])
#                                     if cus_rec:
#                                         if log_recs.login_domain == 'FE':
#                                             log_recs.write({
#                                                              'temp_pwd': temp_pass,
#                                                              'login_pwd': base64.b64encode(hashlib.md5(hashlib.md5('ebglobal' +temp_pass).digest()).digest()),
#                                                              'valid_until':  datetime.strptime(d2(d1).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") + timedelta(days=7),
#                                                              'temp_pwd_issued':  d2(d1).strftime("%Y-%m-%d %H:%M:%S"),
#                                                              'temp_pwd_expiry':  datetime.strptime(d2(d1).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") + timedelta(days=7)
#                                                              })


#                             call_wbsvc = self.send_email_wbsvc(rec.customer_id.login_name,rec.acct_no,temp_pass)
#                             if call_wbsvc == 200:
#                                 id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S"),'proc_text':''})
#                             else:
#                                 id.update({'proc_text':call_wbsvc})
#                         else:
#                             id.update({'proc_text':'The email for this customer account is not verified so far'})
#         if get_acctclose_id:
#             for id in get_acctclose_id:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':id.recordid,
#                             'custId':rec.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"Ac1",
#                            "messageParams":{"reason_code":str(rec.reason_closed_id.code) or '',
#                                             'reason_description':str(rec.reason_closed_id.name),
#                                             "date":str( d2(d1).strftime("%d-%m-%Y")) or '',
#                                             'closure_date':rec.date_closed or ''},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_acctpendclose_id:
#             for id in get_acctpendclose_id:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':id.recordid,
#                             'custId':rec.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"Pc1",
# #                            "messageParams":{"accno":str(rec.acct_no) or '',
# #                                             'login_name':str(rec.customer_id.login_name)},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_acctstatment_id:
#             for id in get_acctstatment_id:
#                 rec = self.env['vb.customer_acct'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':id.recordid,
#                             'custId':rec.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"Ms1",
#                            "messageParams":{"accno":str(rec.acct_no) or '',
#                                             },
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cashdepoist_id:
#             for id in get_cashdepoist_id:
#                 rec = self.env['vb.cash_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         acct_bal = self.env['vb.customer_acct_bal'].search([('acct_id','=',rec.acct_id.id)])
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"CDOFF",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")) or '',
#                                             "bankslipNo":str(rec.bank_refno) or '',
#                                             "accountNo":str(rec.acct_id.acct_no) or '',
#                                             "accountName":str(rec.acct_id.name) or '',
#                                             "tranAmount":"{0:,.2f}".format(rec.tran_amt) or '',
#                                             "availableCash":"{0:,.2f}".format(acct_bal.cash_available_bal) or '',
#                                             'trandingLimit':"{0:,.2f}".format(round(acct_bal.trade_limit,2))},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cashwithdraw_client_id:
#             for id in get_cashwithdraw_client_id:
#                 rec = self.env['vb.cash_tran'].search([('id','=',id.recordid)])
#                 bname=''
#                 bnum=''
#                 bank_rec = self.env['vb.customer_bank'].search([('customer_id','=',rec.acct_id.customer_id.id)])
#                 for i in bank_rec:
#                     bname = str(i.bank_id.name)
#                     bnum = str(i.bank_acct_no)
#                     break
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"CWON",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")) or '',
#                                             "tranId":str(rec.tran_refno) or '',
#                                             "acctno":str(rec.acct_id.acct_no) or '',
#                                             "tranAmount": 'MYR '+"{0:,.2f}".format(abs(rec.tran_amt)) or '',
#                                             "Rdate":str(rec.tran_date) or '',
#                                             "bName":bname or '',
#                                             "bNo":bnum or '',
#                                             },
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cdstrfout1_id:
#             for id in get_cdstrfout1_id:
#                 rec = self.env['vb.asset_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"Ct4",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")),
#                                             "faccNo":str(rec.acct_id.acct_no) or '',
#                                             "faccName":str(rec.acct_id.name) or '',
#                                             "taccno":str(rec.to_cds_no) or '',
#                                             "taccName":str(rec.to_cds_name) or '',
# #                                             "stockCode":str(rec.acct_id.acct_class_id.code) or '',
# #                                             "stockName":str(rec.acct_id.acct_class_id.name) or '',
#                                             "stockCode":str(rec.asset_id.asset_code) or '',
#                                             "stockName":str(rec.asset_id.name) or '',
#                                             "stockQty":str(int(rec.tran_qty)) or '',
#                                             'ref_no':str(rec.tran_refno) or '',
#                                             'reason_code':str(rec.cds_trf_reason)},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})
#         if get_cdstrfout2_id:
#             for id in get_cdstrfout2_id:
#                 rec = self.env['vb.asset_tran'].search([('id','=',id.recordid)])
#                 if rec:
#                     mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#                     if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                         id.write({'proc_text':'Error in MQ server, system params are not specified'})
#                     else:
#                         channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                         data= {
#                             'acctId':rec.acct_id.id,
#                             'custId':rec.acct_id.customer_id.id,
#                             "inboxFlag" : "True",
#                            "publisherApp":"backoffice",
#                            "templateCode":"Ct3",
#                            "messageParams":{"date":str( d2(d1).strftime("%d-%m-%Y")) or '',
#                                             "faccNo":str(rec.acct_id.acct_no) or '',
#                                             "faccName":str(rec.acct_id.name) or '',
#                                             "taccno":str(rec.to_cds_no) or '',
#                                             "taccName":str(rec.to_cds_name) or '',
# #                                             "stockCode":str(rec.acct_id.acct_class_id.code) or '',
# #                                             "stockName":str(rec.acct_id.acct_class_id.name) or '',
#                                             "stockCode":str(rec.asset_id.asset_code) or '',
#                                             "stockName":str(rec.asset_id.name) or '',
#                                             "stockQty":str(int(rec.tran_qty)) or '',
#                                             'request_date':rec.tran_date or '',
#                                             'ref_no':str(rec.tran_refno) or '',
#                                             'reason_code':str(rec.cds_trf_reason)},
#                            "recipients":[{
#                                          "recipientNo":"1",
#                                          "recipientType":"normal",
#                                          "name": id.parm2 or '',
#                                          "email": id.parm1 or '',
#                                          "title": id.parm3 or '',
#                                         }]
#                            }
#                         data_json=json.dumps(data)
#                         channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                         id.update({'state':'Processed','proc_time': d2(d1).strftime("%Y-%m-%d %H:%M:%S")})

    @api.model
    def call_wbsvc_copy1(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','CopyDB')])
        url =   "http://"+str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if (url_rec.parm1 or url_rec.parm2)== False:
            raise Warning('Please provide the params in system parameters code CopyDB')
        else:
            data = dumps({})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,headers=headers,data=data)

    @api.model
    def call_wbsvc_copy2(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','CopyDB')])
        url =   "http://"+str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm6)
        if (url_rec.parm1 or url_rec.parm2)== False:
            raise Warning('Please provide the params in system parameters code CopyDB')
        else:
            data = dumps({})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,headers=headers,data=data)

    @api.multi
    def incomplete_document(self):
        if bool(self.document_ids) == True:
            document = ''
            for doc in self .document_ids:
                if doc.state == 'Rejected':
                    document+= doc.name.split('_')[1]+','
            document = document[:-1]
            message = 'Bank Statement, ' if 'BANKSTMT' in document else ''
            message += 'NRIC front, ' if 'NRIC1' in document else ''
            message += 'NRIC back, ' if 'NRIC2' in document else ''
            message += 'Payslip, ' if 'PAYSLIP' in document else ''
            if bool(message) != True:
                message = 'There is NO document got rejected'
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','incomplete_account_document_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':"Incomplete documents",
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context':{'default_message':message[:-2] if 'NO' not in message else message},
                    }
        else:
            raise Warning("There is no documents related to this account.")

     # Method for Archive button pop up
    @api.multi
    def get_archive_record(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','archive_record_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Archive records',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }


class customer_acct_bal(models.Model):
    _inherit="vb.customer_acct_bal"

    pickup_limit = fields.Float(string='Pickup limit',digits=(15,2),default=0)
    pickup_limit_asat_date = fields.Datetime(string='Pickup limit as at date')

    @api.multi
    def trade_list_buy(self):
        try:
            tree_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
        except ValueError:
            tree_id = False
        return{
               'name': "Transactions Trade List",
               'view_type': 'form',
               'view_mode':"[tree]",
               'view_id': False,
               'res_model': 'vb.trade_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_id, 'tree')],
               'domain': [('acct_id','=',self.acct_id.id),('state','=','Posted'),('balance_amt', '!=', 0),('tran_type', '=', 'Buy')],
               'target':'new',
               }

    @api.multi
    def trade_list_sell(self):
        try:
            tree_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
        except ValueError:
            tree_id = False
        return{
               'name': "Transactions Trade List",
               'view_type': 'form',
               'view_mode':"[tree]",
               'view_id': False,
               'res_model': 'vb.trade_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_id, 'tree')],
               'domain': [('acct_id','=',self.acct_id.id),('state','=','Posted'),('balance_amt', '!=', 0),('tran_type', '=', 'Sell')],
               'target':'new',
               }

    #Pop-up tree view to list down the trades not in SEll or BUY.
    @api.multi
    def trade_list_other(self):
        try:
            tree_id = self.env['ir.model.data'].get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
        except ValueError:
            tree_id = False
        return{
                'name': "Transactions Trade Others List",
                'view_type': 'form',
                'view_mode': "[tree]",
                'view_id': False,
                'res_model': 'vb.trade_tran',
                'type': 'ir.actions.act_window',
                'views': [(tree_id, 'tree')],
                'domain': [('acct_id', '=', self.acct_id.id),('state', '=', 'Posted'),('balance_amt', '!=', 0),('tran_type', '!=', 'Sell'),('tran_type', '!=', 'Buy')],
                'context': {'enable_date_range_bar':1, 'search_fields': 'date_posted', 'group_by': 'tran_type'},
                'target': 'new',
                }

    #Method to get account cash details
    @api.multi
    def cash_tran_currentM(self):
        try:
            tree_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_cash_tran_tree1')[1]
        except ValueError:
            tree_id = False
        return{
               'name': "Transactions Cash Details",
               'view_type': 'form',
               'view_mode':"[tree]",
               'view_id': False,
               'res_model': 'vb.cash_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_id, 'tree')],
               'domain': [('acct_id','=', self.acct_id.id),('state','in',[ 'New','Posted']),('tran_date','>=',str(date.today().replace(day=1)))],
               'target':'new',
               }

    # Method to call form view to realise interest amount in trade (Added 21 Sept. 2017)
    @api.multi
    def get_realised_int_amt(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','realise_interest_amount_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Realise interest amount',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def get_details_withdraw_contra(self):
        rec = self.env['vb.customer_acct_bal'].search([('id','=',self.id),('acct_id.acct_type','=','Contra')])
        if rec:
                #Cash pending out
                if rec.cash_pending_out > 0.0:
                    default_batch_no = "-" + "{0:,.2f}".format(rec.cash_pending_out)
                elif rec.cash_pending_out < 0.0:
                    default_batch_no = "{0:,.2f}".format(rec.cash_pending_out)
                else:
                    default_batch_no = "0.00"
                # Contra loss
                if rec.trade_os_ctra_loss_amt > 0.0:
                    default_start_account = "-" + "{0:,.2f}".format(rec.trade_os_ctra_loss_amt)
                elif rec.trade_os_ctra_loss_amt < 0.0:
                    default_start_account = "{0:,.2f}".format(rec.trade_os_ctra_loss_amt)
                else:
                    default_start_account = "0.00"

                # Potential loss
                if rec.potential_loss_amt2 > 0.0:
                    default_start_customer = "-" + "{0:,.2f}".format(rec.potential_loss_amt2)
                elif rec.potential_loss_amt2 < 0.0:
                    default_start_customer = "{0:,.2f}".format(rec.potential_loss_amt2)
                else:
                    default_start_customer = "0.00"

                # trade pickup amount
                if rec.trade_pickup_amt > 0.0:
                    default_tran_type = "-" + "{0:,.2f}".format(rec.trade_pickup_amt)
                elif rec.trade_pickup_amt < 0.0:
                    default_tran_type = "{0:,.2f}".format(rec.trade_pickup_amt)
                else:
                    default_tran_type = "0.00"

                # net debit sales
                if rec.trade_os_dbsale_amt > 0.0:
                    default_file_name = "-" + "{0:,.2f}".format(rec.trade_os_dbsale_amt)
                elif rec.trade_pickup_amt < 0.0:
                    default_file_name = "{0:,.2f}".format(rec.trade_os_dbsale_amt)
                else:
                    default_file_name = "0.00"

                # Total debt
                end_customer = (rec.cash_available_bal + rec.cash_pending_out ) - (rec.trade_os_ctra_loss_amt +rec.potential_loss_amt2 +
                                 rec.trade_pickup_amt +rec.unrlsd_int_amt +rec.trade_os_others_amt
                                  +rec.trade_os_dbsale_amt)
                x = ( rec.cash_pending_out + rec.trade_os_ctra_loss_amt +
                                                         rec.potential_loss_amt2 + rec.trade_pickup_amt +
                                                         rec.unrlsd_int_amt +rec.trade_os_others_amt +
                                                            rec.trade_os_dbsale_amt)

                if end_customer < 0.0 and '-' not in str(end_customer):
                    default_end_customer = "-" + "{0:,.2f}".format(end_customer)
                elif end_customer < 0.0 and '-' in str(end_customer):
                     default_end_customer = "{0:,.2f}".format(end_customer)
                elif end_customer > 0.0:
                    default_end_customer = "{0:,.2f}".format(end_customer)
                else:
                    default_end_customer = "0.00"

                # Unrealized debit intest
                if rec.unrlsd_int_amt > 0.0:
                    default_start_code = "-" + "{0:,.2f}".format(rec.unrlsd_int_amt)
                elif rec.unrlsd_int_amt < 0.0:
                    default_start_code = "{0:,.2f}".format(rec.unrlsd_int_amt)
                else:
                    default_start_code = "0.00"

                # Misc charges
                if rec.trade_os_others_amt > 0.0:
                    default_end_code = "-" + "{0:,.2f}".format(rec.trade_os_others_amt)
                elif rec.trade_os_others_amt < 0.0:
                    default_end_code = "{0:,.2f}".format(rec.trade_os_others_amt)
                else:
                    default_end_code = "0.00"

                # Utilised Collateral
                if (rec.util_coll_amt1 + rec.util_coll_amt2a) != 0.0:
                    defual_unrlsd_contra = (rec.util_coll_amt1 + rec.util_coll_amt2a)
                else:
                    defual_unrlsd_contra = 0.0

                # Utilized collateral value for the open buy order
                netcollateralvalue  = (rec.paid_share_amt-(rec.util_coll_amt1+rec.util_coll_amt2a))
                if end_customer > 0 and netcollateralvalue > 0:
                    default_estimated_cash_withdraw = default_end_customer
                elif end_customer > 0 and netcollateralvalue < 0 :
                    default_estimated_cash_withdraw = (end_customer- abs(rec.paid_share_amt-(rec.util_coll_amt1+rec.util_coll_amt2a)))
                else:
                    default_estimated_cash_withdraw = "0.00"

                if rec.util_coll_amt2 > 0.0:
                    default_end_account = "-" + "{0:,.2f}".format(rec.util_coll_amt2)
                elif rec.util_coll_amt2 < 0.0:
                    default_end_account = "{0:,.2f}".format(rec.util_coll_amt2)
                else:
                    default_end_account = "0.00"

                if rec.util_coll_amt1 > 0 or rec.util_coll_amt2a > 0 or (rec.trade_os_ctra_loss_amt + rec.trade_os_others_amt +rec.unrlsd_int_amt + rec.trade_os_dbsale_amt + rec.potential_loss_amt)>0:
                    default_trading_cost = "{0:,.2f}".format(-(float(default_estimated_cash_withdraw.replace(',',''))*0.005)) if type(default_estimated_cash_withdraw) == str else "{0:,.2f}".format(-(float(default_estimated_cash_withdraw)*0.005))
                else:
                    default_trading_cost = '0.00'
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','view_vb_customer_acct_bal_withdraw_form')[1]

                except ValueError:
                    form_id = False
                return {
                            'name':'Withdraw limit details (CONTRA)',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(form_id, 'form')],
                            'view_id': form_id,
                            'target': 'new',
                            'context': {'default_start_refno':"{0:,.2f}".format(rec.cash_available_bal) or '0.0',
                                        'default_batch_no':default_batch_no,
                                        'default_start_account': default_start_account,
                                        'default_start_customer': default_start_customer,
                                        'default_tran_type':default_tran_type,
                                        'default_start_code': default_start_code,
                                        'default_end_code': default_end_code,
                                        'default_file_name':default_file_name,
                                        'default_end_customer': default_end_customer,
                                        'default_end_refno': "{0:,.2f}".format(rec.paid_share_amt),
                                        'default_unrlsd_contra': -defual_unrlsd_contra,
                                        'default_end_account': "{0:,.2f}".format((rec.paid_share_amt-(rec.util_coll_amt1+rec.util_coll_amt2a))),
                                        'default_estimated_cash_withdraw':default_estimated_cash_withdraw,
                                        'default_trading_cost':default_trading_cost,
                                        'default_message':"{0:,.2f}".format(rec.withdraw_limit) or '0.0'},

                        }
        else:
            raise Warning("Account type is not Contra")

    @api.multi
    def get_details_withdraw_cash(self):
        rec = self.env['vb.customer_acct_bal'].search([('id','=',self.id),('acct_id.acct_type','=','Cash')])
        if rec:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','view_vb_customer_acct_bal_withdraw_cash_form')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Trade limit details (CASH)',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_start_refno':"{0:,.2f}".format(rec.cash_available_bal) or '0.0','default_batch_no':"{0:,.2f}".format(rec.cash_pending_out) or '0.0','default_file_name':"{0:,.2f}".format(rec.trade_pending_out)  or '0.0',
                                    'default_start_account':"{0:,.2f}".format(rec.trade_os_long_amt )  or '0.0','default_end_code':"{0:,.2f}".format(rec.trade_os_ctra_loss_amt) or '0.0',
                                    'default_end_refno':"{0:,.2f}".format(rec.uplifted_order_buy_amt)  or '0.0',
                                    'default_unrlsd_contra':"{0:,.2f}".format(rec.uplifted_matched_buy_amt) or '0.0','default_message':"{0:,.2f}".format(rec.withdraw_limit) or '0.0'},

                    }
        else:
            raise Warning("Account type is not Contra")

    @api.multi
    def get_details_pickup_limit(self):
        rec = self.env['vb.customer_acct_bal'].search([('id','=',self.id),('acct_id.acct_type','=','Contra')])
        if rec:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_pickup_limit_computation_view')[1]
            return {
                'name':'Pickup limit details (CONTRA)',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'views': [(form_id, 'form')],
                'view_id': form_id,
                'target': 'new',
                'context': {
                    'default_start_refno': "{0:,.2f}".format(rec.cash_available_bal),
                    'default_batch_no': "{0:,.2f}".format(rec.trade_os_ctra_loss_amt),
                    'default_file_name': "{0:,.2f}".format(rec.unrlsd_int_amt),
                    'default_end_customer': "{0:,.2f}".format(rec.trade_os_others_loc),
                    'default_start_account': "{0:,.2f}".format(rec.cash_pending_out),
                    'default_start_code': "{0:,.2f}".format(rec.trade_os_dbsale_amt),
                    'default_end_code': "{0:,.2f}".format(rec.trade_pickup_amt),
                    'default_start_customer': "{0:,.2f}".format(rec.pickup_limit),
                }
            }
        else:
            raise Warning("Account type is not Contra")

    @api.multi
    def get_details_trade_limit(self):
        rec = self.env['vb.customer_acct_bal'].search([('id','=',self.id),('acct_id.acct_type','=','Contra')])
        if rec:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_trade_limit_computation_view')[1]

                # Total debt
                end_customer = rec.trade_os_ctra_loss_amt + rec.unrlsd_int_amt + rec.trade_os_others_amt
                if end_customer > 0.0:
                    default_end_customer = "-" + "{0:,.2f}".format(end_customer)
                elif end_customer < 0.0:
                    default_end_customer = "{0:,.2f}".format(end_customer)
                else:
                    default_end_customer = "0.0"

                # Contra loss
                if rec.trade_os_ctra_loss_amt > 0.0:
                    default_start_account = "-" + "{0:,.2f}".format(rec.trade_os_ctra_loss_amt)
                elif rec.trade_os_ctra_loss_amt < 0.0:
                    default_start_account = "{0:,.2f}".format(rec.trade_os_ctra_loss_amt)
                else:
                    default_start_account = "0.0"

                # Unrealized debit intest
                if rec.unrlsd_int_amt > 0.0:
                    default_start_code = "-" + "{0:,.2f}".format(rec.unrlsd_int_amt)
                elif rec.unrlsd_int_amt < 0.0:
                    default_start_code = "{0:,.2f}".format(rec.unrlsd_int_amt)
                else:
                    default_start_code = "0.0"

                # Misc charges
                if rec.trade_os_others_amt > 0.0:
                    default_end_code = "-" + "{0:,.2f}".format(rec.trade_os_others_amt)
                elif rec.trade_os_others_amt < 0.0:
                    default_end_code = "{0:,.2f}".format(rec.trade_os_others_amt)
                else:
                    default_end_code = "0.0"

                # Potential loss
                if rec.potential_loss_amt > 0.0:
                    default_start_customer = "-" + "{0:,.2f}".format(rec.potential_loss_amt)
                elif rec.potential_loss_amt < 0.0:
                    default_start_customer = "{0:,.2f}".format(rec.potential_loss_amt)
                else:
                    default_start_customer = "0.0"

                # Utilized collateral value
                end_refno = rec.util_coll_amt1 + rec.util_coll_amt2
                if end_refno > 0.0:
                    default_end_refno = "-" + "{0:,.2f}".format(end_refno)
                elif end_refno < 0.0:
                    default_end_refno = "{0:,.2f}".format(end_refno)
                else:
                    default_end_refno = "0.0"

                # Utilized collateral value for the matched buy order
                if rec.util_coll_amt1 > 0.0:
                    defual_unrlsd_contra = "-" + "{0:,.2f}".format(rec.util_coll_amt2)
                elif rec.util_coll_amt1 < 0.0:
                    defual_unrlsd_contra = "{0:,.2f}".format(rec.util_coll_amt2)
                else:
                    defual_unrlsd_contra = "0.0"

                # Utilized collateral value for the open buy order
                if rec.util_coll_amt2 > 0.0:
                    default_end_account = "-" + "{0:,.2f}".format(rec.util_coll_amt1)
                elif rec.util_coll_amt2 < 0.0:
                    default_end_account = "{0:,.2f}".format(rec.util_coll_amt1)
                else:
                    default_end_account = "0.0"

            except ValueError:
                form_id = False
            return {
                        'name':'Trade limit details (CONTRA)',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_start_refno':"{0:,.2f}".format(rec.cash_available_bal) or '0.0',
                                    'default_batch_no':"{0:,.2f}".format(rec.paid_share_amt) or '0.0',
                                    'default_file_name':"{0:,.2f}".format(rec.cash_pending_out)  or '0.0',
                                    'default_end_customer': default_end_customer,
                                    'default_start_account': default_start_account,
                                    'default_start_code': default_start_code,
                                    'default_end_code': default_end_code,
                                    'default_start_customer': default_start_customer,
                                    'default_tran_type':"{0:,.2f}".format(rec.trade_pickup_amt) or '0.0',
                                    'default_end_refno': default_end_refno,
                                    'default_unrlsd_contra': rec.util_coll_amt2,
                                    'default_end_account': default_end_account,
                                    'default_message':"{0:,.2f}".format(rec.trade_limit) or '0.0'},

                    }
        else:
            raise Warning("Account type is not Contra")

    @api.multi
    def get_details_trade_limit_cash(self):
        rec = self.env['vb.customer_acct_bal'].search([('id','=',self.id),('acct_id.acct_type','=','Cash')])
        if rec:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_trade_limit_computation_cash_view')[1]
            except ValueError:
                form_id = False
            print (type(rec.uplifted_matched_buy_amt),type(rec.uplifted_order_buy_amt))
            print (type("{0:,.2f}".format(rec.uplifted_matched_buy_amt)),type("{0:,.2f}".format(rec.uplifted_order_buy_amt)))
            return {
                        'name':'Trade limit details (CASH)',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_start_refno':"{0:,.2f}".format(rec.cash_available_bal) or '0.0','default_batch_no':"{0:,.2f}".format(rec.cash_pending_out) or '0.0','default_file_name':"{0:,.2f}".format(rec.trade_pending_out)  or '0.0',
                                    'default_end_customer':"{0:,.2f}".format(rec.trade_pending_in)  or '0.0',
                                    'default_start_account':"{0:,.2f}".format(rec.trade_os_long_amt )  or '0.0','default_start_code':"{0:,.2f}".format(rec.trade_os_short_amt) or '0.0','default_end_code':"{0:,.2f}".format(rec.trade_os_ctra_loss_amt) or '0.0',
                                    'default_start_customer':"{0:,.2f}".format(rec.trade_os_ctra_gain_amt) or '0.0','default_end_refno':"{0:,.2f}".format(rec.uplifted_order_buy_amt)  or '0.0',
                                    'default_unrlsd_contra':(rec.uplifted_matched_buy_amt) or '0.0','default_end_account':"{0:,.2f}".format(rec.util_coll_amt2) or '0.0','default_message':"{0:,.2f}".format(rec.trade_limit) or '0.0'},

                    }
        else:
            raise Warning("Account type is not Cash")

    # Method to call form view to realise interest amount in cash (Added 21 Sept. 2017)
    @api.multi
    def get_realised_int_amt_cash(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','realise_interest_amount_cash_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Realise cash interest amount',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

class customer_acct_riskcheck(models.Model):
    _inherit = 'vb.customer_acct_riskcheck'

    @api.multi
    def send_email_approvers(self):
        if self.approver1 or self.approver2 or self.approver3 or self.approver4:
            if str(self.approver1) == str(self._uid) or str(self.approver2) == str(self._uid) or str(self.approver3) == str(self._uid) or str(self.approver4) == str(self._uid):
                final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'confirm_approvers_wizard_view')[1]
                message = str('You are one of the approvers for '+self.name+', you are able to do changes')
                return {
                    'name':'Message sent to the approvers.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }


            elif (self.approver1 or self.approver2 or self.approver3 or self.approver4)== self._uid:
                final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'confirm_approvers_wizard_view')[1]
                message = str('you are one of the approver for '+self.name)
                return {
                    'name':'Message sent to the approvers.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }
            else:
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'send_email_approvers_wizard_view')[1]
                except ValueError:
                    form_id = False
                return{
                       'name': "Send email to approvers",
                       'view_type': 'form',
                       'view_mode':"form",
                       'view_id': form_id,
                       'res_model': 'vb.common_wizard',
                       'type': 'ir.actions.act_window',
                       'target':'new'
                       }
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'confirm_approvers_wizard_view')[1]
            message = str('No approvers for '+self.name)
            return {
                'name':'Message sent to the approvers.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

class customer_change_rqs(models.Model):
    _inherit = 'vb.customer_change_rqs'

    # Approve customer change request
    @api.multi
    def get_approve_customer_change_rqs(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','approve_customer_change_rqs_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Approve changes',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

     # Cancel customer change request
    @api.multi
    def get_reject_customer_change_rqs(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','reject_customer_change_rqs_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Reject changes',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

# class login_override(main):
#     @http.route()
#     def web_login(self, redirect=None, *args, **kw):
#         r = super(login_override, self).web_login(redirect=redirect, *args, **kw)
#         dbname = request.session.db
#         registry = openerp.modules.registry.Registry(dbname)
#
#         with registry.cursor() as cr:
#             query = "Select sp_dashbrd_counter(%s)"%request.uid
#             cr.execute(query)
#             result = cr.fetchone()
#         return r

class whitelist(models.Model):
    _inherit = 'vb.bankacct_whitelist'

    @api.multi
    def approve_whitelist(self):
        if self.state == 'New':
            if self._uid != self.write_uid.id:
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_vb_whitelist_approve_view')[1]
                except ValueError:
                    form_id = False
                return {
                            'name':'Reject changes',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(form_id, 'form')],
                            'view_id': form_id,
                            'target': 'new',
                        }
            else:
                raise Warning('You are not allow to approve this.')
        else:
            raise Warning("The record is not in New status")

    @api.multi
    def reject_whitelist(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_vb_whitelist_reject_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Reject changes',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

    @api.multi
    def delete_whitelist(self):
        if self.state != 'Delete':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','wizard_vb_whitelist_delete_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Delete changes',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            raise Warning("The record is already on delete status")

class customer_acct_cedd(models.Model):
    _inherit = 'vb.customer_acct_cedd'

    @api.multi
    def edd_completed(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','edd_pending_check_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'EDD Completed',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
    @api.multi
    def edd_reviewed(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','edd_pending_check_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'EDD Reviewed',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
    @api.multi
    def approve_cedd(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','edd_approve_dashobard_wizard_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Approve EDD',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }

class customer_acct_bal(models.Model):
    _inherit = 'vb.customer_acct_bal'

    @api.multi
    def sync_acct_bal(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','sync_account_balance_wizard')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Approve EDD',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
