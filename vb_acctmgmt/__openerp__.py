# -*- coding: utf-8 -*-

{
    'name': 'VB Account Management',
    'version': '1.2',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Account Management Module
======================================================

This module contains the function for managing the customer as well as their underlying accounts.

    """,
    'data': [
        'wizards/wizards_acctmgmt.xml',
        'views/views_acctmgmt.xml',
        'views/views_customer.xml',
        'views/views_reports.xml',
        'views/actions_acctmgmt.xml',
        'views/menus_acctmgmt.xml',
        'views/qweb_reports_templates.xml',
            ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
