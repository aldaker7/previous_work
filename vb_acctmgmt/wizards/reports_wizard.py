from openerp import models, api
from datetime import datetime, timedelta
from openerp.exceptions import Warning, ValidationError
from datetime import date
from time import gmtime, strftime
from dateutil import parser
import base64

# import json
# from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
# import os
# from datetime import timedelta
# import PyPDF2


class reports_wizard(models.TransientModel):
    _inherit = 'vb.common_wizard'

    @api.multi
    def get_list_of_accounts_opened_report(self):
        '''This method is used to print List of Accounts Opened'''
        '''_defaults = {
        'self.start_date1': date.today().strftime('%Y-%m-%d'),
                    }'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products
                  }
        print "----------------------------",list_of_products
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_funds_deposit_report(self):
        '''This method is used to print Funds Deposited'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products,
                  'AcctNo_List' : list_of_accounts
                  }
        self._check_date_range()
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)  
        # filename = report_name + '.pdf'
        # return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_funds_withdrawal_report(self):
        '''This method is used to print Funds Withdrawal'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))


        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products,
                  'AcctNo_List' : list_of_accounts
                  }
        self._check_date_range()
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_cash_adjustment_transactions_report(self):
        '''This method is used to print Cash Adjustments'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_noof_cash_deposit_transactions_report(self):
        '''This method is used to print No Of Cash Deposit Transactions Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products                  
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)        

    @api.multi
    def get_daily_misc_charges_transactions_report(self):
        '''This method is used to print Daily Misc Credit/Charges Transactions Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
            
        list_of_chargecodes= []    
        for rec_chargecode in self.charge_ids:
            print"-----------------",str(rec_chargecode.name)
            list_of_chargecodes.append(str(rec_chargecode.name))
            
       
        if self.report_tran_type == False:
            raise ValidationError ('Please select a Transaction Type')
        elif self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products,
                  'Charges_List' : list_of_chargecodes
                  }
        if self.report_tran_type == 'Trade Transactions':
            report_name = 'DailyMiscChargesTradeTransactions'
        else:
            report_name = 'DailyMiscChargesCashTransactions'

        #report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_interest_report(self):
        '''This method is used to print Daily Interest Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'FromDate': self.start_date1,
                  'ToDate' : self.end_date1,
                  'Products_List' : list_of_products
                  }

        report_name = self._context.get('reportname')


        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_customer_audit_log_report(self):
        '''This method is used to print Customer Audit Log  Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in Date')
#         if self.auditlog_table== False:
#             raise ValidationError ('Please select a Table')
        list_of_names = []
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                  'PrintedBy': str(self.env.user.name),
                  'DateGiven': self.start_date1,
#                 'Table' : self.auditlog_table
                  'Customers_List': list_of_names
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_summary_cash_balance_report(self):
        '''This method is used to print Summary of Customer Cash Balance'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))


        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'PrintedBy': str(self.env.user.name),
                   'DateGiven': self.start_date1,
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts

                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_summary_portfolio_holding_balance_report(self):
        '''This method is used to print Summary of Portfolio Holding Balance'''

        list_of_customers = []
        for rec in self.customer_ids:
            list_of_customers.append(str(rec.name))
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'PrintedBy': str(self.env.user.name),
                   'Customers_List': list_of_customers,
                   'Products_List' : list_of_products

                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'

        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_summary_of_account_submission_ramci_report(self):
        '''This method is used to print Summary of Account Submission RAMCI(Monthly)'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)


    @api.multi
    def get_accounts_opened_report(self):
        '''This method is used to print List of Accounts Opened'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')
        elif self.date_type_acreport == False:
            raise ValidationError ('Please select data type')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'DateType': str(self.date_type_acreport),
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_inter_account_cash_transfers_report(self):
        '''This method is used to print List of Inter Account Cash Transfers'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_contra_settlement_report(self):
        '''This method is used to print List of Inter Account Cash Transfers'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in To date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'ToDate' : self.start_date1,
                   'BatchNo': str(self.start_date1).split('-')[0]+str(self.start_date1).split('-')[1]+str(self.start_date1).split('-')[2],
                   'PrintedBy': str(self.env.user.name)
                  }

        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_client_trade_position_report(self):
        '''This method is used to print List of Client Trade Position Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'AcctNo_List' : list_of_accounts,
                   'Products_List' : list_of_products,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_outstanding_buy_due_settlement_report(self):
        '''This method is used to print Outstanding Buy Due Settlement Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in Date')

        if len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'DateGiven': self.start_date1,
                   'Products_List' : list_of_products,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_customers_outstanding_report(self):
        '''This method is used to print Customers Outstanding Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))



        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {

                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts,

                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_print_cedd_report(self):
        '''This method is used to print CEDD Report'''
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.id))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()

        params = {
            'AccountId': list_of_accounts,
            'PrintedBy': str(self.env.user.name)
        }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_print_assetlist_report(self):

        list_of_asset_class = []
        for rec in self.asset_class_ids:
            list_of_asset_class.append(str(rec.name))

        list_of_index = []
        for rec in self.index_ids:
            list_of_index.append(str(rec.name))

        share_multiplier = ''
        if self.share_multiplier:
            try:
                isnumber = float(self.share_multiplier)
                share_multiplier = str(isnumber)
            except ValueError:
                raise ValidationError ('Please fill valid number for Share multiplier')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()

        params = {
            'AssetClass': list_of_asset_class,
            'IndexName': list_of_index,
            'MultiplierList': share_multiplier,
            'MarketBoard': '' if self.market_board == False else str(self.market_board),
            'PrintedBy': str(self.env.user.name)
        }
        report_name = self._context.get('reportname')
        if self.state_reporttype == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state_reporttype == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)    

    @api.multi
    def get_contra_statement_report(self):
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()

        if not self.acct_id:
            raise ValidationError ('Please select an Account')

        params = {
            'AccountId': self.acct_id.id,
            'DateGiven': self.start_date1,
            'PrintedBy': str(self.env.user.name)
        }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_collateralised_accounts_report(self):
        '''This method is used to print get Daily Collateralised Accounts Report'''
        list_of_names = []
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError('Please fill in Date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
            'PrintedBy': str(self.env.user.name),
            'ToDate': self.start_date1,
            'Customers_List': list_of_names
        }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_contra_account_summary_report(self):
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
            'accountNo': self.acct_id.id,
            'ToDate': self.start_date1,
            'PrintedBy': str(self.env.user.name)
        }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # If end date entered, ensure it is not earlier that start date
    @api.multi
    def _check_date_range(self):
        for rec in self:
            if rec.start_date1:
                date_start = datetime.strptime(rec.start_date1,'%Y-%m-%d')
                date_end = datetime.strptime(rec.end_date1,'%Y-%m-%d')
                if date_end < date_start:
                    raise models.ValidationError('Entered To  date cannot be earlier than From date!')



    @api.multi
    def get_customer_info(self):
        customer_recs = self.env['vb.customer'].search([('create_date','>=',str(self.start_date1)),('create_date','<=',str(self.end_date1))])
        print 'customer_recs',customer_recs
        wizard_data  = self.read([])[0]
        res = {
                'ids': customer_recs.ids or [],
                'model':'vb.customer',
                'form': wizard_data
               }
        return self.env['report'].get_action(self, 'vb_acctmgmt.customer_list_template', data=res)

    @api.multi
    def get_accounts_info(self):

        customer_acct_recs = self.env['vb.customer_acct'].search([('date_activated','>=',str(self.start_date1)),('date_activated','<=',str(self.end_date1))])
        wizard_data  = self.read([])[0]
        res = {
                'ids': customer_acct_recs.ids or [],
                'model':'vb.customer_acct',
                'form': wizard_data
               }
        return self.env['report'].get_action(self, 'vb_acctmgmt.accounts_list_template', data=res)

    @api.multi
    def get_incomplete_accounts_opening_report(self):
        '''This method is used to print List of Incomplete  Accounts Opening Report(Date Created)'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))



        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)
    
    @api.multi
    def get_incomplete_accounts_opening_dateopened_report(self):
        '''This method is used to print List of Incomplete  Accounts Opening Report(Date Opened)'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))



        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)

    @api.multi
    def get_unsuccessful_fund_transfer_report(self):
        '''This method is used to print Unsuccessful Fund Transfer Exception Report'''

        # customer ids
        list_of_names = []
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))
        # convert from utc to current datetime
        from_date = parser.parse(self.start_datetime) + timedelta(hours=8)
        to_date = parser.parse(self.end_datetime) + timedelta(hours=8)
        # product ids
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if from_date == False:
            raise ValidationError ('Please fill in From date')
        elif to_date == False:
            raise ValidationError ('Please fill in To date')
        elif to_date < from_date:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': str(from_date),
                   'ToDate': str(to_date),
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'CashStatus_List': self.cash_deposit_status,
                   'Customers_List': list_of_names
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)

    @api.multi
    def get_unsuccessful_withdrawal_report(self):
        '''This method is used to print  Withdrawal  Exception Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))


        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_possible_fail_settlement_report(self):
        '''This method is used to print Possible Fail settlement Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        if len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')
        elif self.start_date1 == False:
            raise ValidationError ('Please fill in Settlement date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'SettlementDate': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)


    @api.multi
    def get_orders_cancel_replace_before_match_report(self):
        '''This method is used to print Orders Cancel/Replace Before Match Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_order_book_report(self):
        '''This method is used to print Daily Order Book Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        
        asset_list = []
        for rec in self.asset_ids:
            asset_list.append(str(rec.asset_code))
        
        if len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')
        elif self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.order_status == False:
            raise ValidationError ('Please select an Order Status')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Order_Status':self.order_status,
                   'AssetCodeList': asset_list,
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_contra_report(self):
        '''This method is used to print Daily Contra Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.order_status == False:
            raise ValidationError ('Please select an Order Status')
        elif self.date_type2 == False:
            raise ValidationError ('Please select a Date type')

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts,
                   'DateType' : str(self.date_type2)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # Trade Statistics GST Summary Form
    # Trade Statistics GST Detail Form
    @api.multi
    def get_trade_statistics_GST_report(self):
        '''This method is used to print Daily Contra Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.order_status == False:
            raise ValidationError ('Please select an Order Status')

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # Risk management daily Form
    @api.multi
    def get_risk_management_daily_report(self):
        '''This method is used to print Risk Management Daily'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in To date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'ToDate': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                  }
        report_name = self._context.get('reportname')

        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)

    # Method to print Old ForceSelling Report
    @api.multi
    def get_force_selling_old_report(self):
        if self.start_date1 == False:
            raise ValidationError('Please fill in Batch date')
        forceselling_obj = str(self.forcesell_status)
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'ToDate' : self.start_date1,
                   'ForceSell_Status': forceselling_obj,
                   'PrintedBy': str(self.env.user.name),
                 }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # list paid share force Sell Form
    @api.multi
    def get_list_paid_share_forceSell_report(self):
        '''This method is used to print list paid share force Sell'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
            
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))
            
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'ToDate': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
    
    # list Terms and Condition report
    @api.multi
    def get_list_terms_condition_report(self):
        '''This method is used to print list Terms and Condition report'''
            
        if self.version_list == False:
            raise ValidationError ('Please select T&C Version')
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        
        params = {
                   'ToDate': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                   'termsConditionsVersion' : self.version_list
                  }
                
        report_name = self._context.get('reportname')
        
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
    
    # list Outstanding Contra Loss For Settlement Form
    @api.multi
    def get_list_outstanding_Contra_Loss_For_Settlement_report(self):
        '''This method is used to print list outstanding Contra Loss For Settlement Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
            
        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))
            
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'ToDate': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)    
        

    @api.multi
    def get_CDS_share_deposit_withdrawal_report(self):
        '''This method is used to print List Of CDS share deposit/withdrawal Report'''

        # convert from utc to current datetime
        from_date = parser.parse(self.start_datetime) + timedelta(hours=8)
        to_date = parser.parse(self.end_datetime) + timedelta(hours=8)

        if from_date == False:
            raise ValidationError ('Please fill in From date')
        elif to_date == False:
            raise ValidationError ('Please fill in To date')
        elif to_date < from_date:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.CDS_tran_type == False:
            raise ValidationError ('Please select a Transaction Type')


        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': from_date,
                   'ToDate': to_date,
                   'PrintedBy': str(self.env.user.name),
                   'TranType':self.CDS_tran_type,
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts,
                   'Status' : self.asset_tran_state
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_corp_action_transaction_report(self):
        '''This method is used to print List Of Corporate Action Transaction  Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.order_status == False:
            raise ValidationError ('Please select a Corporate Action')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                    'CorpAction':self.corp_action,
#                    'CorpAction_List' : list_of_corpAction,
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_kibb_corporate_action_dividend_pay_report(self):
        '''This method is used to print List Of Corporate Action Transaction Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.corp_action_status == False:
            raise ValidationError ('Please select a status')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': str(self.start_date1),
                   'ToDate': str(self.end_date1),
                   'CashStatus_List': str(self.corp_action_status),
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts,
                   'PrintedBy': str(self.env.user.name),
                  }
        report_name = self._context.get('reportname')

        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        
        # filename = report_name + '.pdf'
        # return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_net_settlement_report(self):
        '''This method is used to print Daily Net Settlement  Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From Due Date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To Due Date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To Due Date can not be earlier than From Due Date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)


    @api.multi
    def get_cds_fee_payable_report(self):
        '''This method is used to print List of CDS Fee Payable Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_account_status_report(self):
        '''This method is used to print Account Status Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.state1 == False:
            raise ValidationError ('Please select a Status')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'AccountStatus':self.state1,
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_customer_status_report(self):
        '''This method is used to print Customer Status Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        if self.state1 == False:
            raise ValidationError ('Please select a Status')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products,
                   'CustomerStatus':self.state1
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_gl_interface_report(self):
        '''This method is used to print GL Interface  Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in Batch date')
        elif self.start_date1 > datetime.now().strftime("%Y-%m-%d"):
            raise ValidationError ('Batch Date can not be earlier than Today')

        d=datetime.strptime(self.start_date1,'%Y-%m-%d')
        batchno=datetime.strftime(d, "%Y%m%d")

        # added product criteria 
        product= str(self.product_type.code).upper() if self.product_type else ''

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'BatchNo': batchno,
                   'ProductType': product,
                   'PrintedBy': str(self.env.user.name),
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_gl_summary_report(self):
        '''This method is used to print GL Summary  Report By GL No'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in Batch date')
        elif self.product_type.code == False:
            raise ValidationError ('Product selection need to be mandatory')

        d=datetime.strptime(self.start_date1,'%Y-%m-%d')
        batchno=datetime.strftime(d, "%Y%m%d")

        # added product criteria 
        product= str(self.product_type.code).upper()

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'DateGiven' : self.start_date1,
                   'BatchNo': batchno,
                   'ProductType': product,
                   'PrintedBy': str(self.env.user.name),

                  }
        report_name = 'GLSummaryByGLNo'
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

#   To export the G/L summary report for MYOB software
    @api.multi
    def get_gl_summary_csv(self):

        if self.product_type.code == False:
            raise ValidationError ('Product selection need to be mandatory')

        d=datetime.strptime(self.start_date1,'%Y-%m-%d')
        batchno=datetime.strftime(d, "%Y%m%d")

        # added product criteria 
        product= str(self.product_type.code).upper()

        glsumm_recs = self.env['vb.gl_interface_summ'].search([('batch','=',batchno + product)])

        if not glsumm_recs:
            raise Warning("No data available.")

        print_header = ("JournalNumber"+","+"JournalDate"+","+"Memo"+","+"Inclusive"+","+"AccountNumber"
        +","+"DebitExTaxAmount"+","+"DebitIncTaxAmount"+","+"CreditExTaxAmount"+","+"CreditIncTaxAmount"
        +","+"Job"+","+"TaxCode"+","+"NonTaxAmount"+","+"TaxAmount"+","+"ImportDutyAmount"+","+"CurrencyCode"
        +","+"ExchangeRate"+","+"AllocationMemo"+","+"Category")

        data = []

        for i in glsumm_recs:
            data.append([i.jrn_refno,i.jrn_date,i.memo,i.inclusive,"'"+i.glacct_no+"'",i.amt_db_extax,
                         i.amt_db,i.amt_cr_extax,i.amt_cr,i.batch,i.tax_code,0,0,0,i.currency_code,
                         0,0,i.ledger_type])

        count =0
        text1=''
        for i in data:
            for k in i:
                if k == False:
                    k = ''
                text1 += str(k)
                count +=1
                if count == 18:
                    text1+='\n'
                    count = 0
                else:
                    text1+=','

        final_text = print_header +'\n'+text1

        report_name = 'GLSummaryreport'
        filename = report_name + '.csv'

        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text),'file_name':filename})


        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }

#   To export the G/L summary report for MYOB software in text format
    @api.multi
    def get_gl_summary_text(self):

        if self.product_type.code == False:
            raise ValidationError ('Product selection need to be mandatory')

        d=datetime.strptime(self.start_date1,'%Y-%m-%d')
        batchno=datetime.strftime(d, "%Y%m%d")

        # added product criteria 
        product= str(self.product_type.code).upper()

        glsumm_recs = self.env['vb.gl_interface_summ'].search([('batch','=',batchno + product)])
        if not glsumm_recs:
            raise Warning("No data available.")

        print_header = ("JournalNumber"+"\t"+"JournalDate"+"\t"+"Memo"+"\t"+"Inclusive"+"\t"+"AccountNumber"
        +"\t"+"DebitExTaxAmount"+"\t"+"DebitIncTaxAmount"+"\t"+"CreditExTaxAmount"+"\t"+"CreditIncTaxAmount"
        +"\t"+"Job"+"\t"+"TaxCode"+"\t"+"NonTaxAmount"+"\t"+"TaxAmount"+"\t"+"ImportDutyAmount"+"\t"+"CurrencyCode"
        +"\t"+"ExchangeRate"+"\t"+"AllocationMemo"+"\t"+"Category")

        text1=""
        for i in glsumm_recs:
            
            text1 = text1
            
            journal_date = datetime.strptime(i.jrn_date, "%Y-%m-%d").strftime("%d/%m/%Y")
            
            text1 =  text1 + "\t" + str(journal_date)
            
            memo = 'GLInterface'+str(i.batch)
            
            text1 = text1 + "\t" + str(memo)

            text1 =  text1 + "\t"

            text1 =  text1 + "\t" + str(i.glacct_no)

            if not i.amt_db:
                text1 =  text1 + "\t" + '{}.{}'.format('0','00')
            else:
                text1 =  text1 + "\t" + "{0:.2f}".format(i.amt_db)

            if not i.amt_db:
                text1 = text1 + "\t" + '{}.{}'.format('0','00')
            else:
                text1 =  text1 + "\t" + "{0:.2f}".format(i.amt_db)

            if not i.amt_cr:
                text1 = text1 + "\t" + '{}.{}'.format('0','00')
            else:
                text1 =  text1 + "\t" + "{0:.2f}".format(i.amt_cr)

            if not i.amt_cr:
                text1 = text1 + "\t" + '{}.{}'.format('0','00')
            else:
                text1 =  text1 + "\t" + "{0:.2f}".format(i.amt_cr)

            text1 =  text1 + "\t" + "GLINTER"

            if not i.tax_code:
                text1 = text1 + "\t" + "N-T"
            else:
                text1 =  text1 + "\t" + str(i.tax_code)

            text1 =  text1 + "\t" + '{}.{}'.format('0','00')

            text1 =  text1 +  "\t" + '{}.{}'.format('0','00')

            text1 =  text1 + "\t" + '{}.{}'.format('0','00')

            text1 =  text1 + "\t" + str(i.currency_code)

            text1 =  text1 + "\t" + '{}.{}'.format('1','000000')

            text1 =  text1 + "\t" + str(i.jrn_desc)

            text1 =  text1 + "\t" + 'Trade'

            text1 = text1 + "\r\n"


        final_text = print_header +"\r\n"
        final_text1 =  final_text + text1

        report_name = 'GLSummaryreport'
        filename = report_name + '.txt'

        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text1),'file_name':filename})


        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }




    @api.multi
    def get_accounts_failed_riskcheck_report(self):
        '''This method is used to print List of Accounts failed Riskcheck Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()

        params = {

                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }

        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_business_done_report(self):
        '''This method is used to print Business Done Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))


        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'Products_List' : list_of_products,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_corp_action_subs_trans_report(self):
        '''This method is used to print Corporate Action Subscription Transactions'''
        
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        asset_list = []
        for rec in self.asset_ids:
            asset_list.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'fromDate': self.start_date1,
                   'toDate': self.end_date1,
                   'Products_List' : list_of_products,
                   'AssetNameList' : asset_list,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')

        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)

    # Subscription of Five Market Depth reports     
    @api.multi
    def get_subs_5_market_depth_report(self):
        '''This method is used to print Subscription of 5 Market Depth'''
        
        str_month = str(self.month)
        if self.month == False:
            raise ValidationError ('Please select a Month From')
        elif self.year == False:
            raise ValidationError ('Please select a Year From')
        elif len(str(self.month))==1:
            str_month = "0"+str(self.month)
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'month': str_month,
                   'year': str(self.year),
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # Subscription of Five Market Depth for Finance and Operations reports     
    @api.multi
    def get_subs_5_market_depth_finance_operation_report(self):
        '''This method is used to print Subscription of 5 Market Depth for Finance and Operations'''
        str_month = str(self.month)
        if self.month == False:
            raise ValidationError ('Please select a Month From')
        elif self.year == False:
            raise ValidationError ('Please select a Year From')
        elif len(str(self.month))==1:
            str_month = "0"+str(self.month)
            
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'month': str_month,
                   'year': str(self.year),
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # List of Corporate Action Entitlement Report     
    @api.multi
    def get_List_of_Corporate_Action_Entitlement(self):
        '''This method is used to print List of Corporate Action Entitlement Report'''
        #Product List
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        #corp action selections
        corp_action_obj = str(self.corp_action)
        #Asset List
        asset_list = []
        for rec in self.asset_ids:
            asset_list.append(str(rec.name))
        #Dates validations
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.corp_action == False:
            raise ValidationError ('Please select a Corporate Action')   
        #Japser Connection 
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        #Parms
        params = {
                   'Products_List' : list_of_products,
                   'CorpAction_List':corp_action_obj,  
                   'AssetNameList' : asset_list,
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name)
                  }
        #File Type
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)

    # List of Corporate Action subscribe Report - Added 23-May-2018 By Badi 
    @api.multi
    def get_List_of_Corporate_Action_Subscribe(self):
        '''This method is used to print List of Corporate Action Subscribe transaction Report'''
        #Product List
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        #corp action selections
        corp_action_obj = str(self.corp_action)
        #Asset List
        asset_list = []
        for rec in self.asset_ids:
            asset_list.append(str(rec.name))
        dateType= str(self.date_type) if self.date_type else 'Subscribed Date'
        #Dates validations
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From Date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To Date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif self.corp_action == False:
            raise ValidationError ('Please select a Corporate Action') 
        elif self.date_type==False:
            raise ValidationError ('Please select a Date Type')   
        #Japser Connection 
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        #Parms
        params = {
                   'Products_List' : list_of_products,
                   'CorpAction_List': corp_action_obj,  
                   'AssetNameList' : asset_list,
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'DateType': dateType
                  }
        #File Type
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)

    @api.multi
    def get_monthly_stamp_summary_report(self):
        '''This method is used to print Monthly Stamp Summary Report'''
        str_month = ''
        if self.month == False:
            raise ValidationError ('Please select a Month')
        elif self.year == False:
            raise ValidationError ('Please select a Year')

        if len(str(self.month))==1:
            str_month = "0"+str(self.month)
        else:
            str_month = str(self.month)

        str_monthyear = str_month+"/"+str(self.year)


        # objCompany_rec= self.env['res.company'].search([('id','=',1)])
        objAddress_rec = self.env['vb.branch'].search([('code','=','Main')])
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'MonthYear': str_monthyear,
                   'PrintedBy': str(self.env.user.name),
                   #'CompanyName' : str(objCompany_rec.name),
                   'CompanyAddress':str(objAddress_rec.address1)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_daily_settlement_trades_report(self):
        '''This method is used to print Daily Settlement Of Trades  Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        if self.start_date1 == False:
            raise ValidationError ('Please fill in Settlement date')


        # objCompany_rec= self.env['res.company'].search([('id','=',1)])
        # objAddress_rec = self.env['vb.branch'].search([('code','=','Main')])
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'DateGiven': self.start_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products

                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)


    @api.multi
    def get_trade_statistics_report(self):
        '''This method is used to print Trade Statistics Report'''

        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From Date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')



        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate': self.end_date1,
                   'Products_List' : list_of_products,
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_trade_statement_report(self):
        '''This method is used to store  Trade Statement Report for all account numbers for the given date '''

        list_trade_stat= []
        final_list= []
        parms_list=[]


        trade_stat_obj= self.env['vb.trade_tran'].search([('tran_date', '=',self.post_date),
                                                          ('state', '=', 'Posted'),
                                                          ('tran_type', 'in', ['Buy','Sell'])])

        objCompany_rec= self.env['res.company'].search([('id','=',1)])
        objAddress_rec = self.env['vb.branch'].search([('code','=','Main')])

        for rec in trade_stat_obj:
            list_trade_stat.append(rec.acct_id.id)

        #This is to get only Distinct Ids
        final_list = list(set(list_trade_stat))

        #Create Jasper Client Object
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        report_name = 'TradeStatement'



        #Create params dictionary array for the TradeStatement Report
        for acct in final_list:
            acct_rec =self.env['vb.customer_acct'].search([('id','=',acct)])

            params = {
                       'DateGiven': self.post_date,
                       'PrintedBy': str(self.env.user.name),
                       'AccountId':acct,
                       'CustomerID':acct_rec.customer_id.id,
                       'FileName':report_name + '-'+str(acct_rec.acct_no)+'-'+str(datetime.strptime(self.post_date,'%Y-%m-%d').strftime('%Y%m%d'))+'.pdf',
                       'CompanyName' : str(objCompany_rec.name),
                       'CompanyAddress':str(objAddress_rec.display_address),
                       'CompanyRegistrationNumber':str(objCompany_rec.company_registry)
                      }

            parms_list.append(params)
        s= jasper_obj.get_jasper_report_trade_statements(jasper_client, report_name, parms_list)
#         for acct in final_list:
#             docs = self.env['vb.document'].search([('acct_id','=',acct),('doc_class_id.code','=','TRDSTMT'),('file_name','=',report_name + '-'+str(self.env['vb.customer_acct'].search([('id','=',acct)]).acct_no)+'-'+str(datetime.strptime(self.post_date,'%Y-%m-%d').strftime('%Y%m%d'))+'.pdf')])
#             acct_id= self.env['vb.customer_acct'].search([('id','=',acct)])
#             password=self.env['vb.customer_login'].search([('customer_id','=',acct_id.customer_id.id),('login_domain','=','FE')])
#             count =0
#             st=''
#             for i in docs:
#                 count+=1
#                 st=''
#                 st=i.id
#             if st:
#                 doc = self.env['vb.document'].search([('id','=',int(st))])
#                 output = PyPDF2.PdfFileWriter()
#                 path = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm1+'TRDSTMT'
#                 output_file = os.path.join(path,"temp_" + doc.file_name)
#                 input_stream = PyPDF2.PdfFileReader(open(doc.file_loc, "rb"))
#                 print input_stream
#                 if input_stream.isEncrypted:
#                     input_stream.decrypt('')
#                 for n in range(0, input_stream.getNumPages()):
#                     output.addPage(input_stream.getPage(n))
#                 outputStream = open(output_file, "wb")
#                 pwd = str(acct_id.customer_id.name)[:1]+str(acct_id.customer_id.national_id_no)[-6:]
#                 output.encrypt(pwd, pwd, use_128bit=True)
#                 output.write(outputStream)
#                 outputStream.close()
#                 os.rename(output_file, doc.file_loc)
        #Calling the get report method in jasper.py
        return s

    @api.multi
    def get_monthly_account_statement_report(self):
        '''This method is used to store  Monthly Account Statement Report for all account numbers for the given Month '''
        if self.month == False:
            raise ValidationError ('Please select a Month')
        elif self.year == False:
            raise ValidationError ('Please select a Year')

        if len(str(self.month))==1:
            str_month = "0"+str(self.month)

        str_monthyear = str_month+"/"+str(self.year)

        list_customer= []
        parms_list=[]

        #Get the list of customer ids
        customer_obj= self.env['vb.customer'].search([('state', '=', 'Active'),('active', '=', 'TRUE')])

        for rec in customer_obj:
            list_customer.append(rec.id)

        #Create Jasper Client Object
        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        report_name = 'MonthlyAccountStatement'

        #Create params dictionary array for the Monthly Account St Report
        for id in list_customer:

            params = {
                       'MonthYear': str_monthyear,
                       'PrintedBy': str(self.env.user.name),
                       'CustomerID':id,
                       'FileName':report_name + '-'+str(id)+'-'+strftime('%Y%m%d')+'.pdf',
                      }

            parms_list.append(params)

        rep = jasper_obj.get_jasper_report_monthly_account_statements(jasper_client, report_name, parms_list)

#         for id in list_customer:
#             fname=str(report_name + '-'+str(id)+'-'+strftime('%Y%m%d')+'.pdf')
#             docs = self.env['vb.document'].search([('customer_id','=',id),('doc_class_id.code','=','ACCTSTMT'),('file_name_related','=',fname)])
#             customer_id= self.env['vb.customer'].search([('id','=',id)])
#             count =0
#             st=''
#             for i in docs:
#                 count+=1
#                 st=''
#                 st=i.id
#             if st:
#                 doc = self.env['vb.document'].search([('id','=',int(st))])
#                 output = PyPDF2.PdfFileWriter()
#                 path = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm1+'ACCTSTMT'
#                 output_file = os.path.join(path,"temp_" + doc.file_name)
#                 input_stream = PyPDF2.PdfFileReader(open(doc.file_loc, "rb"))
#                 if input_stream.isEncrypted:
#                     input_stream.decrypt('')
#                 for n in range(0, input_stream.getNumPages()):
#                     output.addPage(input_stream.getPage(n))
#                 outputStream = open(output_file, "wb")
#                 pwd = str(customer_id.name)[:1]+str(customer_id.national_id_no)[-6:]
#                 output.encrypt(pwd, pwd, use_128bit=True)
#                 output.write(outputStream)
#                 outputStream.close()
#                 os.rename(output_file, doc.file_loc)

        #Calling the get report method in jasper.py
        return rep


    @api.multi
    def get_error_log_info(self):

        error_log_recs = self.env['vb.error_log'].search([])
        print 'error_log_recs',error_log_recs
        wizard_data  = self.read([])[0]
        res = {
                'ids': error_log_recs.ids or [],
                'model':'vb.error_log',
                'form': wizard_data
               }
        return self.env['report'].get_action(self, 'vb_acctmgmt.error_log_list_template', data=res)

    @api.multi
    def get_cds_account_opening_fees_report(self):
        '''This method is used to print Summary Of NewAcccount Opened Charged Of CDS Account Opening Fees'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    @api.multi
    def get_CDS_balance_reconciliation_report(self):
        '''This method is used to print CDS Balance Reconciliation Report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in Batch  date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')


        d=datetime.strptime(self.start_date1,'%Y-%m-%d')
        batchno=datetime.strftime(d, "%Y%m%d")

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'DateGiven': self.start_date1,
                   'BatchNo' : batchno,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)
    
    # Get CDS opening fee refund report
    @api.multi
    def get_cds_opening_fee_refund_report(self):
        '''This method is used to print List of CDS opening fee refund report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))


        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # Get CS assisted trade daily execution summary report
    @api.multi
    def get_CS_assisted_Trading_Daily_execution_report(self):
        '''This method is used to print List of CS assisted trade daily execution summary report'''
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                   
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
    
    # Get CS KPI report
    @api.multi
    def get_cs_kpi_report(self):
        list_of_users = []
        for rec in self.user_ids:
            list_of_users.append(str(rec.login))
            
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'UserList':list_of_users,
                   'PrintedBy': str(self.env.user.name),
                   
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)

    # Get Source channel report
    @api.multi
    def get_source_channel_report(self):
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.xls'
        return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
    
    # Get Customer account KPI report
    @api.multi
    def get_customer_acct_kpi_report(self):
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))
        
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')
        elif len(list_of_products) == 0:
            raise ValidationError ('Please select a Product')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                   
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
    
    # Get Loyalty point collection report
    @api.multi
    def get_loyalty_point_collection_report(self):
        list_of_names = []
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))
        
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Customers_List' : list_of_names
                   
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
    
    # Get Loyalty point redemption report
    @api.multi
    def get_loyalty_point_redemption_report(self):
        list_of_names = []
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))

        partner_list = []
        for rec in self.loyalty_partner_ids:
            partner_list.append(str(rec.name))
        
        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Customers_List' : list_of_names,
                   'LoyaltyPartners_List': partner_list
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)

    # Get Customer Loyalty Point Balance report
    @api.multi
    def get_customer_loyalty_point_balance_report(self):
        list_of_names = []
        str_month=''
        str_month2=''
        FromMonthYear=''
        ToMonthYear=''
        for rec in self.customer_ids:
            list_of_names.append(str(rec.name))
        
        if self.month == False:
            raise ValidationError ('Please select a Month From')
        elif self.year == False:
            raise ValidationError ('Please select a Year From')
        elif self.month2 == False:
            raise ValidationError ('Please select a Month To')
        elif self.year2 == False:
            raise ValidationError ('Please select a Year To')
 
        if len(str(self.month))==1:
            str_month = "0"+str(self.month)
        else:
            str_month = str(self.month)
        FromMonthYear = str(self.year)+"/"+str_month
 
        if len(str(self.month2))==1:
            str_month2 = "0"+str(self.month2)
        else:
            str_month2 = str(self.month2)
        ToMonthYear = str(self.year2)+"/"+str_month2

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromMonthYear': FromMonthYear,
                   'ToMonthYear': ToMonthYear,
                   'PrintedBy': str(self.env.user.name),
                   'Customers_List': list_of_names
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
    
    # Method to fetch incomplete signup email sent report
    @api.multi
    def get_incomplete_signup_email_sent_report(self):
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        if self.start_date1 == False:
            raise ValidationError ('Please fill in From date')
        elif self.end_date1 == False:
            raise ValidationError ('Please fill in To date')
        elif self.end_date1 < self.start_date1:
            raise ValidationError ('To date can not be earlier than From date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': self.start_date1,
                   'ToDate' : self.end_date1,
                   'PrintedBy': str(self.env.user.name),
                   'Products_List' : list_of_products
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)
        if self.state4 == 'csv':
            filename = report_name + '.csv'
            return jasper_obj.get_jasper_report_csv(jasper_client, report_name, params, filename)
        
    # Method to fetch client trust report
    @api.multi
    def get_client_trust_report(self):
        '''This method is used to print Client Trust Report'''

        if self.start_date1 == False:
            raise ValidationError ('Please fill in To date')

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'Asat' : self.start_date1,                   
                   'PrintedBy': str(self.env.user.name)
                  }
        report_name = self._context.get('reportname')
        filename = report_name + '.pdf'
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
    
    
    @api.multi
    def get_CDS_bulk_transfer_report(self):
        '''This method is used to print List Of CDS Bulk Transfer out Report'''

        # convert from utc to current datetime
        from_date = parser.parse(self.start_datetime) + timedelta(hours=8)
        to_date = parser.parse(self.end_datetime) + timedelta(hours=8)

        if from_date == False:
            raise ValidationError ('Please fill in From date')
        elif to_date == False:
            raise ValidationError ('Please fill in To date')
        elif to_date < from_date:
            raise ValidationError ('To date can not be earlier than From date')
        list_of_products = []
        for rec in self.product_ids:
            list_of_products.append(str(rec.name))

        list_of_accounts = []
        for rec in self.acct_ids:
            list_of_accounts.append(str(rec.acct_no))

        jasper_obj = self.env['vb.jasper']
        jasper_client = jasper_obj.connect_jasper_server_client()
        params = {
                   'FromDate': from_date,
                   'ToDate': to_date,
                   'PrintedBy': str(self.env.user.name),                   
                   'Products_List' : list_of_products,
                   'AcctNo_List' : list_of_accounts                   
                  }
        report_name = self._context.get('reportname')
        if self.state4 == 'pdf':
            filename = report_name + '.pdf'
            return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
        if self.state4 == 'xls':
            filename = report_name + '.xls'
            return jasper_obj.get_jasper_report_xls(jasper_client, report_name, params, filename)        
        return jasper_obj.get_jasper_report(jasper_client, report_name, params, filename)
      