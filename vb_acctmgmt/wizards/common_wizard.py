from openerp import models, fields, api, _
import os
from datetime import datetime,timedelta,date
from openerp.exceptions import Warning
import json
import requests
from json import dumps
import json
from dateutil.relativedelta import relativedelta
import xlrd
import csv
import unicodecsv
import base64
from docutils.parsers.rst.directives import path
import re
import tempfile
# from dateutil.relativedelta import relativedelta
# from pychart.tick_mark import Null

class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'

    # @api.onchange('start_date1')
    # def get_batch_no(self):
    #     raise Warning(_(type(self.start_date1)))
    #     raise Warning(bool(self.start_date1))
    #     if self.start_date1:
    #         raise Warning(_(type(self.start_date1)))
    #         self.message = str(self.start_date1.strftime("%Y%m%d"))
    #     else:
    #         self.message = u''

    @api.onchange('start_date1')
    def get_batchno(self):
        if self.start_date1:
            self.update({'message':str(self.start_date1).split('-')[0]+str(self.start_date1).split('-')[1]+str(self.start_date1).split('-')[2]})
        
    @api.onchange('product_ids')
    def check_acct(self):


        list_of_productids=[]

        if self.product_ids:

            for i in self.product_ids:

                list_of_productids.append(i.id)

            return {'domain':{'acct_ids':[('acct_class_id','=',list_of_productids)]}}
        else:
            return {'domain':{'acct_ids':[('acct_class_id','=','')]}}

    month = fields.Selection([('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'),
                          ('5', 'May'), ('6', 'June'), ('7', 'July'), ('8', 'August'),
                          ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December'), ],
                          string='Month',default=str((date.today().replace(day=1) - timedelta(days=1)).month))
    year = fields.Selection([(num, str(num)) for num in range((datetime.now().year)-1, (datetime.now().year)+1 )], string='Year',
                            default=lambda self: datetime.now().year)

    month2 = fields.Selection([('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'),
                          ('5', 'May'), ('6', 'June'), ('7', 'July'), ('8', 'August'),
                          ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December'), ],
                          string='Month',default=str((date.today().replace(day=1) - timedelta(days=1)).month))
    year2 = fields.Selection([(num, str(num)) for num in range((datetime.now().year)-1, (datetime.now().year)+1 )], string='Year',
                            default=lambda self: datetime.now().year)

    no_of_days = fields.Integer(string='No Of Days')

    customer_ids = fields.Many2many('vb.customer', 'wizard_customer_rel', 'wizard_id', 'customer_id', string='Customers')

    acct_ids = fields.Many2many('vb.customer_acct', 'wizard_customeracct_rel', 'wizard_id', 'customeracct_id', ondelete="cascade",  string='Account')
    acct_ids_sync = fields.Many2many('vb.customer_acct', 'wizard_customeracct_sync_rel', 'wizard_id', 'customeracct_id', ondelete="cascade",  string='Account',domain=[('acct_class_id','=',24)])
    charge_ids = fields.Many2many('vb.charge_code', 'wizard_chargecode_rel', 'wizard_id', 'chargecode_id', string='Charge')
    file_name = fields.Char("File name")
    file_name_related = fields.Char(related='file_name',string='File name')

    loyalty_partner_ids = fields.Many2many('vb.loyalty_partner', 'wizard_loyalty_partner_rel', 'wizard_id', 'loyaltypartner_id', string='Loyalty Partner')

    auditlog_table = fields.Selection([
        ('vb_customer','Customer'),
        ('vb_customer_acct','Customer Account'),
        ], string = 'Table',default='vb_customer')

    report_tran_type = fields.Selection([
        ('Trade Transactions','Trade Transactions'),
        ('Cash Transactions','Cash Transactions'),
        ], string = 'Transactions',default='Cash Transactions')

    GL_report_type = fields.Selection([
        ('By GL No','By GL No'),
        ('By TRX Type','By TRX Type'),
        ], string = 'Transactions',default='By GL No')

    CDS_tran_type = fields.Selection([
        ('Deposit','Deposit'),
        ('Withdraw','Withdraw'),
        ], string = 'Transaction Type',default='Deposit')

    corp_action = fields.Selection('get_corp_action_dist', default='RIGHTS ISSUE')

    order_status = fields.Selection([
        ('New', 'New'),
        ('Sent', 'Sent'),
        ('Acknowledged', 'Acknowledged'),
        ('Cancelled', 'Cancelled'),
        ('Rejected', 'Rejected'),
        ('Matched', 'Matched'),
        ('PartialMatched', 'Partial Matched'),
        ('PartialCancelled', 'Partial Cancelled'),
        ('New-Amo', 'New Amo'),
        ], string='Status',default='New')
    message= fields.Char('message')
    reason_id_buy_suspended = fields.Many2one(comodel_name='vb.common_code',string='Reason for buy suspension',ondelete='restrict',domain="[('code_type','=','ReasonSus')]")
    reason_id_sell_suspended = fields.Many2one(comodel_name='vb.common_code',string='Reason for sell suspension',ondelete='restrict',domain="[('code_type','=','ReasonSus')]")
    upload_document = fields.Binary('Document', attachment=True)
    incom_reason1 = fields.Boolean('NRIC',default=False)
    incom_reason2 = fields.Boolean('Bank statement',default=False)
    incom_reason3 = fields.Boolean('Passport',default=False)
    incom_reason4 = fields.Boolean('Pay slip',default=False)
    incom_reason5 = fields.Boolean('test2',default=False)
    unrlsd_contra = fields.Float()
    net_share_coll = fields.Float()
    # added 3/7/2018 for ListOfKIBBCorporateActionDividendPay report
    corp_action_status = fields.Selection([
        ('New', 'New'),
        ('Posted', 'Posted')
        ], string='Status',default='New')

    # added 3/14/2018
    asset_ids = fields.Many2many('vb.asset', 'wizard_asset_rel', 'wizard_id', 'asset_id', string="Asset")
    from_datetime = fields.Datetime(string="From Datetime")
    to_datetime = fields.Datetime(string="To Datetime")
    estimated_cash_withdraw = fields.Char('Estimated cash withdrawal')
    trading_cost = fields.Char('Trading cost')

    # report type for XLS and CSV
    state_reporttype = fields.Selection([
        ('xls', 'XLS'),
        ('csv', 'CSV')
        ], string='Report Type', default='xls')

    # asset list report
    asset_class_ids = fields.Many2one(comodel_name='vb.common_code',string='Asset class',ondelete='restrict',domain="[('code_type','=','AssetClass')]")
    index_ids = fields.Many2one(comodel_name='vb.common_code',string='Index',ondelete='restrict',domain="[('code_type','=','Index')]")
    share_multiplier = fields.Selection('get_share_multiplier_dist')
    market_board = fields.Selection('get_market_board_dist')
    sync_options = fields.Selection([
        ('Batch','Batch synchronizing'),
        ('Single','Single synchronizing')],string='Sync options',default='Batch')
    bank_ids = fields.Many2one(comodel_name='vb.bankacct',string='Bank', ondelete='restrict', domain="[('active','=',True)]")

    # Terms and Conditions report
    version_list = fields.Selection('get_terms_version')
    # for Account Opened Report
    date_type_acreport = fields.Selection([
        ('Opened', 'Date Opened'),
        ('Activated', 'Date Activated')
        ], string='Data Type', default='Activated')
    # List of Daily Contra Report / KIBB
    date_type2  = fields.Selection([
        ('TranDate', 'Tran date'),
        ('SettlementDate','Settlement date'),
        ], string='Date Type', default='TranDate')
    asset_tran_state = fields.Selection('get_asset_tran_state')



# functions starts here

    @api.model
    def get_corp_action_dist(self):
        data = []
        query = "select distinct(entitle_type) from vb_kibb_corp_action_subs_conv_dtl"
        self._cr.execute(query)
        res = self._cr.fetchall()
        for rec in res:
            roundof = str(rec[0])
            data.append((roundof, roundof))
        return data

    @api.model
    def get_asset_tran_state(self):
        data = []
        query = "select distinct(state) from vb_asset_tran where active = True and state is not null"
        self._cr.execute(query)
        result = self._cr.fetchall()
        for rec in result:
            roundof = str(rec[0])
            data.append((roundof, roundof))
        return data

    @api.model
    def get_share_multiplier_dist(self):
        data = []
        query = "select distinct(share_multiplier) from vb_asset_prod"
        self._cr.execute(query)
        res = self._cr.fetchall()
        for multiplier in res:
            roundof = str(multiplier[0])
            data.append((roundof, roundof))
        return data

    # Get version of terms and conditions from vb_terms_n_conditions
    @api.model
    def get_terms_version(self):
        data = []
        query = "select distinct(version) from vb_terms_n_conditions where active = True"
        self._cr.execute(query)
        res = self._cr.fetchall()
        for version_list in res:
            roundof = str(version_list[0])
            data.append((roundof, roundof))
        return data

    @api.model
    def get_market_board_dist(self):
        data = []
        query = "select distinct(market_board) from vb_asset where market_board != ''"
        self._cr.execute(query)
        res = self._cr.fetchall()
        for market_board in res:
            data.append((market_board[0], market_board[0]))
        return data


    # This method to activate an account if the customer
    # State is new and the account state is active.
    @api.multi
    def get_activate_account_wizard(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs.cds_no is False or account_recs.other_acct_no is False or account_recs.state == 'Active':
            raise Warning(_("Please make sure you filled the cds_no and KIBB, or maybe the account is already activated"))
        else:
            if account_recs:
#                 if account_recs.customer_id.state != 'Active':
#                     account_recs.customer_id.date_activated = datetime.now()
#                     account_recs.customer_id.state = 'Active'
#                     account_recs.customer_id.stage = 'Activated'
                query = 'Update vb_customer_acct SET cds_open_date=%s where id=%s'
                self._cr.execute(query,(datetime.now(),self._context.get('active_id')))
                account_recs.write({'state':'Active','stage':'Activated','activated_by':self._uid, 'date_activated':datetime.now()})
                #reload the page
                return {
                        'type': 'ir.actions.client',
                        'tag': 'reload',
                        }
            else:
                raise Warning(_("No account found"))

    #account opening reject
    @api.multi
    def get_reject_account_wizard(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            if bool(self.note) == False or bool(self.reason_reject) == False:
                raise Warning('Please fill the note.')
            if bool(self.reason_reject) == False:
                raise Warning('Please select the reason for reject')
            account_recs.write({'state':'Rejected','stage':'Rejected','rejected_by':self._uid, 'date_rejected':datetime.now(),'reason_rejected_id':self.reason_reject.id,'comments':self.note})
            return {
                'type': 'ir.actions.client',
                'tag': 'reload',
                }

    #incomplete form
    @api.multi
    def incomplete_form(self):
        account_recs = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        if account_recs:
            query = "Update vb_customer_acct set incomplete_by = %s where id=%s"
            self._cr.execute(query,(self._uid,self._context.get('active_id')))
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            product = account_recs.acct_class_id.name
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
                "custId":account_recs.customer_id.id,
                "acctId":account_recs.id,
               "publisherApp":"backoffice",
               "templateCode":"Cf4",
               "messageParams":{"date":(d2(d1).strftime("%d-%m-%Y")) or '',
                                "product":str(product) or '',
                                "login_name":str(account_recs.customer_id.login_name) or '',
                                "date_opened":str(account_recs.date_opened) or ''},
               "recipients":[{
                             "recipientNo":"1",
                             "recipientType":"normal",
                             "name": account_recs.customer_id.name or '',
                             "email": account_recs.customer_id.email1 or '',
                             "title": account_recs.customer_id.title or '',
                            }]
               }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)

    @api.multi
    def incomplete_customer_form(self):
        customer = self.env['vb.customer'].search([('id','=',self._context.get('active_id'))])
        acct_rec = self.env['vb.customer_acct'].search([('customer_id','=',self._context.get('active_id')),('state','=','Basic')],limit=1)
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        if customer:
            query = "Update vb_customer set incomplete_by = %s where id=%s"
            self._cr.execute(query,(self._uid,self._context.get('active_id')))
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            get_signupstep_query = """SELECT activity_type FROM vb_customer_acct_activity
                                     WHERE 
                                     activity_type like '%s' 
                                     AND customer_id=%s
                                     ORDER BY id desc""" % ('SignUp%',customer.id)
                        
            self._cr.execute(get_signupstep_query)
            customer_step =  self._cr.fetchone()[0]
            step = ''
            if str(customer_step) == 'SignUp1':
                step = '1'
            elif str(customer_step) == 'SignUp2':
                step = '2'
            elif str(customer_step) == 'SignUp2A':
                step = '3'
            elif str(customer_step) == 'SignUp2B':
                step = '4'
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data = {
                    "inboxFlag": "True",
                    "custId": customer.id,
                    "acctId": acct_rec.id,
                    "publisherApp": "backoffice",
                    "templateCode": "IncompleteEmailTemplate",
                    "messageParams": {
                        "url":"https://uat.rakutentrade.my/musashi1493251200/device/accountopeningIncomplete?userid=%s&step=%s"%(str(customer.login_name),step),
#                         "url":"https://uat.rakutentrade.my/musashi1493251200/device/accountopeningIncomplete?userid=%s"%(str(customer.login_name)),
                        "email_type":"html_email",
                        "date": str(d2(d1).strftime("%d-%m-%Y")) or '',
                        "image_path_0":"/mnt/nfs/vbroker/img/logo_rakuten.jpg",
                        "image_path_1":"/mnt/nfs/vbroker/img/content1.jpg",
                        "image_path_2":"/mnt/nfs/vbroker/img/content2.jpg",
                        "image_path_3":"/mnt/nfs/vbroker/img/content2-1.jpg",
                        "image_path_4":"/mnt/nfs/vbroker/img/03_readmore.png",
                        "image_path_5":"/mnt/nfs/vbroker/img/content3.jpg",
                        "image_path_6":"/mnt/nfs/vbroker/img/content4.jpg",
                        "image_path_7":"/mnt/nfs/vbroker/img/content5.jpg",
                        "image_path_8":"/mnt/nfs/vbroker/img/content6.jpg",
                        "image_path_9":"/mnt/nfs/vbroker/img/click_more.png",
                        "image_path_10":"/mnt/nfs/vbroker/img/09_fb.jpg",
                        "image_path_11":"/mnt/nfs/vbroker/img/09_yt.jpg" 
                    },
               "recipients":[{
                             "recipientNo":"1",
                             "recipientType":"normal",
                             "name": customer.name or '',
                             "email": customer.email1 or '',
                             "title": customer.title or '',
                            }]
               }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)

    #account opening completed form
    def complete_form(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
               "publisherApp":"backoffice",
               "templateCode":"Cf2",
               "messageParams":{"date_opened":str(account_recs.date_opened) or '',
                                "product":str(account_recs.acct_class_id.name) or ''},
               "attachments":[{}],
               "recipients":[{
                             "recipientNo":"1",
                             "recipientType":"normal",
                             "name": account_recs.customer_id.name or '',
                             "email": account_recs.customer_id.email1 or '',
                             "title": account_recs.customer_id.title or '',
                            }]
               }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)

    #account activation with login info
    def activation_login(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
               "publisherApp":"backoffice",
               "templateCode":"Aa2",
               "messageParams":{"accno":str(self._context.get('active_id') or ''),
                                "date_opened":str(account_recs.date_opened) or '',
                                "product":(account_recs.acct_class_id.name) or '',
                                "login name":(account_recs.login_domain),
                                "temporary password":(account_recs.temp_pwd) or ''},
               "recipients":[{
                             "recipientNo":"1",
                             "recipientType":"normal",
                             "name": account_recs.customer_id.name or '',
                             "email": account_recs.customer_id.email1 or '',
                             "title": account_recs.customer_id.title or '',
                            }]
               }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)

    #email verification.
    @api.multi
    def email_verification(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Ev1",
                   "messageParams":{"accno":str(self._context.get('active_id') or ''),
                                    "date_opened":str(account_recs.date_opened) or '',
                                    "product":str(account_recs.acct_class_id.name) or '',
                                    "login name":str(account_recs.login_domain) or ''},
                   "attachments":[{}],
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": account_recs.customer_id.name or '',
                                 "email": account_recs.customer_id.email1 or '',
                                 "title": account_recs.customer_id.title or '',
                                }]
                   }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',
                                  routing_key=mq_server_rec.parm5,
                                  body = data_json)

    #password_reset
    def pass_reset(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Pr1",
                   "messageParams":{"accno":str(self._context.get('active_id') or ''),
                                    "date_opened":str(account_recs.date_opened) or '',
                                    "product":(account_recs.temp_pwd) or '',
                                    "login name":(account_recs.login_domain) or ''},
                   "attachments":[{}],
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": account_recs.customer_id.name or '',
                                 "email": account_recs.customer_id.email1 or '',
                                 "title": account_recs.customer_id.title or '',
                                }]
                   }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',
                                  routing_key=mq_server_rec.parm5,
                                  body = data_json)

    #customer account closure confirmation
    #this method is to close an account when the account's state is pending close.
    @api.multi
    def get_close_account_wizard(self):
        account_recs = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if account_recs:
            if self.note==False:
                raise Warning ('Please enter more information in the note')
            else:
                bal_rec = self.env['vb.customer_acct_bal'].search([('acct_id','=',self._context.get('active_id'))])
                if (bal_rec.cash_ledger_bal!=0 or bal_rec.trade_os_long_amt!=0 or bal_rec.trade_os_short_amt!=0 or bal_rec.trade_os_others_amt!=0 or bal_rec.order_buy_amt!=0 or bal_rec.order_sell_amt!=0):
                    raise Warning('All the outstanding Balances should be zero')
                else:
                    account_recs.write({'state':'Closed','stage':'Closed','closed_by':self._uid,'date_closed':datetime.now(),'comments':self.note,'reason_closed_id':self.reason_closed.id})

    @api.multi
    def reject_close_acct_wizard(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            if self.reason_closed==False or self.note==False:
                raise Warning('Please fill in the required fields')
            if acct_rec.state == 'PendingClose':
                acct_rec.update({'state':'Active','comments':self.note})
            else:
                raise Warning('The account can not be closed because the state is not Pending close')
        else:
            raise Warning ('There is no such an account')
    #customer account closure request acknowledgement
    @api.multi
    def account_closure_ack(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        account_recs1 = self.env['vb.customer_acct_bal'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            account_recs.write({'state':'PendingClose'})
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
            data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Ac2",
                   "messageParams":{"accno":str(self._context.get('active_id') or ''),
                                    "date_closed":str(account_recs.date_closed) or '',
                                    "received_request":str(account_recs.closing_request_date) or '',
                                    "out_standing amount":str(account_recs1.trade_os_total_amt) or '',
                                    "cash_ledger_balance":str(account_recs1.cash_ledger_bal) or '',
                                    "porfolio_holding_amount":str(account_recs1.portfolio_val) or ''},
                   "attachments":[{}],
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": account_recs.customer_id.name or '',
                                 "email": account_recs.customer_id.email1 or '',
                                 "title": account_recs.customer_id.title or '',
                                }]
                   }
            data_json=json.dumps(data)
            channel.basic_publish(exchange='',
                                  routing_key=mq_server_rec.parm5,
                                  body = data_json)

#     @api.multi
#     def check_customer_info(self):
#         customer_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))]).customer_id.id
#         return customer_rec
#         #                 self.env['vb.common_wizard'].search([('check_acct_no','=',True),('id','=',self._context.get('active_id'))]).update({'check_acct_no': False})
# #             else:
# #                 self.env['vb.common_wizard'].search([('check_acct_no','=',False),('id','=',self._context.get('active_id'))]).update({'check_acct_no': True})
# #         print":::",self.check_acct_no

    #customer account suspension
    #this method is to reject an account when it state is new.
    @api.multi
    def get_suspend_account_wizard(self):
        customers_recs = self.env['vb.customer_acct'].search([('id','in', self._context.get('active_ids'))])
        if customers_recs:
            if bool(self.reason_suspend) == False or bool(self.note) == False:
                raise Warning('Please fill in all the required fields')
            else:
                customers_recs.write({'state':'Suspended','suspended_by':self._uid,'reason_suspended_id':self.reason_suspend.id, 'date_suspended':datetime.now(),'comments':self.note})
#             mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#             if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                 common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
#                 return {
#                             'type': 'ir.actions.act_window',
#                             'view_type': 'form',
#                             'view_mode': 'form',
#                             'res_model': 'vb.common_wizard',
#                             'views': [(common_id, 'form')],
#                             'view_id': common_id,
#                             'target': 'new',
#                         }
#             channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#             data= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"As2",
#                    "messageParams":{"accno":str(self._context.get('active_id') or ''),
#                                     "date_suspended":str(customers_recs.date_suspended) or '',
#                                     "reason_suspension":str(customers_recs.reason_suspended_id.name) or ''},
#                    "attachments":[{}],
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": customers_recs.customer_id.name or '',
#                                  "email": customers_recs.customer_id.email1 or '',
#                                  "title": customers_recs.customer_id.title or '',
#                                 }]
#                    }
#             data_json=json.dumps(data)
#             channel.basic_publish(exchange='',
#                                   routing_key=mq_server_rec.parm5,
#                                   body = data_json)



    @api.multi
    def incomplete_document(self):
        account_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        if account_rec:
            if bool(self.reason_incomplete) == False or bool(self.note) == False:
                raise Warning("Please fill in all required fields")
            else:
                account_rec.write({'state':'IncompInfo','reason_incomplete':self.reason_incomplete.id,'comments':self.note,'requested_document_by':self._uid})
                mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
                if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                    raise Warning ('Mq Server is missing some values')
                else:
                    document = ''
                    for doc in account_rec.document_ids:
                        if doc.state == 'Rejected':
                            document+= doc.name.split('_')[1]+','
                    document = document[:-1]
                    channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                    data= {
                        "inboxFlag" : "True",
                        'acctId':account_rec.id,
                        'custId':account_rec.customer_id.id,
                       "publisherApp":"backoffice",
                       "templateCode":"IEN1",
                       "messageParams":{'login_name':str(account_rec.customer_id.login_name) or '',
                                        "comment":str(account_rec.comments) or '',
                                        "reason_incomplete":str(account_rec.reason_incomplete.name) or '',
                                        "date":str( d2(d1).strftime("%d-%m-%Y")),
                                        "productType":str(account_rec.acct_class_id.id),
                                        "acct_id":str(account_rec.id),
                                        "doc1":'bks' if 'bankstmt' in document.lower() else '',
                                        "doc2":'icf' if 'nric1' in document.lower() else '',
                                        "doc3":'icb' if 'nric2' in document.lower() else '',
                                        "doc4":'ps' if 'payslip' in document.lower() else '',},
                       "recipients":[{
                                     "recipientNo":"1",
                                     "recipientType":"normal",
                                     "name": account_rec.customer_id.name or '',
                                     "email": account_rec.customer_id.email1 or '',
                                     "title": account_rec.customer_id.title_id.code or '',
                                    }]
                       }
                    data_json=json.dumps(data)
                    channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#     #account suspension uplift
    @api.multi
    def get_uplift_account_wizard(self):
        account_recs = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if account_recs:
            account_recs.write({'state':'Active','uplifted_by':self._uid,'date_uplifted':datetime.now(),'reason_uplifted_id':self.reason_uplift.id,'stage':'Activated'})
            cust_login_recs = self.env['vb.customer_login'].search([('customer_id', '=', account_recs.customer_id.id)])
            if cust_login_recs:
                if bool(self.reason_uplift) == False:
                    raise Warning('Please fill in the required field')
                else:
                    for rec in cust_login_recs:
                        rec.update({'state':'Active', 'enabled_date':datetime.now(), 'enabled_by':self._uid})
#             mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
#             if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
#                 common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
#                 return {
#                             'type': 'ir.actions.act_window',
#                             'view_type': 'form',
#                             'view_mode': 'form',
#                             'res_model': 'vb.common_wizard',
#                             'views': [(common_id, 'form')],
#                             'view_id': common_id,
#                             'target': 'new',
#                         }
#             channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#             data= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"As1",
#                    "messageParams":{"accno":str(self._context.get('active_id')or ''),
#                                     "date_suspended":str(account_recs.date_suspended) or '',
#                                     "date_uplifted":str(account_recs.date_uplifted) or ''},
#                    "attachments":[{}],
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": account_recs.customer_id.name or '',
#                                  "email": account_recs.customer_id.email1 or '',
#                                  "title": account_recs.customer_id.title or '',
#                                 }]
#                    }
#             data_json=json.dumps(data)
#             channel.basic_publish(exchange='',
#                                   routing_key=mq_server_rec.parm5,
#                                   body = data_json)

    @api.multi
    def get_suspend_account_pending_close_wizard(self):
        customers_recs = self.env['vb.customer_acct'].search([('id','in', self._context.get('active_ids'))])
        customer_acct_recs = self.env['vb.customer_acct'].search([('customer_id','=',customers_recs.customer_id.id)])
        if customer_acct_recs:
            if bool(self.note)==False or bool(self.reason_suspend) == False:
                raise Warning ("Please fill in all required fields")
            else:
                count=0
                for i in customer_acct_recs:
                    count = count + 1
                if count == 1 and self.disable_login == True:
                    for rec in self.env['vb.customer_login'].search([('customer_id','=',customers_recs.customer_id.id)]):
                        rec.update({'state':'Disabled','disabled_date':datetime.now(),'disabled_by':self._uid})
                customers_recs.write({'state':'Suspended','suspended_by':self._uid,'reason_suspended_id':self.reason_suspend.id, 'date_suspended':datetime.now()})

#     #activate customer
#     @api.multi
#     def activate_customer(self):
#         #making sure the state is New and make sure it is not already Active
#         customer_rec = self.env['vb.customer'].search([('id', 'in',self._context.get('active_ids'))])
#         if customer_rec:
#             customer_rec.write({'state': 'Active','stage': 'Activated', 'activated_by': self._uid, 'date_activated': datetime.now()})
#
#     #suspend customer
    @api.multi
    def suspend_customer(self):
        #making sure the state is Active and make sure it is not already Suspended
        customer_rec = self.env['vb.customer'].search([('id', '=', self._context.get('active_id'))])
        if customer_rec:
            if self.reason_suspend==False or self.note == False:
                raise Warning('Please fill in all the required fields')
            else:
                customer_rec.write({'state':'Suspended','reason_suspended_id':self.reason_suspend.id,'suspended_by': self._uid, 'date_suspended': datetime.now(),'comments':self.note})
        else:
            raise Warning('The customer is not exist')
#
#     #reject customer if his state is new
#     @api.multi
#     def reject_customer(self):
#         #making sure the state is New and make sure it is not already Rejected
#         customer_rec = self.env["vb.customer"].search([('id', 'in', self._context.get('active_ids'))])
#         if customer_rec:
#             customer_rec.write({'state': 'Rejected','stage':'Rejected'})
#
#     #uplifting customer account
    @api.multi
    def uplift_customer(self):
        #making sure the state is Suspended and make sure it is not already uplifted
        customer_rec = self.env["vb.customer"].search([('id', 'in', self._context.get('active_ids')),('state','=','Suspended')])
        if customer_rec:
            if bool(self.reason_uplift) ==False:
                raise Warning('Please fill in the required field')
            else:
                customer_rec.write({'stage':'Uplifted','state': 'Active', 'uplifted_by': self._uid, 'date_uplifted': datetime.now()})
        else:
            raise Warning("The customer is not existed")

    @api.multi
    def reload_page(self):
        return {
                'type': 'ir.actions.client',
                'tag': 'reload',
                }

    @api.multi
    def forget_login_wizard(self):
        customer_rec = self.env['vb.customer'].search([('id','=',self._context.get('active_id'))])
        if customer_rec:
            mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
            if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(common_id, 'form')],
                            'view_id': common_id,
                            'target': 'new',
                        }
            else:
                temp=''
                for rec in customer_rec.login_ids:
                    if rec.login_domain == 'FE':
                        temp = rec.temp_pwd
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Fn1",
                   "messageParams":{"date":str(datetime.now()) or '',
                                    "login":str(customer_rec.login_name or ''),
                                    "tpin":str(temp) or '',
                                    },
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": customer_rec.name or '',
                                 "email": customer_rec.email1 or '',
                                 "title": customer_rec.title or '',
                                }]
                   }
                data_json=json.dumps(data)
                channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
                forget_login_msg= self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'count_account_active_wizard_view')[1]
                message = 'Forgetting login email has been sent successfully to '+customer_rec.name
                return {
                                'name':'Customer account forget login ',
                                'type': 'ir.actions.act_window',
                                'view_type': 'form',
                                'view_mode': 'form',
                                'res_model': 'vb.common_wizard',
                                'view_id': forget_login_msg,
                                'target': 'new',
                                'context': {'default_message': message},
                        }

    @api.multi
    def approve_RMCI(self):
        pass_doc_count = 0
        checklist_count = 0
        document_count = 0
        passed_count = 0
        self._cr.execute('Select state from vb_customer_acct_cedd where acct_id=%s'%self._context.get('active_id'))
        cedd_state = self._cr.fetchone()[0]
        accts_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id')),('state','in',['New','IncompInfo']),('state','!=','Checked')])
        if not accts_rec:
            raise Warning("Sorry, This account either not new or already checked")
        elif cedd_state not in ['CDDApproved','EDDApproved','EDDRejected']:
            raise Warning("This account must pass CDD/EDD stage first")
        else:
            rec_acct = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
            exist_acct = self.env['vb.customer_acct'].search([('customer_id','=',rec_acct.customer_id.id),('state','=','Active'),('id','!=',rec_acct.id)])
            if not exist_acct :
                for risk_rec in accts_rec.riskcheck_ids:
                    checklist_count = checklist_count + 1
                    if risk_rec.state in ['OK', 'ByPass']:
                        passed_count = passed_count + 1
                for document in accts_rec.document_ids:
                    document_count +=1
                    if document.state not in ['Approved','Archived']:
                        break
                    else:
                        pass_doc_count +=1
                if document_count > 0:
                    if document_count == pass_doc_count:
                        if checklist_count >0:
                            if checklist_count == passed_count:
                                if bool(self.note) == False:
                                    self.note = ''
                                accts_rec.write({'state': 'Checked','risk_check_date':datetime.now(),'comments':self.note,'approved_by':self._uid,'approved_date':date.today()})
                                #after riskchecking manually and all are ok or bybass it will create a record in activity acct with state OK
                                approve_msg= self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'count_account_active_wizard_view')[1]
                                message = "You have approved this account #NO: "+str(accts_rec.acct_no)+"  Which belong to: "+accts_rec.customer_id.name.upper()
                                return {
                                    'name':'Customer account approved ',
                                    'type': 'ir.actions.act_window',
                                    'view_type': 'form',
                                    'view_mode': 'form',
                                    'res_model': 'vb.common_wizard',
                                    'view_id': approve_msg,
                                    'target': 'new',
                                    'context': {'default_message': message},
                                    }
                            else:
                                raise Warning ("Sorry the risk check states are not all OK or/and Bypass")
                    else:
                        raise Warning ("All Documents should be approved before approving the account")
                else:
                    raise Warning("You can't Approve the account without any document")
            else:
                accts_rec.write({'state': 'Checked','risk_check_date':datetime.now(),'comments':self.note,'approved_by':self._uid,'approved_date':date.today()})
                #after riskchecking manually and all are ok or bybass it will create a record in activity acct with state OK
                approve_msg= self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'count_account_active_wizard_view')[1]
                message = "You have approved this account #NO: "+str(accts_rec.acct_no)+"  Which belong to: "+accts_rec.customer_id.name.upper()
                return {
                    'name':'Customer account approved ',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': approve_msg,
                    'target': 'new',
                    'context': {'default_message': message},
                    }


    # 05-Nov-2016 DT
    # Method to create record at vb.procedure_exec_request regarding to trigger the EOD closing to process
    @api.multi
    def eod_closing(self):

        proc_code = 'EOD_CLOSING_'+str(self.post_date)
        proc_lock = self.env['vb.proc_lock'].search([('proc_code','=',proc_code)])
        if proc_lock:
            raise Warning('This function already performed for this date.')
        else:
            proc_lock=self.env['vb.proc_lock'].create({'proc_code':proc_code,'proc_freq':'Day','proc_period':str(self.post_date),'state':'InProgress'})
            exec_rqs = self.env['vb.procedure_exec_request']
            result = exec_rqs.create({'name':'EOD closing', 'procfn':'EOD_CLOSING','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
            final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'show_completion_wizard_view')[1]
            message = "End Of Day closing completed. " + str(result.rec_processed) + " of " + str(result.rec_count) + " jobs processed."
            proc_lock.write({'state':'Processed'})

            if result.rec_skipped != 0:
                message = "End Of Day closing aborted after processing "+ str(result.rec_processed) + " of " + str(result.rec_count) + " jobs."

            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }

    # 05-Nov-2016 DT
    # Method to create record at vb.procedure_exec_request regarding to trigger the EOM closing to process
    # This function currently not in use since the EOD Closing will perform EOM task if it coincide with end of month
    @api.multi
    def eom_closing(self):
        exec_rqs = self.env['vb.procedure_exec_request']
        result = exec_rqs.create({'name':'EOM closing', 'procfn':'EOM_CLOSING','rqs_time':datetime.now(), 'rqsby_id':self._uid, 'parm1':str(self.post_date)})
        final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'show_completion_wizard_view')[1]
        message = "End Of Month closing completed." + str(result.rec_processed) + " of " + str(result.rec_count) + " jobs processed."
        if result.rec_skipped != 0:
            message = "End Of Month closing aborted after processing "+ str(result.rec_processed) + " of " + str(result.rec_count) + " jobs."

        return {
            'name':'Processing completed.',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'view_id': final_view,
            'target': 'new',
            'context': {'default_message': message},
            }

    # 29-Dec-2016 DT
    # Method to create record at vb.procedure_exec_request regarding to trigger the EOM closing to process
    @api.multi
    def eod_gl_gen(self):
        exec_rqs = self.env['vb.procedure_exec_request']
        exec_rqs.create({'name':'EOD Generate GL entries',
                         'procfn':'EOD_GEN_GL','rqs_time':datetime.now(),
                         'rqsby_id':self._uid, 'parm1':str(self.post_date)})
        final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'show_completion_wizard_view')[1]
        message = "EOD generate GL entries completed."
        return {
            'name':'Processing completed.',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'view_id': final_view,
            'target': 'new',
            'context': {'default_message': message},
            }
    @api.multi
    def send_approvers(self):
        get_rec = self.env['vb.customer_acct_riskcheck'].search([('id','=',self._context.get('active_id'))])
        mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
        if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
            common_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'msg_mqserver_param_miss')[1]
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(common_id, 'form')],
                        'view_id': common_id,
                        'target': 'new',
                    }
        message='Email has sent to approvers: \n'
        final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'confirm_approvers_wizard_view')[1]
        if get_rec.approver1 or get_rec.approver2 or get_rec.approver3 or get_rec.approver4:
            if get_rec.approver1:
                get_user1 = self.env['res.users'].search([('id','=',int(get_rec.approver1))])
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Rc1",
                   "messageParams":{'approver1':get_user1.name or '',
                                    "Cname":get_rec.acct_id.customer_id.name or '',
                                    "riskcheckName":str(get_rec.name) or '',
                                    'date':str(datetime.now().strftime("%d-%m-%Y")) or '',

                                    },
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": get_user1.name or '',
                                 "email": get_user1.login or '',
                                 'title':'Mr',
                                }]
                   }
                data_json=json.dumps(data)
                channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
                if get_rec.approver2 or get_rec.approver3 or get_rec.approver4:
                    message += str(get_user1.name+", ")
                else:
                    message += str(get_user1.name+".")

            if get_rec.approver2:
                get_user2 = self.env['res.users'].search([('id','=',int(get_rec.approver2))])
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Rc1",
                   "messageParams":{'approver1':get_user2.name or '',
                                    "Cname":get_rec.acct_id.customer_id.name or '',
                                    "riskcheckName":str(get_rec.name) or '',
                                    'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
                                    },
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": get_user2.name or '',
                                 "email": get_user2.login or '',
                                 'title':'Mr',
                                }]
                   }
                data_json=json.dumps(data)
                channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
                if get_rec.approver3 or get_rec.approver4:
                    message += str(get_user2.name+", ")
                else:
                    message += str(get_user2.name+".")
            if get_rec.approver3:
                get_user3 = self.env['res.users'].search([('id','=',int(get_rec.approver3))])
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Rc1",
                   "messageParams":{'approver1':get_user3.name or '',
                                    "Cname":get_rec.acct_id.customer_id.name or '',
                                    "riskcheckName":str(get_rec.name) or '',
                                    'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
                                    },
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": get_user3.name or '',
                                 "email": get_user3.login or '',
                                 'title':'Mr',
                                }]
                   }
                data_json=json.dumps(data)
                channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
                if get_rec.approver4:
                    message += str(get_user3.name+",")
                else:
                    message += str(get_user3.name+".")
            if get_rec.approver4:
                get_user4 = self.env['res.users'].search([('id','=',int(get_rec.approver4))])
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                data= {
                   "publisherApp":"backoffice",
                   "templateCode":"Rc1",
                   "messageParams":{'approver1':get_user4.name or '',
                                    "Cname":get_rec.acct_id.customer_id.name or '',
                                    "riskcheckName":str(get_rec.name) or '',
                                    'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
                                    },
                   "recipients":[{
                                 "recipientNo":"1",
                                 "recipientType":"normal",
                                 "name": get_user4.name or '',
                                 "email": get_user4.login or '',
                                 'title':'Mr',
                                }]
                   }
                data_json=json.dumps(data)
                channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
                message += str(get_user4.name+".")

            return {
                    'name':'Message sent to the approver.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }
        else:
            return {
                    'name':'Message sent to the approver.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': "There is no approvers to send to you are able to change the state."},
                    }

#             if  get_rec.approver1 and not get_rec.approver2:
#                 get_user1 = self.env['res.users'].search([('id','=',int(get_rec.approver1))])
#                 channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                 data= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"Rc1",
#                    "messageParams":{'approver1':get_user1.name or '',
#                                     "Cname":get_rec.acct_id.customer_id.name or '',
#                                     "riskcheckName":str(get_rec.name) or '',
#                                     'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
#
#                                     },
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": get_user1.name or '',
#                                  "email": get_user1.login or '',
#                                  'title':'Mr',
#                                 }]
#                    }
#                 data_json=json.dumps(data)
#                 channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                 message = str('Messages sent to to approver1: '+get_user1.name)
#                 return {
#                         'name':'Message sent to the approver.',
#                         'type': 'ir.actions.act_window',
#                         'view_type': 'form',
#                         'view_mode': 'form',
#                         'res_model': 'vb.common_wizard',
#                         'view_id': final_view,
#                         'target': 'new',
#                         'context': {'default_message': message},
#                         }
#             if  get_rec.approver2 and not get_rec.approver1:
#                 get_user2 = self.env['res.users'].search([('id','=',int(get_rec.approver2))])
#                 channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                 data= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"Rc1",
#                    "messageParams":{'approver1':get_user2.name or '',
#                                     "Cname":get_rec.acct_id.customer_id.name or '',
#                                     "riskcheckName":str(get_rec.name) or '',
#                                     'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
#                                     },
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": get_user2.name or '',
#                                  "email": get_user2.login or '',
#                                  'title':'Mr',
#                                 }]
#                    }
#                 data_json=json.dumps(data)
#                 channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                 message = str('Messages sent to to approver2: '+get_user2.name)
#                 return {
#                     'name':'Message sent to the approver.',
#                     'type': 'ir.actions.act_window',
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'res_model': 'vb.common_wizard',
#                     'view_id': final_view,
#                     'target': 'new',
#                     'context': {'default_message': message},
#                     }
#
#
#             else:
#                 get_user1 = self.env['res.users'].search([('id','=',int(get_rec.approver1))])
#                 get_user2 = self.env['res.users'].search([('id','=',int(get_rec.approver2))])
#                 channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
#                 data= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"Rc1",
#                    "messageParams":{'approver1':get_user1.name or '',
#                                     "Cname":get_rec.acct_id.customer_id.name or '',
#                                     "riskcheckName":str(get_rec.name) or '',
#                                     'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
#                                     },
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": get_user1.name or '',
#                                  "email": get_user1.login or '',
#                                  'title':'Mr',
#                                 }]
#                    }
#                 data_json=json.dumps(data)
#                 channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                 data2= {
#                    "publisherApp":"backoffice",
#                    "templateCode":"Rc1",
#                    "messageParams":{'approver1':get_user2.name or '',
#                                     "Cname":get_rec.acct_id.customer_id.name or '',
#                                     "riskcheckName":str(get_rec.name) or '',
#                                     'date':str(datetime.now().strftime("%d-%m-%Y")) or '',
#                                     },
#                    "recipients":[{
#                                  "recipientNo":"1",
#                                  "recipientType":"normal",
#                                  "name": get_user2.name or '',
#                                  "email": get_user2.login or '',
#                                  'title':'Mr',
#                                 }]
#                    }
#                 data_json=json.dumps(data2)
#                 channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)
#                 message = str('Messages sent to approver1: '+get_user1.name+' and to approver2: '+get_user2.name)
#                 return {
#                     'name':'Message sent to the approvers.',
#                     'type': 'ir.actions.act_window',
#                     'view_type': 'form',
#                     'view_mode': 'form',
#                     'res_model': 'vb.common_wizard',
#                     'view_id': final_view,
#                     'target': 'new',
#                     'context': {'default_message': message},
#                     }

    @api.multi
    def get_pending_close_account_wizard(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            acct_rec.write({'state':'PendingClose'})
        else:
            raise Warning('There is no account')

    # Method for approving customer change request
    @api.multi
    def approve_customer_change_rqs(self):
        change_rec = self.env['vb.customer_change_rqs'].search([('id','=',self._context.get('active_id'))])
        if change_rec:
            change_rec.write({'state':'Approved', 'date_approved':datetime.now(), 'approved_by':self._uid, 'comments':self.note})

    # Method for rejecting customer change request
    @api.multi
    def reject_customer_change_rqs(self):
        change_rec = self.env['vb.customer_change_rqs'].search([('id','=',self._context.get('active_id'))])
        if change_rec:
            change_rec.write({'state':'Rejected', 'date_rejected':datetime.now(), 'rejected_by':self._uid, 'reason_rejected_id':self.reason_reject_1.id, 'comments':self.note})

    # Method for Archiving Record
    @api.multi
    def archive_record(self):
        arch_rec = self.env['vb.customer_acct'].search([('id','in',self._context.get('active_ids'))])
        if arch_rec:
#             arch_rec.write({'comments':self.note})
            self._cr.execute("SELECT sp_customer_acct_id_arch( %s,%s); ",(arch_rec.id, str(self.note)))
            output = self._cr.fetchall()[0][0]
            if output == -1 or output == 0:
                raise Warning("Archive process has not completed. Get response {0}".format(output))
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'archive_record_completion_wizard_view')[1]
                message = "This record has been archived successfully."
                return {
                    'name':'Successfully archived.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }

    @api.multi
    def resend_tempwd(self):
#         acct_rec = self.env['vb.customer_acct'].search([('customer_id','=',self._context.get('active_id')),('acct_type','=','Cash')])
#         if acct_rec:
#             acct_no=''
#             for i in acct_rec:
#                 acct_no = i.acct_no
#                 break
        customer_id = self.env['vb.customer'].search([('id','=',self._context.get('active_id'))])
        if customer_id.state in ['Active','New']:
            if customer_id.email1_verified == False :
                d1 = datetime.now()
                d2 = lambda d1: datetime.now()
                mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
                if not mq_server_rec.parm1 or not mq_server_rec.parm2 or not mq_server_rec.parm3 or not mq_server_rec.parm4 or not mq_server_rec.parm5:
                    rec.write({'proc_text':'Error in MQ server, system params are not specified'})
                else:
                    channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                    data= {
#                             "inboxFlag" : "True",
#                             'acctId':acct_rec.id,
                        'custId':customer_id.id,
                       "publisherApp":"backoffice",
                       "templateCode":"Aa3",
                       "messageParams":{ "login_name" : str(customer_id.login_name),
                                    "date":str( d2(d1).strftime("%d-%m-%Y")) or ''},
                       "recipients":[{
                                     "recipientNo":"1",
                                     "recipientType":"normal",
                                     "name": str(customer_id.name) or '',
                                     "email": str(customer_id.email1) or '',
                                     "title": str(customer_id.title_id.code) or '',
                                    }]
                       }
                    data_json=json.dumps(data)
                    channel.basic_publish(exchange='',routing_key=mq_server_rec.parm5,body = data_json)

                    final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'verification_email_wizard_view')[1]
                    message = "Email verification has been sent successfully."
                    return {
                    'name':'Email successfully sent.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                        }
#                 url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','SendTpin')])
#                 url =   "http://"+str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
#                 if (url_rec.parm1 or url_rec.parm2 or url_rec.parm5)== False:
#                     raise Warning('Please provide the params in system parameters code SendTpin')
#                 else:
#                     data = dumps({ "login_name" : str(self.env['vb.customer'].search([('id','=',self._context.get('active_id'))]).login_name),
#                                     "date":str( d2(d1).strftime("%d-%m-%Y")) or ''})
#                     headers={'Content-type': 'application/json'}
#                     content = requests.post(url,headers=headers,data=data)
#                     if content.json().get('code')!=200:
#                         raise Warning(str(content.json().get('code'))+": "+content.json().get('message'))
#                     else:
#                         return content.json().get('code')
            else:
                raise Warning ('This customer already verified his email')
        else:
            raise Warning("This customer state is either not active or not new")
#         else:
#             raise Warning("There is no cash account for this customer")

    # Method for realise trade interest amount
    @api.multi
    def realise_trade_intamt(self):
        acct_bal_rec = self.env['vb.customer_acct_bal'].search([('id','in',self._context.get('active_ids'))])
        if acct_bal_rec:
            self._cr.execute("SELECT sp_realise_interest_trade( %s,%s); ",(int(self._uid), int(acct_bal_rec.acct_id.id)))
            output = self._cr.fetchone()[0]
            if output < 0:
                raise Warning("Process has not completed. Got response {0}".format(output))
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'trade_int_amount_realise_completion_wizard_view')[1]
                message = "This trade interest amount has been realised successfully."
                return {
                    'name':'Successfully realised trade interest amount.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }

    # Method for realise cash interest amount
    @api.multi
    def realise_cash_interest_amt(self):
        acct_bal_rec = self.env['vb.customer_acct_bal'].search([('id','in',self._context.get('active_ids'))])
        if acct_bal_rec:
            query = "SELECT sp_realise_interest_cash( %s,%s);"
            self._cr.execute(query,(int(self._uid), int(acct_bal_rec.acct_id.id)))
            output = self._cr.fetchone()[0]
            if output < 0:
                raise Warning("Process has not completed. Got response {0}".format(output))
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'cash_interest_amt_realise_completion_wizard_view')[1]
                message = "This cash interest amount has been realised successfully."
                return {
                    'name':'Successfully realised cash interest amount.',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }


    # Method for extracting specific data in csv format from Source Channel tree view
    @api.multi
    def get_source_channel_recs_csv(self):
        recs = self.env['vb.customer'].search([('id','in',self._context.get('active_ids'))])
        print_header = ("Date/Time created "+","+"Customer name "+","+"Channel "+","+"Source"+","+"Info source"
        +","+"Info source sub"+","+"Date/Time activated "+","+"Status")

        report_name = 'SourceChannelReport'
        filename = report_name + '.csv'
        data = []
        for i in recs:
            data.append([i.create_date,i.name,i.signup_channel,i.source,i.info_source_id.code or False,
                         i.info_source_subid.code,i.date_activated,i.stage])

        count =0
        text1=''
        for i in data:
            for k in i:
                text1 += str(k)
                count +=1
                if count == 8:
                    text1+='\n'
                    count = 0
                else:
                    text1+=','

        final_text = print_header +'\n'+text1
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text),'file_name':filename})

        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }

    @api.multi
    def suspend_buy(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            acct_rec.update({'buy_suspended':True,'buy_suspended_by':self._uid,'date_buy_suspended':datetime.now(),'reason_id_buy_suspended':self.reason_id_buy_suspended.id})
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'buy_sell_suspend_uplift_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_message':'Buy trades for this account has been suspended'},
                    }
        else:
            raise Warning('The account not found. please refresh your page.')

    @api.multi
    def suspend_sell(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            acct_rec.update({'sell_suspended':True,'sell_suspended_by':self._uid,'reason_id_sell_suspended':self.reason_id_sell_suspended.id,'date_sell_suspended':datetime.now()})
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'buy_sell_suspend_uplift_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_message':'Sell trades for this account has been suspended'},
                    }
        else:
            raise Warning('The account not found. please refresh your page.')

    @api.multi
    def uplifted_buy(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            acct_rec.update({'buy_suspended':False,'buy_suspended_lifted_by':self._uid,'date_buy_suspended_lifted':datetime.now()})
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'buy_sell_suspend_uplift_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_message':'Buy trades for this account has been uplifted'},
                    }
        else:
            raise Warning('The account not found. please refresh your page.')

    @api.multi
    def uplifted_sell(self):
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        if acct_rec:
            acct_rec.update({'sell_suspended':False,'sell_suspended_lifted_by':self._uid,'date_sell_suspended_lifted':datetime.now()})
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'buy_sell_suspend_uplift_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                        'context': {'default_message':'Sell trades for this account has been uplifted'},
                    }
        else:
            raise Warning('The account not found. please refresh your page.')

    @api.multi
    def ramci_second_time(self):
        rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        stage1 = False
        stage2 = False
        if rec:
            if self.env['vb.common_code'].search([('id','=',rec.acct_class_id.id)]).code == 'Cash':
        #         if self.acct_type == 'Cash':
        #             self.update({'is_cedd_done': True})
        #         url = "http://172.18.6.105:8080/vbroker/api/v1/ramci"
                url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RamciInfo')])
                url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                data = dumps({"loginName" : str(rec.customer_id.login_name),
                              "acctId" : str(rec.id),
                              "customerName": rec.customer_id.name,
                              "idNo": str(rec.national_id_no),
                              "idType" : str(rec.national_id_type),
                              "country": str(rec.customer_id.nationality_id.code),
                              "dob": str(rec.customer_id.date_of_birth),
                              "mobileNo":str(rec.mobile_phone1),
                              "emailAddress": str(rec.email1),
                              "lastKnownAddress": ""})
                headers={'Content-type': 'application/json'}
                content = requests.post(url,data=data,headers=headers)
                if int(content.json().get('code'))!=200 or content ==False:
                    raise Warning(content.json().get('message'))
                risk_list = []
                quest_list=[]
                if not rec.acct_class_id:
                    raise Warning("Product has not been specified for this Account")
                else:
                    acct_class_rec= rec.acct_class_id
                    if not acct_class_rec.riskprof_id:
                        raise Warning("Risk profile for the Product cannot be located")
                    else:
                        riskprof_rec= acct_class_rec.riskprof_id
                        if not riskprof_rec.checklist_ids:
                            raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                        else:
                            checklist_state= riskprof_rec.checklist_ids
                            quest_recs= riskprof_rec.questionaire_ids
                query ="Delete from vb_customer_acct_riskcheck Where acct_id =%s"%rec.id
                self._cr.execute(query)
                if bool(quest_recs)!=True:
                #to get the risk_checklist_ids recs from the specific product
                    for recs in quest_recs:
                        quest_dict=(0,0,{'question_id':recs.id,'response':recs.desired_response,'scoring':recs.scoring})
                        quest_list.append(quest_dict)
                    #to get the questionaire_ids recs from the specific product
                    for recs in checklist_state:
                        if content.json().get('result'):
                            for i in content.json().get('result'):
                                if str(recs.code) == str(i):
                                    if bool(int(content.json().get('result').get(str(recs.code))) >= recs.result_min and int(content.json().get('result').get(str(recs.code))) <= recs.result_max):
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'OK','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    elif bool(int(content.json().get('result').get(str(recs.code))) == -1 or int(content.json().get('result').get(str(recs.code))) == -1):
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'OK','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    else:
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'Failed','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                        else:
                            raise Warning ("This account doesn't have any risk check list")
                    rec.write({'riskcheck_ids':risk_list,'riskquestion_ids':quest_list})
                else:
                    #to get the questionaire_ids recs from the specific product
                    for recs in checklist_state:
                        if content.json().get('result'):
                            for i in content.json().get('result'):
                                if str(recs.code) == str(i):
                                    if bool(int(content.json().get('result').get(str(recs.code))) >= recs.result_min and int(content.json().get('result').get(str(recs.code))) <= recs.result_max):
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'OK','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    elif bool(int(content.json().get('result').get(str(recs.code))) == -1 or int(content.json().get('result').get(str(recs.code))) == -1):
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'OK','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    else:
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'Failed','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                        else:
                            raise Warning ("This account doesn't have any risk check list")
                    rec.write({'riskcheck_ids':risk_list})
                    if rec.acct_type == 'Cash':
                        query = "update vb_customer_acct set ramci_contra1='t', ramci_contra2='t' where id=%s"%rec.id
                        self._cr.execute(query)
                    else:
                        query = "update vb_customer_acct set ramci_contra1='t' where id=%s"%rec.id
                        self._cr.execute(query)
        #         card_rec = self.env['vb.card_tran'].search([('acct_id','=',self.id)])
        #         if card_rec:
        #             for card in card_rec:
        #                 if card.cardholder_name and card.state in ['Posted','Ok']:
        #                     query=''
        #                     query = 'UPDATE vb_customer_acct SET card_tran_name=%s Where id=%s'
        #                     self._cr.execute(query,(card.cardholder_name,self.id))
                if int(content.json().get('code'))==200:
                    stage1 = True
                    query_cust = "Update vb_customer set ramci_kyc_date=%s,ramci_nrvb_date=%s where id=%s"
                    self._cr.execute(query_cust,(datetime.now(),datetime.now(),rec.customer_id.id))
                url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/'+str(url_rec.parm6)
                data = dumps({ "loginName" : rec.customer_id.login_name,
                              "acctId" : str(rec.id)})
                headers={'Content-type': 'application/json'}
                content = requests.post(url,data=data,headers=headers)
                doc_rec = self.env['vb.document']
                x =content.text.split(',')
                stage2 = True
                if stage1== True and stage2 == True:
                    rec.update({'nrvb_clicked_flag':True})
                list = []
                doc_list =[]
                doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm2
                file_loc = doc_directory +'RAMCI/'
                if os.path.exists(file_loc)== False:
                    os.makedirs(file_loc)
                for recs in self.env['vb.document'].search([('acct_id','=',rec.id),('customer_id','=',rec.customer_id.id)]):
                    doc_list.append(recs.file_name)
                for i in x:
                    index = x.index(i)
                    if 'KYCPDF=' in i:
                        file_name_kyc = str('KYCPDF'+str(rec.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_kycpdf = str(file_loc + file_name_kyc)
                        rec_check = self.env['vb.document'].search([('customer_id','=',rec.customer_id.id),('acct_id','=',rec.id),('name','=',"KYC report")])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)

                        if file_name_kyc not in doc_list or bool(doc_list) == False:
                            kycpdf = base64.b64decode(x[index].split('KYCPDF=')[1])
                            f = open(file_path_kycpdf,'wb')
                            f.write(kycpdf)
                            f.close()
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',rec.customer_id.id,
                                                rec.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                    elif 'IRISSPDF=' in i:
                        file_name_iriss = str('IRISSPDF'+str(rec.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_irisspdf = str(file_loc+file_name_iriss)
                        check_doc2 = self.env['vb.document'].search([('acct_id','=',rec.id),('name','=','IRISS report')])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)

                        if file_name_iriss not in doc_list or bool(doc_list) == False:
                            irisspdf = base64.b64decode(x[index].split('IRISSPDF=')[1])
                            y = open(file_path_irisspdf,'wb')
                            y.write(irisspdf)
                            y.close()
                            query='''INSERT INTO vb_document (file_loc,name, doc_version,file_name,is_open_exist,
                                    customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_irisspdf,'IRISS report',1.0,file_name_iriss,'RAMCI',rec.customer_id.id,
                                            rec.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                            'Internal','New',datetime.now(),datetime.now(),self._uid,self._uid,True))
                    elif 'NRVBPDF' in i:
                        file_name_nrvb=str('NRVBPDF'+str(rec.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_nrvbpdf = str(file_loc + file_name_nrvb)
                        rec_check = self.env['vb.document'].search([('customer_id','=',rec.customer_id.id),('acct_id','=',rec.id),('name','=','NRVB report')])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)
                        if file_name_nrvb not in doc_list or bool(doc_list) == False:
                            nrvbpdf = base64.b64decode(x[index].split('NRVBPDF=')[1])
                            g = open(file_path_nrvbpdf,'wb')
                            g.write(nrvbpdf)
                            g.close()
                            check_doc1 = self.env['vb.document'].search([('acct_id','=',rec.id),('file_name','=',file_name_nrvb),('file_loc','=',file_path_nrvbpdf)])
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',rec.customer_id.id,
                                                    rec.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                    'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                del doc_list[:]
            else:
                stage1 = False
                stage2 = False
                url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RamciInfo')])
                url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                data = dumps({"loginName" : str(rec.customer_id.login_name),
                              "acctId" : str(rec.id),
                              "customerName": rec.customer_id.name,
                              "idNo": str(rec.national_id_no),
                              "idType" : str(rec.national_id_type),
                              "country": str(rec.customer_id.nationality_id.code),
                              "dob": str(rec.customer_id.date_of_birth),
                              "mobileNo":str(rec.mobile_phone1),
                              "emailAddress": str(rec.email1),
                              "lastKnownAddress": ""})
                headers={'Content-type': 'application/json'}
                content = requests.post(url,data=data,headers=headers)

                if int(content.json().get('code'))!=200 or content ==False:

                    raise Warning(content.json().get('message'))
                risk_list = []
                if not rec.acct_class_id:
                    raise Warning("Product has not been specified for this Account")
                else:
                    acct_class_rec= rec.acct_class_id
                    if not acct_class_rec.riskprof_id:
                        raise Warning("Risk profile for the Product cannot be located")
                    else:
                        riskprof_rec= acct_class_rec.riskprof_id
                        if not riskprof_rec.checklist_ids:
                            raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                        else:
                            checklist_state= riskprof_rec.checklist_ids
                exist_list = ()
                for i in content.json().get('result'):
                    if self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self._context.get('active_id'))]):
                        for n in self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self._context.get('active_id'))]):
                            exist_list += (n.id,)
                if exist_list:
                    for i in exist_list:
                        if i!= False:
                            query ="Delete from vb_customer_acct_riskcheck Where id =%s"%i
                            self._cr.execute(query)
                if bool(checklist_state)==True:
        #             query ="Delete from vb_customer_acct_riskcheck Where acct_id =%s"%self.id
        #             self._cr.execute(query)
                    for recs in checklist_state:
                        if content.json().get('result'):
                            for i in content.json().get('result'):
                                if str(recs.code) == str(i):
                                    if bool(int(content.json().get('result').get(str(recs.code))) >= recs.result_min and int(content.json().get('result').get(str(recs.code))) <= recs.result_max):
                                        risk_dict = (0,0,{'name' : recs.name,'state':'OK','riskcheck_id':recs.id,'result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    elif bool(int(content.json().get('result').get(str(recs.code)))>= recs.result_min and int(content.json().get('result').get(str(recs.code))) <= recs.result_max):
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,
                                                          'state':'OK','result_min':recs.result_min,
                                                          'result_max':recs.result_max,
                                                          'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                                    else:
                                        risk_dict = (0,0,{'name' : recs.name,'riskcheck_id':recs.id,'state':'Failed','result_min':recs.result_min,'result_max':recs.result_max,'result_obtained':content.json().get('result').get(str(recs.code)),'list_seq':recs.list_seq})
                                        risk_list.append(risk_dict)
                        else:
                            raise Warning ("This account doesn't have any risk check list")
                    rec.write({'riskcheck_ids':risk_list})

                if int(content.json().get('code'))==200:
                    stage1 = True
                    query_cust = "Update vb_customer set ramci_kyc_date=%s,ramci_nrvb_date=%s where id=%s"
                    self._cr.execute(query_cust,(datetime.now(),datetime.now(),rec.customer_id.id))
                url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/'+str(url_rec.parm6)
                data = dumps({ "loginName" : rec.customer_id.login_name,
                              "acctId" : str(rec.id)})
                headers={'Content-type': 'application/json'}
                content = requests.post(url,data=data,headers=headers)
                doc_rec = self.env['vb.document']
                x =content.text.split(',')
                stage2 = True
                rec.update({'nrvb_clicked_flag':True})
                list = []
                doc_list =[]
                doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm2
                file_loc = doc_directory +'RAMCI/'
                if os.path.exists(file_loc)== False:
                    os.makedirs(file_loc)
                for recs in self.env['vb.document'].search([('acct_id','=',rec.id),('customer_id','=',rec.customer_id.id)]):
                    doc_list.append(recs.file_name)
                for i in x:
                    index = x.index(i)
                    if 'KYCPDF=' in i:
                        file_name_kyc = str('KYCPDF'+str(rec.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_kycpdf = str(file_loc + file_name_kyc)
                        rec_check = self.env['vb.document'].search([('customer_id','=',rec.customer_id.id),('acct_id','=',rec.id),('name','=',"KYC report")])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)

                        if file_name_kyc not in doc_list or bool(doc_list) == False:
                            kycpdf = base64.b64decode(x[index].split('KYCPDF=')[1])
                            f = open(file_path_kycpdf,'wb')
                            f.write(kycpdf)
                            f.close()
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',rec.customer_id.id,
                                                rec.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                    elif 'NRVBPDF' in i:
                        file_name_nrvb=str('NRVBPDF'+str(rec.id)+'_'+str(datetime.now())+'.pdf')
                        file_path_nrvbpdf = str(file_loc + file_name_nrvb)
                        rec_check = self.env['vb.document'].search([('customer_id','=',rec.customer_id.id),('acct_id','=',rec.id),('name','=','NRVB report')])
                        if rec_check:
                            for i in rec_check:
                                query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                                self._cr.execute(query)
                        if file_name_nrvb not in doc_list or bool(doc_list) == False:
                            nrvbpdf = base64.b64decode(x[index].split('NRVBPDF=')[1])
                            g = open(file_path_nrvbpdf,'wb')
                            g.write(nrvbpdf)
                            g.close()
                            check_doc1 = self.env['vb.document'].search([('acct_id','=',rec.id),('file_name','=',file_name_nrvb),('file_loc','=',file_path_nrvbpdf)])
                            query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                            self._cr.execute(query,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',rec.customer_id.id,
                                                    rec.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                    'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                query = "update vb_customer_acct set ramci_contra1='t' where id=%s"%rec.id
                self._cr.execute(query)
                del doc_list[:]

    @api.multi
    def get_prev_ramci(self):
        three_m_before = str(datetime.now() + relativedelta(months=-3))
        acct_rec = self.env['vb.customer_acct'].search([('id','=',self._context.get('active_id'))])
        exist_acct = self.env['vb.customer_acct'].search([('customer_id','=',acct_rec.customer_id.id),('id','!=',self._context.get('active_id')),('state','=','Active')])
        last_nrvb = '0'
        last_id = 0
        is_result = False

        if not acct_rec.acct_class_id:
            raise Warning("Product has not been specified for this Account")
        else:
            acct_class_rec= acct_rec.acct_class_id
            if not acct_class_rec.riskprof_id:
                raise Warning("Risk profile for the Product cannot be located")
            else:
                riskprof_rec= acct_class_rec.riskprof_id
                if not riskprof_rec.checklist_ids:
                    raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                else:
                    checklist_state= riskprof_rec.checklist_ids
        if exist_acct:
            for i in exist_acct:
                if i.customer_id.ramci_nrvb_date >= three_m_before:
                    if i.customer_id.ramci_nrvb_date >= last_nrvb:
                        last_nrvb = i.customer_id.ramci_nrvb_date
                        last_id = i.id
            if last_nrvb > '0':
                current_ramci_ids = ''
                riskcheck_rec = self.env['vb.customer_acct_riskcheck'].search([('acct_id','=',last_id)])
                print riskcheck_rec
                current_riskcheck = self.env['vb.customer_acct_riskcheck'].search([('acct_id','=',acct_rec.id)])
#                 for i in riskcheck_rec:
#                     current_ramci_ids+=str(i.id)+','
                for i in riskcheck_rec:
                    for n in checklist_state:

                        if i.code == n.code:

                            query = "update vb_customer_acct_riskcheck set state=%s,result_obtained=%s where acct_id=%s and code=%s"
                            self._cr.execute(query,(i.state,i.result_obtained,self._context.get('active_id'),n.code))
                for i in self.env['vb.document'].search([('acct_id','=',last_id),('is_open_exist','=','RAMCI')]):

                    query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                             customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                              VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''

                    self._cr.execute(query,(i.file_loc,i.name,i.doc_version,i.file_name,i.is_open_exist,i.customer_id.id,
                                                    self._context.get('active_id'),i.doc_class_id.id,
                                                    i.permission,i.state,datetime.now(),datetime.now(),self._uid,self._uid,True))


#                             delete_query = "DELETE from vb_customer_acct_riskcheck where id =%s and acct_id=%s"
#                             self._cr.execute(delete_query,(i.id,self._context.get('active_id')))
#                 for i in riskcheck_rec:
#                     print i.id
#                     delete_query = "DELETE from vb_customer_acct_riskcheck where id =%s and acct_id=%s"
#                     self._cr.execute(delete_query,(i.id,self._context.get('active_id')))
            acct_rec.update({'nrvb_clicked_flag':True})

    @api.multi
    def upload_doc(self):
        if self.upload_document:
            if os.path.isdir(tempfile.gettempdir()) == False:
                new_dir =  tempfile.mkdtemp()
            file_path = tempfile.gettempdir()+'/'+str(self.file_name)
            with open(file_path, 'wb') as new_file:
                decoded_data = base64.b64decode(self.upload_document)
                new_file.write(decoded_data)
            wb = xlrd.open_workbook(file_path)
            sh = wb.sheet_by_index(0)
            fh = open(file_path.split('.')[0]+'.csv',"wb")
            csv_out = unicodecsv.writer(fh, encoding='utf-8')
            for row_number in xrange (sh.nrows):
                csv_out.writerow(sh.row_values(row_number))
            fh.close()
            os.remove(file_path)
            file=open(file_path.split('.')[0]+'.csv', "r")
            lst = []
            reader = csv.reader(file)
            for line in reader:
                lst.append(line)
            last_number = self.env['vb.config'].search([('code','=','TempAsset'),('code_type','=','Sys')])
            last_processing_date = str(last_number.parm2)
            if last_processing_date and last_processing_date != 'False':
                if str(date.today()) > last_processing_date:
                    count = 1
                else:
                    count = int(last_number.parm1)
            else:
                count = 1
            for i in lst[1:]:
                try:
                    excel_date = ((i[lst[0].index('expired_date')]) if bool(i[lst[0].index('expired_date')]) else False)
                    if excel_date != False:
                        excel_date_final =  int(float(excel_date))
                    query = """INSERT INTO vb_temp_asset (name,asset_code,expired_date,share_multiplier,index_name,price_cap_pct,price_cap,
                             max_buy_amt,max_buy_qty,max_sell_amt,asset_class,max_sell_qty,process_date,process_status,batch_no,market_board,asset_profile_code,marginable) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                    self._cr.execute(query,(str(i[lst[0].index('name')]),
                                            str(i[lst[0].index('asset_code')]),
                                            (str((datetime.fromordinal(datetime(1900, 1, 1).toordinal() + excel_date_final - 2) if bool(excel_date) else '')).split(' ')[0] if bool(excel_date) else ''),
                                            (float(i[lst[0].index('share_multiplier')]) if bool(i[lst[0].index('share_multiplier')]) else 0.0),
                                            (str(i[lst[0].index('index_id')]) if bool(i[lst[0].index('index_id')]) else ''),
                                            float(i[lst[0].index('price_cap_pct')]),
                                            float(i[lst[0].index('price_cap')]),
                                            float(i[lst[0].index('max_buy_amt')]),
                                            float(i[lst[0].index('max_buy_qty')]),
                                            float(i[lst[0].index('max_sell_amt')]),
                                            str(i[lst[0].index('asset_class')]),
                                            float(i[lst[0].index('max_sell_qty')]),
                                            datetime.now(),str('New'),
                                            str(date.today()).replace('-','')+'-'+"%04d" % (count,),
#                                             str(date.today()),
                                            str(i[lst[0].index('market_board')]),
                                            str(i[lst[0].index('asset_profile_id')]),
                                            (True if bool(i[lst[0].index('marginable')]) else False),))
                except ValueError, e:
                    print e,ValueError,i
            os.remove(file_path.split('.')[0]+'.csv')
            count += 1
            last_number.update({'parm1':count,'parm2':str(date.today())})
        else:
            raise Warning("You should select a file to upload")

    @api.multi
    def reject_whitelist(self):
        rec = self.env['vb.bankacct_whitelist'].search([('id','=',self._context.get('active_id')),('state','in',['New','Approved']),('active','=',True)])
        if rec:
            d1 = lambda x: datetime.now()
            rec.update({'state':'Rejected','rejected_by':self._uid,'rejected_date':d1(datetime.now()),'reason_rejected_id':self.reason_reject.id,'reason_rejected':self.note})
        else:
            raise Warning("The record is not in New state")

    @api.multi
    def delete_whitelist(self):
        rec = self.env['vb.bankacct_whitelist'].search([('id','=',self._context.get('active_id')),('state','!=','Delete'),('active','=',True)])
        if rec:
            d1 = lambda x: datetime.now()
            rec.update({'state':'Deleted','approve_reject_by':self._uid,'approve_reject_date':d1(datetime.now())})
        else:
            raise Warning("The record is in Delete state")

    @api.multi
    def approve_whitelist(self):
        rec = self.env['vb.bankacct_whitelist'].search([('id','=',self._context.get('active_id')),('state','=','New'),('active','=',True)])
        if rec:
            d1 = lambda x: datetime.now()
            rec.update({'state':'Approved','approve_by':self._uid,'approve_date':d1(datetime.now()),'reason_rejected_id':self.reason_reject.id,'reason_rejected':self.note})
        else:
            raise Warning("The record is not in New state")

    @api.multi
    def complete_edd(self):
            rec = self.env['vb.customer_acct_cedd'].search([('id','=',self._context.get('active_id'))])
            if rec:
                if rec.is_edd_completed == False:
                    rec.is_edd_completed = True
                    return {
                        'type': 'ir.actions.client',
                        'tag': 'reload',
                        }
                elif rec.is_edd_reviewed == False:
                    rec.is_edd_reviewed = True
                    rec.acct_id.is_cedd_reviewed = True

            else:
                raise Warning("There is no record")
    @api.multi
    def approve_edd(self):
        rec = self.env['vb.customer_acct_cedd'].search([('id','=',self._context.get('active_id'))])
        if rec:
            rec.approve_edd = True
            rec.acct_id.is_cedd_done = True
            rec.state='EDDApproved'
        else:
            raise Warning("There is no record")


    @api.multi
    def sync_all_accounts_balance(self):
        if self.sync_options == 'Batch':
            self._cr.execute("SELECT public.pg_sync_acctbal()")
            output = self._cr.fetchone()[0]
            if output < 0:
                raise Warning("Process has not completed. Got response {0}".format(output))
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_portfolio','temp_asset_successful_wizard')[1]
                message = "Process completed successfully"
                return {
                    'name': 'Successfully batch portfolio',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                }
        elif self.sync_options == 'Single':
            failed_count = 0
            for i in self.acct_ids_sync:
                rec = self.env['vb.customer_acct_bal'].search([('id','=',self._context.get('active_id'))])
                self._cr.execute("SELECT sp_sync_acctbal_by_acct (%s)"%i.id)
                output = self._cr.fetchone()[0]
                if output < 0:
                    failed_count += 1
            if failed_count > 0:
                raise Warning(failed_count+" records failed to proceed")
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_portfolio','temp_asset_successful_wizard')[1]
                message = "Process completed successfully"
                return {
                    'name': 'Successfully Synched ',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                }
