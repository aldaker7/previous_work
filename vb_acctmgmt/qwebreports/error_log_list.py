from datetime import datetime, timedelta,date
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class error_log_list_report(models.AbstractModel):

    _name = 'report.vb_acctmgmt.error_log_list_template'

    @api.model
    def get_error_log_data(self, data):
        error_log_list = []
        for error_log_rec in self.env['vb.error_log'].browse(data):
           
            error_log_dict = {}
            #converting to regular date format
            #write_date = datetime.strptime(error_log_rec.write_date, '%Y-%m-%d %H:%M:%S' )
            #write_date = write_date.strftime("%d-%m-%Y")
            
            error_log_dict.update({
                                    'Function Name':error_log_rec.function_name,
                                    'Severity':error_log_rec.severity,
                                    'Created Date':error_log_rec.create_date,
                                    'Log message': error_log_rec.log_message                                
                                                                        
                                })
            error_log_list.append(error_log_dict)
        return error_log_list

    @api.multi
    def render_html(self, data):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        print 'my wizrd data',data['form']
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'error_log_data': self.get_error_log_data(data['ids']),
        }
         #converting to regular date format
        docargs['data']['start_date1']=datetime.strptime(docargs['data']['start_date1'], '%Y-%m-%d' )
        docargs['data']['start_date1']=docargs['data']['start_date1'].strftime("%d-%m-%Y")
        docargs['data']['end_date1']=datetime.strptime(docargs['data']['end_date1'], '%Y-%m-%d' )
        docargs['data']['end_date1']=docargs['data']['end_date1'].strftime("%d-%m-%Y")
        
        return self.env['report'].render('vb_acctmgmt.error_log_list_template', docargs)
