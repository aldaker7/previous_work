from datetime import datetime, timedelta
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class customer_list_report(models.AbstractModel):

    _name = 'report.vb_acctmgmt.customer_list_template'

    @api.model
    def get_customer_data(self, data):
        customer_list = []
        for customer_rec in self.env['vb.customer'].browse(data):

            customer_dict = {}
            customer_dict.update({
                                    'Customer Name':customer_rec.name,
                                    'Customer Type':customer_rec.customer_type,
                                    'Status': customer_rec.state,
                                    'Date Signed Up':customer_rec.date_signed_up,
                                    'Date Activated':customer_rec.date_activated,
                                    'Last Activity Date':customer_rec.last_activity_date

                                })
            bank_list = []
            if customer_rec.banks_ids:
                for bank_rec in customer_rec.banks_ids:
                    bank_dict = {}
                    bank_dict.update({
                                          'bank_id': bank_rec.bank_id.name or '',
                                          'bank_acct_no': bank_rec.bank_acct_no,
                                          'bank_branch': bank_rec.bank_branch
                                        })
                    bank_list.append(bank_dict)
                customer_dict.update({'bank_accounts' : bank_list})
            else:
                customer_dict.update({'bank_accounts' : bank_list})
            customer_list.append(customer_dict)

        return customer_list

    @api.multi
    def render_html(self, data):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'customer_data': self.get_customer_data(data['ids']),
        }
        return self.env['report'].render('vb_acctmgmt.customer_list_template', docargs)
