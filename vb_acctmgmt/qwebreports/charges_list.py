import datetime
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class charges_list_report(models.AbstractModel):

    _name = 'report.vb_acctmgmt.charges_list_template'
    

    @api.model
    def get_charges_list_data(self, data):
        charges_list = []
        for charges_list_rec in self.env['vb.charge_code'].browse(data):
           
            charges_list_dict = {}
           
           
            charges_list_dict.update({
                                    'Description':charges_list_rec.name,
                                    'Charge Code':charges_list_rec.code,
                                    'Charge Method': charges_list_rec.charge_method,
                                    'Charge Min': charges_list_rec.charge_min,
                                    'Charge Max': charges_list_rec.charge_max, 
                                    'Rate Type': charges_list_rec.rate_type, 
                                    'Rate Pct': charges_list_rec.rate_pct, 
                                    'Fixed Amount': charges_list_rec.fixed_amt,   
                                    'Per Value Of': charges_list_rec.per_value_of,                                           
                                    'PrintedOn':datetime.datetime.today().strftime('%d-%m-%Y'),
                                    'PrintedBy':self._get_user_name()                               
                                })
            charges_list.append(charges_list_dict)
            
        return charges_list
    

    @api.multi
    def render_html(self, data):
               
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('vb_acctmgmt.charges_list_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'charges_list_data':self.get_charges_list_data(self._ids)
        }
        return report_obj.render('vb_acctmgmt.charges_list_template', docargs)
       
    


    def _get_user_name(self, cr, uid, *args):
        #This function is to get the current logged in user
            user_obj = self.pool.get('res.users')
            user_value = user_obj.browse(cr, uid, uid)
            return user_value.name or False
        
        
   
       

