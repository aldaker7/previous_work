import datetime
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class asset_list_report(models.AbstractModel):

    _name = 'report.vb_acctmgmt.asset_list_template'
    

    @api.model
    def get_asset_list_data(self, data):
        asset_list = []
        for asset_list_rec in self.env['vb.asset'].browse(data):
           
            asset_list_dict = {}          
           
            asset_list_dict.update({
                                    'Asset Name':asset_list_rec.name,
                                    'Market':asset_list_rec.market_id.name,
                                    'Market Symbol':asset_list_rec.market_symbol,
                                    'Delivery Basis': asset_list_rec.dlv_basis_id.name,
                                    'Parent Asset': asset_list_rec.parent_asset_id.name,                                           
                                    'PrintedOn':datetime.datetime.today().strftime('%d-%m-%Y'),
                                    'PrintedBy':self._get_user_name()                               
                                })
            asset_list.append(asset_list_dict)
        return asset_list
    

    @api.multi
    def render_html(self, data):
               
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('vb_acctmgmt.asset_list_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'asset_list_data':self.get_asset_list_data(self._ids)
        }
        return report_obj.render('vb_acctmgmt.asset_list_template', docargs)
       
    


    def _get_user_name(self, cr, uid, *args):
        #This function is to get the current logged in user
            user_obj = self.pool.get('res.users')
            user_value = user_obj.browse(cr, uid, uid)
            return user_value.name or False


   
       

