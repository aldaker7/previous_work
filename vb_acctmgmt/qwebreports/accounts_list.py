from datetime import datetime, timedelta,date
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class accounts_list_report(models.AbstractModel):

    _name = 'report.vb_acctmgmt.accounts_list_template'

    @api.model
    def get_accounts_data(self, data):
        accounts_list = []
        for accounts_rec in self.env['vb.customer_acct'].browse(data):

            accounts_dict = {}
            #converting to regular date format
            date_activated = datetime.strptime(accounts_rec.date_activated, '%Y-%m-%d %H:%M:%S' )
            date_activated = date_activated.strftime("%d-%m-%Y")

            accounts_dict.update({
                                    'Account Name':accounts_rec.name,
                                    'Account Number':accounts_rec.acct_no,
                                    'Account Type':accounts_rec.acct_type,
                                    'Status': accounts_rec.state,
                                    'Date Activated':date_activated,
                                    'Customer Name':accounts_rec.customer_id.name

                                })
            accounts_list.append(accounts_dict)
        return accounts_list

    @api.multi
    def render_html(self, data):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        print 'my wizrd data',data['form']
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'accounts_data': self.get_accounts_data(data['ids']),
        }
         #converting to regular date format
        docargs['data']['start_date1']=datetime.strptime(docargs['data']['start_date1'], '%Y-%m-%d' )
        docargs['data']['start_date1']=docargs['data']['start_date1'].strftime("%d-%m-%Y")
        docargs['data']['end_date1']=datetime.strptime(docargs['data']['end_date1'], '%Y-%m-%d' )
        docargs['data']['end_date1']=docargs['data']['end_date1'].strftime("%d-%m-%Y")

        return self.env['report'].render('vb_acctmgmt.accounts_list_template', docargs)
