# -*- coding: utf-8 -*-

{
    "name": "Clean backend theme",
    "version": "1.0",
    "depends": ['web'],
    "license": 'AGPL-3',
    'author': 'VBroker Dev Team',
    'category': 'Themes/Backend',
    'description': """ 
    This module allows Web backend clean and to fold and unfold left menu bar. It will allow user to display view in full screen mode. It will add a new icon button on the leftpanel.
    """,
    "data": ['views/template.xml'],
    'qweb' : ["static/src/xml/*.xml"],
    "auto_install": False,
    "installable": True,
    "application": False,
}
