odoo.define("vb_backend_theme .title",function (require) {
    var core = require('web.core');
    var WebClient =require('web.WebClient');
    var QWeb = core.qweb,
    _t = core._t;   

    WebClient.include({
        start: function() {
            this.set('title_part', {"zopenerp": "Rakuten Trade"});
            return this._super();
            },
        });
});