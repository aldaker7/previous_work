odoo.define('vb_backend_theme.menu_accordion', function(require) {
    "use strict";

    var WebClient = require('web.WebClient');

    WebClient.include({
        attach_accordion_events: function() {
            
            var self = this;
            self.$('.oe_secondary_submenu').hide();
            self.$('.oe_secondary_menu').on('click', 'div.oe_secondary_menu_section', function(ev) {
                var submenu = $(ev.currentTarget).next();
                self.$('.oe_secondary_submenu').not($(ev.currentTarget).next()).hide();
                if (submenu.hasClass('oe_secondary_submenu') === true) {
                    if(!submenu.is(':visible')){
                        submenu.slideToggle();
                    }
                }
            });
            // self.$('.oe_secondary_menus_container').on('click','div.oe_secondary_menu_section', function(ev) {
            //     ev.preventDefault();
            //      ev.stopPropagation();
            //     var link = $(ev.currentTarget).find('a');
            //     console.log(link);
            //     if (link !== undefined) {
            //         link.trigger('click');
            //     }
            // });
        },
        show_application: function() {
            this._super();
            this.attach_accordion_events();
        },
    });
});
