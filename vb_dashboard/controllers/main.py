# -*- coding: utf-8 -*-
import logging

from openerp import http
from openerp.http import request

_logger = logging.getLogger(__name__)

class BarcodeController(http.Controller):

    @http.route(['/customer_overview/'], type='http', auth='user')
    def customer_overview(self, debug=False, **k):
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/customer_overview')
        return request.render('vb_dashboard.customer_overview')

