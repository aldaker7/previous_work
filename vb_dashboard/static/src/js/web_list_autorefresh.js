odoo.define('autorefresh.autorefresh',function(require){
    "use strict";

    var core = require('web.core');
    var ListView = require('web.ListView');
    var FormView = require('web.FormView');
    var CalendarView = require('web_calendar.CalendarView');
    var DashBoard = core.form_tag_registry.get('board');
    var Model = require('web.Model');




    ListView.include({
        init: function(parent, dataset, view_id, options) {
            var self = this;
            this._super.apply(this, arguments);
            if(parent.action && parent.action.auto_refresh > 0){
                self.refresh_interval = setInterval(_.bind(function(){
                    if(this.$el[0].clientWidth != 0){
                        this.reload();
                    }
                }, self), parent.action.auto_refresh*60000);
            }
        },
        destroy : function() {
            this._super.apply(this, arguments);
            if(this.refresh_interval){
                clearInterval(this.refresh_interval);
            }
        }
    });

    DashBoard.include({
        init: function(view, node) {
            var self = this;
            this._super.apply(this, arguments);
            if(view.ViewManager && view.ViewManager.action && view.ViewManager.action.auto_refresh > 0){
                self.refresh_interval = setInterval(_.bind(function(){
                    if(this.$el[0].clientWidth != 0){
                        this.do_reload();
                    }

                }, self),  view.ViewManager.action.auto_refresh*60000);
            }
        new Model("vb.button.demo").call("run_auto_sp"); },
    });

    FormView.include({
        init: function(parent, dataset, view_id, options) {
            var self = this;
            this._super.apply(this, arguments);
            if(parent.action && parent.action.auto_refresh > 0){
                self.refresh_interval = setInterval(_.bind(function(){
                    if(this.$el[0].clientWidth != 0 && this.dataset.index != null){
                        this.reload();
                    }
                }, self), parent.action.auto_refresh*60000);
            }
        },
        destroy : function() {
            this._super.apply(this, arguments);
            if(this.refresh_interval){
                clearInterval(this.refresh_interval);
            }
        }
    });

    CalendarView.include({
        init: function(parent, dataset, view_id, options) {
            var self = this;
            this._super.apply(this, arguments);
            if(parent.action && parent.action.auto_refresh > 0){
                self.refresh_interval = setInterval(_.bind(function(){
                    if(this.$el[0].clientWidth != 0){
                        this.do_search(this.dataset.domain, this.dataset.context, []);
                    }
                }, self), parent.action.auto_refresh*60000);
            }
        },
        destroy : function() {
            this._super.apply(this, arguments);
            if(this.refresh_interval){
                clearInterval(this.refresh_interval);
            }
        }
    });
});
