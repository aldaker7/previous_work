
$(document).find('.acccount-nav-tabs li a:first-child').addClass('active');

odoo.define('vb_dashboard.vb_dashboard', function (require) {

    var core = require('web.core');
    var odoo = require('web.ajax');
    var Model = require('web.Model');
    var WebClient = require('web.WebClient');
    var QWeb = core.qweb;

    $("a[href='#cash']").css({'border-left': '1px solid #ccc', 'border-right': '1px solid #ccc', 'border-top': '1px solid #ccc'});

    $(document).ready(function(){
        var data_dict = {};
        var windows = {};
        var customer_id = location.search.split('customer_id=')[1];
        if (customer_id != null){
        	$("#advanced-search").text(customer_id);
        	customer_data_fetch(customer_id);
        }
		$('#no_customer_alert').hide();
        //Search Button Call
        $( "#search-button" ).click(function () {
        	var search_input_text = $( "#advanced-search" ).val();
        	customer_data_fetch(search_input_text);
        });
       
        function customer_data_fetch(search_input_text){
            //Calling Py Method
            
            new Model('vb.customer').call('get_js_data', [search_input_text]).then(function (data) {
                //Setting a Global Variable, so that it can be used in another methods
                data_dict = data
                if (data == null){
                    $('#no_customer_alert').show();
                    $('#set-div').hide();
                }
                else {
                    $('#no_customer_alert').hide();
                    //Putting the customer data in Web View
                    $("#customer_name").text(data.customer_name);
                    $("#customer_code").text(data.customer_code);
                    $("#customer_class_id").text(data.customer_class_id);                    
                    $("#date_of_birth").text(data.date_of_birth);                    
                    $("#status").text(data.status);
                    $("#last_trans_date").text(data.last_trans_date);
//                     $("#days1").text(data.days1);
                    $("#national_id_no").text(data.national_id_no);
                    $("#date_activated").text(data.date_activated);
                    $("#days").text(data.days);
                    $("#gender_info").text(data.gender_info);
                    $("#gender_info1").text(data.gender_info1);
                    $("#mobile_phone1").text(data.mobile_phone1);
                    $("#home_phone").text(data.home_phone);
                    $("#email1").text(data.email1);
                    $("#email1").attr('href','mailto:'+data.email1);
                    $("#email2").text(data.email2);
                    $("#email2").attr('href','mailto:'+data.email2);
                    $("#nok_ids").text(data.nok_ids);
                    $("#comments").text(data.comments);
                    $("#year").text(data.year);
                    //Getting Customer Image
                    profile_picture_url = get_customer_image_url(data.customer_id);
                    $("#profile_picture").attr("src", profile_picture_url)
                    //Setting permanent address
                    $("#address").text(data.permanent_address.permanent_address);
                    $("#update_address").attr({
                    							'address_id': data_dict.permanent_address.permanent_address_id,
                    							'form_id' : data_dict.permanent_address.form_id
                    						});
                    
                    $("#employer_detail").text(data.employer_detail);
                    $("#employer_occupation").text(data.employer_occupation);
                    //Filling the Account Tabs dynamically
                    $('.acccount-nav-tabs li').remove();
                    if (data.account_data.length === 0) {
                        //If no customer dont have any accounts
                        $('#no_accounts_alert').show();
                        $('#account_contents').hide();
                        $('#account_content_two').hide();
                    } else {
                        //If customer account exists
                        $('#no_accounts_alert').hide();
                        $('#account_contents').show();
                        $('#account_content_two').show();
                        jQuery.each(data.account_data, function(i, val) {
                            $('.acccount-nav-tabs').append('<li><a class="account-details"><span>' + val.account_type + '</span><br /><span>' + val.account_no + '</span></a></li>');
                        });
                    }
                    //Showing the primary div
                    $("#set-div").show();
                    //Onclick event on accounts
                    $('.acccount-nav-tabs li:first-child .account-details').click()
                }
            });            
           
        }

        function openWindow(url) {
            windows[name] = window.open(url, "_self");
            return windows[name];
        }

        function get_customer_image_url(id){
            return window.location.origin + '/web/binary/image?model=vb.customer&field=picture&id='+id;
        }

        //Method to fetch account data
        function account_data_fetch(){

            var self = $(this);
            $('#account_content_one,#account_content_two').fadeOut();
            jQuery.each(data_dict.account_data, function(i, val) {
                if (val.account_no == self.children().last().text()) {
                    $("#cds_no").text(val.cds_no);
                    $('#bank_acct').text(val.bank_acct);
                    $('#purchasing_power').text(val.purchasing_power);
                    $('#cash_in_trust').text(val.cash_in_trust);
                    $('#cash_earmarked').text(val.cash_earmarked);
                    $('#available_cash').text(val.available_cash);
                    $('#due_from_client').text(val.due_from_client);
                    $('#due_to_client').text(val.due_to_client);
                    $('#net_due_to_client').text(val.net_due_to_client);                    
                    return false;
                }
            });            
             //Filling the security questions table
            $('#secques_table tr:gt(0)').remove();
            jQuery.each(data_dict.sec_questions_data, function(i, val) {
                $('#secques_table').append('<tr><td>' + val.question_no + '</td><td>' + val.question_text + '</td><td>' + val.answer +'</td></tr>');
            });
                        
            //Filing the risk table
            jQuery.each(data_dict.account_data, function(i, val) {
                 if (val.account_no == self.children().last().text()) {
                     if (val.risk_data !== null){
                         $('#risk_table tr:gt(0)').remove();
                         jQuery.each(val.risk_data, function(j, risk){
                             $('#risk_table').append('<tr><td>' + risk.risk_area + '</td><td>' + risk.result + '</td><td>' + risk.date_checked +'</td></tr>');
                         });
                     }
                 }
            });
            
            
            //Filing the Helpdesk table
            $('#helpdesk_table tr:gt(0)').remove();
            jQuery.each(data_dict.helpdesk_case_data, function(i, val) {
                if (val.account_no == self.children().last().text()) {
                    $('#helpdesk_table').append('<tr><td>' + val.date_helpdesk + '</td><td>' + val.description_helpdesk + '</td><td>' + val.status_helpdesk +'</td></tr>');
                }
            });

            //Filing the Activity table
            $('#activity_table tr:gt(0)').remove();
            jQuery.each(data_dict.acct_activity_data, function(i, val) {
                if (val.account_no == self.children().last().text()) {
                    $('#activity_table').append('<tr><td>' + val.date_activity + '</td><td>' + val.description_activity + '</td><td>' + val.status_activity +'</td></tr>');
                }
            });

            //Filing the communication table
            $('#communications_table tr:gt(0)').remove();
            jQuery.each(data_dict.acct_communication_data, function(i, val) {
                if (val.account_no == self.children().last().text()) {
                    $('#communications_table').append('<tr><td>' + val.date_comm + '</td><td>' + val.description_comm + '</td><td>' + val.title_comm +'</td></tr>');
                }
            });

            //Filing the Document table
            $('#documents_table tr:gt(0)').remove();
            jQuery.each(data_dict.document_data, function(i, val) {
                if (val.account_no == self.children().last().text()) {
                    $('#documents_table').append("<tr><td>" + "<a href='/web/binary/download_document?id=" +val.document_id+ "' style='text-decoration: none; cursor:pointer;' class='file_class' document_id='" + val.document_id + "' file_loc='"+ val.file_loc + "'>" + val.file_name + '</a>' + '</td>');
                }
            });

            $('#account_content_one,#account_content_two').fadeIn();
            $(this).addClass('active_account_tab');
            $(document).find('.account-details').not(this).each(function () {
                $(this).removeClass('active_account_tab');
            })
        }

        //Account tab onclick
        $(document).on('click', '.account-details', account_data_fetch)

        //Onclick event on  summary
        $("#set-div,#charges,#risk").hide();
        $('#a-summary').children().css('color', 'black');
        $('#a-summary').click(function () {
            $('#charges,#risk').fadeOut();
            $('#summary').fadeIn();
            $(this).children().css('color', 'black');
            $('#a-charges,#a-risk').children().css('color', 'cornflowerblue');
        });

      	//Onclick event on  Charges
        $('#a-charges').click(function () {
            $('#summary,#risk').fadeOut();
            $('#charges').fadeIn();
            $(this).children().css('color', 'black');
            $('#a-summary,#a-risk').children().css('color', 'cornflowerblue');
        });

      	//Onclick event on  risk
        $('#a-risk').click(function () {
            $('#charges,#summary').fadeOut();
            $('#risk').fadeIn();
            $(this).children().css('color', 'black');
            $('#a-charges,#a-summary').children().css('color', 'cornflowerblue');
        });

      	//Onclick event on  Permamnent Address
        $('#permanent_address').click(function () {
        	$("#address").text(data_dict.permanent_address.permanent_address);
        	$("#update_address").attr({
										'address_id': data_dict.permanent_address.permanent_address_id,
										'form_id' : data_dict.permanent_address.form_id
									});
        });

        //Onclick event on  Work Address
        $('#work_adddress').click(function () {
        	$("#address").text(data_dict.work_address.work_address);
        	$("#update_address").attr({
				'address_id': data_dict.work_address.work_address_id,
				'form_id' : data_dict.permanent_address.form_id
			});
        });

        //Onclick event on  Residential Address
        $('#residential_adddress').click(function () {
        	$("#address").text(data_dict.residential_address.residential_address);
        	$("#update_address").attr({
				'address_id': data_dict.residential_address.residential_address_id,
				'form_id' : data_dict.permanent_address.form_id
			});
        });
        
      //Onclick event to Open Odoo Views
        $('.open_view').click(function () {
        	if ($(this).getAttributes().id == 'update_address')
        		//window.location.href = _.str.sprintf('/web?db=%s#id=%s&view_type=form&model=calendar.event', db, id);
        		window_location = _.str.sprintf('/web#id=%s&view_type=form&view_id=%s&model=vb.customer_address',$(this).getAttributes().address_id,$(this).getAttributes().form_id);
        		openWindow(window_location);
        });

    });

});