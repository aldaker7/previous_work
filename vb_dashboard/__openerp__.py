# -*- coding: utf-8 -*-

{
    'name': 'VB Dashboard',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Dashboard',
    'depends': ['vb_helpdesk','board','vb_document','base','web','web_calendar'],
    'description': """

This is the Dashboard module for Virtual Broker Application.
==========================================================
""",
    'data': [
        'security/ir.model.access.csv',
        'views/actions_dashboard.xml',        
        'views/views_dashboard.xml',
        'views/views_operation.xml',
        'views/views_cashmanagement.xml',
        'views/views_settlement.xml',
        'views/views_customeroverview.xml',        
        'views/menus_dashboard.xml',
        'views/data.xml',        
        'views/template.xml',  
    ],
    'qweb':['static/src/xml/*.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
