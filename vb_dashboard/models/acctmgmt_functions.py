from openerp import models, fields, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from openerp.exceptions import Warning
from lxml import etree
from openerp.osv.orm import setup_modifiers
from json import dumps
import requests


class customer(models.Model):
    _inherit = 'vb.customer'   

    @api.model
    def get_address_info(self, adrss_rec):
        if adrss_rec.address1:
            new_adr = str(adrss_rec.address1) + ',' + '\n'
        if adrss_rec.address2:
            new_adr = new_adr + str(adrss_rec.address2) + ',' + '\n'
        if adrss_rec.address3:
            new_adr = new_adr + str(adrss_rec.address3) + '\n'
        if adrss_rec.postcode:
            new_adr = new_adr + str(adrss_rec.postcode) + ','
        if adrss_rec.state_id:
            new_adr = new_adr + str(adrss_rec.state_id.name) + ','
        if adrss_rec.country_id:
            new_adr = new_adr + str(adrss_rec.country_id.name)
        return new_adr

    @api.model
    def get_employer_data(self, occupation):
        if occupation.employer:
            emp_details = "Employed at" + ' ' + str(occupation.employer) 
        else:  
            emp_details = " "      
        if occupation.designation:
            emp_details = emp_details + ' ' + 'as' + ' ' + str(occupation.designation)
        else:
            emp_details = "No Employment Data"
        if occupation.date_start:
            emp_details = emp_details + ' ' + 'since' + ' ' + str(occupation.date_start)
        else:
            emp_details = emp_details + ' ' + 'since' + ' '
        return emp_details
    
    @api.model
    def get_employer_occupation(self, occupation):
        if occupation.employer:
            emp_occupation = str(occupation.designation)
        else:
            emp_occupation = " "
        return emp_occupation

    @api.model
    def get_js_data(self, search_input_text):

        helpdesk_case_list = []
        acct_activity_list = []
        account_data_list = []
        risk_data_list = []
        trans_list = []
        cust_document_list = []
        communication_list = []
        sec_ques_list = []

        account_rec = self.env['vb.customer_acct'].search([('acct_no','=',search_input_text)])
        if account_rec:
            customer_rec = account_rec.customer_id
        else:
            customer_rec = self.search([('code','=',search_input_text)])
        if customer_rec:
            residential_address = employer_detail = employer_occupation = work_address = permanent_address= last_trans_date = ''
            form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'view_customer_address_overview_form')[1]
            #Employer Data
            if customer_rec.jobs_ids:
                for occupation in customer_rec.jobs_ids:
                    employer_detail = self.get_employer_data(occupation)
                    employer_occupation = self.get_employer_occupation(occupation)
            #Customer Address Data
            if customer_rec.address_ids:
                for adrss_rec in customer_rec.address_ids:
                    if adrss_rec.address_type == 'Permanent':
                        permanent_address = {'permanent_address_id': adrss_rec.id,
                                             'permanent_address': self.get_address_info(adrss_rec),
                                             'form_id' : form_id}
                    if adrss_rec.address_type == 'Work':
                        work_address = {'work_address_id': adrss_rec.id,
                                        'work_address': self.get_address_info(adrss_rec),
                                        'form_id' : form_id}
                    if adrss_rec.address_type == 'Residential':
                        residential_address = {'residential_address_id' : adrss_rec.id,
                                               'residential_address' : self.get_address_info(adrss_rec),
                                               'form_id' : form_id}
            date_of_birth = datetime.strptime(customer_rec.date_of_birth, "%Y-%m-%d")
            no_of_year = relativedelta(datetime.now(), date_of_birth).years
            if customer_rec.date_activated:            
                date_activated = datetime.strptime(customer_rec.date_activated, "%Y-%m-%d %H:%M:%S")               
                act_diff = relativedelta(datetime.now(), date_activated)
                act_years = act_diff.years
                act_months = act_diff.months
                act_days = act_diff.days
            else:
                act_diff = 0
                act_years = 0
                act_months = 0
                act_days = 0

            #Last Transaction Date
            query = "select cash_trans.create_date, trade_trans.create_date, portfolio_trans.create_date\
                    from vb_customer cust_rec\
                    left join vb_customer_acct acct on acct.customer_id = cust_rec.id\
                    left join vb_cash_tran cash_trans on cash_trans.acct_id = acct.id\
                    left join vb_trade_tran trade_trans on trade_trans.acct_id = acct.id\
                    left join vb_portfolio portfolio_trans on portfolio_trans.acct_id = acct.id\
                    where cust_rec.id = %s;" % customer_rec.id
            self._cr.execute(query)
            trans_date_data = list(set(self._cr.fetchall()))
            if trans_date_data:
                for rec_trans in trans_date_data:
                    for date in rec_trans:
                        if date != None:
                            trans_list.append(date)                            
                            last_trans_date = datetime.strptime(max(trans_list), "%Y-%m-%d %H:%M:%S.%f").strftime("%d-%m-%Y")
#                             no_of_days1 = relativedelta(datetime.now().date(), last_trans_date).days
            #customer Data
            customer_data_dict = {
                                  'customer_id' : customer_rec.id,
                                  'customer_name': customer_rec.name or '',
                                  'permanent_address': permanent_address or '',
                                  'work_address' : work_address or '',
                                  'residential_address' : residential_address or '',
                                  'last_trans_date': last_trans_date or '',
#                                    'days1': '('+ str(no_of_days1) + ' '+ 'days1'+')',
                                  'customer_code': customer_rec.code or '',
                                  'status': customer_rec.state or '',
                                  'customer_class_id': customer_rec.customer_class_id.name or '',                                  
                                  'date_of_birth': customer_rec.date_of_birth or '',
                                  'national_id_no': customer_rec.national_id_no + ' ' + '(' + customer_rec.national_id_type_id.name + ')' or '',
                                  'date_activated': customer_rec.date_activated or '',
                                  'days': '('+ str(act_years) + ' '+ 'years '+ str(act_months) + ' months ' + str(act_days) + ' days)' or '',
                                  'gender_info': customer_rec.nationality_id.name + ' ' + customer_rec.gender or '',
                                  'gender_info1': customer_rec.marital_status + ' ' + customer_rec.race_id.name or '',
                                  'mobile_phone1': customer_rec.mobile_phone1 or '',
                                  'home_phone': customer_rec.home_phone or '',
                                  'email1': customer_rec.email1 or '',
                                  'email2': customer_rec.email2 or '',
                                  'comments': customer_rec.comments or '',
                                  'year': '('+ str(no_of_year) + ' '+ 'years'+')' or '',
                                  'employer_detail': employer_detail or '',
                                  'employer_occupation':employer_occupation or '',
                                  }

            #Nomination Data
            if customer_rec.nok_ids:
                for nok_rec in customer_rec.nok_ids:
                    customer_data_dict.update({'nok_ids': nok_rec.name})
                    
            #Customer Security Questions and Answer
            cust_sec_ques =  self.env['vb.customer_sec_question'].search([('customer_id', '=', customer_rec.id)], limit=5, order="id desc")
            if cust_sec_ques:
                for secques_rec in cust_sec_ques:
                    sec_ques_dict = {}
                    sec_ques_dict.update({
                                          'customer_id': secques_rec.customer_id.id or '',                                           
                                          'question_no': secques_rec.question_no or '',
                                          'question_text': secques_rec.question_text or '',
                                          'answer': secques_rec.answer or '',                                                   
                                        })
                    sec_ques_list.append(sec_ques_dict)
                   
            #Account Data
            for account_rec in customer_rec.acct_ids:
                account_data_dict = {}
                account_data_dict.update({
                                          'account_cust_id': account_rec.id,
                                          'account_no': account_rec.acct_no or '',
                                          'account_type': account_rec.acct_class_id.name or '',
                                          'cds_no' : account_rec.cds_no,                                                                                                                               
                                          'bank_acct' : account_rec.bank_acct_id.bank_acct_no + ' (' + account_rec.bank_acct_id.bank_name + ')' or '',                                            
                                         })
                balance_rec =  self.env['vb.customer_acct_bal'].search([('acct_id', '=', account_rec.id)], limit=1, order="id desc")
                if balance_rec:
                    account_data_dict.update({
                                              'cash_in_trust': balance_rec.cash_available_bal or 0.00,
                                              'cash_earmarked': balance_rec.cash_earmarked_amt or 0.00,
                                              'available_cash': abs((balance_rec.cash_available_bal or 0.00) - (balance_rec.cash_earmarked_amt or 0.00)),
                                              'due_from_client': balance_rec.trade_os_long_amt or 0.00 + balance_rec.trade_os_others_amt or 0.00,
                                              'due_to_client': balance_rec.trade_os_short_amt or 0.00,
                                              'net_due_to_client': abs((balance_rec.trade_os_long_amt or 0.00 + balance_rec.trade_os_others_amt or 0.00) + (balance_rec.trade_os_short_amt or 0.00))
                                             })
                    account_data_dict.update({
                                              'purchasing_power': account_data_dict.get('available_cash') + account_data_dict.get('net_due_to_client') or 0.00
                                             })

                #Risk Data
                for risk_rec in account_rec.riskcheck_ids:
                    risk_data_dict = {}
                    risk_data_dict.update({
                                           'risk_area': risk_rec.name or '',
                                           'result': risk_rec.state or '',
                                           'date_checked': risk_rec.when_last_checked or ''
                                          })
                    risk_data_list.append(risk_data_dict)
                    account_data_dict.update({'risk_data' : risk_data_list})
                account_data_list.append(account_data_dict)

                #Helpdesk Data
                helpdesk_case_rec =  self.env['vb.helpdesk_case'].search([('acct_id', '=', account_rec.id)], limit=5, order="id desc")
                if helpdesk_case_rec:
                    for case_rec in helpdesk_case_rec:
                        helpdesk_case_dict = {}
                        helpdesk_case_dict.update({
                                                   'account_no': case_rec.acct_id.acct_no or '',
                                                   'date_helpdesk': case_rec.when_requested or '',
                                                   'description_helpdesk': case_rec.name or '',
                                                   'status_helpdesk': case_rec.state or ''
                                                   })
                        helpdesk_case_list.append(helpdesk_case_dict)

                #Account Activity Data
                acct_activity_rec =  self.env['vb.customer_acct_activity'].search([('acct_id', '=', account_rec.id)], limit=5, order="id desc")
                if acct_activity_rec:
                    for activity_rec in acct_activity_rec:
                        acct_activity_dict = {}
                        acct_activity_dict.update({
                                                   'account_no': activity_rec.acct_id.acct_no or '',
                                                   'date_activity': activity_rec.activity_date or '',
                                                   'description_activity': activity_rec.activity_type or '',
                                                   'status_activity': activity_rec.state or ''
                                                   })
                        acct_activity_list.append(acct_activity_dict)

                #Communication Data
                cut_acct_comm_rec =  self.env['vb.msg_log'].search([('acct_id', '=', account_rec.id)], limit=5, order="id desc")
                if cut_acct_comm_rec:
                    for communication_rec in cut_acct_comm_rec:
                        communication_dict = {}
                        communication_dict.update({
                                                   'account_no': communication_rec.acct_id.acct_no or '',
                                                   'date_comm': communication_rec.msg_send_time or '',
                                                   'description_comm': communication_rec.msg_mode or '',
                                                   'title_comm': communication_rec.state or ''
                                                   })
                        communication_list.append(communication_dict)

                #Document Data
                cust_document_rec =  self.env['vb.document'].search([('acct_id', '=', account_rec.id)], limit=5, order="id desc")
                if cust_document_rec:
                    for document_rec in cust_document_rec:
                        cust_document_dict = {}
                        cust_document_dict.update({
                                                   'document_id': document_rec.id or '',
                                                   'account_no': document_rec.acct_id.acct_no or '',
                                                   'file_loc': document_rec.file_loc or '',
                                                   'file_name': document_rec.file_name or ''
                                                   })
                        cust_document_list.append(cust_document_dict)

            #Main Dictionary
            customer_data_dict.update({'account_data': account_data_list, 'helpdesk_case_data': helpdesk_case_list,
                                       'acct_activity_data': acct_activity_list, 'document_data': cust_document_list,
                                       'acct_communication_data': communication_list, 'sec_questions_data': sec_ques_list})
            return customer_data_dict
        else:
            return None
    
    @api.multi
    def resend_tempwd(self):
        if self.state in ['Active','New']:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt','resend_temp_pwd_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':"Resend temporary password",
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            raise Warning("This customer state either not activated or new yet")

    @api.multi
    def call_customer_overview(self):
        _inherit = 'vb.customer'
        return{
                    'name': "Customer Overview",
                    'type': 'ir.actions.act_url',                    
                    'url': '/customer_overview?customer_id=' + str(self.code),
                    'target': 'self',
                    }
    
    @api.multi
    def call_customer_overview1(self):
        if self.id:
            mod_obj = self.env['ir.model.data']
        try:
            form_res = mod_obj.get_object_reference('vb_dashboard', 'db_customer_overview_form')[1]
        except ValueError:
            form_res = False
        return{
               'name': "Customer Overview",
               'view_type': 'form',
               'view_mode':"[form]",               
               'view_id': form_res,
               'res_model': 'vb.customer',
               'type': 'ir.actions.act_window',
                'views': [(form_res, 'form')],
               'res_id': self.id,
               
               }
        
    @api.multi
    def enter_order_cash(self):
        login_name = self.login_name
        customer_name = self.name
        t_pin = self.env['res.users'].search([('id','=',self._uid)]).t_pin
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    accType='Cash Upfront'
                    acct_id=i.acct_no                    
                    if i.buy_suspended== True:
                        if i.sell_suspended== True:
                            trdsus='both'
                        else:
                            trdsus='buy'
                    elif i.sell_suspended== True:
                        trdsus='sell'
                    else:
                        trdsus='none'                    
#                     break
                    if i.state == 'Active':
                        rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','ASSTORDER')])
                        key = rec.parm2
                        return{
                            'name': "Customer Overview",
                            'type': 'ir.actions.act_url',                    
                            'url': str(rec.parm1)+'?&csuid='+str(self._uid)+'&uid='+login_name+'&uacc='+str(acct_id)+'&ucds='+str(self.env['vb.customer_acct'].search([('acct_no','=',acct_id)]).cds_no)+'&key='+str(key)+'&accType='+accType+'&tradeSuspended='+trdsus,
                            'target': 'new',
                        }
                    else:
                        raise Warning('This Account is not activated yet')
    @api.multi
    def get_customer(self):
        mod_obj = self.env['ir.model.data']
        try:
            form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_form')[1]
        except ValueError:
            form_res = False
        return{
               'name': "Customer Details",
               'view_type': 'form',
               'view_mode':"[form]",
               'view_id': form_res,
               'res_model': 'vb.customer',               
               'type': 'ir.actions.act_window',
               'views': [(form_res, 'form')],
               'res_id': self.id,
               
               }
        
    @api.multi
    def get_customer_acct(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    break
            if acct_id:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form1')[1]
                except ValueError:
                    form_res = False
                return{
                       'name': "Customer Account Details",
                       'view_type': 'form',
                       'view_mode':"[form]",
                       'view_id': form_res,
                       'res_model': 'vb.customer_acct',
                       'type': 'ir.actions.act_window',
                       'views': [(form_res, 'form')],
                       'res_id': acct_id,
                       
                       }
        else:
            raise Warning('There is no cashupfront account related to that customer ')
    
    @api.multi
    def create_case(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    break
            try:
                
                mod_obj = self.env['ir.model.data']
                form_res = mod_obj.get_object_reference('vb_helpdesk', 'view_case_form')[1]
            except ValueError:
                form_res = False
               
            return{
                   'name': "Create Case",
                   'view_type': 'form',
                   'view_mode':"[form]",
                   'view_id': form_res,
                   'res_model': 'vb.helpdesk_case',
                   'type': 'ir.actions.act_window',
                   'views': [(form_res, 'form')],
#                    'target': 'inline',
                   'context':{'default_customer_id':self.id}
                   }
    @api.multi
    def get_loyal_customer_cash(self):
        mod_obj = self.env['ir.model.data']
        try:
            tree_res = mod_obj.get_object_reference('vb_loyalty','view_customer_loyalty_balance_tree')[1]
            form_res = mod_obj.get_object_reference('vb_loyalty', 'view_customer_loyalty_bal_form')[1]
        except ValueError:
            form_res = False
        return{
               'name': "Customer Loyalty",
               'view_type': 'form',
               'view_mode':"[tree,form]",
               'view_id': form_res,
               'res_model': 'vb.customer_loyalty_bal',
               'type': 'ir.actions.act_window',
               'views': [(tree_res,'tree'),(form_res, 'form')],
               'domain':[('customer_id','=',self.id)]
               }    
    @api.multi
    def get_customer_bal_cashupfront(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    break
                
            if acct_id:
                if self.env['vb.customer_acct_bal'].search([('acct_id','=',acct_id)]):
                    mod_obj = self.env['ir.model.data']
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                           'name': "Customer Account Details",
                           'view_type': 'form',
                           'view_mode':"[form]",
                           'view_id': form_res,
                           'res_model': 'vb.customer_acct_bal',
                           'type': 'ir.actions.act_window',
                           'views': [(form_res, 'form')],
                           'res_id': self.env['vb.customer_acct_bal'].search([('acct_id','=',acct_id)]).id,
                           
                           }
            else:
                raise Warning('There is no cashupfront account balance related to that customer ')
    
    @api.multi
    def get_order_book_cashupfront(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    break
            if acct_id:
                if self.env['vb.order_book'].search([('acct_id','=',acct_id)]):
                    mod_obj = self.env['ir.model.data']
                    try:
                        form_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_form1')[1]
                        tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_tree1')[1]
                        search_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_search1')[1]
                    except ValueError:
                        form_res = False
                    
                    return{
                           'name': "Transactions Trade Details",
                           'view_type': 'form',
                           'view_mode':"[tree,form]",
                           'view_id': False,
                           'res_model': 'vb.order_book',
                           'type': 'ir.actions.act_window',
                           'views': [(tree_res, 'tree'), (form_res, 'form')],
                           'domain': [('acct_id','=',acct_id)],
                           'context': {'enable_date_range_bar':1,'group_by':'order_type', 'search_fields': 'tran_date'},
                           'search_view_id':[(search_res)],                   
                           }
                else:
                    raise Warning('There is no related order to that account ')
                
    @api.multi
    def get_portfolio_cashupfront(self):
        is_cash = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    is_cash = True
                    break
            if is_cash:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_search')[1]
                except ValueError:
                    form_res = False
                    tree_res = False
                    search_res = False
                return{
                       'name': "Portfolio Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.portfolio',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
                
    @api.multi
    def get_order_activity_cashupfront(self):
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_no=i.acct_no
                    break
            if acct_no:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_order_transaction_form')[1]
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_tree')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_search')[1]
                except ValueError:
                    form_res = False
                
                return{
                       'name': "order Activity Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.order_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_no','=',acct_no)],
                       'context': {'search_default_current_month':1,'group_by':'order_no','enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning('There is no Account number for this account')
            
    @api.multi
    def get_outstanding_trade_cashupfront(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    break
            if acct_id:
                if self.env['vb.trade_tran'].search([('acct_id','=',acct_id)]):
                    mod_obj = self.env['ir.model.data']
                    try:
                        form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
                        search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
                        tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_cus_overview_tree1')[1]
                    except ValueError:
                        form_res = False
                    
                    return{
                           'name': "Outstanding Trade Transaction Details",
                           'view_type': 'form',
                           'view_mode':"[tree,form]",
                           'view_id': False,
                           'res_model': 'vb.trade_tran',
                           'type': 'ir.actions.act_window',
                           'views': [(tree_res, 'tree'), (form_res, 'form')],
                           'domain': [('acct_id','=',acct_id),('state','=','Posted'),('pickup_flag','=',False),'|',('balance_qty','!=',0),('pending_qty','!=',0)],# Nailah asked to remove ('nocontra_flag','=',False) from this domain
                           'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date,date_posted'},
                       'search_view_id':[(search_res)],          
                           }
                else:
                    raise Warning('There is no related order to that account ')
        
    
    @api.model
    def get_years(self):
        query = 'Select date_of_birth,id,date_activated from vb_customer'
        self._cr.execute(query)
        sq=self._cr.fetchall()
        for i in sq:
            if i[0]!= None:
                s = datetime.strptime(i[0].split('.')[0],'%Y-%m-%d')
                final = str(s.strftime("%Y-%m-%d"))+' ('+str(relativedelta(datetime.now(), s).years)+' Years)'
                query1 = "Update vb_customer SET birth_years=%s Where id=%s"
                self._cr.execute(query1,(final,i[1]))
            if i[2]!=None:
                s1=datetime.strptime(i[2].split('.')[0],"%Y-%m-%d %H:%M:%S")
                final1 = str(s1.strftime('%Y-%m-%d %H:%M:%S'))+'('+str(relativedelta(datetime.now(), s1).years)+' Years,'+str(relativedelta(datetime.now(), s1).months)+' Months,'+str(relativedelta(datetime.now(), s1).days)+' Days)'
                query2 = "Update vb_customer SET customer_since=%s Where id=%s"
                self._cr.execute(query2,(final1,i[1]))
                
    @api.multi
    def get_trade_cashupfront(self):
        is_cash=False
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    acct_id=0
                    if i.acct_type=='Cash':
                        acct_id=i.id
                        is_cash=True
                        break
            if is_cash == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Trade Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.trade_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date,date_posted'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning("There is no cash-upfront account related to this customer")
        
        
    @api.multi
    def get_cash_transaction_cashupfront(self):
        is_cash=False
        acct_id=0
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    if i.acct_type=='Cash':
                        acct_id=i.id
                        is_cash=True
                        break
            if is_cash == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Cash Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.cash_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],
                       }
            else:
                raise Warning("There is no cash-upfront account related to this customer")


    @api.multi
    def get_subscription(self):
        is_cash = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    is_cash = True
                    break
            if is_cash == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Subscription",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_subs_conv_dtl',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       }

    @api.multi
    def get_announcment(self):
        is_cash = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Cash':
                    acct_id=i.id
                    is_cash = True
                    break
            if is_cash == True:
                asset_id = self.env['vb.portfolio'].search([('acct_id','=',acct_id)])
                mod_obj = self.env['ir.model.data']
                g=[]
                for i in asset_id:
                    g.append(i.asset_id.id)
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Announcement",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_announce',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('asset_id','in', g)],
                       }
        
#=================================Margin===================================================================
        
    @api.multi
    def get_customer_acct_margin(self):
        ismargin = False
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_id=i.id
                    ismargin = True
                    break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form1')[1]
                except ValueError:
                    form_res = False
                return{
                       'name': "Customer Account Details",
                       'view_type': 'form',
                       'view_mode':"[form]",
                       'view_id': form_res,
                       'res_model': 'vb.customer_acct',
                       'type': 'ir.actions.act_window',
                       'views': [(form_res, 'form')],
                       'res_id': acct_id,
                       
                       }
    @api.multi
    def get_customer_bal_margin(self):
        ismargin = False
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_no=i.acct_no
                    ismargin = True
                    break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                except ValueError:
                    form_res = False
                return{
                       'name': "Customer Account Details",
                       'view_type': 'form',
                       'view_mode':"[form]",
                       'view_id': form_res,
                       'res_model': 'vb.customer_acct_bal',
                       'type': 'ir.actions.act_window',
                       'views': [(form_res, 'form')],
                       'res_id': self.env['vb.customer_acct_bal'].search([('acct_id','=',acct_id)]).id,
                       
                       }
    
    @api.multi
    def get_order_book_margin(self):
        ismargin = False
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_no=i.acct_no
                    ismargin = True
                    break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_form1')[1]
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_tree1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_search1')[1]
                except ValueError:
                    form_res = False
                
                return{
                       'name': "Transactions Trade Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.order_book',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'enable_date_range_bar':1,'group_by':'order_type', 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
                
    @api.multi
    def get_portfolio_margin(self):
        ismargin = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_id=i.id
                    ismargin = True
                    break
            if ismargin:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_search')[1]
                except ValueError:
                    form_res = False
                    tree_res = False
                    search_res = False
                return{
                       'name': "Portfolio Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.portfolio',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
                
    @api.multi
    def get_order_activity_margin(self):
        ismargin = False
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_no=i.acct_no
                    ismargin = True
                    break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_order_transaction_form')[1]
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_tree')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_search')[1]
                except ValueError:
                    form_res = False
                
                return{
                       'name': "order Activity Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.order_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_no','=',acct_no)],
                       'context': {'search_default_current_month':1,'group_by':'order_no','enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning('There is no Account number for this account')
        
        
    @api.multi
    def get_trade_margin(self):
        ismargin=False
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    acct_id=0
                    if i.acct_type=='Margin':
                        acct_id=i.id
                        ismargin=True
                        break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Trade Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.trade_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning("There is no cash-upfront account related to this customer")
        
        
    @api.multi
    def get_cash_transaction_margin(self):
        ismargin=False
        acct_id=0
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    if i.acct_type=='Margin':
                        acct_id=i.id
                        ismargin=True
                        break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Cash Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.cash_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],
                       }
            else:
                raise Warning("There is no Margin account related to this customer")


    @api.multi
    def get_subscription_margin(self):
        ismargin = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_id=i.id
                    ismargin = True
                    break
            if ismargin == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Subscription",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_subs_conv_dtl',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       }

    @api.multi
    def get_announcment_margin(self):
        ismargin = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Margin':
                    acct_id=i.id
                    ismargin = True
                    break
            if ismargin == True:
                asset_id = self.env['vb.portfolio'].search([('acct_id','=',acct_id)])
                mod_obj = self.env['ir.model.data']
                g=[]
                for i in asset_id:
                    g.append(i.asset_id.id)
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Announcement",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_announce',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('asset_id','in', g)],
                       }


    @api.multi
    def enter_order_margin(self):
        login_name = self.login_name
        customer_name = self.name
        t_pin = self.env['res.users'].search([('id','=',self._uid)]).t_pin
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Margin':                    
                    if i.buy_suspended== True:
                        if i.sell_suspended== True:
                            trdsus='both'
                        else:
                            trdsus='buy'
                    elif i.sell_suspended== True:
                        trdsus='sell'
                    else:
                        trdsus='none'                    
#                     break
                    if i.state == 'Active':
                        rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','ASSTORDER')])
                        key = rec.parm2
                        return{
                            'name': "Customer Overview",
                            'type': 'ir.actions.act_url',                    
                            'url': str(rec.parm1)+'?&csuid='+str(self._uid)+'&uid='+login_name+'&uacc='+str(acct_id)+'&ucds='+str(self.env['vb.customer_acct'].search([('acct_no','=',acct_id)]).cds_no)+'&key='+str(key)+'&trdsus='+trdsus,
                            'target': 'new',
                        }
                    else:
                        raise Warning('This Account is not activated yet')
#===========================================Contra=========================================================

    @api.multi
    def get_customer_acct_contra(self):
        iscontra = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra=True
                    break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form1')[1]
                except ValueError:
                    form_res = False
                return{
                       'name': "Customer Account Details",
                       'view_type': 'form',
                       'view_mode':"[form]",
                       'view_id': form_res,
                       'res_model': 'vb.customer_acct',
                       'type': 'ir.actions.act_window',
                       'views': [(form_res, 'form')],
                       'res_id': acct_id,
                       
                       }
    @api.multi
    def get_customer_bal_contra(self):
        iscontra = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra = True
                    break
                
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                except ValueError:
                    form_res = False
                return{
                       'name': "Customer Account Details",
                       'view_type': 'form',
                       'view_mode':"[form]",
                       'view_id': form_res,
                       'res_model': 'vb.customer_acct_bal',
                       'type': 'ir.actions.act_window',
                       'views': [(form_res, 'form')],
                       'res_id': self.env['vb.customer_acct_bal'].search([('acct_id','=',acct_id)]).id,
                       
                       }
    
    @api.multi
    def get_order_book_contra(self):
        iscontra = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra = True
                    break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_form1')[1]
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_tree1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_order_book_search1')[1]
                except ValueError:
                    form_res = False
                
                return{
                       'name': "Transactions Trade Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.order_book',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'enable_date_range_bar':1,'group_by':'order_type', 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
                
    @api.multi
    def get_portfolio_contra(self):
        iscontra = False
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra = True
                    break
            if iscontra:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_portfolio_search')[1]
                except ValueError:
                    form_res = False
                    tree_res = False
                    search_res = False
                return{
                       'name': "Portfolio Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.portfolio',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
                
    @api.multi
    def get_order_activity_contra(self):
        iscontra = False
        acct_no=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_no=i.acct_no
                    iscontra = True
                    break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_order_transaction_form')[1]
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_tree')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_order_activity_inquire_search')[1]
                except ValueError:
                    form_res = False
                
                return{
                       'name': "order Activity Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.order_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_no','=',acct_no)],
                       'context': {'search_default_current_month':1,'group_by':'order_no','enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning('There is no Account number for this account')
        
        
    @api.multi
    def get_trade_contra(self):
        iscontra=False
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    acct_id=0
                    if i.acct_type=='Contra':
                        acct_id=i.id
                        iscontra=True
                        break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Trade Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.trade_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=',acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],                   
                       }
            else:
                raise Warning("There is no cash-upfront account related to this customer")
        
        
    @api.multi
    def get_transaction_contra(self):
        iscontra=False
        acct_id=0
        if self.id:
            if self.acct_ids:
                for i in self.acct_ids:
                    if i.acct_type=='Contra':
                        acct_id=i.id
                        iscontra=True
                        break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_tree1')[1]
                    form_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_form1')[1]
                    search_res = mod_obj.get_object_reference('vb_cashmgmt', 'view_cash_tran_search1')[1]
                except ValueError:
                    form_res = tree_res = search_res = False
                return{
                       'name': "Transactions Cash Details",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.cash_tran',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
                       'search_view_id':[(search_res)],
                       }
            else:
                raise Warning("There is no cash-upfront account related to this customer")


    @api.multi
    def get_subscription_contra(self):
        iscontra = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra = True
                    break
            if iscontra == True:
                mod_obj = self.env['ir.model.data']
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_subscr_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Subscription",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_subs_conv_dtl',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('acct_id','=', acct_id)],
                       }

    @api.multi
    def get_announcment_contra(self):
        iscontra = False
        if self.acct_ids:
            acct_id=0
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    iscontra = True
                    break
            if iscontra == True:
                asset_id = self.env['vb.portfolio'].search([('acct_id','=',acct_id)])
                mod_obj = self.env['ir.model.data']
                g=[]
                for i in asset_id:
                    g.append(i.asset_id.id)
                try:
                    tree_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_tree')[1]
                    form_res = mod_obj.get_object_reference('vb_interface', 'view_kibb_corp_action_announce_inq_form')[1]
                except ValueError:
                    form_res = tree_res =  False
                return{
                       'name': "Announcement",
                       'view_type': 'form',
                       'view_mode':"[tree,form]",
                       'view_id': False,
                       'res_model': 'vb.kibb_corp_action_announce',
                       'type': 'ir.actions.act_window',
                       'views': [(tree_res, 'tree'), (form_res, 'form')],
                       'domain': [('asset_id','in', g)],
                       }
    
    @api.multi
    def get_outstanding_trade_contra(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    break
            if acct_id:
                if self.env['vb.trade_tran'].search([('acct_id','=',acct_id)]):
                    mod_obj = self.env['ir.model.data']
                    try:
                        form_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_form1')[1]
                        search_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_search1')[1]
                        tree_res = mod_obj.get_object_reference('vb_trade', 'view_trade_tran_cus_overview_tree1')[1]
                    except ValueError:
                        form_res = False
                    
                    return{
                           'name': "Outstanding Trade Transaction Details",
                           'view_type': 'form',
                           'view_mode':"[tree,form]",
                           'view_id': False,
                           'res_model': 'vb.trade_tran',
                           'type': 'ir.actions.act_window',
                           'views': [(tree_res, 'tree'), (form_res, 'form')],
                           'domain': [('acct_id','=',acct_id),('appto_tran_id','=',False),('balance_qty','!=',0.0)],# Nailah asked to remove ('nocontra_flag','=',False) from this domain
                           'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date,date_posted'},
                       'search_view_id':[(search_res)],          
                           }
                else:
                    raise Warning('There is no related order to that account ') 
                
    @api.multi
    def enter_order_contra(self):
        login_name = self.login_name
        customer_name = self.name
        t_pin = self.env['res.users'].search([('id','=',self._uid)]).t_pin
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:                
                if i.acct_type=='Contra':
                    accType='Contra'
                    acct_id=i.acct_no                    
                    if i.buy_suspended== True:
                        if i.sell_suspended== True:
                            trdsus='both'
                        else:
                            trdsus='buy'
                    elif i.sell_suspended== True:
                        trdsus='sell'
                    else:
                        trdsus='none'                    
#                     break
                    if i.state == 'Active':
                        rec = self.env['vb.config'].search([('code_type','=','Sys'),('code','=','ASSTORDER')])
                        key = rec.parm2
                        return{
                            'name': "Customer Overview",
                            'type': 'ir.actions.act_url',                    
                            'url': str(rec.parm1)+'?&csuid='+str(self._uid)+'&uid='+login_name+'&uacc='+str(acct_id)+'&ucds='+str(self.env['vb.customer_acct'].search([('acct_no','=',acct_id)]).cds_no)+'&key='+str(key)+'&accType='+accType+'&tradeSuspended='+trdsus,
                            'target': 'new',
                        }
                    else:
                        raise Warning('This Account is not activated yet') 
                    
    @api.multi
    def create_case_contra(self):
        acct_id=0
        if self.acct_ids:
            for i in self.acct_ids:
                if i.acct_type=='Contra':
                    acct_id=i.id
                    break
            try:
                
                mod_obj = self.env['ir.model.data']
                form_res = mod_obj.get_object_reference('vb_helpdesk', 'view_case_form')[1]
            except ValueError:
                form_res = False
               
            return{
                   'name': "Create Case",
                   'view_type': 'form',
                   'view_mode':"[form]",
                   'view_id': form_res,
                   'res_model': 'vb.helpdesk_case',
                   'type': 'ir.actions.act_window',
                   'views': [(form_res, 'form')],
#                    'target': 'inline',
                   'context':{'default_customer_id':self.id}
                   }          
        
class customer_address(models.Model):
    _inherit = 'vb.customer_address'

    @api.multi
    def backto_customer_overview(self):
        return{
                    'name': "Customer Overview",
                    'type': 'ir.actions.act_url',
                    'url': '/customer_overview?customer_id=' + str(self.customer_id.code),
                    'target': 'self',
                    }

