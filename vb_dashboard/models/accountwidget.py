from openerp import models, fields, api, osv
from datetime import datetime, date, timedelta
import datetime as dt
from openerp.exceptions import Warning
import openerp
from openerp.http import request

class vb_account_widget(models.Model):

    _name = 'vb.account.widget'

    def get_first_day(self, dt, d_years=0, d_months=0):
        #d_years, d_months are "deltas" to apply to dt
        y,m = dt.year+d_years,dt.month+d_months
        a,m = divmod(m-1, 12)
        return date(y+a, m+1, 1)
    
    # Account Status count methods
    
    # today suspended account counts
    @api.multi
    def _get_tdsuscunt_count(self):
        for vbaccount_widget in self:           
            vbaccount_widget.suspended_today1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acst_2day_suspended
            vbaccount_widget.suspended_today2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acst_2day_suspended
            vbaccount_widget.suspended_today = vbaccount_widget.suspended_today1 + vbaccount_widget.suspended_today2
            
    @api.multi
    def _get_suscunt_count(self):
        for vbaccount_widget in self:          
            vbaccount_widget.gtoneday_count1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acst_suspended_gt1day
            vbaccount_widget.gtoneday_count2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acst_suspended_gt1day
            vbaccount_widget.gtoneday_count = vbaccount_widget.gtoneday_count1 + vbaccount_widget.gtoneday_count2
                
    @api.multi
    def _get_pndcls_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.req_close1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acst_pdg_close
            vbaccount_widget.req_close2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acst_pdg_close
            vbaccount_widget.req_close =  vbaccount_widget.req_close1 + vbaccount_widget.req_close2
              
    @api.multi
    def _get_dormcunt_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.doracct_count1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acst_dormant_gt3yr
            vbaccount_widget.doracct_count2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acst_dormant_gt3yr
            vbaccount_widget.doracct_count =  vbaccount_widget.doracct_count1 + vbaccount_widget.doracct_count2
               
    @api.multi
    def _get_inactcunt_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.inactacct_count1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acst_inactive_gt1yr
            vbaccount_widget.inactacct_count2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acst_inactive_gt1yr
            vbaccount_widget.inactacct_count = vbaccount_widget.inactacct_count1 + vbaccount_widget.inactacct_count2
            
    @api.multi
    def _get_cgreqcunt_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.ccrqs_count = (self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)])).acst_ac_chg_appr  
    
    @api.multi
    def _get_kibbtoclsacct_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.kibbtoclsacct_count1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acct_pndg_close_kibb
            vbaccount_widget.kibbtoclsacct_count2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acct_pndg_close_kibb
            vbaccount_widget.kibbtoclsacct_count =  vbaccount_widget.kibbtoclsacct_count1 + vbaccount_widget.kibbtoclsacct_count2
            
    @api.multi
    def _get_kibb2day_clsacct(self):
        for vbaccount_widget in self:
            vbaccount_widget.kibb2day_clsacct1 = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acct_pndg_close_kibb
            vbaccount_widget.kibb2day_clsacct2 = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acct_pndg_close_kibb
            vbaccount_widget.kibb2day_clsacct =  vbaccount_widget.kibb2day_clsacct1 + vbaccount_widget.kibb2day_clsacct2
                                    
    #Account Opening count methods                   
    @api.multi
    def _get_cashcunt_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.cash_account = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_opn_cds_ac
        
    @api.multi
    def _get_mrgncunt_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.margin_account = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_pdg_opn_cds_ac       
            
    @api.multi
    def _get_pendcashcheck_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.cash_riskcheck = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_ramci_check
            
    @api.multi
    def _get_pendmargincheck_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.margin_riskcheck = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_pdg_ramci_check
            
    @api.multi
    def _get_pendactive_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.pending_active = self.env['vb.customer_acct'].search_count([('state','=','Checked')])
            
    @api.multi
    def _get_incompcash_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.incomp_cash = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_incomp_app
            
    @api.multi
    def _get_incompmargin_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.incomp_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_incomp_app
                  
    @api.multi
    def _get_incompcontra_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.incomp_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_incomp_app
    
    @api.multi
    def _get_bypassrccash_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.bypass_cash_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_smgmt_appr
            
    @api.multi
    def _get_bypassrcmargin_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.bypass_margin_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_pdg_smgmt_appr
                 
    @api.multi
    def _get_actacb45cash_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.actac_cash_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_2day_activated
    
    @api.multi
    def _get_actacb45margin_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.actac_margin_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_2day_activated
            
    @api.multi        
    def _get_insignup_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.insignup_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_incomp_signup
                        
    @api.multi
    def _get_insignupcs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.insignupcs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_incomp_signup
            
    @api.multi
    def _get_insignupmr_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.insignupmr_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_incomp_signup            
            
    @api.multi
    def _get_emailvercs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.emailvercs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_email_vrfy
            
    @api.multi
    def _get_otpcas_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.otpcas_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_1st_login
            
    @api.multi
    def _get_resubcs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.resubcs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_app_resubmit
                                           
    @api.multi
    def _get_resubmr_acctopen(self):
        for vbaccount_widget in self:         
            vbaccount_widget.resubmr_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_app_resubmit
             
    @api.multi
    def _get_docckcs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.docckcs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_pdg_doc_check
            
    @api.multi
    def _get_docckmr_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.docckmr_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_pdg_doc_check
            
    @api.multi
    def _get_cdsflcs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.cdsflcs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_cds_opn_fail
            
    @api.multi
    def _get_cdsflmr_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.cdsflmr_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_cds_opn_fail
                                    
    
    # Case Management count methods 
    @api.multi
    def _get_newcases_casemgmt(self):   
        for vbaccount_widget in self:
            vbaccount_widget.new_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_tot_new
            
    @api.multi
    def _get_closedcases_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.closed_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_tot_closed
            
    @api.multi
    def _get_opencases_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.open_cases = self.env['vb.helpdesk_case'].search_count([('state','in', ['Open','New'])])
            
    @api.multi
    def _get_opencasesday_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.open_cases_day = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_open_2day
            
    @api.multi
    def _get_opencasesgtday_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.opencases_greater_day = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_open_gt1day
            
    @api.multi
    def _get_newesccasestoday_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.newesc_cases_today = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_open_2day_assignto
            
    @api.multi
    def _get_newescnottoday_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.newesc_not_today = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_extra1
            
    @api.multi
    def _get_closedesccases_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.closed_esc_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_tot_closed_assignto
            
    @api.multi
    def _get_esccasesnew_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.total_newesc_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_tot_new_assignto
            
    @api.multi
    def _get_operteam_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.operteam_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_open_ops

    @api.multi
    def _get_custteam_casemgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.custteam_cases = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).hdsk_open_cust_svc
     
    # Cash Management count methods
    @api.multi
    def _get_pfdonl_count(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdonl_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_online_dep
    
    @api.multi
    def _get_pfdonl_cscount(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdonl_cscount = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_online_dep
    
    @api.multi
    def _get_pfdonl_cncount(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdonl_cncount = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_online_dep
    
    @api.multi
    def _get_pfdonl_mrcount(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdonl_mrcount = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_online_dep
    
    @api.multi
    def _get_pfdoff_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdoff_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_offline_dep
            
    @api.multi
    def _get_pfwcount_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcount_count = self.env['vb.cash_tran'].search_count([('tran_type','=','Withdraw'),('print_date','=',None),('state','=','Posted')])

    @api.multi
    def _get_pfwcimb_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcimb_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_cimb
    
    @api.multi
    def _get_pfwnoncimb_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwnoncimb_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_ncimb
            
    @api.multi
    def _get_tcpfees_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcpfees_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_2day_cds_payable

    @api.multi
    def _get_tcpfeescontra_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcpfees_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_2day_cds_payable

    @api.multi
    def _get_tcpfeesmargin_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcpfees_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_2day_cds_payable

    @api.multi
    def _get_tcsfees_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcsfees_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_2day_ca_subs

    @api.multi
    def _get_tcsfeescontra_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcsfees_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_2day_ca_subs

    @api.multi
    def _get_tcsfeesmargin_cashmgmt(self):
        for vbaccount_widget in self:
            vbaccount_widget.tcsfees_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_2day_ca_subs

    @api.multi
    def _get_cshdep_reject(self):
        for vbaccount_widget in self:
            vbaccount_widget.cshdep_reject = self.env['vb.cash_tran'].search_count([('state','=','Rejected'),('tran_type','=','Deposit'),('product_type','=','Cash'),('tran_date','=',str(datetime.now().date()))])
    
    @api.multi
    def _get_cntdep_reject(self):
        for vbaccount_widget in self:
            vbaccount_widget.cntdep_reject = self.env['vb.cash_tran'].search_count([('state','=','Rejected'),('tran_type','=','Deposit'),('product_type','=','Contra'),('tran_date','=',str(datetime.now().date()))])
    
    @api.multi
    def _get_mrgdep_reject(self):
        for vbaccount_widget in self:
            vbaccount_widget.mrgdep_reject = self.env['vb.cash_tran'].search_count([('state','=','Rejected'),('tran_type','=','Deposit'),('product_type','=','Margin'),('tran_date','=',str(datetime.now().date()))])
    
    # Settlement count methods
    @api.multi
    def _get_pfwcountcash_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcash_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl
            
    @api.multi
    def _get_pfwcountcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcount_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl

    @api.multi
    def _get_pfwcountmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcount_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl

    @api.multi
    def _get_pfwcimbcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcimb_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_cimb

    @api.multi
    def _get_pfwcimbmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwcimb_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_cimb

    @api.multi
    def _get_pfwnoncimbcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwnoncimb_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_ncimb

    @api.multi
    def _get_pfwnoncimbmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfwnoncimb_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_withdrwl_ncimb

    @api.multi
    def _get_pfdoffcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdoff_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_offline_dep

    @api.multi
    def _get_pfdoffmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfdoff_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_offline_dep

    @api.multi
    def _get_pndinracct_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pendinracct_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).cash_pdg_trf

    @api.multi
    def _get_pndinracctcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pendinracct_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).cash_pdg_trf

    @api.multi
    def _get_pndinracctmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pendinracct_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).cash_pdg_trf

    @api.multi
    def _get_pswcds_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pswcds_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).port_pdg_trf

    @api.multi
    def _get_pswcdscontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pswcds_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).port_pdg_trf

    @api.multi
    def _get_pswcdsmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pswcds_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).port_pdg_trf
            
    @api.multi
    def _get_psfail_settlement(self):
        for vbaccount_widget in self:                                
            vbaccount_widget.psfail_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).sett_poss_buyin

    @api.multi
    def _get_psfailcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.psfail_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).sett_poss_buyin

    @api.multi
    def _get_psfailmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.psfail_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).sett_poss_buyin

    @api.multi
    def _get_pfpsfail_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfpsfail_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).sett_poss_forcesell

    @api.multi
    def _get_pfpsfailcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfpsfail_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).sett_poss_forcesell

    @api.multi
    def _get_pfpsfailmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.pfpsfail_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).sett_poss_forcesell
                    
    @api.multi
    def _get_setfail_settlement(self):
        for vbaccount_widget in self: 
            vbaccount_widget.setfail_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).sett_buyin

    @api.multi
    def _get_setfailcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.setfail_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).sett_buyin

    @api.multi
    def _get_setfailmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.setfail_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).sett_buyin

    @api.multi
    def _get_fssetfail_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.fssetfail_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).sett_forcesell
    
    @api.multi
    def _get_fssetfailcontra_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.fssetfail_contra = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).sett_forcesell

    @api.multi
    def _get_fssetfailmargin_settlement(self):
        for vbaccount_widget in self:
            vbaccount_widget.fssetfail_margin = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).sett_forcesell


    # Corporate
    @api.multi
    def _get_corprec_corp(self):
        for vbaccount_widget in self:
            vbaccount_widget.corprec_count = self.env['vb.kibb_corp_action_subs_conv_dtl'].search_count([('entitle_type','not ilike',['Dividend Reinvestment Plan','Stock Expiry & Conversion','RIGHTS ISSUE','Take Over Offer']),('kibb_close_date','>=', str(datetime.now().date()) + ' 00:00:00')])
            
    @api.multi
    def _get_corpsub_corp(self):
        for vbaccount_widget in self:
            vbaccount_widget.corpsub_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).corp_sub_rcvd
         
    @api.multi
    def _get_corppdng_corp(self):
        for vbaccount_widget in self:
            vbaccount_widget.corppndg_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).corp_pndg_can   
    
    @api.multi
    def _get_cntrcunt_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.contra_account = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_pdg_opn_cds_ac  
            
    # Contra counts for account opening added on 26-09-2017
    @api.multi
    def _get_pendcontracheck_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.contra_riskcheck = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_pdg_ramci_check   
               
    @api.multi
    def _get_bypassrccontra_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.bypass_contra_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_pdg_smgmt_appr     
            
    @api.multi
    def _get_actacb45contra_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.actac_contra_checks = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_2day_activated
            
    @api.multi
    def _get_insignupcn_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.insignupcn_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_incomp_signup 
            
    @api.multi
    def _get_resubcn_acctopen(self):
        for vbaccount_widget in self:         
            vbaccount_widget.resubcn_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_app_resubmit          
            
    
    @api.multi
    def _get_docckcn_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.docckcn_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_pdg_doc_check  
            
    @api.multi
    def _get_cdsflcn_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.cdsflcn_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_cds_opn_fail  
            
    #// weekly rejected account method in account opening added on 26-09-2017//
    @api.multi
    def _get_wklyrjtdcn_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.wklyrjtdcn_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Contra'),('rqs_uid','=',self._uid)]).acop_week_rejected   
              
    @api.multi
    def _get_wklyrjtdcs_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.wklyrjtdcs_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).acop_week_rejected
    
    @api.multi
    def _get_wklyrjtdmr_acctopen(self):
        for vbaccount_widget in self:
            vbaccount_widget.wklyrjtdmr_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Margin'),('rqs_uid','=',self._uid)]).acop_week_rejected

    # // Collaterised Account Management//
    @api.multi
    def _get_frsellordrpend_collacctmngt(self):
        for vbaccount_widget in self:
            vbaccount_widget.frsellordrpend_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).mgmt_forcesell_pdg

    @api.multi
    def _get_frsell_ordrsubmit(self):
        for vbaccount_widget in self:
            vbaccount_widget.frsell_ordrsubmit_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).mgmt_forcesell_2day
    
    @api.multi
    def _get_limit_breach(self):
        for vbaccount_widget in self:
            vbaccount_widget.limit_breach_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).risk_limit_xceed
    
    @api.multi
    def _get_limit_breachcl(self):
        for vbaccount_widget in self:
            vbaccount_widget.limit_breachcl_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).mgmt_forcesell_pdg
    
    @api.multi
    def _get_limit_breachal(self):
        for vbaccount_widget in self:
            vbaccount_widget.limit_breachal_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).mgmt_forcesell_pdg
    
    @api.multi
    def _get_ptloss_breach(self):
        for vbaccount_widget in self:
            vbaccount_widget.ptloss_breach_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).mgmt_forcesell_pdg
    
    @api.multi
    def _get_t4pos_buy(self):
        for vbaccount_widget in self:
            vbaccount_widget.t4pos_buy_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).risk_t4_buy
    
    @api.multi
    def _get_ovrdue_ctloss(self):
        for vbaccount_widget in self:
            vbaccount_widget.ovrdue_ctloss_count = self.env['vb.dashbrd_counter'].search([('product_type','=','Cash'),('rqs_uid','=',self._uid)]).risk_od_ctra_loss_bill
    
    @api.multi
    def _get_pending_acct(self):
        for vbaccount_widget in self:
            vbaccount_widget.pending_edd_count = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('is_edd_completed','!=',True),('state','=','CDDRejected')])
            vbaccount_widget.pending_edd_count_cash = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','!=',True),('acct_id.acct_class_id','=',22)])
            vbaccount_widget.pending_edd_count_contra = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','!=',True),('acct_id.acct_class_id','=',24)])
    @api.multi
    def _get_reviewed_edd_acct(self):
        for vbaccount_widget in self:
            vbaccount_widget.review_edd_count = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('is_edd_completed','=',True),('state','=','CDDRejected'),('is_edd_reviewed','!=',True)])
            vbaccount_widget.review_edd_count_cash = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','=',True),('is_edd_reviewed','!=',True),('acct_id.acct_class_id','=',22)])
            vbaccount_widget.review_edd_count_contra = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','=',True),('is_edd_reviewed','!=',True),('acct_id.acct_class_id','=',24)])
            
    @api.multi
    def _get_highrisk_edd_acct(self):
        for vbaccount_widget in self:
            vbaccount_widget.high_risk_edd_count = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('is_edd_completed','=',True),('state','=','CDDRejected'),('is_edd_reviewed','=',True)])
            vbaccount_widget.high_risk_edd_count_cash = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','=',True),('is_edd_reviewed','=',True),('acct_id.acct_class_id','=',22)])
            vbaccount_widget.high_risk_edd_count_contra = self.env['vb.customer_acct_cedd'].search_count([('acct_id.state','=','New'),('state','=','CDDRejected'),('is_edd_completed','=',True),('is_edd_reviewed','=',True),('acct_id.acct_class_id','=',24)])
    
    name = fields.Char("Name")
    desc = fields.Char("Description")
    color = fields.Integer("Color")
    #// Account Status Fields//    
    req_close  = fields.Integer(string="Request Close", compute=_get_pndcls_count)
    gtoneday_count  = fields.Integer(string="Suspended Account more than one day", compute=_get_suscunt_count)    
    suspended_today  = fields.Integer(string="Todays Suspended Account", compute=_get_tdsuscunt_count)
    doracct_count  = fields.Integer(string="Dormant Account", compute=_get_dormcunt_count)
    inactacct_count  = fields.Integer(string="Inactive Account", compute=_get_inactcunt_count)
    ccrqs_count  = fields.Integer(string="Customer Account Change Request", compute=_get_cgreqcunt_count)
    kibbtoclsacct_count = fields.Integer(string="KIBB Pending To Close Account", compute=_get_kibbtoclsacct_count)
    kibb2day_clsacct = fields.Integer(string="KIBB Today Closed Account", compute=_get_kibb2day_clsacct) 
    #// Account Opening Fields//
    actac_margin_checks  = fields.Integer(string="Today Margin Activated Account Until 5:00pm", compute=_get_actacb45margin_acctopen)
    actac_cash_checks  = fields.Integer(string="Today Cash Upfrount Activated Account Until 5:00pm", compute=_get_actacb45cash_acctopen)
    bypass_margin_checks  = fields.Integer(string="Margin Upfront Bypass Riskcheck", compute=_get_bypassrcmargin_acctopen)
    bypass_cash_checks  = fields.Integer(string="Cash Upfront Bypass Riskcheck", compute=_get_bypassrccash_acctopen)
    incomp_margin  = fields.Integer(string="Incomplete Margin", compute=_get_incompmargin_acctopen)
    incomp_cash  = fields.Integer(string="Incomplete Cash", compute=_get_incompcash_acctopen)
    margin_account  = fields.Integer(string="Margin Account Pending Activate", compute=_get_mrgncunt_acctopen)
    cash_account  = fields.Integer(string="Cash Account Pending Activate", compute=_get_cashcunt_acctopen)
    margin_riskcheck  = fields.Integer(string="Margin Account Pending Riskcheck", compute=_get_pendmargincheck_acctopen)
    cash_riskcheck  = fields.Integer(string="Cash Account Pending Riskcheck", compute=_get_pendcashcheck_acctopen)
    pending_active  = fields.Integer(string="Pending Activation", compute=_get_pendactive_acctopen)    
    insignup_count  = fields.Integer(string="Incomplete Signup", compute=_get_insignup_acctopen)
    insignupcs_count  = fields.Integer(string="Incomplete Signup Cash", compute=_get_insignupcs_acctopen)
    insignupmr_count  = fields.Integer(string="Incomplete Signup Margin", compute=_get_insignupmr_acctopen)
    emailvercs_count  = fields.Integer(string="Pending eMail Verfication", compute=_get_emailvercs_acctopen)
    otpcas_count  = fields.Integer(string="Pending First Time Login", compute=_get_otpcas_acctopen)
    resubcs_count  = fields.Integer(string="Application Resubmit Cash", compute=_get_resubcs_acctopen)
    resubmr_count  = fields.Integer(string="Application Resubmit Margin", compute=_get_resubmr_acctopen)
    docckcs_count  = fields.Integer(string="Pending Document Check Cash", compute=_get_docckcs_acctopen)
    docckmr_count  = fields.Integer(string="Pending Document Check Margin", compute=_get_docckmr_acctopen)
    cdsflcs_count  = fields.Integer(string="Pending Document Check Margin", compute=_get_cdsflcs_acctopen)
    cdsflmr_count  = fields.Integer(string="Pending Document Check Margin", compute=_get_cdsflmr_acctopen)
    #// Case Management Fields//
    total_newesc_cases  = fields.Integer(string="Total New Cases Escalated", compute=_get_esccasesnew_casemgmt)
    new_cases  = fields.Integer(string="New Cases", compute=_get_newcases_casemgmt)
    closed_esc_cases  = fields.Integer(string="Closed Escalated", compute=_get_closedesccases_casemgmt)
    closed_cases  = fields.Integer(string="Closed Cases", compute=_get_closedcases_casemgmt)
    newesc_not_today  = fields.Integer(string="New Escalated  more than one day", compute=_get_newescnottoday_casemgmt)
    opencases_greater_day = fields.Integer(string="Open Cases", compute=_get_opencasesgtday_casemgmt)
    newesc_cases_today  = fields.Integer(string="New Escalated Today", compute=_get_newesccasestoday_casemgmt)
    open_cases_day  = fields.Integer(string="Open Cases", compute=_get_opencasesday_casemgmt)
    open_cases  = fields.Integer(string="Open Cases", compute=_get_opencases_casemgmt)
    operteam_cases  = fields.Integer(string="Operation Escalated Cases", compute=_get_operteam_casemgmt)
    custteam_cases  = fields.Integer(string="Customer Service Escalated Cases", compute=_get_custteam_casemgmt)    
    #// Cash Management Start Fields//
    pfdonl_count = fields.Integer(string="Pending Fund Deposit Online", compute=_get_pfdonl_count)
    pfdonl_cscount = fields.Integer(string="Pending Fund Deposit Cash Online", compute=_get_pfdonl_cscount)
    pfdonl_cncount = fields.Integer(string="Pending Fund Deposit Contra Online", compute=_get_pfdonl_cncount)
    pfdonl_mrcount = fields.Integer(string="Pending Fund Deposit Margin Online", compute=_get_pfdonl_mrcount)                                   
    pfdoff_count = fields.Integer(string="Pending Fund Deposit Offline", compute=_get_pfdoff_cashmgmt)
    pfwcount_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwcount_cashmgmt)
    pfwcimb_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwcimb_cashmgmt)
    pfwnoncimb_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwnoncimb_cashmgmt)
    tcpfees_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_tcpfees_cashmgmt)
    tcpfees_contra = fields.Integer(string="Pending Fund CIMB", compute=_get_tcpfeescontra_cashmgmt)
    tcpfees_margin = fields.Integer(string="Pending Fund CIMB", compute=_get_tcpfeesmargin_cashmgmt)
    tcsfees_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_tcsfees_cashmgmt)
    tcsfees_contra = fields.Integer(string="Pending Fund CIMB", compute=_get_tcsfeescontra_cashmgmt)
    tcsfees_margin = fields.Integer(string="Pending Fund CIMB", compute=_get_tcsfeesmargin_cashmgmt)
    cshdep_reject = fields.Integer(string="Today's Rejected Cash Deposit", compute=_get_cshdep_reject)
    cntdep_reject = fields.Integer(string="Today's Rejected Cash Deposit", compute=_get_cntdep_reject)
    mrgdep_reject = fields.Integer(string="Today's Rejected Cash Deposit", compute=_get_mrgdep_reject)
    #// Settlement Fields//
    pfdoff_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_pfdoffcontra_settlement)
    pfdoff_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_pfdoffmargin_settlement)
    pfwcash_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_pfwcountcash_settlement)
    pfwcount_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_pfwcountcontra_settlement)
    pfwcount_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_pfwcountmargin_settlement)
    pfwcimb_contra = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwcimbcontra_settlement)
    pfwcimb_margin = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwcimbmargin_settlement)
    pfwnoncimb_contra = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwnoncimbcontra_settlement)
    pfwnoncimb_margin = fields.Integer(string="Pending Fund CIMB", compute=_get_pfwnoncimbmargin_settlement)
    pendinracct_count = fields.Integer(string="Pending Inter Account", compute=_get_pndinracct_settlement)
    pendinracct_contra = fields.Integer(string="Contra Pending Inter Account", compute=_get_pndinracctcontra_settlement)
    pendinracct_margin = fields.Integer(string="Margin Pending Inter Account", compute=_get_pndinracctmargin_settlement)
    pswcds_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_pswcds_settlement)
    pswcds_contra = fields.Integer(string="Pending Fund CIMB", compute=_get_pswcdscontra_settlement)
    pswcds_margin = fields.Integer(string="Pending Fund CIMB", compute=_get_pswcdsmargin_settlement)
    psfail_count = fields.Integer(string="Pending Fund CIMB", compute=_get_psfail_settlement)
    psfail_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_psfailcontra_settlement)
    psfail_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_psfailmargin_settlement)
    pfpsfail_count = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_pfpsfail_settlement)
    pfpsfail_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_pfpsfailcontra_settlement)
    pfpsfail_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_pfpsfailmargin_settlement)
    setfail_count  = fields.Integer(string="Pending Fund CIMB", compute=_get_setfail_settlement)
    setfail_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_setfailcontra_settlement)
    setfail_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_setfailmargin_settlement)
    fssetfail_count = fields.Integer(string="Pending Fund CIMB", compute=_get_fssetfail_settlement)
    fssetfail_contra = fields.Integer(string="Contra Pending Fund CIMB", compute=_get_fssetfailcontra_settlement)
    fssetfail_margin = fields.Integer(string="Margin Pending Fund CIMB", compute=_get_fssetfailmargin_settlement)
    #// Corporate Fields//
    corprec_count  = fields.Integer(string="Corporate", compute=_get_corprec_corp)
    corpsub_count  = fields.Integer(string="Corporate Action", compute=_get_corpsub_corp)
    corppndg_count  = fields.Integer(string="Corporate Action", compute=_get_corppdng_corp)
    #// Contra Fields for account opening added on 26-09-2017//
    actac_contra_checks  = fields.Integer(string="Today Contra Activated Account Until 6:00pm", compute=_get_actacb45contra_acctopen)
    bypass_contra_checks  = fields.Integer(string="Contra Upfront Bypass Riskcheck", compute=_get_bypassrccontra_acctopen)
    incomp_contra  = fields.Integer(string="Incomplete Contra", compute=_get_incompcontra_acctopen)
    contra_account  = fields.Integer(string="Contra Account Pending Activate", compute=_get_cntrcunt_acctopen)
    contra_riskcheck  = fields.Integer(string="Contra Account Pending Riskcheck", compute=_get_pendcontracheck_acctopen)
    insignupcn_count  = fields.Integer(string="Incomplete Signup Contra", compute=_get_insignupcn_acctopen)
    resubcn_count  = fields.Integer(string="Application Resubmit Contra", compute=_get_resubcn_acctopen)
    docckcn_count  = fields.Integer(string="Pending Document Check Contra", compute=_get_docckcn_acctopen)
    cdsflcn_count  = fields.Integer(string="Pending Document Check Contra", compute=_get_cdsflcn_acctopen)
    #// weekly rejected account in account opening added on 26-09-2017//
    wklyrjtdcn_count = fields.Integer(string="Weekly Rejected Account Contra", compute=_get_wklyrjtdcn_acctopen)
    wklyrjtdcs_count = fields.Integer(string="Weekly Rejected Account Cash", compute=_get_wklyrjtdcs_acctopen)
    wklyrjtdmr_count = fields.Integer(string="Weekly Rejected Account Margin", compute=_get_wklyrjtdmr_acctopen)
    # // Collaterised Account Management//
    frsellordrpend_count = fields.Integer(string="Force Sell Order Pending Senior Management Approval", compute=_get_frsellordrpend_collacctmngt)
    frsell_ordrsubmit_count = fields.Integer(string="Force Sell Order Submitted", compute=_get_frsell_ordrsubmit)
    limit_breach_count = fields.Integer(string="Limit Breach", compute=_get_limit_breach)
    limit_breachcl_count = fields.Integer(string="Threshold Limit Breach (Company Level)", compute=_get_frsellordrpend_collacctmngt)
    limit_breachal_count = fields.Integer(string="Threshold Limit Breach (Account Level)", compute=_get_frsellordrpend_collacctmngt)
    ptloss_breach_count = fields.Integer(string="Potential Loss Threshold Breach", compute=_get_frsellordrpend_collacctmngt)
    t4pos_buy_count = fields.Integer(string="T+4 Position(Buy)", compute=_get_t4pos_buy)
    ovrdue_ctloss_count = fields.Integer(string="Overdue Contra Losses and Debit Bills", compute=_get_ovrdue_ctloss)
    pending_edd_count = fields.Integer(string='Pending EDD Check',compute=_get_pending_acct)
    pending_edd_count_cash = fields.Integer(string='Cash Pending EDD Check',compute=_get_pending_acct)
    pending_edd_count_contra = fields.Integer(string='Contra Pending EDD Check',compute=_get_pending_acct)
    pending_edd_count_margin = fields.Integer(string='Margin Pending EDD Check')
    review_edd_count = fields.Integer(string='review EDD Check',compute=_get_reviewed_edd_acct)
    review_edd_count_cash = fields.Integer(string='Cash review EDD Check',compute=_get_reviewed_edd_acct)
    review_edd_count_contra = fields.Integer(string='Contra review EDD Check',compute=_get_reviewed_edd_acct)
    review_edd_count_margin = fields.Integer(string='Margin review EDD Check')
    high_risk_edd_count = fields.Integer(string='high_risk EDD Check',compute=_get_highrisk_edd_acct)
    high_risk_edd_count_cash = fields.Integer(string='Cash high_risk EDD Check',compute=_get_highrisk_edd_acct)
    high_risk_edd_count_contra = fields.Integer(string='Contra high_risk EDD Check',compute=_get_highrisk_edd_acct)
    high_risk_edd_count_margin = fields.Integer(string='Margin high_risk EDD Check')
    
    
class button_action_demo(models.Model):
    _name = 'vb.button.demo'    
    name = fields.Char(string='Account: No/Name/CDS')
    action = fields.Selection([
        ('ICA', 'Inquire Customer Account'),
        ('ICAB', 'Inquire Customer Account Balance'),
        ], string='Search For', default='ICA',copy=False)
    
    @api.model
    def run_auto_sp(self):
        try:
            query = "Select sp_dashbrd_counter(%s)"%request.session.uid
            self._cr.execute(query)
            result = self._cr.fetchone()
        except (Exception) as Err:
            print "Unable to process due to: ", Err
            pass
        
    
    @api.multi
    def refresh(self):
        query = "Select sp_dashbrd_counter(%s)"%self._uid
        self._cr.execute(query)
        result = self._cr.fetchone()
        return {
            'type':'ir.actions.client',
            'tag':'reload'}
        
        
#         try:
#             form_res = self.env['ir.model.data'].get_object_reference('vb_dashboard', 'view_dashboard_operation')[1]
#         except ValueError:
#             form_res = False
#         return{
#                    'name': "Customer Account Details",
#                    'view_type': 'form',
#                    'view_mode':"[kanban]",
#                    'view_id': form_res,
#                    'res_model': 'board.board',
#                    'type': 'ir.actions.act_window',
#                    'views': [(form_res, 'kanban')],
#                      
#                    }

    
    @api.multi
    def generate_record_name(self):
        if self.action == 'ICA':
            if self.name:
                mod_obj = self.env['ir.model.data']
                acct_id_no = self.env['vb.customer_acct'].search([('acct_no','=',self.name)])
                acct_id_name = self.env['vb.customer_acct'].search([('name','=',str(self.name))])
                acct_id_cds = self.env['vb.customer_acct'].search([('cds_no','=',self.name)])
                similar_rec_acct = self.env['vb.customer_acct'].search([('name','ilike',str(self.name))])
                if acct_id_no:
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Details",
                               'view_type': 'form',
                               'view_mode':"[form]",
                               'view_id': form_res,
                               'res_model': 'vb.customer_acct',
                               'type': 'ir.actions.act_window',
                               'views': [(form_res, 'form')],
                               'res_id': acct_id_no.id,
                                
                               }
                elif acct_id_name:
                    if len(acct_id_name) == 1:
                        try:
                            form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form')[1]
                        except ValueError:
                            form_res = False
                        return{
                                   'name': "Customer Account Details",
                                   'view_type': 'form',
                                   'view_mode':"[form]",
                                   'view_id': form_res,
                                   'res_model': 'vb.customer_acct',
                                   'type': 'ir.actions.act_window',
                                   'views': [(form_res, 'form')],
                                   'res_id': acct_id_name.id,
                                    
                                   }
                    else:
                        list1 = []
                    for i in similar_rec_acct:
                        list1.append(i.id)
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form')[1]
                        tree_res = mod_obj.get_object_reference('vb_acctmgmt','view_customer_acct_tree')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Details",
                               'view_type': 'form',
                               'view_mode':"[tree,form]",
                               'view_id': False,
                               'res_model': 'vb.customer_acct',
                               'type': 'ir.actions.act_window',
                               'views': [(tree_res, 'tree'), (form_res, 'form')],
                               'domain':[('id','in',list1)],
                                
                               }
                elif acct_id_cds:
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Details",
                               'view_type': 'form',
                               'view_mode':"[form]",
                               'view_id': form_res,
                               'res_model': 'vb.customer_acct',
                               'type': 'ir.actions.act_window',
                               'views': [(form_res, 'form')],
                               'res_id': acct_id_cds.id,
                                
                               }
                elif similar_rec_acct:
                    print "YESSSS"
                    list1 = []
                    for i in similar_rec_acct:
                        list1.append(i.id)
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_acct_form')[1]
                        tree_res = mod_obj.get_object_reference('vb_acctmgmt','view_customer_acct_tree')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Details",
                               'view_type': 'form',
                               'view_mode':"[tree,form]",
                               'view_id': False,
                               'res_model': 'vb.customer_acct',
                               'type': 'ir.actions.act_window',
                               'views': [(tree_res, 'tree'), (form_res, 'form')],
                               'domain':[('id','in',list1)],
                                
                               }
                else:
                    raise Warning('Sorry there is no related account for the inserted information')
            else:
                raise Warning('Please insert information for the account you are searching for')
        elif self.action == 'ICAB':
            if self.name:
                mod_obj = self.env['ir.model.data']
                acct_bal_id_no = self.env['vb.customer_acct_bal'].search([('acct_id.acct_no','=',self.name)])
                acct_bal_id_name = self.env['vb.customer_acct_bal'].search([('acct_id.name','=',self.name)])
                acct_bal_id_cds = self.env['vb.customer_acct_bal'].search([('acct_id.cds_no','=',self.name)])
                similar_rec = self.env['vb.customer_acct'].search([('name','ilike',str(self.name))])
                if acct_bal_id_no:
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Balance Details",
                               'view_type': 'form',
                               'view_mode':"[form]",
                               'view_id': form_res,
                               'res_model': 'vb.customer_acct_bal',
                               'type': 'ir.actions.act_window',
                               'views': [(form_res, 'form')],
                               'res_id': acct_bal_id_no.id,
                                
                               }
                elif acct_bal_id_name:
                    if len(acct_bal_id_name)==1:
                        try:
                            form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                        except ValueError:
                            form_res = False
                        return{
                                   'name': "Customer Account Balance Details",
                                   'view_type': 'form',
                                   'view_mode':"[form]",
                                   'view_id': form_res,
                                   'res_model': 'vb.customer_acct_bal',
                                   'type': 'ir.actions.act_window',
                                   'views': [(form_res, 'form')],
                                   'res_id': acct_bal_id_name.id,
                                    
                                   }
                    else:
                        list1 = []
                    for i in similar_rec:
                        list1.append(i.id)
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                        tree_res = mod_obj.get_object_reference('vb_acctmgmt','view_vb_customer_acct_bal_tree')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Balance Details",
                               'view_type': 'form',
                               'view_mode':"[tree,form]",
                               'view_id': False,
                               'res_model': 'vb.customer_acct_bal',
                               'type': 'ir.actions.act_window',
                               'views': [(tree_res, 'tree'), (form_res, 'form')],
                               'domain':[('acct_id','in',list1)],
                                
                               }
                elif acct_bal_id_cds:
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Balance Details",
                               'view_type': 'form',
                               'view_mode':"[form]",
                               'view_id': form_res,
                               'res_model': 'vb.customer_acct_bal',
                               'type': 'ir.actions.act_window',
                               'views': [(form_res, 'form')],
                               'res_id': acct_bal_id_cds.id,
                                
                               }
                elif similar_rec:
                    list1 = []
                    for i in similar_rec:
                        list1.append(i.id)
                    try:
                        form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_vb_customer_acct_bal_form')[1]
                        tree_res = mod_obj.get_object_reference('vb_acctmgmt','view_vb_customer_acct_bal_tree')[1]
                    except ValueError:
                        form_res = False
                    return{
                               'name': "Customer Account Balance Details",
                               'view_type': 'form',
                               'view_mode':"[tree,form]",
                               'view_id': False,
                               'res_model': 'vb.customer_acct_bal',
                               'type': 'ir.actions.act_window',
                               'views': [(tree_res, 'tree'), (form_res, 'form')],
                               'domain':[('acct_id','in',list1)],
                                
                               }
                    
                else:
                    raise Warning('Sorry there is no related account balance for the inserted information')
        else:
            raise Warning('Please select an option')
        
    #Generates a random name between 9 and 15 characters long and writes it to the record.
#     self.write({'name': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(randint(9,15)))})
    
# @api.one
# def generate_record_password(self):
#     #Generates a random password between 12 and 15 characters long and writes it to the record.
#     self.write({'password': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(randint(12,15)))})
#     
# @api.one
# def clear_record_data(self):
#     self.write({
#         'name': '',
#      'password': ''
#     })