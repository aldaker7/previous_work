# -*- coding: utf-8 -*-

{
    'name': 'VB Margin',
    'version': '0.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Margin Module
==========================================

This module contains the function for margin module

    """,
    'data': [
        'security/ir.model.access.csv',
        'views/views_margin.xml',
        'views/actions_margin.xml',
        'views/menus_margin.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
