from openerp import models, fields, api, _
from datetime import datetime,timedelta,date
from openerp.exceptions import Warning
import json
import requests
from json import dumps


class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'
    
    # Notification reject 
    @api.multi
    def get_reject_notification_wizard(self):
        notify_recs = self.env['vb.push_notify'].search([('id','=',self._context.get('active_id')), ('create_uid','!=',self._uid), ('state','=','New')])
        if notify_recs:
            notify_recs.write({'state':'Rejected', 'rejected_by':self._uid, 'date_rejected':datetime.now(),'comments':self.note,'stage':'Rejected'})
        else:
            raise Warning(_('You are not authorized to reject this notification!'))
        
    # Approve notification
    @api.multi
    def get_approve_notification_wizard(self):
        notify_recs = self.env['vb.push_notify'].search([('id','=',self._context.get('active_id')), ('create_uid','!=',self._uid), ('state','=','New')])
        if notify_recs:
            # -- Disable the WebService call -- 090717
            # url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','PushNotify')])
            # url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
            # data = dumps({"pushNotifyId" : str(notify_recs.id),
            #               "title" : str(notify_recs.name),
            #               "notifyText": str(notify_recs.notify_text),
            #               "pushCategory": str(notify_recs.push_category),
            #               "channelType" : str(notify_recs.channel_type),
            #               "customerRange": str(notify_recs.customer_range),
            #               })
            # headers={'Content-type': 'application/json'}
            # content = requests.post(url,data=data,headers=headers)
            # if int(content.json().get('code'))!=200 or content ==False:
            #     raise Warning('Can not connect with web service')
            # else:
            notify_recs.write({'state':'Approved', 'approved_by':self._uid, 'date_approved':datetime.now(),'stage':'Approved'})
        else:
            raise Warning(_('You are not authorized to approve this notification!'))