# -*- coding: utf-8 -*-

{
    'name': 'VB Alert and notification',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Alert Notification Module
======================================================

    """,
    'data': [
             'security/ir.model.access.csv',
             'wizards/wizards_alertnotify.xml',
             'views/view_alertnotify.xml',
             'views/action_alertnotify.xml',
             'views/menu_alertnotify.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
