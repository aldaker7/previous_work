# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Alert notify modules
#
# Author: Daniel Tan
# Last updated: 18-August-2017
#
# ==========================================

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from datetime import datetime
#from gdata.spreadsheet import Custom

# Model for ANNOUNCEMENT table
# This table store various configuration record used through the system.
# Although similar in structure to the Common Code table, this is used primarily in configuration and
# the access rights of this table will be more restrictive.

class announcement(models.Model):
    _name = 'vb.announcement'
    _description = 'Announcement'
    _rec_name = 'name'
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;
    
    @api.constrains('priority')
    def priority_1_9(self):
        if len(str(self.priority)) > 1 or self.priority == 0:
            raise ValidationError("Priority shouldn't be more than 1 number or shouldn't be 0")
    
    @api.constrains('when_start','when_end')
    def date_validation(self):
        if self.when_start > self.when_end:
            raise ValidationError('When start should be less than When end')
        else:
            if self.when_start < str(datetime.now()):
                raise ValidationError("You can't select earlier date as start date")

#    Method to check if the start date less than end date
    @api.constrains('when_start','when_end')
    def _validate_date(self):
        if self.when_end < self.when_start:
            raise ValidationError('Start date should be less than End date!')

    #Method to set ann_text length to 500 maximum and it value should not space key.      
    @api.constrains('ann_text')
    def announcement_size(self):
        #if len(self.ann_text) > 500:
        #    raise ValidationError("Announcement text length must not exceed 500 characters")
        if  str(self.ann_text).isspace()==True:
            raise ValidationError("Announcement value can not be space")

    # field
    name = fields.Char(string='Announcement title',index=True)
    subtitle = fields.Char(string='Sub-title')
    ann_class = fields.Selection([
        ('General', 'General'),
        ('System', 'System'),
        ], string='Announcement class',default='General')
    ann_type = fields.Selection([
        ('Text', 'Text'),
        ('Html', 'Html'),
        ], string='type',default='Text')
    ann_text = fields.Text(string='Announcement text')
    ann_html = fields.Html(string='Announcement html')
    priority = fields.Integer(string='Priority (1-9)',default=1)
    when_start = fields.Datetime(string='When start')
    when_end = fields.Datetime(string='When end')
    state = fields.Selection([
        ('On', 'On'),
        ('Off', 'Off'),
        ('Expiry', 'Expiry'),
        ], string='Status',default=None)
    # Other non-functional record fields
    active = fields.Boolean(string='Active',default=True)    
    list_seq = fields.Integer(string='List order')
    permission = fields.Selection([
        ('Public', 'Public'),
        ('Member', 'Members only'),
        ], string='Permission', default='Public')
    # 04-April-2017
    image_url = fields.Char(string='Image Url/code')
    gen_pdf = fields.Boolean(string='Generate PDF')
     
# Model for PUSH-NOTIFY table

class push_notify(models.Model):
    _name = 'vb.push_notify'
    _description = 'Push notify'
    _rec_name = 'name'
    
    @api.constrains('customer_ids')
    def check_redundant_customer(self):
        for i in self:
            if i.customer_ids:
                customer_list = []
                for rec in i.customer_ids:
                    customer_list.append(rec.customer_id.id)
                for n in customer_list:
                    if customer_list.count(n)>1:
                        raise ValidationError("You can't duplicate the customers in the list below")
                    
    @api.constrains('customer_range','customer_ids')
    def check_list(self):
        for i in self:
            if i.customer_range == 'List':
                if bool(i.customer_ids)== False:
                    raise ValidationError('Please insert some items in the customer list down in the bottom')
    
    @api.model
    def create(self,vals):
        vals['state']='New'
        result=super(push_notify,self).create(vals)
        return result
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;
      
    # Method to call push notify approve wizard      
    @api.multi
    def get_approve_pushnotify_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_alertnotify', 'approve_push_notify_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'approve_push_notify_wizard'. Please contact IT Support"))
        return{
                    'name': "Approve Push Notification",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
    # Method to call push notify reject wizard      
    @api.multi
    def get_reject_pushnotify_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_alertnotify', 'reject_push_notify_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'reject_push_notify_wizard'. Please contact IT Support"))
        return{
                    'name': "Reject Push Notification",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

    # Validate the expiration date must greater than the start date
    @api.multi
    @api.onchange('date_expire')
    @api.constrains('date_start', 'date_expire')
    def _check_expiry(self):
        if self.date_start > self.date_expire:
            raise ValidationError("Your Date expire must be greater than Date start.")

    # field
    name = fields.Char(string='Title',index=True)
    notify_text = fields.Text(string='Notify message')
    notify_html = fields.Html(string='Notify message')
    channel_type = fields.Selection([
        ('All', 'All'),
        ('Web', 'Web'),
        ('Ispeed', 'iSpeed'),        
        ], string='Channel type',default='All')
    push_category = fields.Selection([
        ('Important', 'Important'),
        ('Promotions', 'Promotion'),
        ], string='Category',default='Important')
    customer_range = fields.Selection([
        ('All', 'All'),
        ('List', 'List'),
#         ('Product', 'Product'),
        ], string='Customer range',default='All')
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('Pushed', 'Pushed'),
        ], string='Status')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by',ondelete='restrict')
    date_rejected = fields.Datetime(string='Date/time rejected')
    rejected_by = fields.Many2one(comodel_name='res.users',string='Rejected by',ondelete='restrict')
    comments = fields.Text(string='Comments')
    # Future use - if customer_range = Product
    acct_class_id = fields.Many2one(comodel_name='vb.common_code',string='Product',ondelete='set null')
    customer_ids = fields.One2many(comodel_name='vb.push_notify_customer',inverse_name='pushnotify_id', string='Customer list')
    
    # Other non-functional record fields
    active = fields.Boolean(string='Active',default=True)
    # 06-Sep-2017
    notify_type = fields.Selection([
        ('Notify', 'Notify'),
        ('Push', 'Push'),
        ], string='Notify type',default='Notify')

    date_start = fields.Datetime(string='Date/time start')
    date_expire = fields.Datetime(string='Date/time expire')
    stage = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('Pushed', 'Pushed'),
        ], string='Status',default='New')
   
# Model for PUSH-NOTIFY-CUSTOMER table

class push_notify_customer(models.Model):
    _name = 'vb.push_notify_customer'
    _description = 'Push notify - customer list'
    
    # Disable the DUPLICATE button
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # Perform a soft delete    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False;
    
    # field
    pushnotify_id = fields.Many2one(comodel_name='vb.push_notify', string='Parent',ondelete='cascade')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='set null')
    customer_code = fields.Char(string='Customer code')
    customer_name = fields.Char(string='Customer name',related='customer_id.name',readonly=True)
    # Other non-functional record fields
    active = fields.Boolean(string='Active',default=True)    
    list_seq = fields.Integer(string='List order')

class customer_inbox(models.Model):
    _inherit = 'vb.customer_inbox'
    
    # field
    pushnotify_id = fields.Many2one(comodel_name='vb.push_notify', string='Parent',ondelete='cascade')
    notify_type = fields.Selection([
        ('Notify', 'Notify'),
        ('Push', 'Push'),
        ], string='Notify type',default='Notify')

    date_start = fields.Datetime(string='Date/time start')
    date_expire = fields.Datetime(string='Date/time expire')
        