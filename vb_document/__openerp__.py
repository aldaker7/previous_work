# -*- coding: utf-8 -*-

{
    'name': 'VB Document',
    'version': '1.1',
    'author': 'VB Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_acctmgmt','vb_learning','vb_helpdesk'],
    'description': """

This is the central document management module for Virtual Broker Application.
=============================================================================


""",
    'data': [
        'security/ir.model.access.csv',
        'views/template.xml',
        'wizards/wizards_document.xml',
        'data/setup_data.xml',
        'views/views_document.xml',
        'views/actions_document.xml',
        'views/menus_document.xml',
    ],
    'qweb': ['static/src/xml/document_widget.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
