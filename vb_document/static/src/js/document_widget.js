odoo.define('vb_document.vb_document',function(require){
    "use strict";
   
    var core = require('web.core');
    console.log('core.form_widget_registry',core.form_widget_registry);

    var DocumentFieldUrl = core.form_widget_registry.map.char.extend({
        template: 'DocumentFieldUrl',
        initialize_content: function() {
            this._super();
            var $button = this.$el.find('button');
            $button.click(this.on_button_clicked);
            this.setupFocus($button);
        },
        render_value: function() {
            if (!this.get("effective_readonly")) {
                this._super();
            } else {
            	var field_values = this.view.get_fields_values();
            	console.log(field_values);
            	var tmp = this.session.origin + '/web/binary/download_document?id=' + field_values['id'];
                this.$el.find('a').attr('href', tmp).text(field_values['file_loc']);
            }
        },
        on_button_clicked: function() {
            if (!this.get('value')) {
                this.do_warn(_t("Resource Error"), _t("This resource is empty"));
            } else {
                var url = $.trim(this.get('value'));
                if(/^www\./i.test(url))
                    url = 'http://'+url;
                window.open(url);
            }
        }
    });

core.form_widget_registry.add('document_url', DocumentFieldUrl);

});
