How to set the path of Document Directory?

After installing this module,
1) Go to General > Others > System Config.
2) Look for the name 'document_directory'.
3) Edit the code in it to your own desired system path.

All the documents you upload will be now stored in this directory.
