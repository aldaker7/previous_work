# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Models for Document module
#
# Author: Daniel Tan
# Last updated: 15-August-2016
#
# ==========================================

import re
import tempfile
import os
import base64
from shutil import copyfile,move
from openerp import models, fields, api
from openerp.exceptions import Warning, ValidationError

# Model for CENTRAL DOCUMENT table
# Actual physical storage of documents for this table should be in a separate file store

class document(models.Model):
    _inherit = 'vb.document'

#     Method to get method of approve FAQ 
    @api.multi
    def get_approve_doc(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_document', 'approve_doc_wiz_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Approve document",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to get method of reject document 
    @api.multi
    def get_reject_doc(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_document', 'reject_doc_wiz_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Reject document",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#   Method to get method allow edit document
    @api.multi
    def get_edit_doc(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_document', 'edit_doc_wiz_form_view1')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Edit document",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
            
    @api.multi
    def get_document_directory_path(self):
        '''This method returns you the document directory.'''
        document_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm1
        if not os.path.exists(str(document_directory)):
            raise Warning('The Document Directory doesnt exist. Please set the Document Directory in System > System Parameters > document_directory')
        else:
            return document_directory

    def create_directory(self, file_directory):
        '''This method creates a directory if it does not exists.'''
        if not os.path.exists(file_directory):
            os.makedirs(file_directory)
            return True
        return False

    def write_file_to_directory(self, file_name, file_data, folder_name=False):
        '''This method is used to create a new File.
            filename: name of the file
            file_data: The data of the file.
            folder_name: The name of the folder where the document is created.'''
        #Getting the Document Diretory.
        doc_directory_path = self.get_document_directory_path()
        file_directory = os.path.join(doc_directory_path, folder_name or '')

        #Creating the folder if it doesnt exist.
        self.create_directory(file_directory)
        file_path = os.path.join(file_directory, file_name)

        #Writing a new file
        with open(file_path, 'wb') as new_file:
            decoded_data = base64.b64decode(file_data)
            new_file.write(decoded_data)
        return file_path
    
    def delete_file(self, file_path):
        '''This method deletes the file from the file_path provided.'''
        if os.path.exists(file_path):
            os.remove(file_path)
            return True
        return False
    
    def move_to_trash(self, file_path, file_name):
        '''This method moves the file to Trash DIrectory from the file_path provided.'''
        
        doc_directory_path = self.get_document_directory_path()
        trash_path = os.path.join(doc_directory_path, 'Trash')
        trash_file_path = os.path.join(trash_path, file_name)
        self.create_directory(trash_path)
        self.delete_file(trash_file_path)
        move(file_path, trash_file_path)
        return True
    
    @api.model
    def create(self, vals):
        # to check if there is a temp folder 
        if os.path.isdir(tempfile.gettempdir()) == False:
            new_dir =  tempfile.mkdtemp()
        #for checking about the file size
        if vals.get('document'):
            file_path = tempfile.gettempdir()+'/'+vals.get('file_name')
            encode_data = base64.b64decode(vals.get('document'))
            data = encode_data
            f = open(file_path,'wb')
            f.write(encode_data)
            f.close()
            if os.stat(file_path).st_size >10000000:
                raise ValidationError('Your selected file more than 10 MB')
            else:
                os.remove(file_path)
                doc_directory_path = self.get_document_directory_path()
                folder_name = self.env['vb.common_code'].search([('id','=',vals.get('doc_class_id')),('code_type','=','DocClass')])[0].code
                new_file_path = self.write_file_to_directory(vals.get('file_name'), vals.get('document'), folder_name)
                vals.update({
                                'document':False,
                                'file_loc': new_file_path
                            })
            return super(document, self).create(vals)
        else:
            return super(document, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('document'):
            file_path = tempfile.gettempdir()+'/'+vals.get('file_name')
            encode_data = base64.b64decode(vals.get('document'))
            data = encode_data
            f = open(file_path,'wb')
            f.write(encode_data)
            f.close()
            if os.stat(file_path).st_size >10000000:
                raise ValidationError('Your selected file more than 10 MB')
            else:
                os.remove(file_path)
                #Getting the document directory.
                doc_directory_path = self.get_document_directory_path()
        
                #Setting the folder name where the file will be created.
                if vals.get('doc_class_id'):
                    folder_name = self.env['vb.common_code'].search([('id','=',vals.get('doc_class_id')),('code_type','=','DocClass')])[0].code
                elif self.doc_class_id:
                    folder_name = self.doc_class_id.code
                else:
                    folder_name = False
        
                #This code block is executed when the class type is changed.
                if vals.get('doc_class_id') and not vals.get('document'):
                    if self.file_name:
                        new_folder_path = os.path.join(doc_directory_path, folder_name)
                        self.create_directory(new_folder_path)
                        #Copying the file from old directory to new directory.
                        copyfile(self.file_loc, os.path.join(new_folder_path, self.file_name))
                        #Deleting the old file.
                        self.delete_file(self.file_loc)
                        vals.update({
                                    'file_loc': os.path.join(new_folder_path, self.file_name)
                                    })

        #This code block is executed when the document is uploaded.
        if vals.get('file_name') or vals.get('document'):
            #Creating the document
            new_file_path = self.write_file_to_directory(vals.get('file_name'), vals.get('document'), folder_name or '')
            #Deleting the old file.
            if self.file_loc != new_file_path:
                self.delete_file(self.file_loc)
            vals.update({
                            'document':False,
                            'file_loc': new_file_path
                        })
        return super(document, self).write(vals)

    @api.multi
    def unlink(self):
        for document_rec in self:
            file_path = document_rec.file_loc
            file_name = document_rec.file_name
            #Calling the move_to_trash method 
            #to move the file to Trash from the path provided.
            if os.path.exists(file_path):
                self.move_to_trash(file_path,file_name)
            document_rec.active = False

    @api.constrains('doc_version')
    def get_version(self):
        if self.doc_version == False:
            self.doc_version = ''
        else:
            check_string = bool(re.search('[a-zA-Z]+',self.doc_version))
            if check_string == True :
                raise ValidationError('Document version value is invalid:\nNumeric values only provided ')
            else:
                #making sure last digit is not a point
                if str(self.doc_version)[-1:] == '.':
                    raise ValidationError('Document version value is invalid:\nYou can not put last digit as point "." ')
                if str(self.doc_version).count('.') > 2 :
                    raise ValidationError('Document version value is invalid:\n2 is the max number of "." ')
                else:
                    if str(self.doc_version).count('.') == 2:
                        if str((self.doc_version)[-4:]).count('.') <2 :
                            raise ValidationError('You can not enter more than one digit after the point')
                    if str(self.doc_version).count('.') == 1 :
                        if str((self.doc_version)[-2:]).count('.') <1 :
                            raise ValidationError('You can not enter more than one digit after the point')
                    else:
                        self.update({'doc_version': str(self.doc_version)+'.0'})
                        
    _sql_constraints = [
        ('doc_version_name_uniq', 'unique(name,doc_version,acct_id,file_name,active)', 'Another document with the same name and version and file name already exist !'),
    ]