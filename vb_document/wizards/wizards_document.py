from openerp import models, fields, api, _
from datetime import datetime, timedelta
from openerp.exceptions import Warning

class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'

#     method to approve document when status equal to new, set approved by to 
#     user id and approved datetime to today datetime
    @api.multi
    def approve_document(self):
        #making sure the state is New and make sure it is not already Active
        doc_rec_app = self.env['vb.document'].search([('id', 'in',self._context.get('active_ids')),('write_uid','!=',self._uid)])
        if doc_rec_app:
            doc_rec_app.write({'state': 'Approved','apprej_by': self._uid, 'apprej_date': datetime.now()})
        else:
            raise Warning(_('You are not authorized to approve/reject this document!'))
        
#     method to reject document when status equal to new, set reject by to 
#     user id and reject datetime to today datetime
    @api.multi
    def reject_document(self):
        #making sure the state is New and make sure it is not already Active
        doc_rec_app = self.env['vb.document'].search([('id', 'in',self._context.get('active_ids')),('write_uid','!=',self._uid)])
        if doc_rec_app:
            doc_rec_app.write({'state': 'Rejected','apprej_by': self._uid, 'apprej_date': datetime.now()})
        else:
            raise Warning(_('You are not authorized to approve/reject this document!'))
        
    @api.multi
    def edit_document(self):
        #making sure the state is New and make sure it is not already Active
        doc_rec_edit = self.env['vb.document'].search([('id', 'in',self._context.get('active_ids'))])
        if doc_rec_edit:
            doc_rec_edit.write({'state': 'New', 'apprej_by':self._uid, 'apprej_date':datetime.now()})
        else:
            raise Warning(_('Only user approve/reject this document is allowed to edit it!'))