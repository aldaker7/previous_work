import os.path
from openerp.addons.web import http
from openerp.http import request
from openerp.addons.web.controllers.main import content_disposition
import tempfile
import base64
from os import chdir


class Binary(http.Controller):
    @http.route('/web/binary/download_document/', type='http', auth="public")
    def download_document(self, id, **kw):
        """ Download link for files stored as binary fields.
        :param str id: field holding the ID of vb_document table
        :returns: :class:`werkzeug.wrappers.Response`
        """

        #Getting the document directory path
        document_directory_path = request.env['vb.document'].get_document_directory_path()

        #Getting Document Record
        document_rec = request.env['vb.document'].search([('id','=',int(id))])
        print 'document_rec',document_rec
        #Checking if the file exists, and then fetching the document.
        if os.path.exists(document_rec.file_loc):
            with open(document_rec.file_loc, 'ab+') as doc_file:
                filecontent = doc_file.read()
            if not filecontent:
                return request.not_found()
            else:
                if document_rec.file_name[-3:] == 'pdf':
                #Return the file and filename to the browser.
                    return request.make_response(filecontent,
                                   [('Content-Type', 'application/pdf'),
                                    ('Content-Disposition', 'inline')])
                else:
                    return request.make_response(filecontent,
                                   [('Content-Type', 'attachment'),
                                    ('Content-Disposition', 'inline')])
        else:
            return request.not_found()
        
#     @http.route('/web/binary/download_report', type='http', auth="public")
#     @serialize_exception
#     def download_report(self,filename=None, **kw):
#         """ Download link for files stored as binary fields.
#         :param str filename: field holding the file's name, if any
#         :returns: :class:`werkzeug.wrappers.Response`
#         """
# 
#         #Getting the temp directory path
#         temp_directory_path = tempfile.gettempdir()
# 
# 
#         #Checking if the file exists, and then fetching the document.
#         if os.path.exists(os.path.join(temp_directory_path,filename)):
#             with open(os.path.join(temp_directory_path,filename), 'ab+') as doc_file:
#                 filecontent = doc_file.read()
#             if not filecontent:
#                 return request.not_found()
#             else:
#                 if filename:
#                     #Return the file and filename to the browser.
#                     return request.make_response(filecontent,
#                                    [('Content-Type', 'application/octet-stream'),
#                                     ('Content-Disposition', content_disposition(filename))])
#         else:
#             return request.not_found()