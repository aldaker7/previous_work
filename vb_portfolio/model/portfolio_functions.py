from openerp import models, api, fields
from time import strftime
from datetime import datetime
import json
import os
import base64
from openerp.exceptions import Warning

class portfolio(models.Model):
    _inherit = 'vb.portfolio'

    #Method to get Asset Transactions Details
    @api.multi
    def call_asset_trans_portfo_view(self):
        mod_obj = self.env['ir.model.data']
        try:
            tree_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_portfolio_inquire_tree')[1]
            form_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_porfoilio_inquire_form')[1]
            search_res = mod_obj.get_object_reference('vb_portfolio', 'view_asset_tran_porfolio_inquire_search')[1]
        except ValueError:
            tree_res = False
        return{
               'name': "Asset Transaction Details",
               'view_type': 'form',
               'view_mode':'[tree,form]',
               'view_id': False,
               'res_model': 'vb.asset_tran',
               'type': 'ir.actions.act_window',
               'views': [(tree_res, 'tree'), (form_res, 'form')],
               'domain': [('portfolio_id.id','=',self.id)],
               'context': {'search_default_current_month':1,'enable_date_range_bar':1, 'search_fields': 'tran_date'},
               'search_view_id':[(search_res)],
               }
        
    @api.multi
    def sync_portfolio(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'sync_portfolio_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'void_earmark_shares_wizard'. Please contact IT Support"))
        return{
                   'name': "Sync portfolio",
                   'view_type': 'form',
                   'view_mode': "[form]",
                   'view_id': False,
                   'res_model': 'vb.common_wizard',
                   'type': 'ir.actions.act_window',
                   'target': 'new',
                   'views': [(form_id, 'form')], 
                   }
        
class asset_tran(models.Model):
    _inherit = 'vb.asset_tran'
    
    # This constraint is used primarily for Contra net record
    @api.onchange('acct_id')
    def get_asset(self):
        if self.acct_id:
            list1 = []
            portfolio_rec = self.env['vb.portfolio'].search([('acct_id','=',self.acct_id.id)])
            for i in portfolio_rec:
                list1.append(i.asset_id.name)
            print list1
            if list1:
                return {'domain':{'asset_id':[('name','in',list1)]}}
        else:
                return {'domain':{'asset_id':[('id','=',0)]}}

#     Method to cancel CDS transfer out (share withdrawal asset) 
    @api.multi
    def get_asset_tran_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'cancel_asset_tran_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'cancel_asset_tran_wizard'. Please contact IT Support"))
        return{
                    'name': "Cancel Asset Transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to confirm/post CDS transfer out (share withdrawal asset)
    @api.multi
    def get_asset_tran_post_confirm_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'confirm_asset_tran_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'confirm_asset_tran_wizard'. Please contact IT Support"))
        return{
                   'name': "Confirm Asset Transaction",
                   'view_type': 'form',
                   'view_mode': "[form]",
                   'view_id': False,
                   'res_model': 'vb.common_wizard',
                   'type': 'ir.actions.act_window',
                   'target': 'new',
                   'views': [(form_id, 'form')], 
                   }
                       
#     Method to cancel Earmark shares 
    @api.multi
    def get_earmark_shares_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'cancel_earmark_shares_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'cancel_earmark_shares_wizard'. Please contact IT Support"))
        return{
                    'name': "Cancel Earmark shares",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to Confirm/Post Earmark shares
    @api.multi
    def get_earmark_shares_post_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'post_earmark_shares_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'post_earmark_shares_wizard'. Please contact IT Support"))
        return{
                   'name': "Post Earmark shares",
                   'view_type': 'form',
                   'view_mode': "[form]",
                   'view_id': False,
                   'res_model': 'vb.common_wizard',
                   'type': 'ir.actions.act_window',
                   'target': 'new',
                   'views': [(form_id, 'form')],
                   }
        
#     Method to void Earmark shares 
    @api.multi
    def get_earmark_shares_void_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'void_earmark_shares_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'void_earmark_shares_wizard'. Please contact IT Support"))
        return{
                   'name': "Void Earmark shares",
                   'view_type': 'form',
                   'view_mode': "[form]",
                   'view_id': False,
                   'res_model': 'vb.common_wizard',
                   'type': 'ir.actions.act_window',
                   'target': 'new',
                   'views': [(form_id, 'form')], 
                   }

#   Method to set the portfolio_id of vb.asset_tran to the acct_id from vb.portfolio 
    @api.multi
    @api.onchange('acct_id')
    def _set_portfolio_id(self):
        port_obj=self.env['vb.portfolio'].search([('id', '=', self._context.get('active_id'))])
        if port_obj:
            for rec in port_obj:
                rec.portfolio_id = rec.portfolio_id.acct_id
                
#   Method to get CDS transfer out ids and send auto email to KIBB
    @api.model
    def cds_out_auto_email(self):
            
        process_qid = self.env['vb.proc_queue'].search([('dbtable', '=', 'vb.asset_tran'),('proc_type', '=', 'CDSTrfOutNotify'), ('state', '=', 'New'), ('proc_freq','=','ASAP')])
        if process_qid:
            count=0
            for cdstrfid in process_qid:
                count+=1
                asset_recs = self.env['vb.asset_tran'].search([('id', '=', cdstrfid.recordid)])
                # Create Jasper client object
                jasper_obj = self.env['vb.jasper']
                jasper_client = jasper_obj.connect_jasper_server_client()
                report_name = 'CDSTransferOutStatement'
                params = {
                        'PrintedBy': str(self.env.user.name),
                        'CDSTrId':asset_recs.id,
                        }  
                
                #Getting system directory and creating temp folder
                doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm1
#                 if os.path.isdir(tempfile.gettempdir()) == False:
#                     tempfile.mkdtemp()
                temp_path = doc_directory+'temp'
                if not os.path.isdir(temp_path):
                    os.makedirs(temp_path)
                FileName = report_name +'-'+strftime('%Y%m%d%H%M%S')+str(count)+'.pdf'
                
                var_report = jasper_obj.get_jasper_report_cds_transferout(jasper_client, report_name, params, FileName)
                if var_report:
                    file_path = temp_path+'/'+FileName
                    decode=base64.b64decode(var_report)
                    f = open(file_path,'wb')
                    f.write(decode)
                    f.close()
             
                other_doc = self.env['vb.document'].search([('asset_tran_id', '=', asset_recs.id)])
                  
                kibb_rec=self.env['vb.config'].search([('code_type','=','App'),('code','=','KibbEmailCdsTrf')])
                  
                mq_server_rec=self.env['vb.config'].search([('code','=','MQServer')])
                  
                channel = self.env['rabbitmq.server'].get_rabbitmq_channel()
                  
                if other_doc:
                    data= {
                           "publisherApp":"backoffice",
                           "templateCode":"Ct1",
                           "messageParams":{},
                            "attachments":[{
                                             "attachmentNo":"1",
                                             "attachmentType":"PATH",
                                             "attachmentName":str(FileName),
                                             "attachmentUrl":str(file_path),
                                             },
                                           {
                                             "attachmentNo":"2",
                                             "attachmentType":"PATH",
                                             "attachmentName":str(other_doc.file_name),
                                             "attachmentUrl":str(other_doc.file_loc ),
                                             }],
                           "recipients":[{
                                         "recipientNo":"1",
                                         "recipientType":"normal",
                                         "name": kibb_rec.name or '',
                                         "email": kibb_rec.parm1,
                                         "title":"Mr",
                                        }]
                           }
                    data_json=json.dumps(data)
                    channel.basic_publish(exchange='',
                                          routing_key=mq_server_rec.parm5,
                                          body = data_json)
                    cdstrfid.update({'state':'Processed','parm1':kibb_rec.parm1,'proc_time':datetime.now()})
                      
                else:
                    data= {
                           "publisherApp":"backoffice",
                           "templateCode":"Ct1",
                           "messageParams":{},
                            "attachments":[{
                                             "attachmentNo":"1",
                                             "attachmentType":"PATH",
                                             "attachmentName":str(FileName),
                                             "attachmentUrl":str(file_path),
                                             }],
                           "recipients":[{
                                         "recipientNo":"1",
                                         "recipientType":"normal",
                                         "name": kibb_rec.name or '',
                                         "email": kibb_rec.parm1,
                                         "title":"Mr",
                                        }]
                           }
                    data_json=json.dumps(data)
                    channel.basic_publish(exchange='',
                                          routing_key=mq_server_rec.parm5,
                                          body = data_json)
                    cdstrfid.update({'state':'Processed','parm1':kibb_rec.parm1,'text1':'No internal document attached', 'proc_time':datetime.now()})         
    

class asset(models.Model):
    _inherit = 'vb.asset'

    is_small_mid_cap = fields.Boolean(string='Is small mid cap')
    loyalty_partner_id = fields.Many2one(comodel_name='vb.loyalty_partner', string='Partner name',ondelete='cascade',index=True)
# ========================================== -->
# 18-Sept-2018 -->
# Setup/Asset risk batch update/Asset Approval -->
# ========================================== -->
    approve_status = fields.Selection([
        ('pending_approve', 'Pending Approve'),
        ('approved', 'Approved'),
        ], default='pending_approve')
    asset_approve_uid = fields.Many2one(comodel_name='res.users', string='Approved by')
    asset_approve_date = fields.Datetime(string='Approved date')

    @api.multi
    def get_approve_asset(self):
      if self.approve_status != 'Approved':
        if self._uid != self.write_uid.id:
          try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_portfolio', 'get_approve_button_form_view')[1]
          except ValueError:
            form_id = False
          return {
            'name':'approved',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'target': 'new',
                  }
        else:
          raise Warning('You are not allow to approve this.')

    @api.multi
    def write(self,vals):
        result = super(asset,self).write(vals)
        if vals:
          if 'approve_status' not in vals:
            vals['approve_status'] = 'pending_approve'
            vals['asset_approve_uid'] = None
            vals['asset_approve_date'] = None
            return super(asset,self).write(vals)
        return result
