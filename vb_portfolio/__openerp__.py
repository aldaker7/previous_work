# -*- coding: utf-8 -*-

{
    'name': 'VB Portfolio',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_acctmgmt'],
    'description': """

Virtual Broker Application - Account Management Module
======================================================

This module contains the function for managing the customer as well as their underlying accounts.

    """,
    'data': [
        'wizards/wizards_portfolio.xml',
        'views/views_portfolio.xml',
        'views/actions_portfolio.xml',
        'views/menus_portfolio.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}