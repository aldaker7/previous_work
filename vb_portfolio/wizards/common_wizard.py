from openerp import models, api, _
from datetime import datetime
import requests
from json import dumps
import json
from openerp.exceptions import ValidationError,Warning


class common_wizard(models.TransientModel):
    
    _inherit = 'vb.common_wizard'
      
    @api.multi
    def stock_price_download(self):
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','PriceDownload')])  
#         url =  "http://192.168.0.225:8080/vbroker/api/v1/assets/stockprice"
        url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
        if url == False:
            raise Warning('Please provide the IP in system parameters')
        headers={'Content-type': 'application/json'}
        content = requests.post(url,headers=headers)
        if content.json().get('code')==200 or content == False:
            final_view = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'show_completion_wizard_view')[1]
            message = "Your operation done."
            return {
                'name':'Processing completed.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
                }
        else:
            raise Warning( "Your operation Failed. Cannot connect with web service")
    @api.multi
    def cancel_asset_tran(self):
        asset_recs = self.env['vb.asset_tran'].search([('id','in',self._context.get('active_ids'))])
        for rec in asset_recs:
            rec.write({'state':'Cancelled','stage':'Void','void_by':self._uid, 'date_void':datetime.now(), 'comments': self.note, 'reason_cancelled_id':self.reason_cancel.id})
              
#     @api.multi
#     def post_asset_tran(self):
#         asset_recs = self.env['vb.asset_tran'].search([('id','in',self._context.get('active_ids'))])
#         for rec in asset_recs:
#           rec.write({'state':'Confirmed','stage':'New','posted_by':self._uid, 'date_posted':datetime.now()})
          
    @api.multi
    def confirm_asset_tran(self):
        asset_recs = self.env['vb.asset_tran'].search([('id','in',self._context.get('active_ids'))])
        for rec in asset_recs:
            if rec.payment_amt > 0:
                rec.write({'state':'Confirmed','stage':'New'})
            else:
                raise Warning("Cannot confirm. No payment amount.")
               
#     @api.multi
#     def void_asset_tran(self):
#         asset_recs = self.env['vb.asset_tran'].search([('id', 'in',self._context.get('active_ids'))])
#         for rec in asset_recs:
#             rec.write({'state':'Void','stage':'Void','void_by':self._uid, 'date_void':datetime.now(), 'comments': self.note})
            
    @api.multi
    def cancel_earmark_shares(self):
        asset_recs = self.env['vb.asset_tran'].search([('id', 'in', self._context.get('active_ids'))])
        for rec in asset_recs:
            rec.write({'state':'Cancelled','stage':'Void', 'void_by':self._uid, 'date_void':datetime.now()})
    
    @api.multi
    def post_earmark_shares(self):
        asset_recs = self.env['vb.asset_tran'].search([('id', 'in', self._context.get('active_ids'))])
        for rec in asset_recs:
            rec.write({'state':'Posted','stage':'Posted','posted_by':self._uid, 'date_posted':datetime.now()})
            
    @api.multi
    def void_earmark_shares(self):
        asset_recs = self.env['vb.asset_tran'].search([('id', 'in', self._context.get('active_ids'))])
        for rec in asset_recs:
            rec.write({'state':'Void','stage':'Void','void_by':self._uid,'date_void':datetime.now()})

    @api.multi
    def move_assets(self):
        self._cr.execute("SELECT sp_temp_asset_update( %s,%s); ", (str(self.batch_no), str(self.product_type.name)))
        output = self._cr.fetchone()[0]
        if output < 0:
            raise Warning("Process has not completed. Got response {0}".format(output))
        else:
            final_view = self.env['ir.model.data'].get_object_reference('vb_portfolio','temp_asset_successful_wizard')[1]
            message = "This temporary assets has been moved successfully."
            return {
                'name': 'Successfully move temporary assets.',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': final_view,
                'target': 'new',
                'context': {'default_message': message},
            }
            
    @api.multi
    def sync_portfolios(self):
        if self.sync_options =='Batch':
            self._cr.execute("SELECT public.pg_sync_portfolio()")
            
            output = self._cr.fetchone()[0]
            print "Batch"
            if output < 0:
                raise Warning("Process has not completed. Got response {0}".format(output))
            else:
                url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RefreshAssetCache')])
                if not url_rec:
                    raise Warning("There is no record in vb.config for RefreshCache")
                url = str(url_rec.parm1)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                if url==False:
                    raise Warning('Please provide the IP to connect with webservice for RefreshAssetCache 1')
                data = dumps({})
                headers={'Content-type': 'application/json'}
                content = requests.get(url,data=data,headers=headers)
                print content    
                if content.json().get('code')!=200 or content ==False:
                    raise Warning('Can not connect with web service for RefreshAssetCache 1')
                if url_rec.parm3:
                    url2 = str(url_rec.parm3)+":"+str(url_rec.parm2)+'/'+str(url_rec.parm5)
                    if url2 == False:
                        raise Warning('Please provide the IP to connect with webservice for RefreshAssetCache 2')
                    data = dumps({})
                    headers={'Content-type': 'application/json'}
                    content = requests.get(url2,data=data,headers=headers)  
                    if content.json().get('code')!=200 or content ==False:
                            raise Warning('Can not connect with web service for RefreshAssetCache 2') 
                    else:
                        final_view = self.env['ir.model.data'].get_object_reference('vb_portfolio','temp_asset_successful_wizard')[1]
                        message = "Process completed successfully"
                        return {
                            'name': 'Successfully batch portfolio',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'view_id': final_view,
                            'target': 'new',
                            'context': {'default_message': message},
                        }
                else:
                    raise Warning(_("IP for the second server is not provided in parm3 \ncode: RefreshAssetCache"))
        elif self.sync_options == 'Single':
            count_failed = 0
            for i in self.acct_ids_sync:
                self._cr.execute('SELECT sp_sync_portfolio_by_acct(%s)'%i.id)
                output = self._cr.fetchone()[0]
                print "Single"
                if output < 0:
                    count_passed+=1
            if count_failed > 0:
                raise Warning(str(count_failed)+" records failed to process")
            else:
                final_view = self.env['ir.model.data'].get_object_reference('vb_portfolio','temp_asset_successful_wizard')[1]
                message = "Process completed successfully"
                return {
                    'name': 'Successfully Sync portfolio',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'view_id': final_view,
                    'target': 'new',
                    'context': {'default_message': message},
                    }

    @api.multi
    def approve_asset(self):
        approve_rec = self.env['vb.asset'].search([('id','=',self._context.get('active_id'))])
        if approve_rec:
            approve_rec.write({'approve_status':'approved', 'asset_approve_date':datetime.now(), 'asset_approve_uid':self._uid})
            return {
                'approve_status': 'approved',
                }