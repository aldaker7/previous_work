from openerp import models, fields, api
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


# ===================================
# =           Bank acount           =
# ===================================

class bankacct(models.Model):
    _inherit = 'vb.bankacct'

    stmt_freq       = fields.Selection([
        ('Daily', 'Daily'),
        ('Monthly', 'Monthly'),
        ], string='Statement frequency')


# ================================================
# =           Bank account transaction           =
# ================================================

class bankacct_tran(models.Model):
    _inherit    = 'vb.bankacct_tran'
    
    bank_stmt_id    = fields.Many2one(comodel_name='vb.bankacct_stmt', string='Bank statement',ondelete='cascade',index=True)
    stmt_date       = fields.Date(string='Statement date',index=True)
    bank_tran_code  = fields.Char(string='Bank transaction code',index=True)
    cash_tran_id    = fields.Many2one(comodel_name='vb.cash_tran',string='Related cash transaction')
    tran_side       = fields.Selection([
        ('Bank','Bank'),
        ('Company','Company')
        ],string='Side',default='Bank')
    bank_seqno      = fields.Integer(string='Bank sequence number')

    @api.multi
    def name_get(self):
        result=[]
        for rec in self:
            result.append((rec.id,u"%s" % rec.id))
        return result

    def bulk_confirm_action_button(self, cr, uid, data, context):
        active_ids = context.get('active_ids')
        try:
            form_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'vb_bankrecon', 'bankacct_tran_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'bankacct_tran_wizard'. Please contact IT Support"))
        return{
                    'name': "Bank reconciliation wizard",
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': form_id,
                    'context': {'active_ids': active_ids},
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    }

    @api.multi
    def write(self,vals):
        if vals.get('reconciled'):
            vals['date_reconciled'] = datetime.now()
            vals['reconciled_by'] = self._uid
        res = super(bankacct_tran, self).write(vals)
        if res:
            self._cr.execute("select sum(tran_amt) from vb_bankacct_tran where reconciled=false")
            unrecon_amt = self._cr.fetchone()[0]
            self._cr.execute("select sum(tran_amt) from vb_bankacct_tran where reconciled=true")
            recon_amt = self._cr.fetchone()[0]
            for rec in self:
                query = "update vb_bankacct_stmt set unrecon_amt=%s, recon_amt=%s where id=%s"
                self._cr.execute(query,(unrecon_amt, recon_amt, int(rec.bank_stmt_id)))
                _logger.info("updated new unrecon_amt=%s, recon_amt=%s for bankacct_stmt id=%s" % (unrecon_amt, recon_amt, int(rec.bank_stmt_id)))
        return res

    
# ==============================================
# =           Bank account statement           =
# ==============================================

class bankacct_stmt(models.Model):
    _name = 'vb.bankacct_stmt'
    _rec_name = 'stmt_date'
    
    bank_acct_id    = fields.Many2one(comodel_name='vb.bankacct', string='Bank account',ondelete='restrict',index=True)
    bank_acct_no    = fields.Char(string='Bank account number',index=True)
    stmt_date       = fields.Date(string='Statement date',index=True)
    opening_bal     = fields.Float(string='Opening balance',digits=(15,2),default=0)
    closing_bal     = fields.Float(string='Closing balance',digits=(15,2),default=0)
    unrecon_amt     = fields.Float(string='Un-reconciled amt',digits=(15,2),default=0)
    recon_amt       = fields.Float(string='Reconciled amt',digits=(15,2),default=0)    
    state           = fields.Selection([
        ('New', 'New'),
        ('Processed', 'Processed'),
        ('Closed', 'Closed'),
        ], string='Status', default='New')
    bankacct_tran_id = fields.One2many(comodel_name='vb.bankacct_tran',inverse_name='bank_stmt_id', string='Bank account transaction')

    @api.multi
    def get_list(self):
        mod_obj = self.env['ir.model.data']
        bankacct_tran = self.env['vb.bankacct_tran'].search([('bank_stmt_id','=',self.id)])
        search_res = None
        form_res = None
        if bankacct_tran:
            try:
                tree_res = mod_obj.get_object_reference('vb_bankacct_tran', 'view_bank_acct_tran_tree')[1]
                search_res = mod_obj.get_object_reference('vb_bankacct_tran', 'view_bank_acct_tran_search')[1]
                form_res = mod_obj.get_object_reference('vb_bankacct_tran', 'view_bank_acct_tran_form')[1]
            except ValueError:
                tree_res = False
            return {
                    'name':'Statement date, %s' % self.stmt_date,
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode':'tree, form',
                    'res_model': 'vb.bankacct_tran',
                    'view_id': False,
                    'views': [(tree_res, 'tree'), (form_res, 'form')],
                    'domain':[('bank_stmt_id','=',self.id)],
                    'search_view_id': [(search_res)],
                    'context': {'search_fields': 'unreconciled,reconciled', 'search_default_unreconciled': 1},
                }

    
# =======================================================
# =           Bank account transaction detail           =
# =======================================================

class bankacct_tran_dtl(models.Model):
    _name    = 'vb.bankacct_tran_dtl'
    _description = 'Bank account transaction match details'
    
    bank_tran_id = fields.Many2one(comodel_name='vb.bankacct_tran',string='Related bank transaction',index=True)
    cash_tran_id    = fields.Many2one(comodel_name='vb.cash_tran',string='Related cash transaction')
    tran_refno      = fields.Char(string='Transaction reference')
    tran_amt        = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    tran_loc        = fields.Float(string='Transaction local amt',digits=(15,2),default=0)
    
