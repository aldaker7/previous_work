from openerp import models, fields, api, _
import os
from datetime import datetime,timedelta,date
from openerp.exceptions import Warning
import json
import requests
from json import dumps
import json
from dateutil.relativedelta import relativedelta
import xlrd
import csv
import unicodecsv
import base64
from docutils.parsers.rst.directives import path
import re
import tempfile
import sys
import logging

#Get the logger
_logger = logging.getLogger(__name__)

class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'

    @api.multi
    def upload_bank_stmt_doc(self):

        isfound_sp_update_bank_recon_single = False
        isfound_sp_update_bank_recon_batch = False

        sql = "SELECT p.proname as sp FROM pg_catalog.pg_namespace n JOIN pg_catalog.pg_proc p ON p.pronamespace = n.oid "
        sql +="WHERE n.nspname = 'public';"
        self._cr.execute(sql)
        result = self._cr.fetchall()

        for rec in result:
            if ''.join(rec) == 'sp_update_bank_recon_single':
                isfound_sp_update_bank_recon_single = True
            elif ''.join(rec) == 'sp_update_bank_recon_batch':
                isfound_sp_update_bank_recon_batch = True

        if isfound_sp_update_bank_recon_single == False:
            raise Warning("Store procedure 'sp_update_bank_recon_single' not exist")
        elif isfound_sp_update_bank_recon_batch == False:
            raise Warning("Store procedure 'sp_update_bank_recon_batch' not exist")
        elif self.bank_ids.id == False:
            raise Warning("Please select bank")
        elif self.start_date1 == False:
            raise Warning("Please select date")
        else:
            today = datetime.now().strftime("%Y-%m-%d")
            if self.upload_document:
                # save the file to /tmp folder
                if os.path.isdir(tempfile.gettempdir()) == False:
                    new_dir =  tempfile.mkdtemp()
                file_path = tempfile.gettempdir()+'/'+str(self.file_name)
                with open(file_path, 'wb') as new_file:
                    decoded_data = base64.b64decode(self.upload_document)
                    new_file.write(decoded_data)
                # open and read the file
                file=open(file_path)
                reader = csv.reader(file)
                # convert statement date to date format
                year, month, day = map(int, self.start_date1.split("-"))
                stmt_date_form = date(year, month, day)
                _logger.info("Bank account number entered %s" % self.bank_ids.bank_acct_no)
                _logger.info("Statement date entered %s" % stmt_date_form)
                # initialize
                line = 1
                bank_stmt_header = {}
                bank_stmt_list = []
                for rec in reader:
                    data = {}
                    if line == 3: # account no
                        acctno = (rec[0])[14:24]
                        # account no checking
                        matched = re.search("^(\d{10})$", acctno)
                        if not matched or self.bank_ids.bank_acct_no != acctno:
                            _logger.error("Bank account number entered %s not matched from file account number %s" % (self.bank_ids.bank_acct_no, acctno))
                            raise Warning("Account number entered does not match account number.")
                        bank_stmt_header['acctno'] = acctno
                    elif line == 4: # statement date
                        day, month, year = map(int, (rec[0])[22:32].split("-"))
                        stmt_date = date(year, month, day)
                        # statement date checking
                        if stmt_date != stmt_date_form:
                            _logger.error("Statement date entered %s not matched from file date %s" % (stmt_date_form, stmt_date))
                            raise Warning("Statement date entered does not match statement date.")
                        bank_stmt_header['stmt_date'] = stmt_date
                    elif line == 5: # opening available balance
                        bank_stmt_header['opening_bal'] = float((rec[0][28:]).rstrip())
                    elif line == 6: # closing available balance
                        bank_stmt_header['closing_bal'] = float((rec[0][28:]).rstrip())
                    # start reading at line 8 for the bank statement list details
                    elif line >= 8:
                        # data['no import'] = rec[0].strip()
                        data['bank_seqno'] = rec[1].strip()
                        data['tran_date'] = datetime.strptime(rec[2].strip(), "%d%m%Y")
                        data['bank_tran_code'] = self.translate_bank_code(rec[3].strip())
                        data['tran_desc'] = self.clean_string(rec[4].strip()) + "|" + rec[15].strip() + "|" + self.clean_string(rec[17].strip())
                        # data['no import'] = rec[5].strip()
                        data['other_refno'] = self.clean_string(rec[6].strip())
                        data['tran_amt'] = rec[7].strip() if rec[8].strip() == 'C' else '-' + rec[7].strip()
                        # data['no import'] = rec[8].strip()
                        # data['no import'] = rec[9].strip()
                        # data['no import'] = rec[10].strip()
                        # data['no import'] = rec[11].strip()
                        data['tran_refno'] = self.clean_string(rec[12].strip())
                        # data['no import'] = rec[13].strip()
                        # data['no import'] = rec[14].strip()
                        # data['recipient'] = rec[15].strip()
                        # data['Other Payment Details'] = rec[16].strip()
                        # data['sender_name'] = self.clean_string(rec[17].strip())
                        # add data to the list
                        bank_stmt_list.append((0,0,data))
                    # increment counter
                    line += 1

                # save to db
                self.env['vb.bankacct_stmt'].sudo().create({'bank_acct_no':bank_stmt_header['acctno'],
                                                            'stmt_date':bank_stmt_header['stmt_date'],
                                                            'opening_bal':bank_stmt_header['opening_bal'],
                                                            'closing_bal':bank_stmt_header['closing_bal'],
                                                            'bankacct_tran_id':bank_stmt_list
                                                       })
                _logger.info("create records to vb.bankacct_stmt and vb.bankacct_tran")
                # call stored procedure
                self._cr.execute("SELECT sp_update_bank_recon_single('%s');" % str(bank_stmt_header['stmt_date'].strftime('%Y-%m-%d')))
                output = self._cr.fetchone()[0]
                if output >= 0:
                    _logger.info("sp_update_bank_recon_single is executed")
                    self._cr.execute("SELECT sp_update_bank_recon_batch ('%s');" % str(bank_stmt_header['stmt_date'].strftime('%Y-%m-%d')))
                    output = self._cr.fetchone()[0]
                    if output >= 0:
                        _logger.info("sp_update_bank_recon_batch is executed")
                        try:
                            form_id = self.env['ir.model.data'].get_object_reference('vb_bankrecon', 'import_bank_stmt_wizard_view')[1]
                            message = 'Bank reconciliation checking process is done. Please check the list to reconcile the records.'
                        except ValueError:
                            form_id = False
                            raise Warning(_("Cannot locate required 'confirm_corp_action_approve_cancel_wizard_view'. Please contact IT Support"))
                        return {
                            'name': "Import bank statement result",
                            'view_type': 'form',
                            'view_mode': "[form]",
                            'view_id': False,
                            'res_model': 'vb.common_wizard',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'views': [(form_id, 'form')],
                            'context': {'default_message': message},
                        }
                    else:
                        raise Warning("Calling sp_update_bank_recon_batch is not completed. Got response {0}".format(output))
                else:
                    raise Warning("Calling sp_update_bank_recon_single is not completed. Got response {0}".format(output))
                _logger.info("Import bank statement is successful")
            else:
                raise Warning("Please upload a bank statement document")

    @api.model
    def clean_string(self, rec):
        text = rec[2:]
        text = text[0:len(text) - 1]
        return text

    @api.model
    def translate_bank_code(self, bank_code):
        if bank_code in ['0148','0618']:
            return "Online Deposit"
        elif bank_code == '0060':
            return "CA Subscription"
        elif bank_code in ['0341','0345']:
            return "Refund 3rd party deposit"
        elif bank_code == '0669':
            return "Clients withdrawal"
        elif bank_code == '0143':
            return "Dividend"
        elif bank_code == '0141':
            return "CDS Opening Fee"
        elif bank_code == '0005':
            return "T.T."
        elif bank_code in ['0357','0489','0299']:
            return "T.T."
        elif bank_code in ['0663','0619']:
            return "Clients withdrawal rejected"
        elif bank_code in ['0174','0599','0101','0123','0121']:
            return "Offline Deposit"


    @api.multi
    def confirm_bankacct_tran(self):
        recs = self.env['vb.bankacct_tran'].search([('id','in',self._context.get('active_ids'))])
        if recs:
            count = 0
            for rec in recs:
                rec.write({'reconciled': True, 'reconciled_by': self._uid, 'date_reconciled': datetime.now()})
                count += 1
            success_wizard = self.env['ir.model.data'].get_object_reference('vb_bankrecon', 'bankacct_tran_success_wizard')[1]
            message = "%s reconciled successfully." % str(count)
            return {
                'name': 'Bank reconciliation',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'vb.common_wizard',
                'view_id': success_wizard,
                'target': 'new',
                'context': {'default_message': message},
            }
