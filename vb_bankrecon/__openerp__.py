# -*- coding: utf-8 -*-

{
    'name': 'VB Bank Reconciliation',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Bank reconciliation Module
======================================================

This module contains the views and functions for bank reconciliation.

    """,
    'data': [
        'wizards/wizards_reconcile.xml',
        'views/views_reconcile.xml',
        'views/actions_reconcile.xml',
        'views/menus_reconcile.xml',
            ],

    'installable': True,
    'auto_install': False,
    'application': True,
}