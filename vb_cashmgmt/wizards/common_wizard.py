import sys
import os
import base64
import openerp
from openerp import models, api, fields,_
from datetime import datetime
from openerp.exceptions import Warning


class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'
    
    @api.multi
    def get_recs(self):
        line_feed = "\r\n"
        tran_recs = self.env['vb.cash_tran'].search([('id','in',self._context.get('active_ids'))])
        for k in tran_recs:
            if k.state != "Posted":
                raise Warning("Only posted record can be extracted.")
            elif k.state == "Posted" and k.print_date:
                raise Warning("Records with extraction date cannot be re-extract again!")
            else:
                self._cr.execute("SELECT sp_get_refno( %s,%s); ",("VB_CASH_TRAN","BULKPAYMENT"))
                refno = self._cr.fetchall()[0][0]
                k.update({'bank_refno':refno})
        for i in tran_recs:
                print_text=""
                #Header - Record Type default 01 - Char(2)
                print_text="01"
                
                #Org Code  - Number(5)
                org_code = self.env['vb.config'].search([('code','=','BizChanOrgCode'),('code_type','=','App')])[0].parm1
                print_text= print_text + org_code
                
                #Org Name - Char(40) left justified append with space
                org = self.env['res.company'].search([('id','=',1)])
                
                org_name = org.name
                print_text= print_text + org_name.ljust(40)
                
                #Crediting Date - Number(8) - DDMMYYYY
                print_text= print_text + datetime.now().strftime('%0d%0m%Y')
                
                #Security Code -Number(16) - fill with zeros
                print_text= print_text + "%016d"%0
                
                #Filler -spaces(2)
                print_text= print_text + "  "
                
                #Add new line
                print_text = print_text+ line_feed
              
                total_amt = 0.0
                no_recs =len(tran_recs)
               
                for i in tran_recs :
                    
                    #Deatil - Record Type default 02 - Char(2)
                    print_text=print_text+ "02"
                
                    #BNM Code  - Number(7) - BNM for CIMB is 35
                    if i.bnm_code:
                        print_text= print_text + i.bnm_code.ljust(7, '0')
                    else:
                        print_text= print_text + " ".ljust(7, '0')
                   
                    #Bank Acct No- Char(16)    
                    #Remove "-" and "/"
                                     
                    bank_acct_no = i.acct_id.bank_acct_id.bank_acct_no
                    if bank_acct_no:                         
                        bank_acct_no = bank_acct_no.replace("-","")
                        bank_acct_no = bank_acct_no.replace("/","")  
                        bank_acct_no = bank_acct_no.ljust(16)   
                    else:
                        bank_acct_no = " ".ljust(16)
                        
                    if len(str(bank_acct_no)) > 16:
                        bank_acct_no = bank_acct_no[:16]
                    else:
                        bank_acct_no = bank_acct_no
                    
                    print_text = print_text + bank_acct_no
                    
                    #Beneficiary Name   - Char(40) 
                    #Remove "-" and "/"
                    
                    beneficary_name = i.acct_id.customer_id.banks_ids.bank_acct_name
                    beneficary_name = beneficary_name.replace("-","")
                    beneficary_name = beneficary_name.replace("/","")
                    beneficary_name = beneficary_name[:40]
                    beneficary_name = beneficary_name.ljust(40)
                    print_text = print_text + beneficary_name
                    
                    #Payment Amount- Number(11) 
                    #Remove decimal point 100.00 as 10000
                    tran_amt = i.tran_amt           
                    
                    #add with total amt 
                    total_amt = total_amt + tran_amt
                    
                    
                    
                    abs_tran_amt = abs(tran_amt)
                    abs_tran_amt = '%.2f' % abs_tran_amt  
                    
                    
                    abs_tran_amt_str = str(abs_tran_amt)
                    abs_tran_amt_str = abs_tran_amt_str.replace(".","")
                    abs_tran_amt_str = "%011d"%float(abs_tran_amt_str)
                    
                    abs_tran_amt_str = str(abs_tran_amt_str)
                    
                    if len(abs_tran_amt_str) > 11:
                        abs_tran_amt_str = abs_tran_amt_str[:11]
                    else:
                        abs_tran_amt_str = str(abs_tran_amt_str)
                     
                    print "::::abs_tran_amt_atr",abs_tran_amt_str
                    
                    print_text = print_text + abs_tran_amt_str
                    
                   
                    #Reference Number - Char(30)
                    #Remove "-" and "/"
                    ref_no = i.tran_refno
                    
                    if ref_no:
                        print "::::ref_no",ref_no
                        ref_no = ref_no.replace("-","")
                        ref_no = ref_no.replace("/","")
                        ref_no = ref_no.ljust(30)   
                    else:
                        ref_no = " ".ljust(30)   
                   
                    print_text = print_text + ref_no
                    
                    
                    #Beneficiary ID - Char(20)
                    #Remove "-" and "/"
                    beneficiary_id = i.acct_id.customer_id.national_id_no
                    beneficiary_id = beneficiary_id.replace("-","")
                    beneficiary_id = beneficiary_id.replace("/","")
                    beneficiary_id = beneficiary_id.ljust(20)
                    
                    print "::::beneficiary_id",beneficiary_id
                    
                    print_text = print_text +beneficiary_id
                    
               
                    #Transaction Type - Num(1) - Default to 2
                    print_text = print_text + str(2)
                    
                    #Email - Char(70)
                    beneficiary_email = i.acct_id.customer_id.email1
                    beneficiary_email = beneficiary_email.ljust(70)
                    
                    print "::::beneficiary_email",beneficiary_email
                    
                    print_text = print_text + beneficiary_email
                    
                    #Payment Reference Number - Char(5)
                    
                    payment_refno = i.bank_refno
                    if payment_refno:
                        payment_refno = payment_refno.ljust(5)
                    else:
                        payment_refno = " ".ljust(5)   
                    
                    print "::::payment_refno",payment_refno
                    
                    print_text = print_text + payment_refno
                    
                    #Payment Description - Char(20)
                    payment_desc = i.tran_desc
                    if payment_desc:
                       payment_desc = payment_desc.ljust(20)
                    else:
                        payment_desc = " ".ljust(20)
                    
                    print "::::payment_desc",payment_desc
                    
                    print_text = print_text + payment_desc
                    
                    
                    
                    #Add new line
                    print_text = print_text+ line_feed
                    
                    
                    #Record type - 00
                    print_text=print_text+ "00"
                     
                    #Payment Reference Number - Char(5)
                    payment_refno = i.bank_refno
                    if payment_refno:
                        payment_refno = payment_refno.ljust(5)
                    else:
                        payment_refno = " ".ljust(5)
                         
                    print_text = print_text + payment_refno
                    
                    print_text = print_text + str(i.id).ljust(30)
                     
                    payment_desc = i.tran_desc
                    if payment_desc:
                       payment_desc = payment_desc.ljust(500)
                    else:
                        payment_desc = " ".ljust(500)
                         
                    print_text = print_text + payment_desc
                     
                    #Payment Amount- Number(11) 
                    #Remove decimal point 100.00 as 10000
                    tran_amt = i.tran_amt           
                     
                    #add with total amt 
#                     total_amt = total_amt + tran_amt
                     
                    abs_tran_amt = abs(tran_amt)
                    abs_tran_amt = '%.2f' % abs_tran_amt  
                     
                     
                    abs_tran_amt_str = str(abs_tran_amt)
                    abs_tran_amt_str = abs_tran_amt_str.replace(".","")
                    abs_tran_amt_str = "%011d"%float(abs_tran_amt_str)
                    
                    abs_tran_amt_str = str(abs_tran_amt_str)
                    
                    if len(abs_tran_amt_str) > 11:
                        abs_tran_amt_str = abs_tran_amt_str[:11]
                    else:
                        abs_tran_amt_str = str(abs_tran_amt_str)
                    
                    print_text = print_text + abs_tran_amt_str
                     
                    print_text = print_text + datetime.strptime(i.tran_date, ('%Y-%m-%d')).strftime('%d%m%Y')
                     
                    #Filler -spaces(10)
                    print_text= print_text + "          "
                    
                    print_text= print_text+line_feed
                    
                       
                #Record Type Num(2) Default to "03"
                print_text = print_text + "03"
                 
                 
                #Total No Of recs - Num(6)
                
                no_recs = "%06d"%no_recs
                
                print "::::no_recs",no_recs
                
                print_text = print_text + str(no_recs)
                
                #Total Amount Num(13)
                #take absolute value for printing
                abs_total_amt = abs(total_amt)
                abs_total_amt = '%.2f' % abs_total_amt  
                
                abs_total_amt_str = str(abs_total_amt)
                abs_total_amt_str = abs_total_amt_str.replace(".","")
                abs_total_amt_str = "%013d"%float(abs_total_amt_str)
               
                print "::::abs_total_amt_str",abs_total_amt_str
                
                print_text = print_text + abs_total_amt_str
        
                
                report_name = "CashTransactions"
                filename = "CashTransactions" + '.txt'
                
                wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(print_text),'file_name':filename})
                
                for extract_rec in tran_recs:
                    extract_rec.update({'print_date':datetime.now()})
                      
            
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_download_wizard_form')[1]
                except ValueError:
                    form_id = False
                return {
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(form_id, 'form')],
                            'view_id': form_id,
                            'res_id' : wizard_rec.id,
                            'target': 'new',
                            'context': {},
                        }
       
    @api.multi
    def cancel_cash_tran(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','in',self._context.get('active_ids'))])
        for rec in tran_recs:
            rec.write({'state':'Cancelled','stage':'Void','cancelled_by':self._uid, 'date_cancelled':datetime.now(), 'comments': self.note, 'reason_cancelled_id':self.reason_cancel.id})

    @api.multi
    def post_cash_tran(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','in',self._context.get('active_ids'))])
        for rec in tran_recs:
            if rec.tran_type == 'Deposit' and rec.tran_mode == 'Offline':
                if rec.document_ids:
                    for i in rec.document_ids:
                        if i.state !='Approved':
                            raise Warning ("Please approve all the documents")
                        else:
                            rec.write({'state':'Posted','stage':'Posted','posted_by':self._uid, 'date_posted':datetime.now()})
            
            rec.write({'state':'Posted','stage':'Posted','posted_by':self._uid, 'date_posted':datetime.now()})

    @api.multi
    def void_cash_tran(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','in',self._context.get('active_ids'))])
        for rec in tran_recs:
            rec.write({'state':'Void','stage':'Void','void_by':self._uid, 'date_void':datetime.now(), 'comments': self.note, 'reason_void_id':self.reason_void.id})
            
    @api.multi
    def cancel_cash_transfer_req(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','=',self._context.get('active_id'))])
        tran_recs.write({'state':'Cancelled','stage':'Void','cancelled_by':self._uid, 'date_cancelled':datetime.now(),
                         'comments': self.note, 'reason_cancelled_id': self.reason_cancel.id})
            
    @api.multi
    def post_cash_transfer_req(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','=',self._context.get('active_id'))])
        tran_recs.write({'state':'Posted','stage':'Posted','posted_by':self._uid, 'date_posted':datetime.now()})
            
    @api.multi
    def void_cash_transfer_req(self):
        tran_recs = self.env['vb.cash_tran'].search([('id','=',self._context.get('active_id'))])
        tran_recs.write({'state':'Void','stage':'Void','void_by':self._uid, 'date_void':datetime.now(),
                         'comments': self.note, 'reason_void_id':self.reason_void.id})
        
    # Method for extracting specific data in csv format from Cash deposit and Cash withdrawal tree view
    @api.multi
    def get_cash_tran_rec_csv(self): 
        tran_recs = self.env['vb.cash_tran'].search([('id','in',self._context.get('active_ids'))])
        for k in tran_recs:
            if k.state != "Posted":
                raise Warning("Only posted record can be extracted.")
            
        print_header = ("Customer account"+","+"Customer bank"+","+"Tran date"+","+"Date/Time posted"+","+"Transaction type code"
        +","+"Transaction reference"+","+"Bank reference"+","+"Payment Mode"+","+"Transaction amount"+","+"Currency"+","+"Status")
        
        for rec in tran_recs:
            if rec.tran_type == 'Deposit':
                report_name = 'CashDepositReport'
                filename = report_name + '.csv'
            elif rec.tran_type == 'Withdraw':
                report_name = 'CashWithdrawReport'
                filename = report_name + '.csv'
        
        data = []
        for i in tran_recs:
            data.append([i.acct_id.acct_no,i.customer_bank_name,i.tran_date,i.date_posted,i.tran_type,
                         i.tran_refno,i.bank_refno,i.tran_mode,i.tran_amt,i.currency_name,i.state])
            
        count =0
        text1=''
        for i in data:
            for k in i:
                text1 += str(k)
                count +=1
                if count == 11:
                    text1+='\n'
                    count = 0
                else:
                    text1+=','

        final_text = print_header +'\n'+text1
        wizard_rec = self.env['vb.common_wizard'].create({'file':base64.b64encode(final_text),'file_name':filename})
        
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'view_download_wizard_form')[1]
        except ValueError:
            form_id = False
        return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'res_id' : wizard_rec.id,
                    'target': 'new',
                    'context': {},
                }
    @api.multi    
    def send_new_withdraw_amt(self):
        withdraw_rec = self.env['vb.cash_tran'].search([('id','=',self._context.get('active_id'))])
        if withdraw_rec:
            withdraw_limit = self.env['vb.customer_acct_bal'].search([('acct_id','=',withdraw_rec.acct_id.id)]).withdraw_limit
            if withdraw_limit < self.unrlsd_contra :
                raise Warning(_('The withdraw amount is exceeding the withdraw limit \n Withdraw limit is '+str(withdraw_limit)))
            elif self.unrlsd_contra <= 0:
                raise Warning(_("Transaction amount should be greater than 0"))
            else:
                print withdraw_rec.tran_amt
