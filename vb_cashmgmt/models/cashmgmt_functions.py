from openerp import models, api, _
from datetime import datetime
from openerp.exceptions import Warning, ValidationError

class cash_tran(models.Model):
    _inherit = 'vb.cash_tran'
    
    # this button used at cash_tran form 
    @api.multi
    def get_cash_tran_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'cancel_cash_tran_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'cancel_cash_tran_wizard'. Please contact IT Support"))
        return{
                    'name': "Cancel Cash Transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

    @api.multi
    def get_cash_tran_post_wizard(self):
        if self.tran_type == 'Deposit' and self.tran_mode == 'Offline':
            if not self.document_ids:
                raise Warning(_('There are no documents provided'))
            else:
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'post_cash_tran_wizard')[1]
                except ValueError:
                    form_id = False
                    raise Warning(_("Cannot locate required 'post_cash_tran_wizard'. Please contact IT Support"))
                return{
                            'name': "Post Cash Transaction",
                            'view_type': 'form',
                            'view_mode':"[form]",
                            'view_id': False,
                            'res_model': 'vb.common_wizard',
                            'type': 'ir.actions.act_window',
                            'target':'new',
                            'views': [(form_id, 'form')],
                            }
        else:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'post_cash_tran_wizard')[1]
            except ValueError:
                form_id = False
                raise Warning(_("Cannot locate required 'post_cash_tran_wizard'. Please contact IT Support"))
            return{
                        'name': "Post Cash Transaction",
                        'view_type': 'form',
                        'view_mode':"[form]",
                        'view_id': False,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'views': [(form_id, 'form')],
                        }

    @api.multi
    def get_cash_tran_void_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'void_cash_tran_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'void_cash_tran_wizard'. Please contact IT Support"))
        return{
                    'name': "Void Cash Transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
#   Method to cancel Cash transfer request      
    @api.multi
    def get_cash_transfer_req_cancel_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'cancel_cash_transfer_req_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'cancel_cash_transfer_req_wizard'. Please contact IT Support"))
        return{
                    'name': "Cancel Cash Transfer Request",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
#   Method to post Cash transfer request        
    @api.multi
    def get_cash_transfer_req_post_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'post_cash_transfer_req_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'post_cash_transfer_req_wizard'. Please contact IT Support"))
        return{
                    'name': "Post Cash Transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }
        
#   Method to void Cash transfer request        
    @api.multi
    def get_cash_transfer_req_void_wizard(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'void_cash_transfer_req_wizard')[1]
        except ValueError:
            form_id = False
            raise Warning(_("Cannot locate required 'void_cash_transfer_req_wizard'. Please contact IT Support"))
        return{
                    'name': "Void Cash Transaction",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

    @api.multi
    def amend_cash_withdraw(self):
        if self.tran_type == "Withdraw" and self.state == 'New':
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_cashmgmt', 'amend_cash_withdraw_wizard')[1]
            except ValueError:
                form_id = False
            return{
                        'name': "Amend Withdraw Cash Transaction",
                        'view_type': 'form',
                        'view_mode':"[form]",
                        'view_id': False,
                        'res_model': 'vb.common_wizard',
                        'type': 'ir.actions.act_window',
                        'target':'new',
                        'views': [(form_id, 'form')],
                        }
        else:
            raise Warning(_("Transaction type should be Withdraw and/or state should be New"))

