# -*- coding: utf-8 -*-

{
    'name': 'VB Cash management',
    'version': '1.2',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_acctmgmt'],
    'description': """

Virtual Broker Application - Cash Management Module
======================================================

This module contains the functions for Cash Account Management.

    """,
    'data': [
        'views/views_cashmgmt.xml',
        'wizards/wizards_cashmgmt.xml',
        'views/actions_cashmgmt.xml',
        'views/menus_cashmgmt.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
