from openerp import models, fields

# This model will store information for use with the web front-end

class statistic_trade(models.Model):
    _name = 'vb.statistic_transaction'
    _description = 'Transaction statistics'
    
    # fields
    stat_date = fields.Date(string='Statistic date', index=True)                # Should be current date
    market = fields.Char(string='Market')                                       # Should default to BursaMalaysia
    product = fields.Char(string='Product')                                     # Blank / CashUpfront / Margin / Collateral
    tran_type = fields.Char(string='Transaction type')                          # Buy / Sell / CashDeposit / CashWithdral / ShrDeposit / ShrWithdraw
    trade_count = fields.Integer(string='Trade count')           # vb_trade_tran=>count() at product level
    trade_gross_value = fields.Float(string='Trade gross value',digits=(15,2))  # vb_trade_tran=>sum(abs(gross_amt)) at product level
    trade_net_value = fields.Float(string='Trade net value',digits=(15,2))      # vb_trade_tran=>sum(abs(net_amt)) at product level
    trade_balance_amt = fields.Float(string='Balance amt',digits=(15,2))        # vb_trade_tran=>sum(balance_amt) at product level
    trade_brkg_amt = fields.Float(string='Brokerage',digits=(11,2))             # vb_trade_tran=>sum(brkg_amt) at product level
    trade_brkg_raio = fields.Float(string='Brokerage ratio',digits=(5,2))  # vb_trade_tran=>sum(brkg_amt/total_trade_count)
    trade_brkg_gst = fields.Float(string='Brokerage GST',digits=(11,2))         # vb_trade_tran=>sum(brkg_gst) at product level
    trade_stamp_amt = fields.Float(string='Stamp duty',digits=(11,2))           # vb_trade_tran=>sum(stamp_amt) at product level
    trade_stamp_gst = fields.Float(string='Stamp duty GST',digits=(11,2))       # vb_trade_tran=>sum(stamp_gst) at product level
    trade_clearing_amt = fields.Float(string='Clearing fees',digits=(11,2))     # vb_trade_tran=>sum(clearing_amt) at product level
    trade_clearing_gst = fields.Float(string='Clearing fees GST',digits=(11,2)) # vb_trade_tran=>sum(clearing_gst) at product level
    trade_tax_amt = fields.Float(string='Govern tax',digits=(11,2))
    trade_tax_gst = fields.Float(string='Govern tax GST',digits=(11,2))
    trade_chrge1_amt = fields.Float(string='Other charges 1',digits=(11,2))
    trade_chrge1_gst = fields.Float(string='Other charges 1 GST',digits=(11,2))
    trade_chrge2_amt = fields.Float(string='Other charges 2',digits=(11,2))
    trade_chrge2_gst = fields.Float(string='Other charges 2 GST',digits=(11,2))
    trade_chrge3_amt = fields.Float(string='Other charges 3',digits=(11,2))
    trade_chrge3_gst = fields.Float(string='Other charges 3 GST',digits=(11,2))
    trade_commission_amt = fields.Float(string='Commission amt',digits=(11,2))
    trade_commission_gst = fields.Float(string='Commission GST',digits=(11,2))
    order_total_count = fields.Integer(string='Order count')                    # vb_order_book=>count() at product level
    order_new_count = fields.Integer(string='Order new count')                  # vb-order_book=>count(),tran_type=[New,AMO] at product level
    order_cancel_count = fields.Integer(string='Order cancellation count')      # vb-order_book=>count(),tran_type=Cancel at product level
    order_amend_count = fields.Integer(string='Order amendment count')          # vb-order_book=>count(),tran_type=Amend at product level
    order_matched_count = fields.Integer(string='Matched order count')          # vb-order_book=>count(),state=Matched at product level
    order_partial_matched_count = fields.Integer(string='Partial matched order count') # vb-order_book=>count(),state=[PartialMatched,PartialCancelled] at product level
    order_unmatched_count = fields.Integer(string='Unmatched order count')      # vb-order_book=>count(),state=[Acknowledged] at product level
    order_rejected_count = fields.Integer(string='Rejected order count')        # vb-order_book=>count(),state=[Rejected] at product level
    tran_amt = fields.Float(string='Transaction amount',digits=(15,2))
    tran_count = fields.Integer(string='Transaction count')
    tran_acct_count = fields.Integer(string='Transaction account count')

class statistic_acct(models.Model):
    _name = 'vb.statistic_account'
    _description = 'Account statistics'
    
    # fields
    stat_date = fields.Date(string='Statistic date', index=True)
    market = fields.Char(string='Market')
    product = fields.Char(string='Product')
    application_new_count = fields.Integer(string='Application new_count')
    customer_count = fields.Integer(string='Customer total count')
    customer_new_count = fields.Integer(string='Customer newly activated count')
    customer_new_count_web = fields.Integer(string='Customer newly activated count - website')
    customer_new_count_newspaper = fields.Integer(string='Customer newly activated count - newspaper')
    customer_new_count_smedia = fields.Integer(string='Customer newly activated count - social media')
    customer_new_count_seminar = fields.Integer(string='Customer newly activated count - seminar')
    customer_new_count_radio= fields.Integer(string='Customer newly activated count - radio')
    customer_new_count_mouth = fields.Integer(string='Customer newly activated count - mouth')
    customer_active_count = fields.Integer(string='Total active customer count')
    customer_acct_count = fields.Integer(string='Customer account count')
    customer_acct_new_count = fields.Integer(string='Customer account newly activated count')
    customer_acct_active_count = fields.Integer(string='Total active customer account count')
    customer_acct_close_count = fields.Integer(string='Customer account newly closed count')
    customer_acct_suspend_count = fields.Integer(string='Customer account newly suspend count')
    customer_acct_asset_count = fields.Integer(string='Customer account with asset count')
    avg_asset_value = fields.Float(string='Avg asset value per customer',digits=(15,2))
    portfolio_holding_value = fields.Float(string='Portfolio market value',digits=(15,2))
    portfolio_holding_cost = fields.Float(string='Portfolio cost value',digits=(15,2))
    cash_ledger_balance = fields.Float(string='Cash ledger balance',digits=(15,2))
    cash_available_balance = fields.Float(string='Cash available balance',digits=(15,2))
    trade_outstanding_balance = fields.Float(string='Trade outstanding balance',digits=(15,2))
    trade_buy_outstanding_balance = fields.Float(string='Trade buy outstanding balance',digits=(15,2))
    trade_sell_outstanding_balance = fields.Float(string='Trade sell outstanding balance',digits=(15,2))
    trade_other_outstanding_balance = fields.Float(string='Trade other outstanding balance',digits=(15,2))
