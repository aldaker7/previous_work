# -*- coding: utf-8 -*-

{
    'name': 'VB Statistics',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'description': """

Virtual Broker Application - Statistics Module
======================================================

This module contains the functions to provide statistics info.

    """,
    'data': [
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
