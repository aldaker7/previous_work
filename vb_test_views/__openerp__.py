# -*- coding: utf-8 -*-

{
    'name': 'VB Test views',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Test Data Module
======================================================

This module contains test data for testing purposes only

    """,
    'data': [
        'wizards/wizards_test.xml',
        'views/view_test.xml',
        'views/action_test.xml',
        'views/menu_test.xml',
    ],
    
    'installable': True,
    'auto_install': False,
    'application': True,
}
