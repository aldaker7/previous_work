from openerp import models, api
from datetime import datetime

class common_wizards(models.TransientModel):
    _inherit='vb.common_wizard'

#    Method to create record at vb.procedure_exec_request regarding to closing order
    @api.multi
    def Clear_Transactions(self):
        ExecRqs = self.env['vb.procedure_exec_request']
        ExecRqs.create({'name':'Clear transactions and balances', 'procfn':'CLEAR_TRAN_BAL','rqs_time':datetime.now(), 'rqsby_id':self._uid})
        ShowCompletionDisplay = self.env['ir.model.data'].get_object_reference('vb_test_views', 'show_completion_view')[1]
        message = "Request completed."
        return {
            'name':'Processing completed.',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vb.common_wizard',
            'view_id': ShowCompletionDisplay,
            'target': 'new',
            'context': {'default_message': message},
            }

