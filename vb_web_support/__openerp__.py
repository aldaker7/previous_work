# -*- coding: utf-8 -*-

{
    'name': 'VB Web Support',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Web Support Module
======================================================

This module contains the functions to support the Web front-end.

    """,
    'data': [
        'views/views_websupp.xml',
        'views/actions_websupp.xml',
        'views/menus_websupp.xml',        
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
