from openerp import models, fields

# This model will store information for use with the web front-end

class web_list(models.Model):
    _name = 'vb.web_list'
    _description = 'Web list'
    
    # fields
    list_seq = fields.Integer(string='List sequence')
    name = fields.Char(string="List item title")
    header1 = fields.Char(string="Header 1")
    header2 = fields.Char(string="Header 2")
    desc1 = fields.Char(string="Description 1")
    desc2 = fields.Char(string="Description 2")
    header1_html = fields.Html(string="Header html 1")
    header2_html = fields.Html(string="Header html 2")
    desc1_html = fields.Html(string="Description 1")
    desc2_html = fields.Html(string="Description 2")
    main_content_html = fields.Html(string='Main HTML content')
    apprej_date = fields.Datetime(string='Date/time approved/rejected')
    apprej_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected by',ondelete='restrict')
    held_date = fields.Datetime(string='Date/time put on hold')
    held_by = fields.Many2one(comodel_name='res.users',string='Held by',ondelete='restrict')
    relase_date = fields.Datetime(string='Date/time released')
    release_by = fields.Many2one(comodel_name='res.users',string='Released by',ondelete='restrict')
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Rejected', 'Rejected'),
        ('OnHold', 'On hold'),
        ], string='Status', default='New')
    active = fields.Boolean(string='Active',default=True)
    url_link = fields.Char(string='URL',default=True)
    url_thumbnail = fields.Char(string='Thumbnail URL')
    list_id = fields.Many2one(string='Web list',comodel_name='vb.common_code',domain="[('code_type','=','WebList')]")
    list_code = fields.Char(string='List code',related='list_id.code',store=True,readonly=True)
    # 08-March-2017
    category_id = fields.Many2one(string='Category',comodel_name='vb.common_code',domain="[('code_type','=','WebListCat')]")
