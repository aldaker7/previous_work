# -*- coding: utf-8 -*-

{
    'name': 'VB Contra',
    'version': '0.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Margin Module
==========================================

This module contains the function for margin module

    """,
    'data': [
        'views/contra_views.xml',
#         'views/template.xml',
#         "views/contra_wizard_views.xml",
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
