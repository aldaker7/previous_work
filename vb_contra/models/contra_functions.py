import os
from openerp import models, api, fields, _
from datetime import datetime, date, timedelta
from openerp.exceptions import Warning,ValidationError
import requests
from json import dumps
import json
import string, random, base64, hashlib
from cookielib import domain_match

class customer_acct(models.Model):
    _inherit = 'vb.customer_acct'
    
    @api.multi
    @api.constrains('document_ids','is_staff','comments')
    def cash_first(self):
        if self.acct_class_id.id == 24 and bool(self.rel_acct_id)== True and self.rel_acct_id.state in ['New','Incompinfo']:
            raise ValidationError("Customer has Cash Upfront application that is pending for Risk Check.\nPlease proceed it first.")
    
    @api.multi
    def get_cdd_edd(self):
        if self.riskcheck_ids:
            for i in self.riskcheck_ids:
                if i.state == 'New':
                    raise Warning("Please check if there is any New records in risk check first")
                elif i.state == 'Failed' and i.code != "Kyc02":
                    raise Warning("Cannot proceed to CDD/EDD while "+str(i.name)+" is Failed")
        for i in self:
#             if i.acct_type == 'Contra':
            qry = "Select id from vb_customer_acct_cedd where acct_id=%s"%self.id
            self._cr.execute(qry)
            acctid = self._cr.fetchone()
            if acctid:
                query = """select q01,q02,q03,q04,q05,q06a,q06b,q06c,q07,q08,q09,
                            q10,q11,q12,q13,state,cdd_checked_date,cdd_checked_by,
                            q31a,q31b,q32,q33,q34a,q34b,q35a,q35b,q36,q37,edd_checked_date,
                            edd_checked_by,q14
                           from vb_customer_acct_cedd where acct_id =%s"""%i.id
                self._cr.execute(query)
                result = self._cr.fetchone()
                        
                q01 = result[0] 
                q02 = result[1] 
                q03 = result[2] 
                q04 = result[3] 
                q05 = result[4] 
                q6a = result[5] 
                q6b = result[6] 
                q6c = result[7] 
                q07 = result[8] 
                q08 = result[9]
                q09 = result[10] 
                q10 = result[11] 
                q11 = result[12] 
                q12 = result[13] 
                q13 = result[14] 
                state = result[15] 
                cdd_check_date = result[16]
                cdd_checkedby = result[17]
                q31a = result[18]
                q31b = result[19]
                q32 = result[20]
                q33 = result[21]
                q34a = result[22]
                q34b = result[23]
                q35a = result[24]
                q35b = result[25]
                q36 = result[26]
                q37 = result[27]
                edd_checked_date = result[28]
                edd_checked_by = result[29]
#                     edd = result[30]
                q14 = result[30]
                if result:
                    mod_obj = self.env['ir.model.data']
                    try:
                        form_res = mod_obj.get_object_reference('vb_contra', 'view_cdd_edd_form')[1]
                    except ValueError:
                        form_res = False
                    return{
                           'name': "CDD & EDD",
                           'view_type': 'form',
                           'view_mode':"[form]",
                           'view_id': False,
                           'res_model': 'vb.customer_acct_cedd',
                           'type': 'ir.actions.act_window',
                           'views': [(form_res, 'form')],
                           'domain': [('acct_id','=', self.id)],
                           'context': {'default_acct_id':self.id,'default_q01':q01,'default_q02':q02,'default_q03':q03,
                                       'default_q04':q04,'default_q05':q05,'default_q06a':q6a,'default_q06b':q6b,
                                       'default_q06c':q6c,'default_q07':q07,'default_q08':q08,'default_q09':q09,
                                       'default_q10':q10,'default_q11':q11,'default_q12':q12,'default_q13':q13,
                                       'default_q14':q14,'default_cdd_checked_date':cdd_check_date,'default_cdd_checked_by':cdd_checkedby,
                                       'default_q31a':q31a,'default_q31b':q31b,'default_q32':q32,'default_q33':q33,
                                       'default_q34a':q34a,'default_q34b':q34b,'default_q35a':q35a,'default_q35b':q35b,
                                       'default_q36':q36,'default_q37':q37,'default_edd_checked_date':edd_checked_date,
                                       'default_edd_checked_by':edd_checked_by,'default_state':state},
                           'target':'new',
                               }
                else:
                    raise Warning("There is no CDD/EDD for this record in DB")
            else:
                raise Warning('There is no CDD/EDD for this record in DB')
#             else:
#                 raise Warning("This account is not Contra")

class customer_acct(models.Model):
    _inherit = 'vb.customer_acct'
    
    @api.multi
    def ramci_contra(self):
        if self.show_nrvb == True:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_acctmgmt', 'ramci_second_time_wizard_view')[1]
            except ValueError:
                form_id = False
            return {
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }
        else:
            stage1 = False
            stage2 = False
            url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RamciInfo')])
            url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)
            data = dumps({"loginName" : str(self.customer_id.login_name),
                          "acctId" : str(self.id),
                          "customerName": self.customer_id.name,
                          "idNo": str(self.national_id_no),
                          "idType" : str(self.national_id_type),
                          "country": str(self.customer_id.nationality_id.code),
                          "dob": str(self.customer_id.date_of_birth),
                          "mobileNo":str(self.mobile_phone1),
                          "emailAddress": str(self.email1),
                          "lastKnownAddress": ""})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)  
            print (content.json())
            if int(content.json().get('code'))!=200 or content ==False:
                raise Warning(_(content.json().get('message')))
            print (content.json())
            risk_list = []
            if not self.acct_class_id:
                raise Warning("Product has not been specified for this Account")
            else:
                acct_class_rec= self.acct_class_id
                if not acct_class_rec.riskprof_id:
                    raise Warning("Risk profile for the Product cannot be located")
                else:
                    riskprof_rec= acct_class_rec.riskprof_id
                    if not riskprof_rec.checklist_ids:
                        raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                    else:
                        checklist_state= riskprof_rec.checklist_ids
                        print checklist_state
            exist_list = ()
            for i in content.json().get('result'):
                if self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self.id)]):
                    for n in self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self.id)]):
                        exist_list += (n.id,)
            if exist_list:
                for i in exist_list:
                    if i!= False:
                        query ="Delete from vb_customer_acct_riskcheck Where id =%s"%i
                        self._cr.execute(query)
            if bool(checklist_state)==True:
    #             query ="Delete from vb_customer_acct_riskcheck Where acct_id =%s"%self.id
    #             self._cr.execute(query)   
                for rec in checklist_state:
                    if content.json().get('result'):
                        for i in content.json().get('result'):
                            print rec.code, str(i)
                            if str(rec.code) == str(i):
                                print "HERE"
                                if bool(int(content.json().get('result').get(str(rec.code))) >= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                    risk_dict = (0,0,{'name' : rec.name,'state':'OK','riskcheck_id':rec.id,'result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                elif bool(int(content.json().get('result').get(str(rec.code)))>= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,
                                                      'state':'OK','result_min':rec.result_min,
                                                      'result_max':rec.result_max,
                                                      'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                                else:
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'Failed','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
                    else:
                        raise Warning ("This account doesn't have any risk check list")
                self.write({'riskcheck_ids':risk_list})
            if int(content.json().get('code'))==200:
                stage1 = True
                query_cust = "Update vb_customer set ramci_kyc_date=%s,ramci_nrvb_date=%s where id=%s"
                self._cr.execute(query_cust,(datetime.now(),datetime.now(),self.customer_id.id))
            url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/'+str(url_rec.parm6)
            data = dumps({ "loginName" : self.customer_id.login_name,
                          "acctId" : str(self.id)})
            headers={'Content-type': 'application/json'}
            content = requests.post(url,data=data,headers=headers)  
            doc_rec = self.env['vb.document']
            x =content.text.split(',')
            stage2 = True
            
            self.update({'nrvb_clicked_flag':True})
            list = []
            doc_list =[]
            doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm2
            file_loc = doc_directory +'RAMCI/'
            if os.path.exists(file_loc)== False:
                os.makedirs(file_loc)
            for rec in self.env['vb.document'].search([('acct_id','=',self.id),('customer_id','=',self.customer_id.id)]):
                doc_list.append(rec.file_name)
            for i in x:
                index = x.index(i)
                if 'KYCPDF=' in i:    
                    file_name_kyc = str('KYCPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                    file_path_kycpdf = str(file_loc + file_name_kyc)
                    rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=',"KYC report")])
                    if rec_check:
                        for i in rec_check:
                            query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                            self._cr.execute(query)
                    
                    if file_name_kyc not in doc_list or bool(doc_list) == False:
                        kycpdf = base64.b64decode(x[index].split('KYCPDF=')[1])
                        f = open(file_path_kycpdf,'wb')
                        f.write(kycpdf)
                        f.close()
                        query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                         customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                          VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                        self._cr.execute(query,(file_path_kycpdf,'KYC report',1.0,file_name_kyc,'RAMCI',self.customer_id.id,
                                            self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                            'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
                elif 'NRVBPDF' in i: 
                    file_name_nrvb=str('NRVBPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                    file_path_nrvbpdf = str(file_loc + file_name_nrvb)
                    rec_check = self.env['vb.document'].search([('customer_id','=',self.customer_id.id),('acct_id','=',self.id),('name','=','NRVB report')])
                    if rec_check:
                        for i in rec_check:
                            query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                            self._cr.execute(query)
                    if file_name_nrvb not in doc_list or bool(doc_list) == False:
                        nrvbpdf = base64.b64decode(x[index].split('NRVBPDF=')[1])
                        g = open(file_path_nrvbpdf,'wb')
                        g.write(nrvbpdf)
                        g.close()
                        check_doc1 = self.env['vb.document'].search([('acct_id','=',self.id),('file_name','=',file_name_nrvb),('file_loc','=',file_path_nrvbpdf)])
                        query='''INSERT INTO vb_document (file_loc, name, doc_version,file_name,is_open_exist,
                         customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                          VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                        self._cr.execute(query,(file_path_nrvbpdf,'NRVB report',1.0,file_name_nrvb,'RAMCI',self.customer_id.id,
                                                self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                                'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
            if self.acct_type == 'Cash':
                query = "update vb_customer_acct set ramci_contra1='t', ramci_contra2='t' where id=%s"%self.id
                self._cr.execute(query)
            else:
                query = "update vb_customer_acct set ramci_contra1='t' where id=%s"%self.id
                self._cr.execute(query)
            del doc_list[:]
        
    @api.multi
    def ramci_contra_iriss(self):
        stage1 = False
        stage2 = False
        url_rec = self.env['vb.config'].search([('code_type','=','WebSvc'),('code','=','RamciInfo')])
        url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/iriss'
        data = dumps({"loginName" : str(self.customer_id.login_name),
                      "acctId" : str(self.id),
                      "idNo": str(self.national_id_no),
                      "idType" : str(self.national_id_type),
                      "country": str(self.customer_id.nationality_id.code),
                      "dob": str(self.customer_id.date_of_birth),
                      "mobileNo":str(self.mobile_phone1),
                      "emailAddress": str(self.email1),
                      "lastKnownAddress": ""})
        headers={'Content-type': 'application/json'}
        content = requests.post(url,data=data,headers=headers)  
        if int(content.json().get('code'))!=200 or content ==False:
            raise Warning(content.json().get('message'))
        risk_list = []
        if not self.acct_class_id:
            raise Warning("Product has not been specified for this Account")
        else:
            acct_class_rec= self.acct_class_id
            if not acct_class_rec.riskprof_id:
                raise Warning("Risk profile for the Product cannot be located")
            else:
                riskprof_rec= acct_class_rec.riskprof_id
                if not riskprof_rec.checklist_ids:
                    raise Warning("There is no risk checklist defined for the risk profile associated to this Product")
                else:
                    checklist_state= riskprof_rec.checklist_ids
                    
        exist_list = ()
        for i in content.json().get('result'):
            if self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self.id)]):
                for n in self.env['vb.customer_acct_riskcheck'].search([('code','=',str(i)),('acct_id','=',self.id)]):
                    exist_list += (n.id,)
        
        if exist_list:
            for i in exist_list:
                if i!= False:
                    query ="Delete from vb_customer_acct_riskcheck Where id =%s"%i
                    self._cr.execute(query)
        if checklist_state:
            for rec in checklist_state:
                    if content.json().get('result'):
                        for i in content.json().get('result'):
                            if rec.result_min >= 0:
                                if str(rec.code) == str(i):
                                    if bool(int(content.json().get('result').get(str(rec.code))) >= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                        risk_dict = (0,0,{'name' : rec.name,'state':'OK','riskcheck_id':rec.id,'result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                        risk_list.append(risk_dict)
                                         
                                    elif bool(int(content.json().get('result').get(str(rec.code)))>= rec.result_min and int(content.json().get('result').get(str(rec.code))) <= rec.result_max):
                                        risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,
                                                          'state':'OK','result_min':rec.result_min,
                                                          'result_max':rec.result_max,
                                                          'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                        risk_list.append(risk_dict)
                                    else:
                                        risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,'state':'Failed','result_min':rec.result_min,'result_max':rec.result_max,'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                        risk_list.append(risk_dict)
                            else:
                                if str(rec.code) == str(i):
                                    risk_dict = (0,0,{'name' : rec.name,'riskcheck_id':rec.id,
                                                      'state':'OK','result_min':rec.result_min,'result_max':rec.result_max,
                                                      'result_obtained':content.json().get('result').get(str(rec.code)),'list_seq':rec.list_seq})
                                    risk_list.append(risk_dict)
            self.write({'riskcheck_ids':risk_list})
        query = "update vb_customer_acct set ramci_contra2='t' where id=%s"%self.id
        self._cr.execute(query)
        if int(content.json().get('code'))==200:
            stage1 = True
            query_cust = "Update vb_customer set ramci_iriss_date=%s where id=%s"
            self._cr.execute(query_cust,(datetime.now(),self.customer_id.id))
            self.customer_id.update({'ramci_iriss_date' : datetime.now()})
        url = str(url_rec.parm1)+':'+str(url_rec.parm2)+'/'+str(url_rec.parm5)+'/'+str(url_rec.parm6)
        data = dumps({ "loginName" : self.customer_id.login_name,
                      "acctId" : str(self.id),
                      "productType":"CCRIS_SEARCH",})
        headers={'Content-type': 'application/json'}
        content = requests.post(url,data=data,headers=headers)
        doc_rec = self.env['vb.document']
        x =content.text.split(',')
        stage2 = True
        self.update({'iriss_clicked_flag':True})
        list = []
        doc_list =[]
        doc_directory = self.env['vb.config'].search([('code','=','DocDir'),('code_type','=','Sys')])[0].parm2
        file_loc = doc_directory +'RAMCI/'
        if os.path.exists(file_loc)== False:
            os.makedirs(file_loc)
        for rec in self.env['vb.document'].search([('acct_id','=',self.id),('customer_id','=',self.customer_id.id)]):
            doc_list.append(rec.file_name)
        
        for i in x:
            index = x.index(i)
            if 'IRISSPDF' in i:
                file_name_iriss = str('IRISSPDF'+str(self.id)+'_'+str(datetime.now())+'.pdf')
                file_path_irisspdf = str(file_loc+file_name_iriss)
                check_doc2 = self.env['vb.document'].search([('acct_id','=',self.id),('name','=','IRISS report')])
                if check_doc2:
                    for i in check_doc2:
                        query = 'DELETE FROM vb_document WHERE id=%s'% i.id
                        self._cr.execute(query)
                 
                if file_name_iriss not in doc_list or bool(doc_list) == False:
                    irisspdf = base64.b64decode(x[index].split('IRISSPDF=')[1])
                    y = open(file_path_irisspdf,'wb')
                    y.write(irisspdf)
                    y.close()
                    query='''INSERT INTO vb_document (file_loc,name, doc_version,file_name,is_open_exist,
                            customer_id,acct_id,doc_class_id,permission,state,create_date,write_date,create_uid,write_uid,active)
                            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
                    self._cr.execute(query,(file_path_irisspdf,'IRISS report',1.0,file_name_iriss,'RAMCI',self.customer_id.id,
                                    self.id,self.env['vb.common_code'].search([('code_type','=','DocClass'),('code','=','RAMCI')]).id,
                                    'Internal','Approved',datetime.now(),datetime.now(),self._uid,self._uid,True))
        del doc_list[:]
        
        
class customer_acct_cedd(models.Model):
    _inherit = 'vb.customer_acct_cedd'
    
    @api.multi
    def get_customers_address(self,auto_commit=False):
        recs = self.env['vb.customer_acct_cedd'].search([('acct_id','=',self.acct_id.id)])
        recs_lst = []
        for i in recs:
            recs_lst.append(i.id)
        sorted_recs = sorted(recs_lst)
        del sorted_recs[-1]
        delete_recs = self.env['vb.customer_acct_cedd'].search([('id','in',sorted_recs)])
        for i in delete_recs:
            self._cr.execute("Delete from vb_customer_acct_cedd where id=%s"%i.id)
        rec = self.env['vb.customer_address'].search([('customer_id','=',self.acct_id.customer_id.id),('address_type','=','Permanent')])
        current_customer_address = ''
        for i in rec:
            current_customer_address = i.display_address
            break
        if self.env['vb.customer_address'].search([('display_address','=',current_customer_address),('customer_id','!=',self.acct_id.customer_id.id),('address_type','=','Permanent')]):
            list = []
            recs = self.env['vb.customer_address'].search([('display_address','=',current_customer_address),('customer_id','!=',self.acct_id.customer_id.id),('address_type','=','Permanent')])
            for i in recs:
                list.append(i.customer_id.id)
            if self.state== False:
                self.state = 'New'
            mod_obj = self.env['ir.model.data']
            try:
                tree_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_tree')[1]
                form_res = mod_obj.get_object_reference('vb_acctmgmt', 'view_customer_form')[1]
            except ValueError:
                tree_res =  False
            return{
                   'name': "Customers address",
                   'view_type': 'form',
                   'view_mode':"[tree,form]",
                   'view_id': False,
                   'res_model': 'vb.customer',
                   'type': 'ir.actions.act_window',
                   'views': [(tree_res, 'tree'),(form_res,'form')],
                   'target':'new',
                   'domain':[('id','in',list)]
                   }
        else:
            raise Warning("There is no other customer with the same corresponding address")