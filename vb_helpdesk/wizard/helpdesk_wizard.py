from openerp import models, fields, api, _
from datetime import datetime
from openerp.exceptions import Warning


class help_desk_case(models.Model):
    _inherit = "vb.helpdesk_case"
    @api.multi
    def get_close_case(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_helpdesk', 'close_case_form_wizard_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Close Case",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }