from openerp import models, fields, api
from datetime import datetime, time
from openerp.exceptions import ValidationError
from json import dumps
import json
# from werkzeug.routing import ValidationError

# This model will store any quick information (such as help info line, emergency contacts, etc)
# that the service desk person needs (besides assessing the Knowledge base)

class helpdesk_qkinfo(models.Model):
    _name = 'vb.helpdesk_qkinfo'
    _description = 'Helpdesk quick info'
    
    # fields
    name = fields.Char(size=100,string="Information title")
    general_desc = fields.Text(string='Content')
    
# This model stores the details of the cases in this helpdesk module.  This is the primary model for this module.

class helpdesk_case(models.Model):
    _name = 'vb.helpdesk_case'
    _description = 'Helpdesk case'
    
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    @api.onchange('customer_id')
    def get_customer_info(self):
        if self.customer_id:
            rec = self.env['vb.customer'].search([('id','=',self.customer_id.id)])
            if rec:
                self.contact_person = rec.name
                self.contact_email = rec.email1
                self.contact_phone = rec.mobile_phone1
                self.contact_title = rec.title_id.name
        else:
            self.contact_person =  self.contact_phone = self.contact_email = False or ''
    # Compute age of the case
    @api.depends('case_date') 
    def _compute_day_diff(self):
        for rec in self:
            d1 = datetime.strptime(rec.case_date,'%Y-%m-%d')
            d2 = datetime.today()
            d3 = lambda d2: fields.datetime.now()
            rec.age=abs((d3(d2) - d1).days + 1)

    # fields
    name = fields.Char(size=100,string='Case title',index=True)
    case_ref = fields.Char(size=20,string='Case reference')
    case_date = fields.Date(string='Case date',default=lambda self:fields.date.today())
    category_id = fields.Many2one(comodel_name='vb.common_code', string='Case category',ondelete='restrict',index=True, domain="[('code_type','=','CaseCat')]")
    state = fields.Selection([
        ('New', 'New'),
        ('Open', 'Open'),
        ('Closed', 'Closed'),
        ('Cancel', 'Cancelled'),
        ], string='Status', default='New')    
    acct_id = fields.Many2one(comodel_name='vb.customer_acct',string='Customer account',ondelete='set null',index=True,domain="[('customer_id','=',customer_id)]")
    acct_no = fields.Char(string='Account number',related='acct_id.acct_no',readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer',string='Customer',ondelete='cascade',store=True)
    general_desc = fields.Char(size=100,string='General description')
    detail_desc = fields.Text(string='Detail description')
    when_requested = fields.Datetime(string='Date/Time requested',default=lambda self: fields.datetime.now())
    who_requested = fields.Char(string='Requested by',)
    how_requested = fields.Selection([
        ('WebSite','Web site'),
        ('Email', 'Email'),
        ('Phone', 'Phone'),
        ('Message', 'Messaging'),
        ], string='How requested',default='Email')
    case_severity = fields.Selection([
        ('High', 'High'),
        ('Medium', 'Medium'),
        ('Low', 'Low'),
        ('None', 'None'),
        ], string='Severity',default='Medium')
    due_date = fields.Datetime(string='Date/Time to resolved')
    contact_person = fields.Char(size=100,string='Contact name', index=True)
    contact_email = fields.Char(size=100,string='Contact email')
    contact_phone = fields.Char(size=20,string='Contact phone')
    contact_title = fields.Char(size=20,string='Contact title')
    customer_ref = fields.Char(size=20,string='Customer reference')
    when_resolved = fields.Datetime(string='Date/Time resolved')
    resolve_note = fields.Text('Resolution notes')
    age = fields.Integer(string='Age', compute='_compute_day_diff', readonly=True, store=False)
    activity_ids = fields.One2many(comodel_name='vb.case_activity',inverse_name='case_id', string='Activities')
    escalation_ids = fields.One2many(comodel_name='vb.case_escalate',inverse_name='case_id', string='Escalations')
    document_ids = fields.One2many(comodel_name='vb.document',inverse_name='case_id', string='Related documents')
    active = fields.Boolean(string='Active record',default=True)
    assigned_to_id = fields.Many2one(comodel_name='res.users',string='User assigned to',index=True)
    # 02-Dec-2016 DT
    notified_on_new = fields.Boolean(string='Notified on new')
    notified_date = fields.Datetime(string='Date/Time notified')
    # 23-Jan-2017
    case_type = fields.Char(string='Case type')
    # 07-JanMar-2017
    escalated_to_id = fields.Many2one(comodel_name='res.users',string='Internal party')
    escalated_to = fields.Char(string='External party')
    escalated_email = fields.Char(string='Email of external party')
    when_escalated = fields.Datetime(string='Date/Time escalated')

class customer(models.Model):
    _inherit = 'vb.customer'

    acct_case_cash_ids=fields.One2many(comodel_name='vb.helpdesk_case', inverse_name='customer_id', string="Helpdesk Cases Cash",domain=[('acct_id.acct_type', '=', 'Cash')])
    acct_case_margin_ids=fields.One2many(comodel_name='vb.helpdesk_case', inverse_name='customer_id', string="Helpdesk Cases Margin",domain=[('acct_id.acct_type', '=', 'Margin')])
    acct_case_contra_ids=fields.One2many(comodel_name='vb.helpdesk_case', inverse_name='customer_id', string="Helpdesk Cases Contra",domain=[('acct_id.acct_type', '=', 'Contra')])

# This model stores the activities that is associated with the case.  Activities can be Pending,

class case_activity(models.Model):
    _name = 'vb.case_activity'
    _description = 'Case activity'
    
    @api.multi
    def write(self,vals):
        print self._uid, self.create_uid
        if self._uid != self.create_uid.id:
            raise ValidationError("You can't update Activity of other user.")
        else:
            super(case_activity,self).write(vals)

    @api.onchange('activity_date','activity_type_id','activity_note','activity_ref','ext_party','int_party_id','state')
    def checking_user(self):
        if self._uid != self.create_uid.id:
            raise ValidationError("You can't update Activity of other user.")
    
    # fields
    case_id = fields.Many2one(comodel_name='vb.helpdesk_case',string='Helpdesk case', ondelete='cascade')
    activity_date = fields.Datetime(string='Activity date',default=lambda self: fields.datetime.now())
    activity_type_id = fields.Many2one(comodel_name='vb.common_code',string='Activity type',ondelete='restrict',index=True)
    name = fields.Char(string='Activity type',related="activity_type_id.name",readonly=True)
    activity_note = fields.Text(string='Note',help="Describe the activity and result")
    activity_ref = fields.Char(size=20,string='Reference',help='This may be document reference involved in this activity.')
    ext_party = fields.Char(size=100,string='External party',help='External party assigned to this activity.')
    int_party_id = fields.Many2one(comodel_name='res.users',string='Internal party',help='Internal user assigned to this activity.')
    when_assigned = fields.Datetime(string='Date/Time assigned',default=lambda self: fields.datetime.now())
    when_due = fields.Datetime(string='Date/Time due')
    state = fields.Selection([
        ('Pending', 'Pending'),
        ('Completed', 'Completed'),
        ('Cancel', 'Cancelled'),
        ], string='Status', default='Pending')

# This model stores the escalation that has been done.

class case_escalate(models.Model):
    _name = 'vb.case_escalate'
    _description = 'Case escalation'
    _rec_name = 'case_id'
    
#     @api.multi
#     def create(self, vals):
#         print 'aaaaaaaaaaaaa'
#         resuluper((case_escalate,self).create(vals)
# #         if vals.get('escalated_to_id'):
# #             print 'SSSSSSSSSSS',self.escalated_to_id
#         
#         return result
    # fields
    case_id = fields.Many2one(comodel_name='vb.helpdesk_case',string='Helpdesk case', ondelete='cascade',index=True)
    escalated_to_id = fields.Many2one(comodel_name='res.users',string='Internal party')
    escalated_to = fields.Char(size=100,string='External party')
    escalated_email = fields.Char(size=40,string='Email of external party')
    when_escalated = fields.Datetime(string='Date/Time escalated',default=lambda self: fields.datetime.now())
    escalation_note = fields.Text('Escalation notes')

#   Method to allow only future or today date time at when escalated
    @api.constrains('when_escalated')
    def check_esc_date(self):
        date1 = datetime.now()
        date2 = lambda date1: datetime.now()
        #convert the datetime.now without the seconds
        d3 = date2(date1).strftime('%Y-%m-%d %H:%M')
        if self.when_escalated < d3 :
            raise ValidationError("Date/Time escalated should be future date")

# This model replaces the case_doc and provides the required M2O relationship required

class document(models.Model):
    _inherit = 'vb.document'
    
    # 12-Dec-2016
    # Add relationship to vb_document
    case_id = fields.Many2one(comodel_name='vb.helpdesk_case', string='Helpdesk case',ondelete='cascade',index=True)

# ====================================================================================================================
# 12-Dec-2016 DEPRECATED MODELS

# This model has been deprecated and replaced by vb.document
    
class case_doc(models.Model):
    _name = 'vb.case_doc'
    _description = 'Case document store'
    
    # field
    case_id = fields.Many2one(comodel_name='vb.helpdesk_case',string='Helpdesk case',ondelete='cascade',index=True)
    name = fields.Char(size=100,string='Document title')
    doc_desc = fields.Text(string="General description")
    doc_ref = fields.Char(size=20,string='Document reference')
    doc_date = fields.Char(string='Document date')
    doc_author = fields.Text(string="Author")
    doc_version = fields.Text(string="Version")
    doc_content_bin = fields.Binary(string='File attachment',help='This is for document files such as PDF and DOC')
    file_name_bin = fields.Char(string='File name',help='This is the name of the file attachment')
    notes = fields.Text(string='Notes and comments')

# This model will the various categories of cases that can occur.
# e.g. Inquiry, Issues, Assistant, etc
# 09-Dec-2016
# This table has been deprecated - using vb.common_store to store

class case_cat(models.Model):
    _name = 'vb.case_category'
    _description = 'Case category'
    
    @api.multi
    def copy(self):
        raise ValidationError("Sorry you are unable to duplicate records")
    
    # fields
    name = fields.Char(size=100,string="Category name")
    total_cases = fields.Integer(string="Total cases")
    open_cases = fields.Integer(string="Open cases")
    overdue_cases = fields.Integer(string="Overdue cases")
    mtd_new_cases = fields.Integer(string="MTD new cases")
    mtd_closed_cases = fields.Integer(string="MTD Closed cases")
    
