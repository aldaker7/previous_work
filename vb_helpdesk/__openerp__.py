# -*- coding: utf-8 -*-

{
    'name': 'VB Helpdesk',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Helpdesk',
    'depends': ['web','vb_base','vb_acctmgmt','vb_cashmgmt','vb_trade','vb_portfolio'],
    'description': """

This is the helpdesk module for Virtual Broker Application.
==========================================================

The functions in the Helpdesk module are used primarily by the Customer Service Representative
or agent assist the customer with whatever issues or inquiries the custmer may want assistance on.
""",
    'data': [
        'views/views_helpdesk.xml',
        'views/actions_helpdesk.xml',
        'views/menus_helpdesk.xml',
        'wizard/case_wizard.xml',
        'security/ir.model.access.csv',
        'data/setup_data.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
