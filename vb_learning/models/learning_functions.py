from openerp import models, api
from datetime import datetime, timedelta
from openerp.exceptions import Warning

class faq(models.Model):
    _inherit = 'vb.faq'
    
    @api.multi
    def copy(self):
        raise Warning("Sorry you are unable to duplicate records")

	# Override the write method to store each of modify_by and modify_date    
    # FAQ write method
    @api.multi
    def write(self, vals):
        recs = self.env['res.users'].search([('id', '=', self._uid)])
        vals['modify_by']=recs.name
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        vals['modify_date']=d2(d1)
        return super(faq, self).write(vals)
    
    # Override the write method to store each of modify_by and modify_date
    @api.model
    def create(self, vals):
        recs = self.env['res.users'].search([('id','=',self._uid)])
        vals['modify_by']=recs.name
        d1 = datetime.now()
        d2 = lambda d1: datetime.now()
        vals['modify_date']=d2(d1)
        return super(faq, self).create(vals)
    
#     Method to get method of approve FAQ 
    @api.multi
    def get_approve_faq(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_learning', 'approve_faq_wiz_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Approve FAQ",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to get method of reject FAQ 
    @api.multi
    def get_reject_faq(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_learning', 'reject_faq_wiz_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Reject FAQ",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }

#     Method to get method of reject FAQ 
    @api.multi
    def get_edit_faq(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_learning', 'edit_faq_wiz_form_view')[1]
        except ValueError:
            form_id = False
        return{
                    'name': "Allow Edit FAQ",
                    'view_type': 'form',
                    'view_mode':"[form]",
                    'view_id': False,
                    'res_model': 'vb.common_wizard',
                    'type': 'ir.actions.act_window',
                    'target':'new',
                    'views': [(form_id, 'form')],
                    }