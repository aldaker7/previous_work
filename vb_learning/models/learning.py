# MODEL DEFINITION FOR VB-BROKER APPLICATION
# ==========================================
#
# Models for knowledgebase module
#
# Author: Daniel Tan
# Last updated: 15-August-2016
#
# ==========================================

from openerp import models, fields, api
from openerp.exceptions import ValidationError


class knowledge(models.Model):
    _name = 'vb.knowledge'
    _description = 'Knowledgebase'
    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False
    
    # fields
    name = fields.Char(string="Topic title",index=True)
    kb_class_id = fields.Many2one(comodel_name='vb.common_code',string='Knowledge class',ondelete='restrict',domain="[('code_type','=','KbClass')]")
    topic_desc = fields.Text(string="Topic description")
    topic_id = fields.Many2one(comodel_name='vb.knowledge',string='Parent topic',ondelete='cascade',index=True)
    subtopic_ids = fields.One2many(comodel_name='vb.knowledge', inverse_name='topic_id', string="Sub-topics")
    no_subtopics = fields.Integer(string='Number of subtopic', compute='_countsubtopic')
    #document_ids = fields.One2many(comodel_name='vb.document', inverse_name='subtopic_id', string="Related documentation")
    no_documents = fields.Integer(string='Number of documentations', compute='_countdoc')    
    kb_content = fields.Html(string='Knowledge content')
    submit_date = fields.Datetime(string='Date/time submitted')
    submit_by = fields.Many2one(comodel_name='res.users',string='Submitted by',ondelete='restrict')
    apprej_date = fields.Datetime(string='Date/time approved/rejected')
    apprej_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected by',ondelete='restrict')
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('Archived', 'Archived'),
        ], string='Status', default='New')

    # Count number of sub-topics
    @api.depends('no_subtopics')
    def _countsubtopic(self):
        for rec in self:
            rec.no_subtopics = len(rec.subtopic_ids)

    # Count number of documents
    @api.depends('no_documents')
    def _countdoc(self):
        for rec in self:
            rec.no_documents = len(rec.document_ids)
            
class faq(models.Model):
    _name = 'vb.faq'
    _description = 'Frequency asked questions'
    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.active = False

    # fields
    name = fields.Char(string="FAQ description",index=True)
    faq_class_id = fields.Many2one(comodel_name='vb.common_code',string='FAQ class ID',ondelete='restrict',domain="[('code_type','=','FaqClass')]")
    faq_class = fields.Char(string='FAQ class',related='faq_class_id.code',store=True)
    faq_section_id = fields.Many2one(comodel_name='vb.common_code',string='FAQ Section ID',ondelete='restrict',domain="[('code_type','=','FaqSection')]")
    faq_section = fields.Char(string='FAQ Section',related='faq_section_id.code',store=True)
    faq_no = fields.Integer(string="FAQ number", default=1)
    question = fields.Html(string="Question")
    answer = fields.Html(string="Answer")
    submit_date = fields.Datetime(string='Date/time submitted')
    submit_by = fields.Many2one(comodel_name='res.users',string='Submitted by',ondelete='restrict')
    apprej_date = fields.Datetime(string='Date/time approved/rejected')
    apprej_by = fields.Many2one(comodel_name='res.users',string='Approved/rejected by',ondelete='restrict')
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('Archived', 'Archived'),
        ], string='Status', default='New')
    #Addded to get the self._uid of user modifying the record and datetime of modifying.
    modify_by = fields.Char(string='Modified by')
    modify_date = fields.Datetime(string="Date/time modified")
#   Method to check faq_no length [D 23-12-016-Badi]
    @api.multi
    @api.onchange('faq_no')
    def faq_lenght(self):
        len_faq= len(str(self.faq_no))
        if len_faq > 8 and len_faq < 11:
            raise ValidationError('Faq number cannot exceed 8 digits!')
        else:
            if len_faq > 10 :
                self.faq_no= u''

    # SQL constraint
    _sql_constraints = [
        ('faq_no_uniq', 'unique(faq_class_id,faq_section_id,faq_no,active)', 'Another FAQ with the same FAQ No./name already exist !'),
    ]

class document(models.Model):
    _inherit = 'vb.document'

    topic_id = fields.Many2one(comodel_name='vb.knowledge',string='Knowledgebase',ondelete='cascade',index=True)