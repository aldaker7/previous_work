from openerp import models, fields, api, _
from datetime import datetime, timedelta
from openerp.exceptions import Warning

class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'

#     method to approve faq when status equal to new, set approved by to 
#     user id and approved datetime to today datetime
    @api.multi
    def approve_faq(self):
        #making sure the state is New and make sure it is not already ACTIVE
#         recs = self.env['res.users'].search([('id','=',self._uid)])
        faq_rec_app = self.env['vb.faq'].search([('id', 'in',self._context.get('active_ids'))])
        if faq_rec_app:
            faq_rec_app.write({'state': 'Approved','apprej_by': self._uid, 'apprej_date': datetime.now(), 'modify_by': self._uid, 'modify_date':datetime.now()})

#     method to reject faq when status equal to new, set rejected by to 
#     user id and rejected datetime to today datetime
    @api.multi
    def reject_faq(self):
        #making sure the state is New and make sure it is not already Active
#         recs = self.env['res.users'].search([('id','=',self._uid)])
        faq_rec_reje = self.env['vb.faq'].search([('id', 'in',self._context.get('active_ids'))])
        if faq_rec_reje:
            faq_rec_reje.write({'state': 'Rejected','apprej_by': self._uid, 'apprej_date': datetime.now(), 'modify_by': self._uid, 'modify_date':datetime.now()})

#     method to allow edit FAQ when status equal to approved, set rejected/approved by to 
#     user id and rejected/approved datetime to today/now datetime
    @api.multi
    def edit_faq(self):
        #making sure the state is New and make sure it is not already Active
        faq_rec_edit = self.env['vb.faq'].search([('id', 'in',self._context.get('active_ids'))])
        if faq_rec_edit:
            faq_rec_edit.write({'state': 'New', 'apprej_by':self._uid, 'apprej_date':datetime.now(), 'modify_by': self._uid, 'modify_date':datetime.now()})