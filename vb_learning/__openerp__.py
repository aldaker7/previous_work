# -*- coding: utf-8 -*-

{
    'name': 'VB learning',
    'version': '1.1',
    'author': 'VBroker Dev Team',
    'category': 'Learning',
    'depends': ['vb_base'],
    'description': """

This is the learning module for Virtual Broker Application.
====================================================

Detail write-up coming soon

    """,
    'data': [
        'security/ir.model.access.csv',
        'wizard/wizards_learning.xml',
        'views/view_learning.xml',
        'views/action_learning.xml',
        'views/menu_learning.xml',
        'security/ir.model.access.csv',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
