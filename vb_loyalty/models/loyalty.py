from openerp import models, fields,api
from datetime import datetime, timedelta

# Model for CUSTOMER LOYAL table
# This model is to store information about the customer loyalty programme and points earned / redeemed
# Targeted number of records: Large - average 1 records per customer

class customer_loyalty(models.Model):
    _name = 'vb.customer_loyalty'
    _description = 'Customer loyalty account'
    _order = 'loyalty_pgm_id,customer_id'
    _rec_name = 'customer_id'
    
    # field 
    loyalty_pgm_id = fields.Many2one(comodel_name='vb.common_code',string='Loyalty programme',ondelete='restrict',domain="[('code_type','=','LoyaltyPgm')]")
    loyalty_acct_name = fields.Char(string='Loyalty account name')
    loyalty_acct_no = fields.Char(string='Loyalty account number')
    date_joined = fields.Date(string='Date joined')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    referrer_id = fields.Many2one(comodel_name='vb.customer', string='Referrer',ondelete='restrict',index=True)
    date_suspended = fields.Datetime(string='Date/time suspended')
    reason_suspended_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for suspension',ondelete='restrict',copy=False,domain="[('code_type','=','LtReasonSus')]")
    suspended_by = fields.Many2one(comodel_name='res.users',string='Suspended by',ondelete='restrict')
    date_uplifted = fields.Datetime(string='Date/time uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by',ondelete='restrict')
    reason_uplifted_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for uplifting',ondelete='restrict',copy=False,domain="[('code_type','=','LtReasonUplift')]")
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Suspended', 'Suspended'),        
        ('Upgraded', 'Upgraded'),
        ], string='Status', default='New')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)
    loyal_pts_balance = fields.Float(string='Loyal points balance',digits=(11,2))
    pts_pending = fields.Float(string='Loyal points pending',digits=(11,2))
    ytd_pts_begin = fields.Float(string='Loyal points at beginning of year',digits=(11,2))
    ytd_pts_earned = fields.Float(string='Loyal points YTD earned',digits=(11,2))
    ytd_pts_redeemed = fields.Float(string='Loyal points YTD redeemed',digits=(11,2))
    ytd_pts_expired = fields.Float(string='Loyal points YTD expired',digits=(11,2))
    ytd_pts_expiring = fields.Float(string='Loyal points YTD expiring',digits=(11,2))
    updated_asat = fields.Datetime(string='When last updated')
    mtd_pts_expirying = fields.Float(string='Loyal points current month expiring',digits=(11,2))
    mtd1_pts_expirying = fields.Float(string='Loyal points next month expiring',digits=(11,2))
    mtd2_pts_expirying = fields.Float(string='Loyal points 3rd expiry month',digits=(11,2))
    mtd_pts_expirying_date = fields.Date('Loyal points next expiry month date')
    mtd1_pts_expirying_date = fields.Date('Loyal points 2nd expiry month date')
    mtd2_pts_expirying_date = fields.Date('Loyal points 3rd expiry month date')
    # For future - upgrading from one loyalty programe to another
    date_upgraded = fields.Datetime(string='Date/time upgraded')
    upgraded_by = fields.Many2one(comodel_name='res.users',string='Upgraded by',ondelete='restrict')
    upgraded_pgm_id = fields.Many2one(comodel_name='vb.common_code',string='Upgraded loyalty programme',ondelete='restrict',domain="[('code_type','=','LoyaltyPgm')]")
    pts_transferred = fields.Float(string='Loyal points transferred',digits=(11,2))
    # added by Nandha 25-01-17
    referral_counter = fields.Integer(string='Referral Counter',digits=(3,0))
    friends_referred = fields.Integer(string='Friends Referred',digits=(3,0))

# Model for LOYAL CARD table
# This model is to store the information about loyal card issued to customer
# Used only when physical loyalty card is issued

class loyalty_card(models.Model):
    _name = 'vb.loyalty_card'
    _description = 'Loyalty card'
    
    # field
    loyalty_acct_id = fields.Many2one(comodel_name='vb.customer_loyalty', string='Customer loyalty account',ondelete='cascade',index=True)
    card_no = fields.Char(size=30,string='Card No.')
    issue_date = fields.Date(string='Issue Date')
    expiry_date = fields.Date(string='Expiry Date')
    name_on_card = fields.Char(size=100,string='Name on card')
    despatch_date = fields.Date(string='Despatched Date')
    card_state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Expired', 'Expired'),        
        ('Lost', 'Lost'),
        ('Stolen', 'Stolen'),
        ], string='Status', default='New')
    date_reported = fields.Datetime(string='Date/time reported lost/stolen')
    reported_by = fields.Char(size=100,string='Reported lost/stolen by')
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)        
    
# Model for CUSTOMER LOYAL TRAN table
# This model is to store the additional information about the customer bank accounts
# Targeted number of records: Large - average 100-200 records per customer

class customer_loyalty_tran(models.Model):
    _name = 'vb.customer_loyalty_tran'
    _description = 'Customer loyalty transaction'
    _order = 'loyalty_acct_id,tran_date'
    _rec_name = 'customer_id'
    
    @api.multi
    def get_expiry_date(self):
        for i in self:
            if i.tran_type=='Earned':
                if i.tran_date:
                    i.expiry_date = str(datetime.strptime(i.tran_date, "%Y-%m-%d")+ timedelta(days=1095)).split(' ')[0][:-3]
    
    # fields
    loyalty_acct_id = fields.Many2one(comodel_name='vb.customer_loyalty', string='Customer loyalty account',ondelete='cascade',index=True)
    loyalty_acct_no = fields.Char(string='Loyalty account number',related='loyalty_acct_id.loyalty_acct_no',readonly=True)
    loyalty_pgm_name = fields.Char(string='Loyalty programme name',related='loyalty_acct_id.loyalty_pgm_id.name',readonly=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    tran_refno = fields.Char(string='Transaction reference')
    tran_date = fields.Date(string='Transaction date',)
    tran_type_id = fields.Many2one(comodel_name='vb.tran_type', string='Transaction type')
    tran_type = fields.Char(string='Transaction type',related='tran_type_id.code',store=True,readonly=True)
    #    ('Earned', 'Earned'),
    #    ('Redeemed', 'Redeemer'),
    #    ('Adjust', 'Adjustment'),
    tran_desc = fields.Text(string='Description')
    tran_qty = fields.Float(string='Transaction pts',digits=(11,2),default=0)
    tran_amt = fields.Float(string='Transaction amt',digits=(15,2),default=0)
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Pending', 'Pending'),
        ('Posted', 'Posted'),
        ('Void', 'Void'),
        ('Rejected', 'Rejected'),
        ], string='Status', default='New')
    stage = fields.Selection([
        ('New', 'New'),
        ('Open', 'Open'),
        ('Posted', 'Posted'),
        ('Closed', 'Closed'),
        ('Failed','Failed')
        ], string='Stage', default='New')
    date_sent = fields.Datetime(string='Date/time sent')
    date_rejected = fields.Datetime(string='Date/time rejected')
    date_posted = fields.Datetime(string='Date/time posted')
    posted_by = fields.Many2one(comodel_name='res.users',string='Posted by',ondelete='restrict')
    date_void = fields.Datetime(string='Date/time  void')
    void_by = fields.Many2one(comodel_name='res.users',string='Void by',ondelete='restrict')
    date_approved = fields.Datetime(string='Date/time approved')
    approved_by = fields.Many2one(comodel_name='res.users',string='Approved by',ondelete='restrict')
    rule_id = fields.Many2one(comodel_name='vb.loyalty_reward_rule', string='Rule Type',ondelete='cascade',index=True)
    loyalty_partner_id = fields.Many2one(comodel_name='vb.loyalty_partner', string='Partner name',ondelete='cascade',index=True)
    partner_refno = fields.Char(string='Parter ref. no.')
    reject_comment = fields.Char(string='Reason for rejection')
    expiry_date = fields.Char('Expiry Date',compute=get_expiry_date)
    #added by nandha
    customer_loyalty_partner_id = fields.Many2one(comodel_name='vb.customer_loyalty_partner', string='Partner name',ondelete='cascade',index=True)
    loyalty_pts_bal = fields.Float(related='loyalty_acct_id.loyal_pts_balance',string='Loyalty pts balance',readonly=True)
    #added by nandha (17-11-2017)
    acct_id = fields.Many2one(comodel_name='vb.customer_acct', string='Customer account',ondelete='cascade',index=True)
    expiration_date = fields.Char(related='expiry_date',store=True)
    
# Model for Partner table
# This model is to store information about the RT Point Partner Details
# Targeted number of records: Large - average 1 records per Partner

class loyalty_partner(models.Model):
    _name = 'vb.loyalty_partner'
    _description = 'Loyal partners'
    
    # field
    loyalty_pgm_id = fields.Many2one(comodel_name='vb.common_code',string='Loyalty programme',ondelete='restrict',domain="[('code_type','=','LoyaltyPgm')]")
    name = fields.Char(size=100,string='Partner name',index=True,copy=False)
    short_name = fields.Char(size=30,string='Short name')
    bizregno = fields.Char(size=20,string='Business Reg. No.')
    contact_name = fields.Char(size=60,string='Contact person')
    contact_email = fields.Char(size=40,string='Contact email')
    contact_mobile = fields.Char(size=20,string='Contact mobile')
    address1 = fields.Char(size=100,string='Address 1',)
    address2 = fields.Char(size=100,string='Address 2')
    address3 = fields.Char(size=100,string='Address 3')
    city = fields.Char(size=40,string='City',)
    postcode = fields.Char(size=20,string='Postal code',)
    state_id = fields.Many2one(comodel_name='res.country.state', string='State',ondelete='restrict')
    country_id = fields.Many2one(comodel_name='res.country', string='Country',ondelete='restrict')
    country_code = fields.Char(string='Country code', related='country_id.code')
    email1 = fields.Char(size=40,string='Primary email')
    email2 = fields.Char(size=40,string='Alternate email')
    work_phone1 = fields.Char(size=20,string='Work phone1')
    work_phone2 = fields.Char(size=20,string='Work phone2')
    website = fields.Char(size=100,string='Website address')
    start_date = fields.Date(string='Start date')
    end_date = fields.Date(string='End date')
    date_suspended = fields.Datetime(string='Date/time suspended')
    reason_suspended_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for suspension',ondelete='restrict',copy=False,domain="[('code_type','=','LtReasonSus')]")
    suspended_by = fields.Many2one(comodel_name='res.users',string='Suspended by',ondelete='restrict')
    date_uplifted = fields.Datetime(string='Date/time uplifted')
    uplifted_by = fields.Many2one(comodel_name='res.users',string='Uplifted by',ondelete='restrict')
    reason_uplifted_id = fields.Many2one(comodel_name='vb.common_code',string='Reason for uplifting',ondelete='restrict',copy=False,domain="[('code_type','=','LtReasonUplift')]")
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Suspended', 'Suspended'),        
        ], string='Status', default='New')
    website = fields.Char(size=100,string='Website address')
    rtpoints_convert_factor = fields.Float(string='Convert Factor',digits=(11,2),default=0)
    comments = fields.Text(string='Comments')
    active = fields.Boolean(string='Active',default=True)
    #added by Nandha.
    partner_code = fields.Char(size=30,string='Partner code')
    
# Model for rule table
# This model is to store information about the RT Point Rules

class loyalty_reward_rule(models.Model):
    _name = 'vb.loyalty_reward_rule'
    _description = 'Loyalty Reward Rules'
    _rec_name = 'rule_type_id'
    
    # field
    loyalty_pgm_id = fields.Many2one(comodel_name='vb.common_code',string='Loyalty programme',ondelete='restrict',domain="[('code_type','=','LoyaltyPgm')]")
    rule_type = fields.Char(size=30,string='Rule Type Description')
    start_date = fields.Date(string='Start date')
    end_date = fields.Date(string='End date')
    points = fields.Float(string='Points',digits=(11,2),default=0)
    max_points = fields.Float(string='Max_points',digits=(11,2),default=0)
    per_value_of = fields.Float(string='Per value of',digits=(11,2),default=0)
    input_rnd_amt = fields.Float(string='Rounding amt for input',digits=(11,2),default=1)
    input_rnd_opt = fields.Selection([
        ('Up', 'Round up'),
        ('Down','Round down'),
        ('HalfAdj','Half-adjust'),
        ('None','None'),
      ],string='Rounding opt for input', default='None')
    result_rnd_amt = fields.Float(string='Rounding amt for result',digits=(11,2),default=1)
    result_rnd_opt = fields.Selection([
        ('Up', 'Round up'),
        ('Down','Round down'),
        ('HalfAdj','Half-adjust'),
        ('None','None'),
      ],string='Rounding opt for result', default='None')
    promo_start_date = fields.Date(string='Promo start date')
    promo_end_date = fields.Date(string='Promo end date')
    promo_points = fields.Float(string='Points',digits=(11,2),default=0)
    promo_per_value_of = fields.Float(string='Promo per value of',digits=(11,2),default=0)    
    active = fields.Boolean(string='Active',default=True)
    # added field referral count by Nandha 25-01-17
    referral_count = fields.Integer(string='Referral Count',digits=(4,0),default=0)
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('InActive', 'InActive'),        
        ], string='Status', default='New')
    range_ids = fields.One2many(comodel_name='vb.loyalty_reward_rule_range', inverse_name='rule_id', string='Range values')
    # 18 July 2017 
    rule_type_id = fields.Many2one(comodel_name='vb.common_code',string='Rule Type',ondelete='restrict',domain="[('code_type','=','RTRuleType')]")
    promo_multiply_points = fields.Float(string='Promo points multiply',digits=(11,2),default=0)
    # devtask 15380
    promo_code = fields.Char(string='Promotion code')

# Model for extended rule table
# This model is to store information about the Rules by range

class loyalty_reward_rule_range(models.Model):
    _name = 'vb.loyalty_reward_rule_range'
    _description = 'Loyalty Reward rule range'
    _order = 'rule_id,range_no'
    
    rule_id = fields.Many2one(comodel_name='vb.loyalty_reward_rule', string='Loyalty rule',ondelete='cascade',index=True)
    range_no = fields.Integer(string='Line no',digits=(3,0),default=1)
    range_start = fields.Float(string='Start of range',digits=(11,2))
    range_end = fields.Float(string='End of range',digits=(11,2))
    points = fields.Float(string='Points',digits=(11,2),default=0)
    per_value_of = fields.Float(string='Per value of',digits=(11,2),default=0)
    promo_points = fields.Float(string='Promo points',digits=(11,2),default=0)
    promo_per_value_of = fields.Float(string='Promo per value of',digits=(11,2),default=0)

# Model for vb_customer_loyalty_partner table
# This model is to store information about the Customer Loyalty Partner
# Targeted number of records: Large - average 1 records per Loyalty Partner

class customer_loyalty_partner(models.Model):
    _name = 'vb.customer_loyalty_partner'
    _description = 'Customer Loyalty Partner'
    _rec_name = 'customer_id'
    
    # field
    loyalty_pgm_id = fields.Many2one(comodel_name='vb.common_code',string='Loyalty programme',ondelete='restrict',domain="[('code_type','=','LoyaltyPgm')]")
    loyalty_partner_id = fields.Many2one(comodel_name='vb.loyalty_partner', string='Partner name',ondelete='cascade',index=True)
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    member_no = fields.Char(string='Partner loyalty member number')
    card_no = fields.Char(string='Card No.')
    card_expiry_date = fields.Date(string='Expiry Date')
    name_on_card = fields.Char(string='Name on card')
    card_type = fields.Char(string='Card type')
    email = fields.Char(string='Registered email')
    active = fields.Boolean(string='Active',default=True)    
    state = fields.Selection([
        ('New', 'New'),
        ('Active', 'Active'),
        ('Pending', 'Pending'),        
        ('Rejected', 'Rejected'),        
        ], string='Status', default='New')
    reject_comment = fields.Char(string='Reason for rejection')
    date_sent = fields.Datetime(string='Date/time sent to partner')
    date_rcvd = fields.Datetime(string='Date/time received from partner')
    
# Model for vb_customer_loyalty_bal table
# This model is to store information about the Customer Loyalty Balance Points
# Targeted number of records: Large - average 1 records per Customer Loyalty

class customer_loyalty_bal(models.Model):
    _name = 'vb.customer_loyalty_bal'
    _description = 'Customer Loyalty Balance'
    _rec_name = 'customer_id'
    
    # field    
    loyalty_acct_id = fields.Many2one(comodel_name='vb.customer_loyalty', string='Customer loyalty account',ondelete='cascade',index=True)
    month_year = fields.Char(size=10,string='Year-Month (YYYY-MM)')
    pts_earned = fields.Float(string='Loyal points earned',digits=(11,2))
    pts_redeemed = fields.Float(string='Loyal points redeemed',digits=(11,2))
    pts_expired = fields.Float(string='Loyal points expired',digits=(11,2))
    pts_expiring = fields.Float(string='Loyal points expiring',digits=(11,2))
    active = fields.Boolean(string='Active',default=True) 
    # Added 13 July 2017
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    
class customer_loyalty_partner_log(models.Model):
    _name = 'vb.customer_loyalty_partner_log'
    _description = 'Customer Loyalty Partner Log'
    
    # fields    
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',ondelete='cascade',index=True)
    date_time = fields.Date(string='Date time')
    input_log = fields.Char(string='Register/Redeem info to the Partner')
    output_log = fields.Char(string='Output result from Partner')
    status = fields.Char(string='Status like Success or Failure')
    log_type = fields.Char(string='Register/Redeem')
    
