# -*- coding: utf-8 -*-

{
    'name': 'VB Loyalty',
    'version': '0.1',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base'],
    'description': """

Virtual Broker Application - Loyalty Rewards Module
======================================================

This module contains the function for loyalty rewards module

    """,
    'data': [
        'security/ir.model.access.csv',
        'views/loyalty_views.xml',
        'views/actions_loyalty.xml',
        'views/menus_loyalty.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
