# -*- coding: utf-8 -*-

{
    'name': 'VB Sanction',
    'version': '1.0',
    'author': 'VBroker Dev Team',
    'category': 'Virtual Broker',
    'depends': ['vb_base','vb_helpdesk'],
    'description': """

Virtual Broker Application - Account Management Module
======================================================

This module contains the views and functions for UN Sanction list.

    """,
    'data': [
        'wizards/sanction_wizard_views.xml',
        'views/views_sanction.xml',
        'views/action_sanction.xml',
        'views/menu_sanction.xml',
            ],

    'installable': True,
    'auto_install': False,
    'application': True,
}