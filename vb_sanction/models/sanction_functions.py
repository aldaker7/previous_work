from openerp import models, fields, api, tools, _
from datetime import  datetime, date, timedelta
from openerp.exceptions import Warning
from openerp.http import request

class sanction_suspect(models.Model):
    
    _inherit = 'vb.sanction_suspect'
    
    @api.multi
    def create_case(self):
        if not self.helpdesk_id:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','view_case_form_sanction')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Create a case',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.helpdesk_case',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'current',
                        'context':{'default_sanction_id':self.id,'default_customer_id':self.customer_id.id or None}
                    }
        else:
            raise Warning(_("This record already has a case"))

    @api.multi
    def name_get(self):
        super(sanction_suspect, self).name_get()
        result=[]
        for rec in self:
            if rec.helpdesk_id:
                case_exist = True
            result.append((rec.id,u"%s " % (rec.customer_id.name)))
        return result
    
    @api.multi
    def whitelist_customer_sanction(self):
        if not self.whitelisted_id:
            whitelist_rec = self.env['vb.sanction_whitelist']
            if not whitelist_rec.search([('customer_id','=',self.customer_id.id)]):
                try:
                    form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','sanction_suspect_whitelist_form_view')[1]
                except ValueError:
                    form_id = False
                return {
                            'name':'Whitelist Customer',
                            'type': 'ir.actions.act_window',
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'vb.common_wizard',
                            'views': [(form_id, 'form')],
                            'view_id': form_id,
                            'target': 'new',
                        }
            else:
                raise Warning(_("This customer already whitelisted"))
        else:
            raise Warning(_("This customer already whitelisted"))
            
class sanction_whitelist(models.Model):
    
    _inherit = 'vb.sanction_whitelist'
    
    @api.multi
    def name_get(self):
        super(sanction_whitelist, self).name_get()
        result=[]
        active_action = request.__dict__.get('jsonrequest').get('params').get('kwargs').get('context').get('params').get('action')
        action_suspect_list = request.env['ir.model.data'].get_object('vb_sanction', 'action_vb_sanction_suspect_list')
        if active_action == action_suspect_list.id:
            if self.state == 'Approved':
                for rec in self:
                    result.append((rec.id,u"%s" % ('Yes')))
        else:
            for rec in self:
                result.append((rec.id,u"%s" % (rec.customer_id.name)))
        return result
    
    @api.multi
    def approve_whitelist(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','sanction_whitelist_approve_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Approve customer whitelist',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
    
    @api.multi
    def reject_whitelist(self):
        try:
            form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','sanction_whitelist_reject_form_view')[1]
        except ValueError:
            form_id = False
        return {
                    'name':'Reject customer whitelist',
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vb.common_wizard',
                    'views': [(form_id, 'form')],
                    'view_id': form_id,
                    'target': 'new',
                }
    
class sanction_screen_job(models.Model):
    
    _inherit = 'vb.sanction_screen_job'
    
    @api.multi
    def get_sanction_list(self):
        if self.batch_no:
            try:
                tree_id = self.env['ir.model.data'].get_object_reference('vb_sanction','view_sanction_list_tree')[1]
                form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','view_sanction_list_form')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Reject customer whitelist',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode':"[tree,form]",
                        'res_model': 'vb.sanction_list',
                        'views': [(tree_id, 'tree'), (form_id, 'form')],
                        'view_id': False,
                        'domain':[('screenjob_id','=',self.id)],
                        'target': 'current',
                }
            
    @api.multi
    def get_sanction_matches(self):
        if self.batch_no:
            try:
                tree_id = self.env['ir.model.data'].get_object_reference('vb_sanction','view_sanction_suspect_list')[1]
                form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','view_sanction_suspect_form')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Matched customer',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode':"[tree,form]",
                        'res_model': 'vb.sanction_suspect',
                        'views': [(tree_id, 'tree'), (form_id, 'form')],
                        'view_id': False,
                        'domain':[('screenjob_id','=',self.id)],
                        'target': 'current',
                }
        