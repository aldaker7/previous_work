from openerp import models, fields, api, tools, _
from datetime import  datetime, date, timedelta
from openerp.exceptions import Warning
from openerp.http import request

class sanction_suspect(models.Model):

    _name = 'vb.sanction_suspect'
    _rec_name = 'customer_id'

    screenjob_id = fields.Many2one(comodel_name='vb.sanction_screen_job', string='Sanction job',index=True)
    screenjob_date = fields.Date(related='screenjob_id.batch_date',string='Batch date')
    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',index=True)
    customer_name = fields.Char(related='customer_id.name',string='Customer name')
    sanctions_name = fields.Char(string='Sanction name')
    matched_score = fields.Char(string='Matched score')
    reference_no = fields.Char(string='Ref no')
    curr_score = fields.Float(string='Current score',digits=(15,2),default=0)
    prev_score = fields.Float(string='Previous score',digits=(15,2),default=0)
    whitelisted_id = fields.Many2one(comodel_name='vb.sanction_whitelist', string='Whitelisted',index=True)
    remarks = fields.Text(string='Remarks')
    helpdesk_id = fields.Many2one('vb.helpdesk_case', string='Case name',ondelete='cascade')
    manual_entry = fields.Selection([('Yes','Yes'),('No','No')],default='No',string='Manual entry')
    whitelist_available = fields.Char(string='Whitelisted',default='N/A')
    case_exist = fields.Boolean(default=False)
    suspect_dtl = fields.One2many(comodel_name='vb.sanction_suspect_dtl', inverse_name='suspect_id', string="Positive Match details")
    state = fields.Selection([
                            ('New','New'),
                            ('CaseCreated','Case created'),
                            ('Whitelisted','Whitelisted')],default='New')
    active = fields.Boolean(default=True)

class sanction_suspect_dtl(models.Model):

    _name = 'vb.sanction_suspect_dtl'

    suspect_id = fields.Many2one(comodel_name='vb.sanction_suspect',string='Suspect name',index=True)
    sanction_id = fields.Many2one(comodel_name='vb.sanction_list',string='Sanction id',index=True)
    reference_no = fields.Char(string='Reference no')
    matched_score = fields.Float(string='Matched score',digits=(15,2),default=0)
    manual_entry = fields.Selection([('Yes','Yes'),('No','No')],default='No',string='Manual entry')
    date_added = fields.Date(string='Date added')
    active = fields.Boolean(string='Active',default=True)

# class sanction_suspect_list_dtl(models.Model):
#
#     _name = 'vb.sanction_suspect_list_dtl'
#
#     screenjob_id = fields.Many2one(comodel_name='vb.sanction_screen_job', string='Sanction job',index=True)
#     customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',index=True)
#     customer_name = fields.Char(related='customer_id.name',string='Customer name')
#     national_id_no = fields.Char(related='customer_id.national_id_no')
# #     passport_no = fields.Char(related)
#     nationality = fields.Char(related='customer_id.nationality_id.name',string='Nationality')
#     gender = fields.Selection(related='customer_id.gender',string='Gender')
#     dob = fields.Date(related='customer_id.date_of_birth',string='Date of birth')
#     sanction_list_id = fields.Many2one(comodel_name='vb.sanction_list',string='Sanction list name')
#     reference_no = fields.Char(string='Ref no')
#     sn_name = fields.Char(string='Sn name')
#     sn_other_name = fields.Char(string='Sn other name')
#     sn_national_id_no = fields.Char(string='Sn national id no')
#     sn_passport_no = fields.Char(string='Sn passport no')
#     sn_nationality = fields.Char(string='Sn nationality')
#     sn_gender = fields.Char(string='Sn gender')
#     sn_dob = fields.Char(string='Sn DOB')
#     name_score = fields.Float(string='Name score',digits=(15,2),default=0)
#     other_name_score = fields.Float(string='Other name score',digits=(15,2),default=0)
#     national_id_no_score = fields.Float(string='National id score',digits=(15,2),default=0)
#     nationality_score = fields.Float(string='Nationality score',digits=(15,2),default=0)
#     gender_score = fields.Float(string='Gender score',digits=(15,2),default=0)
#     dob_score = fields.Float(string='DOB score',digits=(15,2),default=0)
#     matched_score = fields.Float(string='Matched score',digits=(15,2),default=0)
#     active = fields.Boolean(string='Active',default=True)

class sanction_screen_job(models.Model):

    _name = 'vb.sanction_screen_job'
    _rec_name = 'batch_no'

    batch_no = fields.Char(string='Batch no')
    batch_date = fields.Date(string='Batch date')
    no_of_sanction_records = fields.Integer(string='Sanction no.',default=0)
    no_of_customers = fields.Integer(string='Customer no',default=0)
    no_of_ptnl_suspect = fields.Integer(string='Suspect no',default=0)
    manual_entry = fields.Selection([('Yes','Yes'),('No','No')],default='No',string='Manual entry')
    active = fields.Boolean(string='Active',default=True)

class sanction_list(models.Model):

    _name = 'vb.sanction_list'
    screenjob_id = fields.Many2one(comodel_name='vb.sanction_screen_job', string='Batch no',index=True)
    source = fields.Char(string='Source')
    reference_no = fields.Char(string='Ref no')
    name = fields.Char(string='Name')
    title = fields.Char(string='Title',default='N/A')
    designation = fields.Char(string='Designation')
    date_of_birth = fields.Char(string='Date of birth')
    place_of_birth = fields.Char(string='Place of birth')
    other_names = fields.Char(string='Other name')
    nationality = fields.Char(string='Nationality',default='Unknown')
    passport_no = fields.Char(string='Passport No')
    national_id_no = fields.Char(string='National Id no')
    address = fields.Char(string='Address')
    other_info = fields.Char(string='Other info')
    manual_entry = fields.Selection([('Yes','Yes'),('No','No')],default='No',string='Manual entry')
    gender = fields.Char('Gender')
    active = fields.Boolean(string='Active',default=True)

class sanction_whitelist(models.Model):

    _name = 'vb.sanction_whitelist'

    customer_id = fields.Many2one(comodel_name='vb.customer', string='Customer',index=True)
    state = fields.Selection([
        ('New','New'),
        ('Approved','Approved'),
        ('Rejected','Rejected')
        ],string='Status',default='New')
    whitelist_date = fields.Date(string='Whitelisted date')
    whitelist_by = fields.Many2one(comodel_name = 'res.users',string='Whitelisted by',index=True)
    expiry_date = fields.Date(string='Expiry date')
    date_rejected = fields.Datetime(string='Rejected date')
    rejected_by = fields.Many2one(comodel_name = 'res.users',string='Rejected by',index=True)
    curr_score = fields.Float(string='Current score')
    prev_score = fields.Float(string='Previous score')
    reason_rejected_id = fields.Many2one(string='Rejection reason',comodel_name='vb.common_code')
    remarks = fields.Text(string='Remark')
    manual_entry = fields.Selection([('Yes','Yes'),('No','No')],default='No',string='Manual entry')
    active = fields.Boolean(string='Active',default=True)

class helpdesk_case(models.Model):
    _inherit = 'vb.helpdesk_case'

    @api.multi
    def name_get(self):
        super(helpdesk_case, self).name_get()
        result=[]
        if 'jsonrequest' in request.__dict__:
            if 'params' in request.__dict__.get('jsonrequest'):
                if 'kwargs' in request.__dict__.get('jsonrequest').get('params'):
                    if 'context' in request.__dict__.get('jsonrequest').get('params').get('kwargs'):
                        if 'params' in request.__dict__.get('jsonrequest').get('params').get('kwargs').get('context'):
                            if 'action' in request.__dict__.get('jsonrequest').get('params').get('kwargs').get('context').get('params'):
                                active_action = request.__dict__.get('jsonrequest').get('params').get('kwargs').get('context').get('params').get('action')
                                action_suspect_list = request.env['ir.model.data'].get_object('vb_sanction', 'action_vb_sanction_suspect_list')
                                if active_action == action_suspect_list.id:
                                    for rec in self:
                                        result.append((rec.id,u"%s"%(rec.case_ref)))
                                else:
                                    for rec in self:
                                        result.append((rec.id,u"%s (%s)" % (rec.name,rec.case_ref)))
                            else:
                                for rec in self:
                                    result.append((rec.id,u"%s (%s)" % (rec.name,rec.case_ref)))
        return result

    @api.model
    def create(self,vals):
        result=super(helpdesk_case,self).create(vals)
        self._cr.execute('Select max(id) from vb_helpdesk_case')
        ids = self._cr.fetchone()[0]
        if 'sanction_id' in vals:
            if bool(str(vals['sanction_id']) != '0'):
                sanction_suspect_rec = self.env['vb.sanction_suspect'].search([('id','=',vals['sanction_id'])])
                if sanction_suspect_rec:
                    query = """UPDATE vb_sanction_suspect set helpdesk_id=%s, case_exist=True , state='CaseCreated' where id=%s"""
                    result
                    self._cr.execute(query,(ids,vals['sanction_id']))
                    return result
            else:
                return result
        else:
            return result


    sanction_id = fields.Integer(default=0)
