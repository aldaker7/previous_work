from openerp import models, fields, api, _
import os
from datetime import datetime,timedelta,date
from openerp.exceptions import Warning
from dateutil.relativedelta import relativedelta

class common_wizard(models.TransientModel):

    _inherit = 'vb.common_wizard'

    @api.multi
    def whitelist_customer(self):
        whitelist_rec = self.env['vb.sanction_whitelist']
        cur_rec = self.env['vb.sanction_suspect'].search([('id','=',self._context.get('active_id'))])
        
        whitelist_rec.create({
            'customer_id':cur_rec.customer_id.id,
            'curr_score':cur_rec.curr_score,
            'prev_score':cur_rec.prev_score,
            'expiry_date':date.today()+relativedelta(months=+6),
            'remarks':cur_rec.remarks,
            'whitelist_date':date.today(),
            'whitelist_by':self._uid,
            'state':'Approved'
            })
        new_whitelist_id = whitelist_rec.search([('customer_id','=',cur_rec.customer_id.id)]).id
			
		
        cur_rec.update({'whitelisted_id':new_whitelist_id,'state':'Whitelisted'})
        
        #calling a SP to whitelist all records with same customer id
        call_sp_whitelist = "Select sp_sanction_suspect_whitelist(%s,%s,%s,%s,%s,%s)"
        self._cr.execute(call_sp_whitelist,(self._uid,cur_rec.customer_id.id,str(cur_rec.matched_score),str(cur_rec.reference_no),
                                            new_whitelist_id,self._context.get('active_id')))
        output = self._cr.fetchall()[0][0]
        if output == -1 or output == 0:
            raise Warning("Whitelist process has no completed. Get response {0}".format(output))
        else:
            try:
                form_id = self.env['ir.model.data'].get_object_reference('vb_sanction','sanction_suspect_whitelist_confirm_form_view')[1]
            except ValueError:
                form_id = False
            return {
                        'name':'Customer Whiltelisted',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'vb.common_wizard',
                        'views': [(form_id, 'form')],
                        'view_id': form_id,
                        'target': 'new',
                    }

    @api.multi
    def approve_whitelist_wizard(self):
        cur_rec = self.env['vb.sanction_whitelist'].search([('id','=',self._context.get('active_id'))])
        if cur_rec:
          cur_rec.update({'state':"Approved",'whitelist_date':date.today(),'whitelist_by':self._uid})

    @api.multi
    def reject_whitelist_wizard(self):
        cur_rec = self.env['vb.sanction_whitelist'].search([('id','=',self._context.get('active_id'))])
        if cur_rec:
          cur_rec.update({'state':"Rejected",'date_rejected':date.today(),'rejected_by':self._uid})
        suspect_rec = self.env['vb.sanction_suspect'].search([('whitelisted_id','=',cur_rec.id)])
        for i in suspect_rec:
            i.update({'whitelisted_id':False,'state':'New'})
        cur_rec.update({'active':False})
